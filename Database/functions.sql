-- drop all functions
DROP FUNCTION IF EXISTS IsNumber;
DROP FUNCTION IF EXISTS IsEmail;
DROP FUNCTION IF EXISTS NoSpaces;
DROP FUNCTION IF EXISTS SplitString;

DELIMITER //

-- create all functions
CREATE FUNCTION IsNumber (str varchar(255)) RETURNS tinyint
RETURN str REGEXP '^[0-9]+$';//

CREATE FUNCTION IsEmail (str varchar(255)) RETURNS tinyint
RETURN str REGEXP '^[a-zA-Z0-9][a-zA-Z0-9._-]*@[a-zA-Z0-9][a-zA-Z0-9._-]*\\.[a-zA-Z]{2,4}$';//

CREATE FUNCTION NoSpaces (str varchar(255)) RETURNS tinyint
RETURN str NOT REGEXP '[[:space:]]';//

CREATE FUNCTION SplitString (str text, delim char(1), pos int) RETURNS text
RETURN REPLACE(
	SUBSTRING(
		SUBSTRING_INDEX(str, delim, pos),
		CHAR_LENGTH(
			SUBSTRING_INDEX(str, delim, pos - 1)
		) + 1
	),
	delim,
	''
)//

DELIMITER ;