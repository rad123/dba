-- drop all triggers
DROP TRIGGER IF EXISTS b_insert_tienda;
DROP TRIGGER IF EXISTS b_update_tienda;
DROP TRIGGER IF EXISTS b_insert_cuenta;
DROP TRIGGER IF EXISTS b_update_cuenta;
DROP TRIGGER IF EXISTS b_insert_personal;
DROP TRIGGER IF EXISTS b_update_personal;
DROP TRIGGER IF EXISTS b_insert_inventario;
DROP TRIGGER IF EXISTS b_update_inventario;
DROP TRIGGER IF EXISTS b_insert_repuesto;
DROP TRIGGER IF EXISTS b_update_repuesto;
DROP TRIGGER IF EXISTS b_insert_herramienta;
DROP TRIGGER IF EXISTS b_update_herramienta;
DROP TRIGGER IF EXISTS b_insert_equipo_venta;
DROP TRIGGER IF EXISTS b_update_equipo_venta;
DROP TRIGGER IF EXISTS b_insert_proveedor;
DROP TRIGGER IF EXISTS b_update_proveedor;
DROP TRIGGER IF EXISTS b_insert_factura_compra;
DROP TRIGGER IF EXISTS b_update_factura_compra;
DROP TRIGGER IF EXISTS b_insert_cliente;
DROP TRIGGER IF EXISTS b_update_cliente;
DROP TRIGGER IF EXISTS b_insert_notificacion;
DROP TRIGGER IF EXISTS b_update_notificacion;
DROP TRIGGER IF EXISTS b_insert_equipo;
DROP TRIGGER IF EXISTS b_update_equipo;
DROP TRIGGER IF EXISTS b_insert_orden;
DROP TRIGGER IF EXISTS b_update_orden;
DROP TRIGGER IF EXISTS b_insert_factura_venta;
DROP TRIGGER IF EXISTS b_update_factura_venta;
DROP TRIGGER IF EXISTS b_insert_venta_web;
DROP TRIGGER IF EXISTS b_update_venta_web;
DROP TRIGGER IF EXISTS b_insert_reparacion;
DROP TRIGGER IF EXISTS b_update_reparacion;
DROP TRIGGER IF EXISTS b_insert_repuesto_reparacion;
DROP TRIGGER IF EXISTS b_update_repuesto_reparacion;
DROP TRIGGER IF EXISTS b_insert_cuenta_cobrar;
DROP TRIGGER IF EXISTS b_update_cuenta_cobrar;
DROP TRIGGER IF EXISTS b_insert_forma_pago_v;
DROP TRIGGER IF EXISTS b_update_forma_pago_v;
DROP TRIGGER IF EXISTS b_insert_cuenta_cobrar_forma_pago_v;
DROP TRIGGER IF EXISTS b_update_cuenta_cobrar_forma_pago_v;
DROP TRIGGER IF EXISTS b_insert_factura_venta_forma_pago_v;
DROP TRIGGER IF EXISTS b_update_factura_venta_forma_pago_v;
DROP TRIGGER IF EXISTS b_insert_cuenta_forma_pago_v;
DROP TRIGGER IF EXISTS b_update_cuenta_forma_pago_v;
DROP TRIGGER IF EXISTS b_insert_cuenta_pagar;
DROP TRIGGER IF EXISTS b_update_cuenta_pagar;
DROP TRIGGER IF EXISTS b_insert_forma_pago_c;
DROP TRIGGER IF EXISTS b_update_forma_pago_c;
DROP TRIGGER IF EXISTS b_insert_cuenta_pagar_forma_pago_c;
DROP TRIGGER IF EXISTS b_update_cuenta_pagar_forma_pago_c;
DROP TRIGGER IF EXISTS b_insert_factura_compra_forma_pago_c;
DROP TRIGGER IF EXISTS b_update_factura_compra_forma_pago_c;
DROP TRIGGER IF EXISTS b_insert_cuenta_forma_pago_c;
DROP TRIGGER IF EXISTS b_update_cuenta_forma_pago_c;
DROP TRIGGER IF EXISTS b_insert_orden_factura_venta;
DROP TRIGGER IF EXISTS b_update_orden_factura_venta;
DROP TRIGGER IF EXISTS b_insert_orden_equipo;
DROP TRIGGER IF EXISTS b_update_orden_equipo;
DROP TRIGGER IF EXISTS b_insert_factura_venta_equipo_venta;
DROP TRIGGER IF EXISTS b_update_factura_venta_equipo_venta;
DROP TRIGGER IF EXISTS b_insert_venta_web_equipo_venta;
DROP TRIGGER IF EXISTS b_update_venta_web_equipo_venta;
DROP TRIGGER IF EXISTS b_insert_factura_compra_inventario;
DROP TRIGGER IF EXISTS b_update_factura_compra_inventario;

DELIMITER //

-- create all triggers
CREATE TRIGGER b_insert_tienda BEFORE INSERT ON Tienda
FOR EACH ROW 
BEGIN

	IF NOT (NEW.RIF > 0 AND NEW.RIF <= 9999999999) THEN
		BEGIN
			SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'RIF invalido';
		END;
	END IF;
	
	IF NOT IsEmail(NEW.Email) THEN
		BEGIN
			SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Email invalido';
		END;
	END IF;

	IF NOT IsNumber(NEW.Telefono) THEN
		BEGIN
			SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Telefono invalido';
		END;
	END IF;

END;//

CREATE TRIGGER b_update_tienda BEFORE UPDATE ON Tienda
FOR EACH ROW 
BEGIN

	IF NOT (NEW.RIF > 0 AND NEW.RIF <= 9999999999) THEN
		BEGIN
			SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'RIF invalido';
		END;
	END IF;

	IF NOT IsEmail(NEW.Email) THEN
		BEGIN
			SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Email invalido';
		END;
	END IF;

	IF NOT IsNumber(NEW.Telefono) THEN
		BEGIN
			SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Telefono invalido';
		END;
	END IF;

END;//

CREATE TRIGGER b_insert_cuenta BEFORE INSERT ON Cuenta
FOR EACH ROW 
BEGIN

	IF NOT (NEW.RIF_tienda > 0 AND NEW.RIF_tienda <= 9999999999) THEN
		BEGIN
			SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'RIF de la tienda invalido';
		END;
	END IF;

	IF NOT IsNumber(NEW.Numero) THEN
		BEGIN
			SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Numero de cuenta invalido';
		END;
	END IF;

END;//

CREATE TRIGGER b_update_cuenta BEFORE UPDATE ON Cuenta
FOR EACH ROW 
BEGIN

	IF NOT (NEW.RIF_tienda > 0 AND NEW.RIF_tienda <= 9999999999) THEN
		BEGIN
			SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'RIF de la tienda invalido';
		END;
	END IF;

	IF NOT IsNumber(NEW.Numero) THEN
		BEGIN
			SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Numero de cuenta invalido';
		END;
	END IF;

END;//

CREATE TRIGGER b_insert_personal BEFORE INSERT ON Personal
FOR EACH ROW
BEGIN

	IF NOT (NEW.CI > 0 AND NEW.CI <= 999999999) THEN
		BEGIN
			SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Cedula invalida';
		END;
	END IF;

	IF NOT (NEW.RIF > 0 AND NEW.RIF <= 9999999999) THEN
		BEGIN
			SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'RIF invalido';
		END;
	END IF;

	IF NOT IsEmail(NEW.Email) THEN
		BEGIN
			SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Email invalido';
		END;
	END IF;

	IF NOT IsNumber(NEW.Telefono) THEN
		BEGIN
			SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Telefono invalido';
		END;
	END IF;

	IF NOT (NEW.Tipo = 'TE') THEN
		BEGIN
			SET NEW.Especialidad = NULL;
		END;
	END IF;

END;//

CREATE TRIGGER b_update_personal BEFORE UPDATE ON Personal
FOR EACH ROW
BEGIN

	IF NOT (NEW.CI > 0 AND NEW.CI <= 999999999) THEN
		BEGIN
			SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Cedula invalida';
		END;
	END IF;

	IF NOT (NEW.RIF > 0 AND NEW.RIF <= 9999999999) THEN
		BEGIN
			SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'RIF invalido';
		END;
	END IF;

	IF NOT IsEmail(NEW.Email) THEN
		BEGIN
			SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Email invalido';
		END;
	END IF;

	IF NOT IsNumber(NEW.Telefono) THEN
		BEGIN
			SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Telefono invalido';
		END;
	END IF;

	IF NOT (NEW.Tipo = 'TE') THEN
		BEGIN
			SET NEW.Especialidad = NULL;
		END;
	END IF;

END;//

CREATE TRIGGER b_insert_inventario BEFORE INSERT ON Inventario
FOR EACH ROW
BEGIN

	IF NOT NoSpaces(NEW.Codigo) THEN
		BEGIN
			SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Codigo invalido';
		END;
	END IF;

	IF NOT (NEW.Cantidad > 0 AND NEW.Cantidad <= 9999) THEN
		BEGIN
			SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Cantidad invalida';
		END;
	END IF;

END;//

CREATE TRIGGER b_update_inventario BEFORE UPDATE ON Inventario
FOR EACH ROW
BEGIN

	IF NOT NoSpaces(NEW.Codigo) THEN
		BEGIN
			SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Codigo invalido';
		END;
	END IF;

	IF NOT (NEW.Cantidad > 0 AND NEW.Cantidad <= 9999) THEN
		BEGIN
			SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Cantidad invalida';
		END;
	END IF;

	UPDATE Repuesto SET Cantidad = NEW.Cantidad WHERE Codigo = NEW.Codigo;
	UPDATE Herramienta SET Cantidad = NEW.Cantidad WHERE Codigo = NEW.Codigo;
	UPDATE EquipoVenta SET Cantidad = NEW.Cantidad WHERE Codigo = NEW.Codigo;

END;//

CREATE TRIGGER b_insert_repuesto BEFORE INSERT ON Repuesto
FOR EACH ROW
BEGIN

	IF NOT NoSpaces(NEW.Codigo) THEN
		BEGIN
			SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Codigo invalido';
		END;
	END IF;

	IF NOT (NEW.Cantidad > 0 AND NEW.Cantidad <= 9999) THEN
		BEGIN
			SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Cantidad invalida';
		END;
	END IF;

	IF NOT (NEW.Precio >= 0) THEN
		BEGIN
			SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Precio invalido';
		END;
	END IF;

	SELECT COUNT(Codigo) INTO @CantHerramienta FROM Herramienta WHERE Codigo = NEW.Codigo LIMIT 1;

	IF @CantHerramienta > 0 THEN
		SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'El codigo ya se encuentra registrado como Herramienta';
	END IF;

	SELECT COUNT(Codigo) INTO @CantEquipVenta FROM EquipoVenta WHERE Codigo = NEW.Codigo LIMIT 1;

	IF @CantEquipVenta > 0 THEN
		SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'El codigo ya se encuentra registrado como EquipoVenta';
	END IF;

	SELECT Cantidad INTO @CantExists FROM Inventario WHERE Codigo = NEW.Codigo LIMIT 1;

	IF @CantExists THEN
		BEGIN
			SET NEW.Cantidad = @CantExists;
		END;
	END IF;

END;//

CREATE TRIGGER b_update_repuesto BEFORE UPDATE ON Repuesto
FOR EACH ROW
BEGIN

	IF NOT NoSpaces(NEW.Codigo) THEN
		BEGIN
			SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Codigo invalido';
		END;
	END IF;

	IF NOT (NEW.Cantidad > 0 AND NEW.Cantidad <= 9999) THEN
		BEGIN
			SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Cantidad invalida';
		END;
	END IF;

	IF NOT (NEW.Precio >= 0) THEN
		BEGIN
			SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Precio invalido';
		END;
	END IF;

	SELECT COUNT(Codigo) INTO @CantHerramienta FROM Herramienta WHERE Codigo = NEW.Codigo LIMIT 1;

	IF @CantHerramienta > 0 THEN
		SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'El codigo ya se encuentra registrado como Herramienta';
	END IF;

	SELECT COUNT(Codigo) INTO @CantEquipVenta FROM EquipoVenta WHERE Codigo = NEW.Codigo LIMIT 1;

	IF @CantEquipVenta > 0 THEN
		SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'El codigo ya se encuentra registrado como EquipoVenta';
	END IF;

	SET NEW.Cantidad = OLD.Cantidad;

END;//

CREATE TRIGGER b_insert_herramienta BEFORE INSERT ON Herramienta
FOR EACH ROW
BEGIN

	IF NOT NoSpaces(NEW.Codigo) THEN
		BEGIN
			SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Codigo invalido';
		END;
	END IF;

	IF NOT (NEW.CI_Personal > 0 AND NEW.CI_Personal <= 999999999) THEN
		BEGIN
			SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Cedula del personal invalida';
		END;
	END IF;

	IF NOT (NEW.Cantidad > 0 AND NEW.Cantidad <= 9999) THEN
		BEGIN
			SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Cantidad invalida';
		END;
	END IF;

	IF NOT (NEW.Precio >= 0) THEN
		BEGIN
			SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Precio invalido';
		END;
	END IF;

	IF NOT (NEW.Cant_Uso > 0 AND NEW.Cant_Uso <= 9999) THEN
		BEGIN
			SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Cantidad de uso invalida';
		END;
	END IF;

	IF NOT (NEW.Precio_Uso >= 0) THEN
		BEGIN
			SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Precio de uso invalido';
		END;
	END IF;

	IF NOT (NEW.Cant_Uso <= NEW.Cantidad) THEN
		BEGIN
			SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'La cantidad de uso supera la cantidad en existencia';
		END;
	END IF;

	SELECT COUNT(Codigo) INTO @CantRep FROM Repuesto WHERE Codigo = NEW.Codigo LIMIT 1;

	IF @CantRep > 0 THEN
		SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'El codigo ya se encuentra registrado como Repuesto';
	END IF;

	SELECT COUNT(Codigo) INTO @CantEquipVenta FROM EquipoVenta WHERE Codigo = NEW.Codigo LIMIT 1;

	IF @CantEquipVenta > 0 THEN
		SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'El codigo ya se encuentra registrado como EquipoVenta';
	END IF;
 
	SELECT Cantidad INTO @CantExists FROM Inventario WHERE Codigo = NEW.Codigo LIMIT 1;

	IF @CantExists THEN
		BEGIN
			SET NEW.Cantidad = @CantExists;
		END;
	END IF;

END;//

CREATE TRIGGER b_update_herramienta BEFORE UPDATE ON Herramienta
FOR EACH ROW
BEGIN

	IF NOT NoSpaces(NEW.Codigo) THEN
		BEGIN
			SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Codigo invalido';
		END;
	END IF;

	IF NOT (NEW.CI_Personal > 0 AND NEW.CI_Personal <= 999999999) THEN
		BEGIN
			SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Cedula del personal invalida';
		END;
	END IF;

	IF NOT (NEW.Cantidad > 0 AND NEW.Cantidad <= 9999) THEN
		BEGIN
			SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Cantidad invalida';
		END;
	END IF;

	IF NOT (NEW.Precio >= 0) THEN
		BEGIN
			SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Precio invalido';
		END;
	END IF;

	IF NOT (NEW.Cant_Uso > 0 AND NEW.Cant_Uso <= 9999) THEN
		BEGIN
			SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Cantidad de uso invalida';
		END;
	END IF;

	IF NOT (NEW.Precio_Uso >= 0) THEN
		BEGIN
			SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Precio de uso invalido';
		END;
	END IF;

	IF NOT (NEW.Cant_Uso <= NEW.Cantidad) THEN
		BEGIN
			SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'La cantidad de uso supera la cantidad en existencia';
		END;
	END IF;

	SELECT COUNT(Codigo) INTO @CantRep FROM Repuesto WHERE Codigo = NEW.Codigo LIMIT 1;

	IF @CantRep > 0 THEN
		SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'El codigo ya se encuentra registrado como Repuesto';
	END IF;

	SELECT COUNT(Codigo) INTO @CantEquipVenta FROM EquipoVenta WHERE Codigo = NEW.Codigo LIMIT 1;

	IF @CantEquipVenta > 0 THEN
		SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'El codigo ya se encuentra registrado como EquipoVenta';
	END IF;

	SET NEW.Cantidad = OLD.Cantidad;

END;//

CREATE TRIGGER b_insert_equipo_venta BEFORE INSERT ON EquipoVenta
FOR EACH ROW
BEGIN

	IF NOT NoSpaces(NEW.Codigo) THEN
		BEGIN
			SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Codigo invalido';
		END;
	END IF;

	IF NOT (NEW.Cantidad > 0 AND NEW.Cantidad <= 9999) THEN
		BEGIN
			SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Cantidad invalida';
		END;
	END IF;

	IF NOT (NEW.Precio >= 0) THEN
		BEGIN
			SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Precio invalido';
		END;
	END IF;

	SELECT COUNT(Codigo) INTO @CantRep FROM Repuesto WHERE Codigo = NEW.Codigo LIMIT 1;

	IF @CantRep > 0 THEN
		SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'El codigo ya se encuentra registrado como Repuesto';
	END IF;

	SELECT COUNT(Codigo) INTO @CantHerramienta FROM Herramienta WHERE Codigo = NEW.Codigo LIMIT 1;

	IF @CantHerramienta > 0 THEN
		SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'El codigo ya se encuentra registrado como Herramienta';
	END IF;

	SELECT Cantidad INTO @CantExists FROM Inventario WHERE Codigo = NEW.Codigo LIMIT 1;

	IF @CantExists THEN
		BEGIN
			SET NEW.Cantidad = @CantExists;
		END;
	END IF;

END;//

CREATE TRIGGER b_update_equipo_venta BEFORE UPDATE ON EquipoVenta
FOR EACH ROW
BEGIN

	IF NOT NoSpaces(NEW.Codigo) THEN
		BEGIN
			SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Codigo invalido';
		END;
	END IF;

	IF NOT (NEW.Cantidad > 0 AND NEW.Cantidad <= 9999) THEN
		BEGIN
			SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Cantidad invalida';
		END;
	END IF;

	IF NOT (NEW.Precio >= 0) THEN
		BEGIN
			SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Precio invalido';
		END;
	END IF;

	SELECT COUNT(Codigo) INTO @CantRep FROM Repuesto WHERE Codigo = NEW.Codigo LIMIT 1;

	IF @CantRep > 0 THEN
		SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'El codigo ya se encuentra registrado como Repuesto';
	END IF;

	SELECT COUNT(Codigo) INTO @CantHerramienta FROM Herramienta WHERE Codigo = NEW.Codigo LIMIT 1;

	IF @CantHerramienta > 0 THEN
		SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'El codigo ya se encuentra registrado como Herramienta';
	END IF;

	SET NEW.Cantidad = OLD.Cantidad;

END;//

CREATE TRIGGER b_insert_proveedor BEFORE INSERT ON Proveedor
FOR EACH ROW
BEGIN

	IF NOT (NEW.RIF > 0 AND NEW.RIF <= 9999999999) THEN
		BEGIN
			SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'RIF invalido';
		END;
	END IF;

	IF NOT IsEmail(NEW.Email) THEN
		BEGIN
			SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Email invalido';
		END;
	END IF;

	IF NOT IsNumber(NEW.Telefono) THEN
		BEGIN
			SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Telefono invalido';
		END;
	END IF;

END;//

CREATE TRIGGER b_update_proveedor BEFORE UPDATE ON Proveedor
FOR EACH ROW
BEGIN

	IF NOT (NEW.RIF > 0 AND NEW.RIF <= 9999999999) THEN
		BEGIN
			SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'RIF invalido';
		END;
	END IF;

	IF NOT IsEmail(NEW.Email) THEN
		BEGIN
			SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Email invalido';
		END;
	END IF;

	IF NOT IsNumber(NEW.Telefono) THEN
		BEGIN
			SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Telefono invalido';
		END;
	END IF;

END;//

CREATE TRIGGER b_insert_factura_compra BEFORE INSERT ON FacturaCompra
FOR EACH ROW
BEGIN

	IF NOT IsNumber(NEW.Numero) THEN
		BEGIN
			SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Numero de factura invalido';
		END;
	END IF;

	IF NOT (NEW.RIF_Proveed > 0 AND NEW.RIF_Proveed <= 9999999999) THEN
		BEGIN
			SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'RIF del proveedor invalido';	
		END;
	END IF;

	IF NOT (NEW.Monto >= 0) THEN
		BEGIN
			SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Monto invalido';
		END;
	END IF;

END;//

CREATE TRIGGER b_update_factura_compra BEFORE UPDATE ON FacturaCompra
FOR EACH ROW
BEGIN

	IF NOT IsNumber(NEW.Numero) THEN
		BEGIN
			SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Numero de factura invalido';
		END;
	END IF;

	IF NOT (NEW.RIF_Proveed > 0 AND NEW.RIF_Proveed <= 9999999999) THEN
		BEGIN
			SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'RIF del proveedor invalido';	
		END;
	END IF;

	IF NOT (NEW.Monto >= 0) THEN
		BEGIN
			SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Monto invalido';
		END;
	END IF;

END;//

CREATE TRIGGER b_insert_cliente BEFORE INSERT ON Cliente
FOR EACH ROW
BEGIN

	IF NOT IsNumber(NEW.Identif) THEN
		BEGIN
			SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Cedula/RIF/Pasaporte invalido';
		END;
	END IF;

	IF NOT IsEmail(NEW.Email) THEN
		BEGIN
			SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Email invalido';
		END;
	END IF;

	IF NOT IsNumber(NEW.Telefono) THEN
		BEGIN
			SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Telefono invalido';
		END;
	END IF;

END;//

CREATE TRIGGER b_update_cliente BEFORE UPDATE ON Cliente
FOR EACH ROW
BEGIN

	IF NOT IsNumber(NEW.Identif) THEN
		BEGIN
			SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Cedula/RIF/Pasaporte invalido';
		END;
	END IF;

	IF NOT IsEmail(NEW.Email) THEN
		BEGIN
			SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Email invalido';
		END;
	END IF;

	IF NOT IsNumber(NEW.Telefono) THEN
		BEGIN
			SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Telefono invalido';
		END;
	END IF;

END;//

CREATE TRIGGER b_insert_notificacion BEFORE INSERT ON Notificacion
FOR EACH ROW
BEGIN

	IF NOT IsNumber(NEW.Identif_Cliente) THEN
		BEGIN
			SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Cedula/RIF/Pasaporte del cliente invalido';
		END;
	END IF;

END;//

CREATE TRIGGER b_update_notificacion BEFORE UPDATE ON Notificacion
FOR EACH ROW
BEGIN

	IF NOT IsNumber(NEW.Identif_Cliente) THEN
		BEGIN
			SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Cedula/RIF/Pasaporte del cliente invalido';
		END;
	END IF;

END;//

CREATE TRIGGER b_insert_equipo BEFORE INSERT ON Equipo
FOR EACH ROW
BEGIN

	IF NOT NoSpaces(NEW.Serial_Eq) THEN
		BEGIN
			SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Serial invalido';
		END;
	END IF;

	IF NOT IsNumber(NEW.Identif_Cliente) THEN
		BEGIN
			SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Cedula/RIF/Pasaporte del cliente invalido';
		END;
	END IF;

	IF NOT (NEW.Precio >= 0) THEN
		BEGIN
			SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Precio invalido';
		END;
	END IF;

END;//

CREATE TRIGGER b_update_equipo BEFORE UPDATE ON Equipo
FOR EACH ROW
BEGIN

	IF NOT NoSpaces(NEW.Serial_Eq) THEN
		BEGIN
			SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Serial invalido';
		END;
	END IF;

	IF NOT IsNumber(NEW.Identif_Cliente) THEN
		BEGIN
			SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Cedula/RIF/Pasaporte del cliente invalido';
		END;
	END IF;

	IF NOT (NEW.Precio >= 0) THEN
		BEGIN
			SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Precio invalido';
		END;
	END IF;

END;//

CREATE TRIGGER b_insert_orden BEFORE INSERT ON Orden
FOR EACH ROW
BEGIN

	IF NOT IsNumber(NEW.Numero) THEN
		BEGIN
			SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Numero de orden invalido';
		END;
	END IF;

	IF NOT (NEW.Total >= 0) THEN
		BEGIN
			SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Total invalido';
		END;
	END IF;

	IF NOT (NEW.F_inicio <= NEW.F_fin) THEN
		BEGIN
			SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'La fecha de inicio debe ser menor o igual a la fecha fin';
		END;
	END IF;

END;//

CREATE TRIGGER b_update_orden BEFORE UPDATE ON Orden
FOR EACH ROW
BEGIN

	IF NOT IsNumber(NEW.Numero) THEN
		BEGIN
			SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Numero de orden invalido';
		END;
	END IF;

	IF NOT (NEW.Total >= 0) THEN
		BEGIN
			SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Total invalido';
		END;
	END IF;

	IF NOT (NEW.F_inicio <= NEW.F_fin) THEN
		BEGIN
			SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'La fecha de inicio debe ser menor o igual a la fecha fin';
		END;
	END IF;

END;//

CREATE TRIGGER b_insert_factura_venta BEFORE INSERT ON FacturaVenta
FOR EACH ROW
BEGIN

	IF NOT IsNumber(NEW.Numero) THEN
		BEGIN
			SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Numero de factura invalido';
		END;
	END IF;

	IF NOT IsNumber(NEW.Identif_Cliente) THEN
		BEGIN
			SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Cedula/RIF/Pasaporte del cliente invalido';
		END;
	END IF;

	IF NOT (NEW.Monto >= 0) THEN
		BEGIN
			SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Monto invalido';
		END;
	END IF;

END;//

CREATE TRIGGER b_update_factura_venta BEFORE UPDATE ON FacturaVenta
FOR EACH ROW
BEGIN

	IF NOT IsNumber(NEW.Numero) THEN
		BEGIN
			SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Numero de factura invalido';
		END;
	END IF;

	IF NOT IsNumber(NEW.Identif_Cliente) THEN
		BEGIN
			SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Cedula/RIF/Pasaporte del cliente invalido';
		END;
	END IF;

	IF NOT (NEW.Monto >= 0) THEN
		BEGIN
			SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Monto invalido';
		END;
	END IF;

END;//

CREATE TRIGGER b_insert_venta_web BEFORE INSERT ON VentaWeb
FOR EACH ROW
BEGIN

	IF NOT IsNumber(NEW.Numero) THEN
		BEGIN
			SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Numero de venta invalido';
		END;
	END IF;

	IF NOT IsNumber(NEW.Identif_Cliente) THEN
		BEGIN
			SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Cedula/RIF/Pasaporte del cliente invalido';
		END;
	END IF;

	IF NOT IsNumber(NEW.Num_Factura) THEN
		BEGIN
			SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Numero de factura invalido';
		END;
	END IF;

END;//

CREATE TRIGGER b_update_venta_web BEFORE UPDATE ON VentaWeb
FOR EACH ROW
BEGIN

	IF NOT IsNumber(NEW.Numero) THEN
		BEGIN
			SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Numero de venta invalido';
		END;
	END IF;

	IF NOT IsNumber(NEW.Identif_Cliente) THEN
		BEGIN
			SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Cedula/RIF/Pasaporte del cliente invalido';
		END;
	END IF;

	IF NOT IsNumber(NEW.Num_Factura) THEN
		BEGIN
			SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Numero de factura de venta invalido';
		END;
	END IF;

END;//

CREATE TRIGGER b_insert_reparacion BEFORE INSERT ON Reparacion
FOR EACH ROW
BEGIN

	IF NOT IsNumber(NEW.Numero_Orden) THEN
		BEGIN
			SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Numero de orden invalido';
		END;
	END IF;

	IF NOT (NEW.CI_Personal > 0 AND NEW.CI_Personal <= 999999999) THEN
		BEGIN
			SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Cedula del personal invalida';
		END;
	END IF;

	IF NOT (NEW.Comision >= 0) THEN
		BEGIN
			SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Comision invalida';
		END;
	END IF;

	IF NOT (NEW.Total >= 0) THEN
		BEGIN
			SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Total invalido';
		END;
	END IF;

	IF NOT (NEW.F_asig <= NEW.F_rep) THEN
		BEGIN
			SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'La fecha de inicio debe ser menor o igual a la fecha fin';
		END;
	END IF;

END;//

CREATE TRIGGER b_update_reparacion BEFORE UPDATE ON Reparacion
FOR EACH ROW
BEGIN

	IF NOT IsNumber(NEW.Numero_Orden) THEN
		BEGIN
			SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Numero de orden invalido';
		END;
	END IF;

	IF NOT (NEW.CI_Personal > 0 AND NEW.CI_Personal <= 999999999) THEN
		BEGIN
			SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Cedula del personal invalida';
		END;
	END IF;

	IF NOT (NEW.Comision >= 0) THEN
		BEGIN
			SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Comision invalida';
		END;
	END IF;

	IF NOT (NEW.Total >= 0) THEN
		BEGIN
			SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Total invalido';
		END;
	END IF;

	IF NOT (NEW.F_asig <= NEW.F_rep) THEN
		BEGIN
			SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'La fecha de inicio debe ser menor o igual a la fecha fin';
		END;
	END IF;

END;//

CREATE TRIGGER b_insert_repuesto_reparacion BEFORE INSERT ON Repuesto_Reparacion
FOR EACH ROW
BEGIN

	IF NOT NoSpaces(NEW.Codigo_Repues) THEN
		BEGIN
			SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Codigo del repuesto invalido';
		END;
	END IF;

	IF NOT IsNumber(NEW.Num_Ord_Rep) THEN
		BEGIN
			SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Numero de orden de reparacion invalido';
		END;
	END IF;

	IF NOT (NEW.CI_Pers_Rep > 0 AND NEW.CI_Pers_Rep <= 999999999) THEN
		BEGIN
			SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Cedula invalida';
		END;
	END IF;

	IF NOT (NEW.Cant_Rep > 0 AND NEW.Cant_Rep <= 9999) THEN
		BEGIN
			SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Cantidad invalida';
		END;
	END IF;

	IF NOT (NEW.Precio_Rep >= 0) THEN
		BEGIN
			SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Precio invalido';
		END;
	END IF;

END;//

CREATE TRIGGER b_update_repuesto_reparacion BEFORE UPDATE ON Repuesto_Reparacion
FOR EACH ROW
BEGIN

	IF NOT NoSpaces(NEW.Codigo_Repues) THEN
		BEGIN
			SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Codigo del repuesto invalido';
		END;
	END IF;

	IF NOT IsNumber(NEW.Num_Ord_Rep) THEN
		BEGIN
			SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Numero de orden de reparacion invalido';
		END;
	END IF;	

	IF NOT (NEW.CI_Pers_Rep > 0 AND NEW.CI_Pers_Rep <= 999999999) THEN
		BEGIN
			SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Cedula invalida';
		END;
	END IF;

	IF NOT (NEW.Cant_Rep > 0 AND NEW.Cant_Rep <= 9999) THEN
		BEGIN
			SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Cantidad invalida';
		END;
	END IF;

	IF NOT (NEW.Precio_Rep >= 0) THEN
		BEGIN
			SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Precio invalido';
		END;
	END IF;

END;//

CREATE TRIGGER b_insert_cuenta_cobrar BEFORE INSERT ON CuentaCobrar
FOR EACH ROW
BEGIN

	IF NOT IsNumber(NEW.Numero) THEN
		BEGIN
			SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Numero de la cuenta invalido';
		END;
	END IF;	

	IF NOT IsNumber(NEW.Identif_Cliente) THEN
		BEGIN
			SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Cedula/RIF/Pasaporte del cliente invalido';
		END;
	END IF;

	IF NOT IsNumber(NEW.Num_Factura) THEN
		BEGIN
			SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Numero de factura de venta invalido';
		END;
	END IF;

	IF NOT (NEW.Saldo >= 0) THEN
		BEGIN
			SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Saldo invalido';
		END;
	END IF;

	IF NOT (NEW.Monto >= 0) THEN
		BEGIN
			SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Monto invalido';
		END;
	END IF;

	IF NOT (DATE(NEW.F_aper) <= NEW.F_lim) THEN
		BEGIN
			SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'La fecha de inicio debe ser menor o igual a la fecha fin';
		END;
	END IF;

END;//

CREATE TRIGGER b_update_cuenta_cobrar BEFORE UPDATE ON CuentaCobrar
FOR EACH ROW
BEGIN

	IF NOT IsNumber(NEW.Numero) THEN
		BEGIN
			SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Numero de la cuenta invalido';
		END;
	END IF;	

	IF NOT IsNumber(NEW.Identif_Cliente) THEN
		BEGIN
			SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Cedula/RIF/Pasaporte del cliente invalido';
		END;
	END IF;

	IF NOT IsNumber(NEW.Num_Factura) THEN
		BEGIN
			SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Numero de factura de venta invalido';
		END;
	END IF;

	IF NOT (NEW.Saldo >= 0) THEN
		BEGIN
			SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Saldo invalido';
		END;
	END IF;

	IF NOT (NEW.Monto >= 0) THEN
		BEGIN
			SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Monto invalido';
		END;
	END IF;

	IF NOT (DATE(NEW.F_aper) <= NEW.F_lim) THEN
		BEGIN
			SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'La fecha de inicio debe ser menor o igual a la fecha fin';
		END;
	END IF;

END;//

CREATE TRIGGER b_insert_forma_pago_v BEFORE INSERT ON FormaPagoV
FOR EACH ROW
BEGIN

	IF NOT (NEW.Nref > 0 AND NEW.Nref <= 999999999) THEN
		BEGIN
			SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Numero de referencia invalido';
		END;
	END IF;

	IF NOT (NEW.Monto >= 0) THEN
		BEGIN
			SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Monto invalido';
		END;
	END IF;

	IF NEW.Tipo = 'EF' THEN
		BEGIN
			SET NEW.Nref = NULL;
		END;
	END IF;

END;//

CREATE TRIGGER b_update_forma_pago_v BEFORE UPDATE ON FormaPagoV
FOR EACH ROW
BEGIN

	IF NOT (NEW.Nref > 0 AND NEW.Nref <= 999999999) THEN
		BEGIN
			SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Numero de referencia invalido';
		END;
	END IF;

	IF NOT (NEW.Monto >= 0) THEN
		BEGIN
			SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Monto invalido';
		END;
	END IF;

	IF NEW.Tipo = 'EF' THEN
		BEGIN
			SET NEW.Nref = NULL;
		END;
	END IF;

END;//

CREATE TRIGGER b_insert_cuenta_cobrar_forma_pago_v BEFORE INSERT ON CuentaCobrar_FormaPagoV
FOR EACH ROW
BEGIN

	IF NOT IsNumber(NEW.Num_Cuenta) THEN
		BEGIN
			SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Numero de la cuenta por cobrar invalido';
		END;
	END IF;

	IF NOT IsNumber(NEW.Cliente_Cuenta) THEN
		BEGIN
			SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Cedula/RIF/Pasaporte del cliente de la cuenta invalido';
		END;
	END IF;

	SELECT Saldo INTO @SaldoCuenta FROM CuentaCobrar WHERE Numero = NEW.Num_Cuenta LIMIT 1;

	SELECT Monto INTO @MontoPago FROM FormaPagoV WHERE Fecha = NEW.Fecha_Pago LIMIT 1;

	SET @SaldoActual = @SaldoCuenta - @MontoPago;

	IF @SaldoActual < 0 THEN
		SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'El pago de la cuenta por cobrar supera el saldo actual de la cuenta';
	ELSE
		UPDATE CuentaCobrar SET Saldo = @SaldoActual, F_aper = F_aper WHERE Numero = NEW.Num_Cuenta;
	END IF;

END;//

CREATE TRIGGER b_update_cuenta_cobrar_forma_pago_v BEFORE UPDATE ON CuentaCobrar_FormaPagoV
FOR EACH ROW
BEGIN

	IF NOT IsNumber(NEW.Num_Cuenta) THEN
		BEGIN
			SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Numero de la cuenta por cobrar invalido';
		END;
	END IF;

	IF NOT IsNumber(NEW.Cliente_Cuenta) THEN
		BEGIN
			SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Cedula/RIF/Pasaporte del cliente de la cuenta invalido';
		END;
	END IF;

END;//

CREATE TRIGGER b_insert_factura_venta_forma_pago_v BEFORE INSERT ON FacturaVenta_FormaPagoV
FOR EACH ROW
BEGIN

	IF NOT IsNumber(NEW.Num_Factura) THEN
		BEGIN
			SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Numero de la factura de venta invalido';
		END;
	END IF;

END;//

CREATE TRIGGER b_update_factura_venta_forma_pago_v BEFORE UPDATE ON FacturaVenta_FormaPagoV
FOR EACH ROW
BEGIN

	IF NOT IsNumber(NEW.Num_Factura) THEN
		BEGIN
			SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Numero de la factura de venta invalido';
		END;
	END IF;

END;//

CREATE TRIGGER b_insert_cuenta_forma_pago_v BEFORE INSERT ON Cuenta_FormaPagoV
FOR EACH ROW
BEGIN

	IF NOT IsNumber(NEW.Num_Cuenta) THEN
		BEGIN
			SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Numero de cuenta invalido';
		END;
	END IF;

END;//

CREATE TRIGGER b_update_cuenta_forma_pago_v BEFORE UPDATE ON Cuenta_FormaPagoV
FOR EACH ROW
BEGIN

	IF NOT IsNumber(NEW.Num_Cuenta) THEN
		BEGIN
			SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Numero de cuenta invalido';
		END;
	END IF;

END;//

CREATE TRIGGER b_insert_cuenta_pagar BEFORE INSERT ON CuentaPagar
FOR EACH ROW
BEGIN

	IF NOT IsNumber(NEW.Numero) THEN
		BEGIN
			SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Numero de la cuenta invalido';
		END;
	END IF;	

	IF NOT IsNumber(NEW.Num_Factura) THEN
		BEGIN
			SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Numero de factura de compra invalido';
		END;
	END IF;

	IF NOT (NEW.Saldo >= 0) THEN
		BEGIN
			SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Saldo invalido';
		END;
	END IF;

	IF NOT (NEW.Monto >= 0) THEN
		BEGIN
			SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Monto invalido';
		END;
	END IF;

	IF NOT (DATE(NEW.F_aper) <= NEW.F_lim) THEN
		BEGIN
			SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'La fecha de inicio debe ser menor o igual a la fecha fin';
		END;
	END IF;

END;//

CREATE TRIGGER b_update_cuenta_pagar BEFORE UPDATE ON CuentaPagar
FOR EACH ROW
BEGIN

	IF NOT IsNumber(NEW.Numero) THEN
		BEGIN
			SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Numero de la cuenta invalido';
		END;
	END IF;	

	IF NOT IsNumber(NEW.Num_Factura) THEN
		BEGIN
			SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Numero de factura de compra invalido';
		END;
	END IF;

	IF NOT (NEW.Saldo >= 0) THEN
		BEGIN
			SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Saldo invalido';
		END;
	END IF;

	IF NOT (NEW.Monto >= 0) THEN
		BEGIN
			SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Monto invalido';
		END;
	END IF;

	IF NOT (DATE(NEW.F_aper) <= NEW.F_lim) THEN
		BEGIN
			SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'La fecha de inicio debe ser menor o igual a la fecha fin';
		END;
	END IF;

END;//

CREATE TRIGGER b_insert_forma_pago_c BEFORE INSERT ON FormaPagoC
FOR EACH ROW
BEGIN

	IF NOT (NEW.Nref > 0 AND NEW.Nref <= 999999999) THEN
		BEGIN
			SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Numero de referencia invalido';
		END;
	END IF;

	IF NOT (NEW.Monto >= 0) THEN
		BEGIN
			SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Monto invalido';
		END;
	END IF;

	IF NEW.Tipo = 'EF' THEN
		BEGIN
			SET NEW.Nref = NULL;
		END;
	END IF;

END;//

CREATE TRIGGER b_update_forma_pago_c BEFORE UPDATE ON FormaPagoC
FOR EACH ROW
BEGIN

	IF NOT (NEW.Nref > 0 AND NEW.Nref <= 999999999) THEN
		BEGIN
			SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Numero de referencia invalido';
		END;
	END IF;

	IF NOT (NEW.Monto >= 0) THEN
		BEGIN
			SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Monto invalido';
		END;
	END IF;

	IF NEW.Tipo = 'EF' THEN
		BEGIN
			SET NEW.Nref = NULL;
		END;
	END IF;

END;//

CREATE TRIGGER b_insert_cuenta_pagar_forma_pago_c BEFORE INSERT ON CuentaPagar_FormaPagoC
FOR EACH ROW
BEGIN

	IF NOT IsNumber(NEW.Num_Cuenta) THEN
		BEGIN
			SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Numero de la cuenta por pagar invalido';
		END;
	END IF;

	IF NOT (NEW.RIF_Proveed > 0 AND NEW.RIF_Proveed <= 9999999999) THEN
		BEGIN
			SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'RIF del proveedor invalido';
		END;
	END IF;

	SELECT Saldo INTO @SaldoCuenta FROM CuentaPagar WHERE Numero = NEW.Num_Cuenta LIMIT 1;

	SELECT Monto INTO @MontoPago FROM FormaPagoC WHERE Fecha = NEW.Fecha_Pago LIMIT 1;

	SET @SaldoActual = @SaldoCuenta - @MontoPago;

	IF @SaldoActual < 0 THEN
		SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'El pago de la cuenta por pagar supera el saldo actual de la cuenta';
	ELSE
		UPDATE CuentaPagar SET Saldo = @SaldoActual, F_aper = F_aper WHERE Numero = NEW.Num_Cuenta;
	END IF;

END;//

CREATE TRIGGER b_update_cuenta_pagar_forma_pago_c BEFORE UPDATE ON CuentaPagar_FormaPagoC
FOR EACH ROW
BEGIN

	IF NOT IsNumber(NEW.Num_Cuenta) THEN
		BEGIN
			SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Numero de la cuenta por pagar invalido';
		END;
	END IF;

	IF NOT (NEW.RIF_Proveed > 0 AND NEW.RIF_Proveed <= 9999999999) THEN
		BEGIN
			SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'RIF del proveedor invalido';
		END;
	END IF;

END;//

CREATE TRIGGER b_insert_factura_compra_forma_pago_c BEFORE INSERT ON FacturaCompra_FormaPagoC
FOR EACH ROW
BEGIN

	IF NOT IsNumber(NEW.Num_Factura) THEN
		BEGIN
			SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Numero de factura de compra invalido';
		END;
	END IF;

	IF NOT (NEW.RIF_Proveed > 0 AND NEW.RIF_Proveed <= 9999999999) THEN
		BEGIN
			SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'RIF del proveedor invalido';
		END;
	END IF;

END;//

CREATE TRIGGER b_update_factura_compra_forma_pago_c BEFORE UPDATE ON FacturaCompra_FormaPagoC
FOR EACH ROW
BEGIN

	IF NOT IsNumber(NEW.Num_Factura) THEN
		BEGIN
			SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Numero de factura de compra invalido';
		END;
	END IF;

	IF NOT (NEW.RIF_Proveed > 0 AND NEW.RIF_Proveed <= 9999999999) THEN
		BEGIN
			SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'RIF del proveedor invalido';
		END;
	END IF;

END;//

CREATE TRIGGER b_insert_cuenta_forma_pago_c BEFORE INSERT ON Cuenta_FormaPagoC
FOR EACH ROW
BEGIN

	IF NOT IsNumber(NEW.Num_Cuenta) THEN
		BEGIN
			SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Numero de la cuenta invalido';
		END;
	END IF;

END;//

CREATE TRIGGER b_update_cuenta_forma_pago_c BEFORE UPDATE ON Cuenta_FormaPagoC
FOR EACH ROW
BEGIN

	IF NOT IsNumber(NEW.Num_Cuenta) THEN
		BEGIN
			SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Numero de la cuenta invalido';
		END;
	END IF;

END;//

CREATE TRIGGER b_insert_orden_factura_venta BEFORE INSERT ON Orden_FacturaVenta
FOR EACH ROW
BEGIN

	IF NOT IsNumber(NEW.Num_Orden) THEN
		BEGIN
			SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Numero de la orden invalido';
		END;
	END IF;

	IF NOT IsNumber(NEW.Num_Factura) THEN
		BEGIN
			SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Numero de factura de venta invalido';
		END;
	END IF;

END;//

CREATE TRIGGER b_update_orden_factura_venta BEFORE UPDATE ON Orden_FacturaVenta
FOR EACH ROW
BEGIN

	IF NOT IsNumber(NEW.Num_Orden) THEN
		BEGIN
			SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Numero de la orden invalido';
		END;
	END IF;

	IF NOT IsNumber(NEW.Num_Factura) THEN
		BEGIN
			SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Numero de factura de venta invalido';
		END;
	END IF;

END;//

CREATE TRIGGER b_insert_orden_equipo BEFORE INSERT ON Orden_Equipo
FOR EACH ROW
BEGIN

	IF NOT NoSpaces(NEW.Serial_Equipo) THEN
		BEGIN
			SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Serial del equipo invalido';
		END;
	END IF;

	IF NOT IsNumber(NEW.Num_Orden) THEN
		BEGIN
			SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Numero de la orden invalido';
		END;
	END IF;

END;//

CREATE TRIGGER b_update_orden_equipo BEFORE UPDATE ON Orden_Equipo
FOR EACH ROW
BEGIN

	IF NOT NoSpaces(NEW.Serial_Equipo) THEN
		BEGIN
			SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Serial del equipo invalido';
		END;
	END IF;

	IF NOT IsNumber(NEW.Num_Orden) THEN
		BEGIN
			SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Numero de la orden invalido';
		END;
	END IF;

END;//

CREATE TRIGGER b_insert_factura_venta_equipo_venta BEFORE INSERT ON FacturaVenta_EquipoVenta
FOR EACH ROW
BEGIN

	IF NOT NoSpaces(NEW.Codigo_Equipo) THEN
		BEGIN
			SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Codigo del equipo invalido';
		END;
	END IF;

	IF NOT IsNumber(NEW.Num_Factura) THEN
		BEGIN
			SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Numero de factura de venta invalido';
		END;
	END IF;

	IF NOT (NEW.Cantidad > 0 AND NEW.Cantidad <= 9999) THEN
		BEGIN
			SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Cantidad invalida';
		END;
	END IF;

	IF NOT (NEW.PVP >= 0) THEN
		BEGIN
			SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'PVP invalido';
		END;
	END IF;

	SELECT Cantidad INTO @Cant FROM EquipoVenta WHERE Codigo = NEW.Codigo_Equipo LIMIT 1;

	SET @CantActual = @Cant - NEW.Cantidad;

	IF @CantActual < 0 THEN
		SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'La cantidad del equipo que se desea vender supera la cantidad actual';
	ELSE
		UPDATE Inventario SET Cantidad = @CantActual WHERE Codigo = NEW.Codigo_Equipo;
	END IF;

END;//

CREATE TRIGGER b_update_factura_venta_equipo_venta BEFORE UPDATE ON FacturaVenta_EquipoVenta
FOR EACH ROW
BEGIN

	IF NOT NoSpaces(NEW.Codigo_Equipo) THEN
		BEGIN
			SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Codigo del equipo invalido';
		END;
	END IF;

	IF NOT IsNumber(NEW.Num_Factura) THEN
		BEGIN
			SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Numero de factura de venta invalido';
		END;
	END IF;

	IF NOT (NEW.Cantidad > 0 AND NEW.Cantidad <= 9999) THEN
		BEGIN
			SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Cantidad invalida';
		END;
	END IF;

	IF NOT (NEW.PVP >= 0) THEN
		BEGIN
			SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'PVP invalido';
		END;
	END IF;

	SELECT Cantidad INTO @CantEquipo FROM EquipoVenta WHERE Codigo = NEW.Codigo_Equipo LIMIT 1;

	SET @CantActual = 0;

	IF NEW.Cantidad > OLD.Cantidad THEN
		SET @CantActual = NEW.Cantidad - OLD.Cantidad;
		SET @CantEquipo = @CantEquipo - @CantActual;
	ELSE
		IF OLD.Cantidad > NEW.Cantidad THEN
			SET @CantActual = OLD.Cantidad - NEW.Cantidad;
			SET @CantEquipo = @CantEquipo + @CantActual;
		END IF;
	END IF;

	IF @CantEquipo < 0 THEN
		SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'La cantidad del equipo presenta inconsistencia en cuanto a la cantidad en existencia';
	ELSE
		IF @CantActual > 0 THEN
			UPDATE Inventario SET Cantidad = @CantEquipo WHERE Codigo = NEW.Codigo_Equipo;
		END IF;
	END IF;

END;//

CREATE TRIGGER b_insert_venta_web_equipo_venta BEFORE INSERT ON VentaWeb_EquipoVenta
FOR EACH ROW
BEGIN

	IF NOT NoSpaces(NEW.Codigo_Equipo) THEN
		BEGIN
			SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Codigo del equipo invalido';
		END;
	END IF;

	IF NOT IsNumber(NEW.Num_Venta) THEN
		BEGIN
			SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Numero de la venta invalido';
		END;
	END IF;

	IF NOT (NEW.Cantidad > 0 AND NEW.Cantidad <= 9999) THEN
		BEGIN
			SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Cantidad invalida';
		END;
	END IF;

	IF NOT (NEW.Precio >= 0) THEN
		BEGIN
			SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'PVP invalido';
		END;
	END IF;

	SELECT Cantidad INTO @Cant FROM EquipoVenta WHERE Codigo = NEW.Codigo_Equipo LIMIT 1;

	SET @CantActual = @Cant - NEW.Cantidad;

	IF @CantActual < 0 THEN
		SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'La cantidad del equipo que se desea vender supera la cantidad actual';
	ELSE
		UPDATE Inventario SET Cantidad = @CantActual WHERE Codigo = NEW.Codigo_Equipo;
	END IF;

END;//

CREATE TRIGGER b_update_venta_web_equipo_venta BEFORE UPDATE ON VentaWeb_EquipoVenta
FOR EACH ROW
BEGIN

	IF NOT NoSpaces(NEW.Codigo_Equipo) THEN
		BEGIN
			SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Codigo del equipo invalido';
		END;
	END IF;

	IF NOT IsNumber(NEW.Num_Venta) THEN
		BEGIN
			SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Numero de la venta invalido';
		END;
	END IF;

	IF NOT (NEW.Cantidad > 0 AND NEW.Cantidad <= 9999) THEN
		BEGIN
			SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Cantidad invalida';
		END;
	END IF;

	IF NOT (NEW.Precio >= 0) THEN
		BEGIN
			SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'PVP invalido';
		END;
	END IF;

	SELECT Cantidad INTO @CantEquipo FROM EquipoVenta WHERE Codigo = NEW.Codigo_Equipo LIMIT 1;

	SET @CantActual = 0;

	IF NEW.Cantidad > OLD.Cantidad THEN
		SET @CantActual = NEW.Cantidad - OLD.Cantidad;
		SET @CantEquipo = @CantEquipo - @CantActual;
	ELSE
		IF OLD.Cantidad > NEW.Cantidad THEN
			SET @CantActual = OLD.Cantidad - NEW.Cantidad;
			SET @CantEquipo = @CantEquipo + @CantActual;
		END IF;
	END IF;

	IF @CantEquipo < 0 THEN
		SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'La cantidad del equipo presenta inconsistencia en cuanto a la cantidad en existencia';
	ELSE
		IF @CantActual > 0 THEN
			UPDATE Inventario SET Cantidad = @CantEquipo WHERE Codigo = NEW.Codigo_Equipo;
		END IF;
	END IF;

END;//

CREATE TRIGGER b_insert_factura_compra_inventario BEFORE INSERT ON FacturaCompra_Inventario
FOR EACH ROW
BEGIN

	IF NOT NoSpaces(NEW.Cod_Inv) THEN
		BEGIN
			SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Codigo del inventario invalido';
		END;
	END IF;

	IF NOT IsNumber(NEW.Num_Fact) THEN
		BEGIN
			SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Numero de factura de compra invalido';
		END;
	END IF;

	IF NOT (NEW.Proveed_Fact > 0 AND NEW.Proveed_Fact <= 9999999999) THEN
		BEGIN
			SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'RIF del proveedor invalido';
		END;
	END IF;

	IF NOT (NEW.Cantidad > 0 AND NEW.Cantidad <= 9999) THEN
		BEGIN
			SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Cantidad invalida';
		END;
	END IF;

	IF NOT (NEW.Costo >= 0) THEN
		BEGIN
			SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Costo invalido';
		END;
	END IF;

	UPDATE Inventario SET Cantidad = Cantidad + NEW.Cantidad, F_Registro = F_Registro WHERE Codigo = NEW.Cod_Inv;

END;//

CREATE TRIGGER b_update_factura_compra_inventario BEFORE UPDATE ON FacturaCompra_Inventario
FOR EACH ROW
BEGIN

	IF NOT NoSpaces(NEW.Cod_Inv) THEN
		BEGIN
			SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Codigo del inventario invalido';
		END;
	END IF;

	IF NOT IsNumber(NEW.Num_Fact) THEN
		BEGIN
			SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Numero de factura de compra invalido';
		END;
	END IF;

	IF NOT (NEW.Proveed_Fact > 0 AND NEW.Proveed_Fact <= 9999999999) THEN
		BEGIN
			SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'RIF del proveedor invalido';
		END;
	END IF;

	IF NOT (NEW.Cantidad > 0 AND NEW.Cantidad <= 9999) THEN
		BEGIN
			SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Cantidad invalida';
		END;
	END IF;

	IF NOT (NEW.Costo >= 0) THEN
		BEGIN
			SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Costo invalido';
		END;
	END IF;

	SELECT Cantidad INTO @CantInv FROM Inventario WHERE Codigo = NEW.Cod_Inv LIMIT 1;

	SET @CantActual = 0;

	IF NEW.Cantidad > OLD.Cantidad THEN
		SET @CantActual = NEW.Cantidad - OLD.Cantidad;
		SET @CantInv = @CantInv + @CantActual;
	ELSE
		IF OLD.Cantidad > NEW.Cantidad THEN
			SET @CantActual = OLD.Cantidad - NEW.Cantidad;
			SET @CantInv = @CantInv - @CantActual;
		END IF;
	END IF;

	IF @CantInv < 0 THEN
		SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'La cantidad del inventario presenta inconsistencia en cuanto a la cantidad en existencia';
	ELSE
		IF @CantActual > 0 THEN
			UPDATE Inventario SET Cantidad = @CantInv, F_Registro = F_Registro WHERE Codigo = NEW.Cod_Inv;
		END IF;
	END IF;

END;//

DELIMITER ;