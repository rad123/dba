-- drop all tables
DROP TABLE IF EXISTS FacturaCompra_Inventario;
DROP TABLE IF EXISTS VentaWeb_EquipoVenta;
DROP TABLE IF EXISTS FacturaVenta_EquipoVenta;
DROP TABLE IF EXISTS Orden_Equipo;
DROP TABLE IF EXISTS Orden_FacturaVenta;
DROP TABLE IF EXISTS Cuenta_FormaPagoC;
DROP TABLE IF EXISTS FacturaCompra_FormaPagoC;
DROP TABLE IF EXISTS CuentaPagar_FormaPagoC;
DROP TABLE IF EXISTS FormaPagoC;
DROP TABLE IF EXISTS CuentaPagar;
DROP TABLE IF EXISTS Cuenta_FormaPagoV;
DROP TABLE IF EXISTS FacturaVenta_FormaPagoV;
DROP TABLE IF EXISTS CuentaCobrar_FormaPagoV;
DROP TABLE IF EXISTS FormaPagoV;
DROP TABLE IF EXISTS CuentaCobrar;
DROP TABLE IF EXISTS Repuesto_Reparacion;
DROP TABLE IF EXISTS Reparacion;
DROP TABLE IF EXISTS VentaWeb;
DROP TABLE IF EXISTS FacturaVenta;
DROP TABLE IF EXISTS Orden;
DROP TABLE IF EXISTS Equipo;
DROP TABLE IF EXISTS Notificacion;
DROP TABLE IF EXISTS Cliente;
DROP TABLE IF EXISTS FacturaCompra;
DROP TABLE IF EXISTS Proveedor;
DROP TABLE IF EXISTS EquipoVenta;
DROP TABLE IF EXISTS Herramienta;
DROP TABLE IF EXISTS Repuesto;
DROP TABLE IF EXISTS Inventario;
DROP TABLE IF EXISTS Personal;
DROP TABLE IF EXISTS Categoria;
DROP TABLE IF EXISTS Modelo;
DROP TABLE IF EXISTS Marca;
DROP TABLE IF EXISTS Cuenta;
DROP TABLE IF EXISTS Tienda;

-- create all tables
CREATE TABLE Tienda (

	RIF BigInt(10) NOT NULL COMMENT 'Clave primaria (P.K.). RIF de la tienda.',
	Tipo_RIF enum('J', 'G') NOT NULL COMMENT 'J - Empresa privada, G - Empresa del Gobierno.',
	Direccion varchar(50) NOT NULL COMMENT 'Direccion o ubicacion de la tienda.',
	Nombre varchar(50) UNIQUE NOT NULL COMMENT 'Nombre que identifica la tienda (unico).',
	Email varchar(100) UNIQUE NOT NULL COMMENT 'Email de la tienda (unico).',
	Telefono varchar(20) NOT NULL COMMENT 'Numero telefonico de contacto.',

	CHECK (RIF > 0 AND RIF <= 9999999999),

	CONSTRAINT RIF_P_KEY PRIMARY KEY (RIF) 

) ENGINE=InnoDB DEFAULT CHARSET='utf8';

CREATE TABLE Cuenta (

	Numero varchar(20) NOT NULL COMMENT 'Clave Primaria (P.K.). Identifica la cuenta.',
	RIF_tienda BigInt(10) NOT NULL COMMENT 'Clave Foranea (F.K.). Viene de la relacion Tienda.',
	Tipo enum('Ahorro', 'Corriente') NOT NULL COMMENT 'Representa el tipo de cuenta.',
	Banco varchar(20) NOT NULL COMMENT 'Representa el nombre del banco al que corresponde la cuenta.',

	CHECK (RIF_tienda > 0 AND RIF_tienda <= 9999999999),

	CONSTRAINT NUMERO_P_KEY PRIMARY KEY (Numero),

	FOREIGN KEY (RIF_tienda) REFERENCES Tienda (RIF)
	ON UPDATE RESTRICT
	ON DELETE CASCADE

) ENGINE=InnoDB DEFAULT CHARSET='utf8';

CREATE TABLE Marca (

	ID int NOT NULL AUTO_INCREMENT COMMENT 'Clave Primaria (P.K.).',
	Nombre varchar(50) NOT NULL COMMENT 'Nombre de la marca.',

	CONSTRAINT ID_P_KEY PRIMARY KEY (ID)

) ENGINE=InnoDB DEFAULT CHARSET='utf8';

CREATE TABLE Modelo (

	ID int NOT NULL AUTO_INCREMENT COMMENT 'Clave Primaria (P.K.).',
	ID_Marca int NOT NULL COMMENT 'Clave Foranea (F.K.). Viene de la relacion Marca.',
	Nombre varchar(50) NOT NULL COMMENT 'Nombre del modelo.',

	CONSTRAINT ID_P_KEY PRIMARY KEY (ID),

	FOREIGN KEY (ID_Marca) REFERENCES Marca (ID)
	ON UPDATE CASCADE
	ON DELETE CASCADE

) ENGINE=InnoDB DEFAULT CHARSET='utf8';

CREATE TABLE Categoria (

	ID int NOT NULL AUTO_INCREMENT COMMENT 'Clave Primaria (P.K.).',
	ID_Modelo int NOT NULL COMMENT 'Clave Foranea (F.K.). Viene de la relacion Modelo.',
	Nombre varchar(50) NOT NULL COMMENT 'Nombre de la categoria.',

	CONSTRAINT ID_P_KEY PRIMARY KEY (ID),

	FOREIGN KEY (ID_Modelo) REFERENCES Modelo (ID)
	ON UPDATE CASCADE
	ON DELETE CASCADE

) ENGINE=InnoDB DEFAULT CHARSET='utf8';

CREATE TABLE Personal (

	CI int(9) NOT NULL COMMENT 'Clave primaria (P.K.). Identificacion del personal.',
	RIF BigInt(10) UNIQUE NOT NULL COMMENT 'RIF que lo identifica.',
	Tipo_RIF enum('J', 'G') NOT NULL COMMENT 'J - Empresa privada, G - Empresa del Gobierno.',
	Direccion varchar(50) NOT NULL COMMENT 'Direccion de vivienda.',
	Nombre varchar(50) NOT NULL COMMENT 'Nombre completo.',
	Email varchar(100) UNIQUE NOT NULL COMMENT 'Email del personal (unico).', 
	Telefono varchar(20) UNIQUE NOT NULL COMMENT 'Numero de telefono (unico).',
	Especialidad varchar(50) NULL COMMENT 'Especialidad del personal (hardware, software, reparaciones de micas, etc.), en caso de ser un tecnico.',
	Tipo enum('GR', 'TE', 'AS') NOT NULL COMMENT 'GR - Gerente, TC - Tecnico, AS - Asistente Administrativo.',

	CONSTRAINT CI_VALUE CHECK (CI > 0 AND CI <= 999999999),
	CONSTRAINT RIF_VALUE CHECK (RIF > 0 AND RIF <= 9999999999),

	CONSTRAINT CI_P_KEY PRIMARY KEY (CI)

) ENGINE=InnoDB DEFAULT CHARSET='utf8';

CREATE TABLE Inventario (

	Codigo varchar(5) NOT NULL COMMENT 'Clave Primaria (P.K.). Identifica el inventario.',
	F_Registro timestamp NOT NULL COMMENT 'Fecha y hora en que se registra el inventario.',
	Cantidad int(4) NOT NULL COMMENT 'Cantidad en inventario.',

	CHECK (Cantidad > 0 AND Cantidad <= 9999),

	CONSTRAINT CODIGO_P_KEY PRIMARY KEY (Codigo)

) ENGINE=InnoDB DEFAULT CHARSET='utf8';

CREATE TABLE Repuesto (

	Codigo varchar(5) NOT NULL COMMENT 'Clave Primaria (P.K.). Clave Foranea (F.K.). Viene de la relacion Inventario.',
	Nombre varchar(50) NOT NULL COMMENT 'Nombre del repuesto.',
	Descripcion varchar(200) NULL COMMENT 'Descripcion o detalle del repuesto.',
	Cantidad int(4) NOT NULL COMMENT 'Cantidad en existencia.',
	Precio decimal(9,2) NOT NULL DEFAULT 0 COMMENT 'Precio del repuesto.',

	CHECK (Cantidad > 0 AND Cantidad <= 9999),
	CHECK (Precio >= 0),

	CONSTRAINT CODIGO_P_KEY PRIMARY KEY (Codigo),

	FOREIGN KEY (Codigo) REFERENCES Inventario (Codigo)
	ON UPDATE CASCADE
	ON DELETE CASCADE

) ENGINE=InnoDB DEFAULT CHARSET='utf8';

CREATE TABLE Herramienta (

	Codigo varchar(5) NOT NULL COMMENT 'Clave Primaria (P.K.). Clave Foranea (F.K.). Viene de la relacion Inventario.',
	CI_Personal int(9) NULL COMMENT 'Clave Foranea (F.K.). Viene de la relacion Personal.',
	Nombre varchar(50) NOT NULL COMMENT 'Nombre de la herramienta.',
	Descripcion varchar(200) NULL COMMENT 'Descripcion o detalle de la herramienta.',
	Cantidad int(4) NOT NULL COMMENT 'Cantidad en existencia.',
	Precio decimal(9,2) NOT NULL DEFAULT 0 COMMENT 'Precio de la herramienta.',
	Cant_Uso int(4) NULL DEFAULT 0 COMMENT 'Cantidad utilizada por el Personal.',
	Precio_Uso decimal(9,2) NULL DEFAULT 0 COMMENT 'Precio de la herramienta para su uso por el Personal.',

	CHECK (CI_Personal > 0 AND CI_Personal <= 999999999),
	CHECK (Cantidad > 0 AND Cantidad <= 9999),
	CHECK (Precio >= 0),
	CHECK (Cant_Uso > 0 AND Cant_Uso <= 9999),
	CHECK (Precio_Uso >= 0),

	CONSTRAINT CODIGO_P_KEY PRIMARY KEY (Codigo),

	FOREIGN KEY (Codigo) REFERENCES Inventario (Codigo)
	ON UPDATE CASCADE
	ON DELETE CASCADE,

	FOREIGN KEY (CI_Personal) REFERENCES Personal (CI)
	ON UPDATE CASCADE
	ON DELETE CASCADE

) ENGINE=InnoDB DEFAULT CHARSET='utf8';

CREATE TABLE EquipoVenta (

	Codigo varchar(5) NOT NULL COMMENT 'Clave Primaria (P.K.). Clave Foranea (F.K.). Viene de la relacion Inventario.',
	ID_Categoria int NOT NULL COMMENT 'Clave Foranea (F.K.). Viene de la relacion Categoria.',
	Nombre varchar(50) NOT NULL COMMENT 'Nombre del equipo.',
	Descripcion varchar(200) NULL COMMENT 'Descripcion o detalle del equipo.',
	Cantidad int(4) NOT NULL COMMENT 'Cantidad en existencia.',
	Precio decimal(9,2) NOT NULL DEFAULT 0 COMMENT 'Precio del equipo.',

	CHECK (Cantidad > 0 AND Cantidad <= 9999),
	CHECK (Precio >= 0),

	CONSTRAINT CODIGO_P_KEY PRIMARY KEY (Codigo),

	FOREIGN KEY (Codigo) REFERENCES Inventario (Codigo)
	ON UPDATE CASCADE
	ON DELETE CASCADE,

	FOREIGN KEY (ID_Categoria) REFERENCES Categoria (ID)
	ON UPDATE CASCADE
	ON DELETE CASCADE	

) ENGINE=InnoDB DEFAULT CHARSET='utf8';

CREATE TABLE Proveedor (

	RIF BigInt(10) NOT NULL COMMENT 'RIF que lo identifica.',
	Tipo_RIF enum('J', 'G') NOT NULL COMMENT 'J - Empresa privada, G - Empresa del Gobierno.',
	Direccion varchar(50) NOT NULL COMMENT 'Direccion del proveedor.',
	Nombre varchar(50) NOT NULL COMMENT 'Nombre del proveedor.',
	Email varchar(100) UNIQUE NULL COMMENT 'Email del proveedor (unico).', 
	Telefono varchar(20) UNIQUE NOT NULL COMMENT 'Numero de telefono (unico).',

	CHECK (RIF > 0 AND RIF <= 9999999999),

	CONSTRAINT RIF_P_KEY PRIMARY KEY (RIF)

) ENGINE=InnoDB DEFAULT CHARSET='utf8';

CREATE TABLE FacturaCompra (

	Numero varchar(5) NOT NULL COMMENT 'Clave Primaria (P.K.). Identifica la factura.',
	RIF_Proveed BigInt(10) NOT NULL COMMENT 'Clave Primaria (P.K.). Clave Foranea (F.K.). Viene de la relacion Proveedor.',
	Fecha timestamp NOT NULL COMMENT 'Fecha en que se genera la factura.',
	Estado varchar(20) NOT NULL COMMENT 'Estado de la factura.',
	Descripcion varchar(200) NULL COMMENT 'Descripcion o detalle de la factura.',
	Monto decimal(9,2) NOT NULL DEFAULT 0 COMMENT 'Monto total de la factura.',

	CHECK (RIF_Proveed > 0 AND RIF_Proveed <= 9999999999),
	CHECK (Monto >= 0),

	CONSTRAINT P_KEY PRIMARY KEY (Numero, RIF_Proveed),

	FOREIGN KEY (RIF_Proveed) REFERENCES Proveedor (RIF)
	ON UPDATE NO ACTION
	ON DELETE NO ACTION

) ENGINE=InnoDB DEFAULT CHARSET='utf8';

CREATE TABLE Cliente (

	Identif varchar(15) NOT NULL COMMENT 'Clave Primaria (P.K.). Identificador del cliente.',
	Tipo_Identif enum('V', 'E', 'J', 'G') NOT NULL COMMENT 'V - Venezolano (cedula), E - Extranjero (Pasaporte), J - Empresa privada, G - Empresa del Gobierno.',
	Direccion varchar(50) NOT NULL COMMENT 'Direccion del cliente.',
	Nombre varchar(50) NOT NULL COMMENT 'Nombre completo del cliente.',
	Email varchar(100) UNIQUE NULL COMMENT 'Email del cliente (unico).', 
	Telefono varchar(20) UNIQUE NOT NULL COMMENT 'Numero de telefono (unico).',

	CONSTRAINT IDENTIF_P_KEY PRIMARY KEY (Identif)

) ENGINE=InnoDB DEFAULT CHARSET='utf8';

CREATE TABLE Notificacion (

	F_Hora timestamp NOT NULL COMMENT 'Clave Primaria (P.K.). Fecha y hora en que el cliente recibio la notificacion.',
	Identif_Cliente varchar(15) NOT NULL COMMENT 'Clave Primaria (P.K.). Clave Foranea (F.K.). Viene de la relacion Cliente.',
	Via enum('email', 'sms') NOT NULL COMMENT 'Via por la cual se envia la notificacion.',
	Texto varchar(255) NOT NULL COMMENT 'Contenido del mensaje enviado.',

	CONSTRAINT P_KEY PRIMARY KEY (F_Hora, Identif_Cliente),

	FOREIGN KEY (Identif_Cliente) REFERENCES Cliente (Identif)
	ON UPDATE NO ACTION
	ON DELETE NO ACTION

) ENGINE=InnoDB DEFAULT CHARSET='utf8';

CREATE TABLE Equipo (

	Serial_Eq varchar(20) NOT NULL COMMENT 'Clave Primaria (P.K.). Serial que identifica el equipo.',
	ID_Categoria int NOT NULL COMMENT 'Clave Foranea (F.K.). Viene de la relacion Categoria.',
	Identif_Cliente varchar(15) NOT NULL COMMENT 'Clave Foranea (F.K.). Viene de la relacion Cliente.',
	Nombre varchar(50) NOT NULL COMMENT 'Nombre del equipo',
	Descripcion varchar(200) NULL COMMENT 'Descripcion o detalle del equipo.',
	Precio decimal(9,2) NOT NULL DEFAULT 0 COMMENT 'Precio del equipo.',

	CHECK (Precio >= 0),

	CONSTRAINT SERIAL_EQ_P_KEY PRIMARY KEY (Serial_Eq),

	FOREIGN KEY (ID_Categoria) REFERENCES Categoria (ID)
	ON UPDATE CASCADE
	ON DELETE CASCADE,

	FOREIGN KEY (Identif_Cliente) REFERENCES Cliente (Identif)
	ON UPDATE RESTRICT
	ON DELETE NO ACTION

) ENGINE=InnoDB DEFAULT CHARSET='utf8';

CREATE TABLE Orden (

	Numero varchar(5) NOT NULL COMMENT 'Clave Primaria (P.K.). Identifica la orden.',
	F_inicio timestamp NOT NULL COMMENT 'Fecha en que se inicio la orden.',
	F_fin timestamp NOT NULL COMMENT 'Fecha de culminacion de la orden.',
	Estado enum('ESP', 'REV', 'REP', 'DEV') NOT NULL COMMENT 'ESP - En espera, REV - Revisado, REP - Reparado, DEV - Devolucion.',
	Total decimal(9,2) NOT NULL DEFAULT 0 COMMENT 'Costo total de la orden.',

	CHECK (Total >= 0),

	CONSTRAINT NUMERO_P_KEY PRIMARY KEY (Numero)

) ENGINE=InnoDB DEFAULT CHARSET='utf8';

CREATE TABLE FacturaVenta (

	Numero varchar(5) NOT NULL COMMENT 'Clave Primaria (P.K.). Identifica la factura.',
	Identif_Cliente varchar(15) NOT NULL COMMENT 'Clave Foranea (F.K.). Viene de la relacion Cliente.',
	Fecha timestamp NOT NULL COMMENT 'Fecha en que se genera la factura.',
	Estado varchar(20) NOT NULL COMMENT 'Estado de la factura.',
	Descripcion varchar(200) NULL COMMENT 'Descripcion o detalle de la factura.',
	Monto decimal(9,2) NOT NULL DEFAULT 0 COMMENT 'Monto total de la factura.',

	CHECK (Monto >= 0),

	CONSTRAINT NUMERO_P_KEY PRIMARY KEY (Numero),

	FOREIGN KEY (Identif_Cliente) REFERENCES Cliente (Identif)
	ON UPDATE NO ACTION
	ON DELETE NO ACTION

) ENGINE=InnoDB DEFAULT CHARSET='utf8';

CREATE TABLE VentaWeb (

	Numero varchar(5) NOT NULL COMMENT 'Clave Primaria (P.K.). Identifica la venta web.',
	Identif_Cliente varchar(15) NOT NULL COMMENT 'Clave Foranea (F.K.). Viene de la relacion Cliente.',
	Num_Factura varchar(5) NOT NULL COMMENT 'Clave Foranea (F.K). Viene de la relacion FacturaVenta.',
	Concepto varchar(200) NOT NULL COMMENT 'Concepto o detalle de la venta.',
	Estado enum('PAPB', 'APROB', 'RECHAZ') NOT NULL COMMENT 'PAPB - Por aprobar, APROB - Aprobada, RECHAZ - Rechazada.',

	CONSTRAINT NUMERO_P_KEY PRIMARY KEY (Numero),

	FOREIGN KEY (Identif_Cliente) REFERENCES Cliente (Identif)
	ON UPDATE RESTRICT
	ON DELETE RESTRICT,

	FOREIGN KEY (Num_Factura) REFERENCES FacturaVenta (Numero)
	ON UPDATE NO ACTION
	ON DELETE NO ACTION

) ENGINE=InnoDB DEFAULT CHARSET='utf8';

CREATE TABLE Reparacion (

	F_asig timestamp NOT NULL COMMENT 'Clave Primaria (P.K.). Fecha de inicio de la reparacion.',
	CI_Personal int(9) NOT NULL COMMENT 'Clave Primaria (P.K.). Clave Foranea (F.K.). Viene de la relacion Personal.',
	Numero_Orden varchar(5) NOT NULL COMMENT 'Clave Primaria (P.K.). Clave Foranea (F.K.). Viene de la relacion Orden.',
	F_rep timestamp NOT NULL COMMENT 'Fecha de culminacion de la reparacion.',
	Descripcion varchar(200) NULL COMMENT 'Descripcion o detalle de la reparacion.',
	Comision decimal(9,2) NOT NULL DEFAULT 0 COMMENT 'Comision que le corresponde al tecnico que realizo la reparacion.',
	Total decimal(9,2) NOT NULL DEFAULT 0 COMMENT 'Monto total de la reparacion.',
	Estado enum('ESP', 'REV', 'REP', 'DEV') NOT NULL COMMENT 'ESP - En espera, REV - Revisado, REP - Reparado, DEV - Devolucion.',

	CHECK (CI_Personal > 0 AND CI_Personal <= 999999999),
	CHECK (Comision >= 0),
	CHECK (Total >= 0),

	CONSTRAINT P_KEY PRIMARY KEY (F_asig, CI_Personal, Numero_Orden),

	FOREIGN KEY (CI_Personal) REFERENCES Personal (CI)
	ON UPDATE RESTRICT
	ON DELETE NO ACTION,

	FOREIGN KEY (Numero_Orden) REFERENCES Orden (Numero)
	ON UPDATE RESTRICT
	ON DELETE NO ACTION

) ENGINE=InnoDB DEFAULT CHARSET='utf8';

CREATE TABLE Repuesto_Reparacion (

	Codigo_Repues varchar(5) NOT NULL COMMENT 'Clave Primaria (P.K.). Clave Foranea (F.K.). Viene de la relacion Repuesto.',
	F_asig_Rep timestamp NOT NULL COMMENT 'Clave Primaria (P.K.). Clave Foranea (F.K.). Viene de la relacion Reparacion.',
	CI_Pers_Rep int(9) NOT NULL COMMENT 'Clave Primaria (P.K.). Clave Foranea (F.K.). Viene de la relacion Reparacion.',
	Num_Ord_Rep varchar(5) NOT NULL COMMENT 'Clave Primaria (P.K.). Clave Foranea (F.K.). Viene de la relacion Reparacion.',
    Cant_Rep int(4) NOT NULL COMMENT 'Cantidad utilizada del repuesto para la reparacion.',
	Precio_Rep decimal(9,2) NOT NULL DEFAULT 0 COMMENT 'Precio del repuesto en el momento en que se hizo la reparacion.',

	CHECK (CI_Pers_Rep > 0 AND CI_Pers_Rep <= 999999999),
	CHECK (Cant_Rep > 0 AND Cant_Rep <= 9999),
	CHECK (Precio_Rep >= 0),

	CONSTRAINT P_KEY PRIMARY KEY (Codigo_Repues, F_asig_Rep, CI_Pers_Rep, Num_Ord_Rep),

	FOREIGN KEY (Codigo_Repues) REFERENCES Repuesto (Codigo)
	ON UPDATE RESTRICT
	ON DELETE CASCADE,

	FOREIGN KEY (F_asig_Rep, CI_Pers_Rep, Num_Ord_Rep) REFERENCES Reparacion (F_asig, CI_Personal, Numero_Orden)
	ON UPDATE RESTRICT
	ON DELETE CASCADE

) ENGINE=InnoDB DEFAULT CHARSET='utf8';

CREATE TABLE CuentaCobrar (

	Numero varchar(5) NOT NULL COMMENT 'Clave Primaria (P.K.). Identifica la cuenta.',
	Identif_Cliente varchar(15) NOT NULL COMMENT 'Clave Primaria (P.K.). Clave Foranea (F.K.). Viene de la relacion Cliente.',
	Num_Factura varchar(5) NOT NULL COMMENT 'Clave Foranea (F.K.). Viene de la relacion FacturaVenta.',
	F_aper timestamp NOT NULL COMMENT 'Fecha de apertura de la cuenta.',
	F_lim date NOT NULL COMMENT 'Fecha limite de la cuenta.',
	Estado varchar(20) NOT NULL COMMENT 'Estado de la cuenta.',
	Descripcion varchar(200) NULL COMMENT 'Descripcion o detalle de la cuenta.',
	Saldo decimal(9,2) NOT NULL DEFAULT 0 COMMENT 'Saldo actual de la cuenta.',
	Monto decimal(9,2) NOT NULL DEFAULT 0 COMMENT 'Monto total de la cuenta.',

	CHECK (Saldo >= 0),
	CHECK (Monto >= 0),

	CONSTRAINT P_KEY PRIMARY KEY (Numero, Identif_Cliente),

	FOREIGN KEY (Identif_Cliente) REFERENCES Cliente (Identif)
	ON UPDATE NO ACTION
	ON DELETE CASCADE,

	FOREIGN KEY (Num_Factura) REFERENCES FacturaVenta (Numero)
	ON UPDATE NO ACTION
	ON DELETE CASCADE

) ENGINE=InnoDB DEFAULT CHARSET='utf8';

CREATE TABLE FormaPagoV (

	Fecha timestamp NOT NULL COMMENT 'Clave primaria (P.K.). Fecha y hora en que se efectua el pago.',
	Banco varchar(20) NULL COMMENT 'Nombre del banco.',
	Nref int(9) NULL COMMENT 'Numero de referencia.',
	Descripcion varchar(200) NULL COMMENT 'Descripcion o detalle del pago.',
	Tipo enum('EF', 'TRF', 'DEP', 'CH', 'TD', 'TC') NOT NULL COMMENT 'Modo de pago (EF – Efectivo, TRF – Transferencia, DEP – Deposito, CH – Cheque, TD – Tarjeta Debito, TC – Tarjeta Credito).',
	Monto decimal(9,2) NOT NULL DEFAULT 0 COMMENT 'Monto a pagar.',

	CHECK (Nref > 0 AND Nref <= 999999999),
	CHECK (Monto >= 0),

	CONSTRAINT FECHA_P_KEY PRIMARY KEY (Fecha)

) ENGINE=InnoDB DEFAULT CHARSET='utf8';

CREATE TABLE CuentaCobrar_FormaPagoV (

	Num_Cuenta varchar(5) NOT NULL COMMENT 'Clave Primaria (P.K.). Clave Foranea (F.K.). Viene de la relacion CuentaCobrar.',
	Cliente_Cuenta varchar(15) NOT NULL COMMENT 'Clave Primaria (P.K.). Clave Foranea (F.K.). Viene de la relacion CuentaCobrar.',
	Fecha_Pago timestamp NOT NULL COMMENT 'Clave primaria (P.K.). Clave Foranea (F.K.). Viene de la relacion FormaPagoV.',

	CONSTRAINT P_KEY PRIMARY KEY (Num_Cuenta, Cliente_Cuenta, Fecha_Pago),

	FOREIGN KEY (Num_Cuenta) REFERENCES CuentaCobrar (Numero)
	ON UPDATE RESTRICT
	ON DELETE CASCADE,

	FOREIGN KEY (Cliente_Cuenta) REFERENCES CuentaCobrar (Identif_Cliente)
	ON UPDATE RESTRICT
	ON DELETE CASCADE,

	FOREIGN KEY (Fecha_Pago) REFERENCES FormaPagoV (Fecha)
	ON UPDATE RESTRICT
	ON DELETE CASCADE

) ENGINE=InnoDB DEFAULT CHARSET='utf8';

CREATE TABLE FacturaVenta_FormaPagoV (

	Num_Factura varchar(5) NOT NULL COMMENT 'Clave Primaria (P.K.). Clave Foranea (F.K.). Viene de la relacion FacturaVenta.',
	Fecha_Pago timestamp NOT NULL COMMENT 'Clave primaria (P.K.). Clave Foranea (F.K.). Viene de la relacion FormaPagoV.',

	CONSTRAINT P_KEY PRIMARY KEY (Num_Factura, Fecha_Pago),

	FOREIGN KEY (Num_Factura) REFERENCES FacturaVenta (Numero)
	ON UPDATE RESTRICT
	ON DELETE CASCADE,

	FOREIGN KEY (Fecha_pago) REFERENCES FormaPagoV (Fecha)
	ON UPDATE RESTRICT
	ON DELETE CASCADE 

) ENGINE=InnoDB DEFAULT CHARSET='utf8';

CREATE TABLE Cuenta_FormaPagoV (

	Num_Cuenta varchar(20) NOT NULL COMMENT 'Clave Primaria (P.K.). Clave Foranea (F.K.). Viene de la relacion Cuenta.',
	Fecha_Pago timestamp NOT NULL COMMENT 'Clave primaria (P.K.). Clave Foranea (F.K.). Viene de la relacion FormaPagoV.',

	CONSTRAINT P_KEY PRIMARY KEY (Num_Cuenta, Fecha_Pago),

	FOREIGN KEY (Num_Cuenta) REFERENCES Cuenta (Numero)
	ON UPDATE RESTRICT
	ON DELETE CASCADE,

	FOREIGN KEY (Fecha_Pago) REFERENCES FormaPagoV (Fecha)
	ON UPDATE RESTRICT
	ON DELETE CASCADE

) ENGINE=InnoDB DEFAULT CHARSET='utf8';

CREATE TABLE CuentaPagar (

	Numero varchar(5) NOT NULL COMMENT 'Clave Primaria (P.K.). Identifica la cuenta.',
	RIF_Proveed BigInt(10) NOT NULL COMMENT 'Clave Primaria (P.K.). Clave Foranea (F.K.). Viene de la relacion Proveedor.',
	Num_Factura varchar(5) NOT NULL COMMENT 'Clave Foranea (F.K.). Viene de la relacion FacturaCompra.',
	F_aper timestamp NOT NULL COMMENT 'Fecha de apertura de la cuenta.',
	F_lim date NOT NULL COMMENT 'Fecha limite de la cuenta.',
	Estado varchar(20) NOT NULL COMMENT 'Estado de la cuenta.',
	Descripcion varchar(200) NULL COMMENT 'Descripcion o detalle de la cuenta.',
	Saldo decimal(9,2) NOT NULL DEFAULT 0 COMMENT 'Saldo actual de la cuenta.',
	Monto decimal(9,2) NOT NULL DEFAULT 0 COMMENT 'Monto total de la cuenta.',

	CHECK (Saldo >= 0),
	CHECK (Monto >= 0),

	CONSTRAINT P_KEY PRIMARY KEY (Numero, RIF_Proveed),

	FOREIGN KEY (RIF_Proveed) REFERENCES Proveedor (RIF)
	ON UPDATE NO ACTION
	ON DELETE CASCADE,

	FOREIGN KEY (Num_Factura) REFERENCES FacturaCompra (Numero)
	ON UPDATE NO ACTION
	ON DELETE CASCADE

) ENGINE=InnoDB DEFAULT CHARSET='utf8';

CREATE TABLE FormaPagoC (

	Fecha timestamp NOT NULL COMMENT 'Clave primaria (P.K.). Fecha y hora en que se efectua el pago.',
	Banco varchar(20) NULL COMMENT 'Nombre del banco.',
	Nref int(9) NULL COMMENT 'Numero de referencia.',
	Descripcion varchar(200) NULL COMMENT 'Descripcion o detalle del pago.',
	Tipo enum('EF', 'TRF', 'DEP', 'CH', 'TD', 'TC') NOT NULL COMMENT 'Modo de pago (EF – Efectivo, TRF – Transferencia, DEP – Deposito, CH – Cheque, TD – Tarjeta Debito, TC – Tarjeta Credito).',
	Monto decimal(9,2) NOT NULL DEFAULT 0 COMMENT 'Monto a pagar.',

	CHECK (Nref > 0 AND Nref <= 999999999),
	CHECK (Monto >= 0),

	CONSTRAINT FECHA_P_KEY PRIMARY KEY (Fecha)

) ENGINE=InnoDB DEFAULT CHARSET='utf8';

CREATE TABLE CuentaPagar_FormaPagoC (

	Num_Cuenta varchar(5) NOT NULL COMMENT 'Clave Primaria (P.K.). Clave Foranea (F.K.). Viene de la relacion CuentaPagar.',
	RIF_Proveed BigInt(10) NOT NULL COMMENT 'Clave Primaria (P.K.). Clave Foranea (F.K.). Viene de la relacion CuentaPagar.',
	Fecha_Pago timestamp NOT NULL COMMENT 'Clave primaria (P.K.). Clave Foranea (F.K.). Viene de la relacion FormaPagoC.',

	CHECK (RIF_Proveed > 0 AND RIF_Proveed <= 9999999999),

	CONSTRAINT P_KEY PRIMARY KEY (Num_Cuenta, RIF_Proveed, Fecha_Pago),

	FOREIGN KEY (Num_Cuenta) REFERENCES CuentaPagar (Numero)
	ON UPDATE NO ACTION
	ON DELETE NO ACTION,

	FOREIGN KEY (RIF_Proveed) REFERENCES CuentaPagar (RIF_Proveed)
	ON UPDATE NO ACTION
	ON DELETE NO ACTION,

	FOREIGN KEY (Fecha_Pago) REFERENCES FormaPagoC (Fecha)
	ON UPDATE NO ACTION
	ON DELETE NO ACTION

) ENGINE=InnoDB DEFAULT CHARSET='utf8';

CREATE TABLE FacturaCompra_FormaPagoC (

	Num_Factura varchar(5) NOT NULL COMMENT 'Clave Primaria (P.K.). Clave Foranea (F.K.). Viene de la relacion FacturaCompra.',
	RIF_Proveed BigInt(10) NOT NULL COMMENT 'Clave Primaria (P.K.). Clave Foranea (F.K.). Viene de la relacion FacturaCompra.',
	Fecha_Pago timestamp NOT NULL COMMENT 'Clave primaria (P.K.). Clave Foranea (F.K.). Viene de la relacion FormaPagoC.',

	CHECK (RIF_Proveed > 0 AND RIF_Proveed <= 9999999999),

	CONSTRAINT P_KEY PRIMARY KEY (Num_Factura, RIF_Proveed, Fecha_Pago),

	FOREIGN KEY (Num_Factura) REFERENCES FacturaCompra (Numero)
	ON UPDATE NO ACTION
	ON DELETE NO ACTION,

	FOREIGN KEY (RIF_Proveed) REFERENCES FacturaCompra (RIF_Proveed)
	ON UPDATE NO ACTION
	ON DELETE NO ACTION,

	FOREIGN KEY (Fecha_pago) REFERENCES FormaPagoC (Fecha)
	ON UPDATE NO ACTION
	ON DELETE NO ACTION 	

) ENGINE=InnoDB DEFAULT CHARSET='utf8';

CREATE TABLE Cuenta_FormaPagoC (

	Num_Cuenta varchar(20) NOT NULL COMMENT 'Clave Primaria (P.K.). Clave Foranea (F.K.). Viene de la relacion Cuenta.',
	Fecha_Pago timestamp NOT NULL COMMENT 'Clave primaria (P.K.). Clave Foranea (F.K.). Viene de la relacion FormaPagoC.',

	CONSTRAINT P_KEY PRIMARY KEY (Num_Cuenta, Fecha_Pago),

	FOREIGN KEY (Num_Cuenta) REFERENCES Cuenta (Numero)
	ON UPDATE RESTRICT
	ON DELETE CASCADE,

	FOREIGN KEY (Fecha_Pago) REFERENCES FormaPagoC (Fecha)
	ON UPDATE RESTRICT
	ON DELETE CASCADE

) ENGINE=InnoDB DEFAULT CHARSET='utf8';

CREATE TABLE Orden_FacturaVenta (

	Num_Orden varchar(5) NOT NULL COMMENT 'Clave Primaria (P.K.). Clave Foranea (F.K.). Viene de la relacion Orden.',
	Num_Factura varchar(5) NOT NULL COMMENT 'Clave Primaria (P.K.). Clave Foranea (F.K.). Viene de la relacion FacturaVenta.',

	CONSTRAINT P_KEY PRIMARY KEY (Num_Orden, Num_Factura),

	FOREIGN KEY (Num_Orden) REFERENCES Orden (Numero)
	ON UPDATE NO ACTION
	ON DELETE NO ACTION,

	FOREIGN KEY (Num_Factura) REFERENCES FacturaVenta (Numero)
	ON UPDATE NO ACTION
	ON DELETE NO ACTION

) ENGINE=InnoDB DEFAULT CHARSET='utf8';

CREATE TABLE Orden_Equipo (

	Num_Orden varchar(5) NOT NULL COMMENT 'Clave Primaria (P.K.). Clave Foranea (F.K.). Viene de la relacion Orden.',
	Serial_Equipo varchar(20) NOT NULL COMMENT 'Clave Primaria (P.K.). Clave Foranea (F.K.). Viene de la relacion Equipo.',
	Falla_Equipo varchar(200) NOT NULL COMMENT 'Descripcion de la falla que presenta el equipo.',

	CONSTRAINT P_KEY PRIMARY KEY (Num_Orden, Serial_Equipo),

	FOREIGN KEY (Num_Orden) REFERENCES Orden (Numero)
	ON UPDATE NO ACTION
	ON DELETE NO ACTION,

	FOREIGN KEY (Serial_Equipo) REFERENCES Equipo (Serial_Eq)
	ON UPDATE NO ACTION
	ON DELETE NO ACTION

) ENGINE=InnoDB DEFAULT CHARSET='utf8';

CREATE TABLE FacturaVenta_EquipoVenta (

	Num_Factura varchar(5) NOT NULL COMMENT 'Clave Primaria (P.K.). Clave Foranea (F.K.). Viene de la relacion FacturaVenta.',
	Codigo_Equipo varchar(5) NOT NULL COMMENT 'Clave Primaria (P.K.). Clave Foranea (F.K.). Viene de la relacion EquipoVenta.',
	Cantidad int(4) NOT NULL COMMENT 'Cantidad a vender.',
	PVP decimal(9,2) NOT NULL DEFAULT 0 COMMENT 'Precio de venta del equipo.',

	CHECK (Cantidad > 0 AND Cantidad <= 9999),
	CHECK (PVP >= 0),

	CONSTRAINT P_KEY PRIMARY KEY (Num_Factura, Codigo_Equipo),

	FOREIGN KEY (Num_Factura) REFERENCES FacturaVenta (Numero)
	ON UPDATE NO ACTION
	ON DELETE NO ACTION,

	FOREIGN KEY (Codigo_Equipo) REFERENCES EquipoVenta (Codigo)
	ON UPDATE NO ACTION
	ON DELETE NO ACTION

) ENGINE=InnoDB DEFAULT CHARSET='utf8';

CREATE TABLE VentaWeb_EquipoVenta (

	Num_Venta varchar(5) NOT NULL COMMENT 'Clave Primaria (P.K.). Clave Foranea (F.K.). Viene de la relacion VentaWeb.',
	Codigo_Equipo varchar(5) NOT NULL COMMENT 'Clave Primaria (P.K.). Clave Foranea (F.K.). Viene de la relacion EquipoVenta.',
	Cantidad int(4) NOT NULL COMMENT 'Cantidad a vender.',
	Precio decimal(9,2) NOT NULL DEFAULT 0 COMMENT 'Precio de venta del equipo.',

	CHECK (Cantidad > 0 AND Cantidad <= 9999),
	CHECK (Precio >= 0),

	CONSTRAINT P_KEY PRIMARY KEY (Num_Venta, Codigo_Equipo),

	FOREIGN KEY (Num_Venta) REFERENCES VentaWeb (Numero)
	ON UPDATE NO ACTION
	ON DELETE NO ACTION,

	FOREIGN KEY (Codigo_Equipo) REFERENCES EquipoVenta (Codigo)
	ON UPDATE NO ACTION
	ON DELETE NO ACTION

) ENGINE=InnoDB DEFAULT CHARSET='utf8';

CREATE TABLE FacturaCompra_Inventario (

	Num_Fact varchar(5) NOT NULL COMMENT 'Clave Primaria (P.K.). Clave Foranea (F.K.). Viene de la relacion FacturaCompra.',
	Proveed_Fact BigInt(10) NOT NULL COMMENT 'Clave Primaria (P.K.). Clave Foranea (F.K.). Viene de la relacion FacturaCompra.',
	Cod_Inv varchar(5) NOT NULL COMMENT 'Clave Primaria (P.K.). Clave Foranea (F.K.). Viene de la relacion Inventario.',
	Cantidad int(4) NOT NULL COMMENT 'Cantidad a vender.',
	Costo decimal(9,2) NOT NULL DEFAULT 0 COMMENT 'Precio de costo del producto.',

	CHECK (Proveed_Fact > 0 AND Proveed_Fact <= 9999999999),
	CHECK (Cantidad > 0 AND Cantidad <= 9999),
	CHECK (Costo >= 0),

	CONSTRAINT P_KEY PRIMARY KEY (Num_Fact, Proveed_Fact, Cod_Inv),

	FOREIGN KEY (Num_Fact) REFERENCES FacturaCompra (Numero)
	ON UPDATE NO ACTION
	ON DELETE NO ACTION,

	FOREIGN KEY (Proveed_Fact) REFERENCES FacturaCompra (RIF_Proveed)
	ON UPDATE NO ACTION
	ON DELETE NO ACTION,

	FOREIGN KEY (Cod_Inv) REFERENCES Inventario (Codigo)
	ON DELETE NO ACTION

) ENGINE=InnoDB DEFAULT CHARSET='utf8';