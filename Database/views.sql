-- drop all views
DROP VIEW IF EXISTS Reporte_Diario_Equipos;
DROP VIEW IF EXISTS Ventas_Web_Diarias;
DROP VIEW IF EXISTS Equipos_Tecnicos_Diarios;
DROP VIEW IF EXISTS Pagos_Semanal;
DROP VIEW IF EXISTS Ingresos_Obtenidos;
DROP VIEW IF EXISTS Ventas_Realizadas;
DROP VIEW IF EXISTS Repuestos_Utilizados;
DROP VIEW IF EXISTS Existencia_Repuestos;
DROP VIEW IF EXISTS Estado_Venta_Web_Diaria;

-- create all views
CREATE VIEW Reporte_Diario_Equipos AS
SELECT Equipo.Nombre AS nombre_equipo, Equipo.Serial_Eq AS serial_equipo, Orden.Estado AS estado_orden 
FROM ((Orden INNER JOIN Orden_Equipo ON Orden.Numero = Orden_Equipo.Num_Orden) 
      INNER JOIN Equipo ON Orden_Equipo.Serial_Equipo = Equipo.Serial_Eq) 
WHERE (DATE(Orden.F_inicio) = CURRENT_DATE)
ORDER BY Orden.Estado, Orden.F_inicio DESC;

CREATE VIEW Ventas_Web_Diarias AS
SELECT VentaWeb.Numero AS num_venta, VentaWeb.Concepto AS concepto_venta, VentaWeb.Estado
AS estado_venta_web, VentaWeb.Num_Factura AS num_factura_venta, VentaWeb.Identif_Cliente 
AS identif_cliente, Cliente.Tipo_Identif AS tipo_identif_cliente, Cliente.Nombre AS nombre_cliente,
FacturaVenta.Fecha AS fecha_venta, FacturaVenta_EquipoVenta.Cantidad AS cantidad_equipo_venta,
FacturaVenta_EquipoVenta.PVP AS pvp_equipo_venta FROM 
(((VentaWeb INNER JOIN FacturaVenta ON VentaWeb.Num_Factura = FacturaVenta.Numero)
		   INNER JOIN Cliente ON VentaWeb.Identif_Cliente = Cliente.Identif)
		   INNER JOIN FacturaVenta_EquipoVenta ON FacturaVenta.Numero = FacturaVenta_EquipoVenta.Num_Factura)
WHERE (DATE(FacturaVenta.Fecha) = CURRENT_DATE)
ORDER BY FacturaVenta.Fecha, VentaWeb.Estado DESC;

CREATE VIEW Equipos_Tecnicos_Diarios AS 
SELECT Personal.Nombre AS tecnico, Personal.CI AS cedula_personal, Equipo.Serial_Eq AS serial_equipo,
Equipo.Nombre AS nombre_equipo, Reparacion.Estado AS estado_reparacion, Orden.Estado AS estado_orden 
FROM ((((Orden INNER JOIN Orden_Equipo ON Orden.Numero = Orden_Equipo.Num_Orden) 
               INNER JOIN Equipo ON Orden_Equipo.Serial_Equipo = Equipo.Serial_Eq) 
               INNER JOIN Reparacion ON Orden.Numero = Reparacion.Numero_Orden)  
               INNER JOIN Personal ON Personal.CI = Reparacion.CI_Personal) 
WHERE (DATE(Orden.F_inicio) = CURRENT_DATE)  
ORDER BY Personal.CI, Orden.F_inicio DESC;

CREATE VIEW Pagos_Semanal AS 
SELECT Cliente.Identif AS identif_cliente, Cliente.Nombre AS nombre_cliente, Cliente.Direccion
AS direccion_cliente, Cliente.Telefono AS telefono_cliente, Cliente.Email AS email_cliente,
CuentaCobrar.Monto AS monto_cuenta_cobrar, FormaPagoV.Descripcion AS descripcion_forma_pago_v
FROM (((Cliente INNER JOIN CuentaCobrar ON Cliente.Identif = CuentaCobrar.Identif_Cliente) 
                INNER JOIN CuentaCobrar_FormaPagoV ON CuentaCobrar.Numero = CuentaCobrar_FormaPagoV.Num_Cuenta) 
                INNER JOIN FormaPagoV ON CuentaCobrar_FormaPagoV.Fecha_Pago = FormaPagoV.Fecha)
WHERE (DATE(FormaPagoV.Fecha) >= DATE_SUB(CURRENT_DATE, INTERVAL 6 DAY) AND DATE(FormaPagoV.Fecha) <= (CURRENT_DATE))
ORDER BY FormaPagoV.Fecha DESC;

CREATE VIEW Ingresos_Obtenidos AS
SELECT Categoria.ID AS id_categoria, Categoria.Nombre AS nombre_categoria, SUM(Orden.Total) AS monto_total FROM
(((Equipo INNER JOIN Orden_Equipo ON Equipo.Serial_Eq = Orden_Equipo.Serial_Equipo)
		 INNER JOIN Orden ON Orden_Equipo.Num_Orden = Orden.Numero)
     INNER JOIN Categoria ON Equipo.ID_Categoria = Categoria.ID)
WHERE (Orden.Estado = 'REP' AND DATE(Orden.F_fin) >= DATE_SUB(CURRENT_DATE, INTERVAL 6 DAY) AND DATE(Orden.F_fin) <= (CURRENT_DATE))
GROUP BY Equipo.ID_Categoria
ORDER BY Categoria.ID, Orden.F_fin DESC;

CREATE VIEW Ventas_Realizadas AS
SELECT Cliente.Identif AS identif_cliente, Cliente.Nombre AS nombre_cliente, Cliente.Direccion
AS direccion_cliente, Cliente.Telefono AS telefono_cliente, Cliente.Email AS email_cliente,
EquipoVenta.Nombre AS articulo, FacturaVenta.Monto AS monto, FacturaVenta_EquipoVenta.Cantidad AS cantidad,
FacturaVenta.Fecha AS fecha FROM
(((Cliente INNER JOIN FacturaVenta ON Cliente.Identif = FacturaVenta.Identif_Cliente)
	     INNER JOIN FacturaVenta_EquipoVenta ON FacturaVenta.Numero = FacturaVenta_EquipoVenta.Num_Factura)
	     INNER JOIN EquipoVenta ON EquipoVenta.Codigo = FacturaVenta_EquipoVenta.Codigo_Equipo)
WHERE (DATE(FacturaVenta.Fecha) >= DATE_SUB(CURRENT_DATE, INTERVAL 6 DAY) AND DATE(FacturaVenta.Fecha) <= (CURRENT_DATE))
ORDER BY FacturaVenta.Fecha DESC;

CREATE VIEW Repuestos_Utilizados AS
SELECT Repuesto.Nombre AS nombre_repuesto, Repuesto.Precio AS precio_repuesto_actual,
Repuesto_Reparacion.Cant_Rep AS cantidad_usada, Repuesto_Reparacion.Precio_Rep AS precio_repuesto_uso,
Orden.Numero AS numero_orden, Orden.F_inicio AS fecha_inicio_orden,
Orden.F_fin AS fecha_fin_orden, Orden.Estado AS estado_orden, Orden.Total AS total_orden FROM
(((Repuesto INNER JOIN Repuesto_Reparacion ON Repuesto.Codigo = Repuesto_Reparacion.Codigo_Repues)
          INNER JOIN Reparacion ON Repuesto_Reparacion.F_asig_Rep = Reparacion.F_asig AND
          Repuesto_Reparacion.CI_Pers_Rep = Reparacion.CI_Personal AND
          Repuesto_Reparacion.Num_Ord_Rep = Reparacion.Numero_Orden)
          INNER JOIN Orden ON Reparacion.Numero_Orden = Orden.Numero)
WHERE (DATE(Reparacion.F_asig) >= DATE_SUB(CURRENT_DATE, INTERVAL 6 DAY) AND DATE(Reparacion.F_asig) <= (CURRENT_DATE))
ORDER BY Reparacion.F_asig DESC;

CREATE VIEW Existencia_Repuestos AS 
SELECT Codigo, Nombre, Cantidad FROM Repuesto WHERE (Cantidad > 0)
ORDER BY Nombre;

CREATE VIEW Estado_Venta_Web_Diaria AS
SELECT VentaWeb.Numero, VentaWeb.Concepto, VentaWeb.Estado FROM
VentaWeb INNER JOIN FacturaVenta ON VentaWeb.Num_Factura = FacturaVenta.Numero
WHERE (DATE(FacturaVenta.Numero) >= DATE_SUB(CURRENT_DATE, INTERVAL 6 DAY) AND DATE(FacturaVenta.Numero) <= (CURRENT_DATE))
ORDER BY VentaWeb.Estado;