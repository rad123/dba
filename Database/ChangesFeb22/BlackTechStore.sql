USE mysql;

DROP DATABASE IF EXISTS BlacTechStore;

CREATE DATABASE IF NOT EXISTS BlackTechStore COLLATE='utf8_bin';

USE BlackTechStore;

-- Drop all tables
DROP TABLE IF EXISTS FacturaCompra_Inventario;
DROP TABLE IF EXISTS VentaWeb_EquipoVenta;
DROP TABLE IF EXISTS FacturaVenta_EquipoVenta;
DROP TABLE IF EXISTS Orden_Equipo;
DROP TABLE IF EXISTS Orden_FacturaVenta;
DROP TABLE IF EXISTS Cuenta_FormaPagoC;
DROP TABLE IF EXISTS FacturaCompra_FormaPagoC;
DROP TABLE IF EXISTS CuentaPagar_FormaPagoC;
DROP TABLE IF EXISTS FormaPagoC;
DROP TABLE IF EXISTS CuentaPagar;
DROP TABLE IF EXISTS Cuenta_FormaPagoV;
DROP TABLE IF EXISTS FacturaVenta_FormaPagoV;
DROP TABLE IF EXISTS CuentaCobrar_FormaPagoV;
DROP TABLE IF EXISTS FormaPagoV;
DROP TABLE IF EXISTS CuentaCobrar;
DROP TABLE IF EXISTS Repuesto_Reparacion;
DROP TABLE IF EXISTS Reparacion;
DROP TABLE IF EXISTS VentaWeb;
DROP TABLE IF EXISTS FacturaVenta;
DROP TABLE IF EXISTS Orden;
DROP TABLE IF EXISTS Equipo;
DROP TABLE IF EXISTS Notificacion;
DROP TABLE IF EXISTS Cliente;
DROP TABLE IF EXISTS FacturaCompra;
DROP TABLE IF EXISTS Proveedor;
DROP TABLE IF EXISTS EquipoVenta;
DROP TABLE IF EXISTS Herramienta;
DROP TABLE IF EXISTS Repuesto;
DROP TABLE IF EXISTS Inventario;
DROP TABLE IF EXISTS Personal;
DROP TABLE IF EXISTS Categoria;
DROP TABLE IF EXISTS Modelo;
DROP TABLE IF EXISTS Marca;
DROP TABLE IF EXISTS Cuenta;
DROP TABLE IF EXISTS Tienda;

-- Drop all functions
DROP FUNCTION IF EXISTS IsNumber;
DROP FUNCTION IF EXISTS IsEmail;

-- Drop all triggers
DROP TRIGGER IF EXISTS b_insert_tienda;
DROP TRIGGER IF EXISTS b_update_tienda;
DROP TRIGGER IF EXISTS b_insert_cuenta;
DROP TRIGGER IF EXISTS b_update_cuenta;
DROP TRIGGER IF EXISTS b_insert_personal;
DROP TRIGGER IF EXISTS b_update_personal;
DROP TRIGGER IF EXISTS b_insert_proveedor;
DROP TRIGGER IF EXISTS b_update_proveedor;
DROP TRIGGER IF EXISTS b_insert_factura_compra;
DROP TRIGGER IF EXISTS b_update_factura_compra;
DROP TRIGGER IF EXISTS b_insert_cliente;
DROP TRIGGER IF EXISTS b_update_cliente;
DROP TRIGGER IF EXISTS b_insert_notificacion;
DROP TRIGGER IF EXISTS b_update_notificacion;
DROP TRIGGER IF EXISTS b_insert_equipo;
DROP TRIGGER IF EXISTS b_update_equipo;
DROP TRIGGER IF EXISTS b_insert_orden;
DROP TRIGGER IF EXISTS b_update_orden;
DROP TRIGGER IF EXISTS b_insert_factura_venta;
DROP TRIGGER IF EXISTS b_update_factura_venta;
DROP TRIGGER IF EXISTS b_insert_venta_web;
DROP TRIGGER IF EXISTS b_update_venta_web;
DROP TRIGGER IF EXISTS b_insert_reparacion;
DROP TRIGGER IF EXISTS b_update_reparacion;
DROP TRIGGER IF EXISTS b_insert_repuesto_reparacion;
DROP TRIGGER IF EXISTS b_update_repuesto_reparacion;
DROP TRIGGER IF EXISTS b_insert_cuenta_cobrar;
DROP TRIGGER IF EXISTS b_update_cuenta_cobrar;
DROP TRIGGER IF EXISTS b_insert_cuenta_cobrar_forma_pago_v;
DROP TRIGGER IF EXISTS b_update_cuenta_cobrar_forma_pago_v;
DROP TRIGGER IF EXISTS b_insert_factura_venta_forma_pago_v;
DROP TRIGGER IF EXISTS b_update_factura_venta_forma_pago_v;
DROP TRIGGER IF EXISTS b_insert_cuenta_forma_pago_v;
DROP TRIGGER IF EXISTS b_update_cuenta_forma_pago_v;
DROP TRIGGER IF EXISTS b_insert_cuenta_pagar;
DROP TRIGGER IF EXISTS b_update_cuenta_pagar;
DROP TRIGGER IF EXISTS b_insert_cuenta_pagar_forma_pago_c;
DROP TRIGGER IF EXISTS b_update_cuenta_pagar_forma_pago_c;
DROP TRIGGER IF EXISTS b_insert_factura_compra_forma_pago_c;
DROP TRIGGER IF EXISTS b_update_factura_compra_forma_pago_c;
DROP TRIGGER IF EXISTS b_insert_cuenta_forma_pago_c;
DROP TRIGGER IF EXISTS b_update_cuenta_forma_pago_c;
DROP TRIGGER IF EXISTS b_insert_orden_factura_venta;
DROP TRIGGER IF EXISTS b_update_orden_factura_venta;
DROP TRIGGER IF EXISTS b_insert_orden_equipo;
DROP TRIGGER IF EXISTS b_update_orden_equipo;
DROP TRIGGER IF EXISTS b_insert_factura_venta_equipo_venta;
DROP TRIGGER IF EXISTS b_update_factura_venta_equipo_venta;
DROP TRIGGER IF EXISTS b_insert_venta_web_equipo_venta;
DROP TRIGGER IF EXISTS b_update_venta_web_equipo_venta;
DROP TRIGGER IF EXISTS b_insert_factura_compra_inventario;

-- Drop all views
DROP VIEW IF EXISTS Reporte_Diario_Equipos;
DROP VIEW IF EXISTS Equipos_Tecnicos_Diarios;
DROP VIEW IF EXISTS Pagos_Semanal;

-- Create all tables
CREATE TABLE Tienda (

	RIF BigInt(10) NOT NULL COMMENT 'Clave primaria (P.K.). RIF de la tienda.',
	Tipo_RIF enum('J', 'G') NOT NULL COMMENT 'J � Empresa privada, G Empresa del Gobierno.',
	Direccion varchar(50) NOT NULL COMMENT 'Direcci�n o ubicaci�n de la tienda.',
	Nombre varchar(50) UNIQUE NOT NULL COMMENT 'Nombre que identifica la tienda (�nico).',
	Email varchar(100) UNIQUE NOT NULL COMMENT 'Email de la tienda (�nico).',
	Telefono varchar(20) NOT NULL COMMENT 'N�mero telef�nico de contacto.',

	CHECK (RIF > 0 AND RIF <= 9999999999),

	CONSTRAINT RIF_P_KEY PRIMARY KEY (RIF) 

) ENGINE=InnoDB DEFAULT CHARSET='utf8';

CREATE TABLE Cuenta (

	Numero varchar(20) NOT NULL COMMENT 'Clave Primaria (P.K.). Identifica la cuenta.',
	RIF_tienda BigInt(10) NOT NULL COMMENT 'Clave For�nea (F.K.). Viene de la relaci�n Tienda.',
	Tipo enum('Ahorro', 'Corriente') NOT NULL COMMENT 'Representa el tipo de cuenta.',
	Banco varchar(20) NOT NULL COMMENT 'Representa el nombre del banco al que corresponde la cuenta.',

	CHECK (RIF_tienda > 0 AND RIF_tienda <= 9999999999),

	CONSTRAINT NUMERO_P_KEY PRIMARY KEY (Numero),

	FOREIGN KEY (RIF_tienda) REFERENCES Tienda (RIF)
	ON DELETE CASCADE

) ENGINE=InnoDB DEFAULT CHARSET='utf8';

CREATE TABLE Marca (

	ID int NOT NULL AUTO_INCREMENT COMMENT 'Clave Primaria (P.K.).',
	Nombre varchar(50) NOT NULL COMMENT 'Nombre de la marca.',

	CONSTRAINT ID_P_KEY PRIMARY KEY (ID)

) ENGINE=InnoDB DEFAULT CHARSET='utf8';

CREATE TABLE Modelo (

	ID int NOT NULL AUTO_INCREMENT COMMENT 'Clave Primaria (P.K.).',
	ID_Marca int NOT NULL COMMENT 'Clave For�nea (F.K.). Viene de la relaci�n Marca.',
	Nombre varchar(50) NOT NULL COMMENT 'Nombre del modelo.',

	CONSTRAINT ID_P_KEY PRIMARY KEY (ID),

	FOREIGN KEY (ID_Marca) REFERENCES Marca (ID)
	ON DELETE CASCADE

) ENGINE=InnoDB DEFAULT CHARSET='utf8';

CREATE TABLE Categoria (

	ID int NOT NULL AUTO_INCREMENT COMMENT 'Clave Primaria (P.K.).',
	ID_Modelo int NOT NULL COMMENT 'Clave For�nea (F.K.). Viene de la relaci�n Modelo.',
	Nombre varchar(50) NOT NULL COMMENT 'Nombre de la categor�a.',

	CONSTRAINT ID_P_KEY PRIMARY KEY (ID),

	FOREIGN KEY (ID_Modelo) REFERENCES Modelo (ID)
	ON DELETE CASCADE

) ENGINE=InnoDB DEFAULT CHARSET='utf8';

CREATE TABLE Personal (

	CI int(9) NOT NULL COMMENT 'Clave primaria (P.K.). Identificaci�n del personal.',
	RIF BigInt(10) UNIQUE NOT NULL COMMENT 'RIF que lo identifica.',
	Tipo_RIF enum('J', 'G') NOT NULL COMMENT 'J � Empresa privada, G Empresa del Gobierno.',
	Direccion varchar(50) NOT NULL COMMENT 'Direcci�n de vivienda.',
	Nombre varchar(50) NOT NULL COMMENT 'Nombre completo.',
	Email varchar(100) UNIQUE NOT NULL COMMENT 'Email del personal (�nico).', 
	Telefono varchar(20) NOT NULL COMMENT 'N�mero de tel�fono.',
	Especialidad varchar(50) NULL COMMENT 'Especialidad del personal (hardware, software, reparaciones de micas, etc.), en caso de ser un t�cnico.',
	Tipo enum('GR', 'TE', 'AS') NOT NULL COMMENT 'GR � Gerente, TC � T�cnico, AS � Asistente Administrativo.',

	CONSTRAINT CI_VALUE CHECK (CI > 0 AND CI <= 999999999),
	CONSTRAINT RIF_VALUE CHECK (RIF > 0 AND RIF <= 9999999999),

	CONSTRAINT CI_P_KEY PRIMARY KEY (CI)

) ENGINE=InnoDB DEFAULT CHARSET='utf8';

CREATE TABLE Inventario (

	Codigo varchar(5) NOT NULL COMMENT 'Clave Primaria (P.K.). Identifica el inventario.',
	F_Registro timestamp NOT NULL COMMENT 'Fecha y hora en que se registra el inventario.',
	Cantidad int(4) NOT NULL COMMENT 'Cantidad en inventario.',

	CHECK (Cantidad > 0 AND Cantidad <= 9999),

	CONSTRAINT CODIGO_P_KEY PRIMARY KEY (Codigo)

) ENGINE=InnoDB DEFAULT CHARSET='utf8';

CREATE TABLE Repuesto (

	Codigo varchar(5) NOT NULL COMMENT 'Clave Primaria (P.K.). Identifica el repuesto.',
	Nombre varchar(50) NOT NULL COMMENT 'Nombre del repuesto.',
	Descripcion varchar(200) NULL COMMENT 'Descripci�n o detalle del repuesto.',
	Cantidad int(4) NOT NULL COMMENT 'Cantidad en existencia.',
	Precio decimal(9,2) NOT NULL DEFAULT 0 COMMENT 'Precio del repuesto.',

	CHECK (Cantidad > 0 AND Cantidad <= 9999),
	CHECK (Precio >= 0),

	CONSTRAINT CODIGO_P_KEY PRIMARY KEY (Codigo)

) ENGINE=InnoDB DEFAULT CHARSET='utf8';

ALTER TABLE `Repuesto` ADD INDEX `NombreRepuesto` (`Nombre`);

CREATE TABLE Herramienta (

	Codigo varchar(5) NOT NULL COMMENT 'Clave Primaria (P.K.). Identifica la herramienta.',
	CI_Personal int(9) NOT NULL COMMENT 'Clave For�nea (F.K.). Viene de la relaci�n Personal.',
	Nombre varchar(50) NULL COMMENT 'Nombre de la herramienta.',
	Descripcion varchar(200) NULL COMMENT 'Descripci�n o detalle de la herramienta.',
	Cantidad int(4) NOT NULL COMMENT 'Cantidad en existencia.',
	Precio decimal(9,2) NOT NULL DEFAULT 0 COMMENT 'Precio de la herramienta.',

	CHECK (CI_Personal > 0 AND CI_Personal <= 999999999),
	CHECK (Cantidad > 0 AND Cantidad <= 9999),
	CHECK (Precio >= 0),

	CONSTRAINT CODIGO_P_KEY PRIMARY KEY (Codigo),

	FOREIGN KEY (CI_Personal) REFERENCES Personal (CI)
	ON DELETE CASCADE

) ENGINE=InnoDB DEFAULT CHARSET='utf8';

CREATE TABLE EquipoVenta (

	Codigo varchar(5) NOT NULL COMMENT 'Clave Primaria (P.K.). Identifica el equipo.',
	ID_Categoria int NOT NULL COMMENT 'Clave For�nea (F.K.). Viene de la relaci�n Categoria.',
	Nombre varchar(50) NULL COMMENT 'Nombre del equipo.',
	Descripcion varchar(200) NULL COMMENT 'Descripci�n o detalle del equipo.',
	Cantidad int(4) NOT NULL COMMENT 'Cantidad en existencia.',
	Precio decimal(9,2) NOT NULL DEFAULT 0 COMMENT 'Precio del equipo.',

	CHECK (Cantidad > 0 AND Cantidad <= 9999),
	CHECK (Precio >= 0),

	CONSTRAINT CODIGO_P_KEY PRIMARY KEY (Codigo),

	FOREIGN KEY (ID_Categoria) REFERENCES Categoria (ID)
	ON DELETE CASCADE	

) ENGINE=InnoDB DEFAULT CHARSET='utf8';

CREATE TABLE Proveedor (

	RIF BigInt(10) NOT NULL COMMENT 'RIF que lo identifica.',
	Tipo_RIF enum('J', 'G') NOT NULL COMMENT 'J � Empresa privada, G Empresa del Gobierno.',
	Direccion varchar(50) NOT NULL COMMENT 'Direcci�n del proveedor.',
	Nombre varchar(50) NOT NULL COMMENT 'Nombre del proveedor.',
	Email varchar(100) NULL COMMENT 'Email del proveedor.', 
	Telefono varchar(20) NOT NULL COMMENT 'N�mero de tel�fono.',

	CHECK (RIF > 0 AND RIF <= 9999999999),

	CONSTRAINT RIF_P_KEY PRIMARY KEY (RIF)

) ENGINE=InnoDB DEFAULT CHARSET='utf8';

ALTER TABLE `Proveedor` ADD INDEX `NombreProveedor` (`Nombre`);

CREATE TABLE FacturaCompra (

	Numero varchar(5) NOT NULL COMMENT 'Clave Primaria (P.K.). Identifica la factura.',
	RIF_Proveed BigInt(10) NOT NULL COMMENT 'Clave Primaria (P.K.). Clave For�nea (F.K.). Viene de la relaci�n Proveedor.',
	Fecha timestamp NOT NULL COMMENT 'Fecha en que se gener� la factura.',
	Estado varchar(20) NOT NULL COMMENT 'Estado de la factura.',
	Descripcion varchar(200) NULL COMMENT 'Descripci�n o detalle de la factura.',
	Monto decimal(9,2) NOT NULL DEFAULT 0 COMMENT 'Monto total de la factura.',

	CHECK (RIF_Proveed > 0 AND RIF_Proveed <= 9999999999),
	CHECK (Monto >= 0),

	CONSTRAINT P_KEY PRIMARY KEY (Numero, RIF_Proveed),

	FOREIGN KEY (RIF_Proveed) REFERENCES Proveedor (RIF)
	ON DELETE CASCADE

) ENGINE=InnoDB DEFAULT CHARSET='utf8';

ALTER TABLE `facturacompra` ADD INDEX `Fechacompra` (`Fecha`);

CREATE TABLE Cliente (

	Identif varchar(15) NOT NULL COMMENT 'Clave Primaria (P.K.). Identificador del cliente.',
	Tipo_Identif enum('V', 'E', 'J', 'G') NOT NULL COMMENT 'V � Venezolano (c�dula), E � Extranjero (Pasaporte), J � Empresa privada, G - Empresa del Gobierno.',
	Direccion varchar(50) NOT NULL COMMENT 'Direcci�n del cliente.',
	Nombre varchar(50) NOT NULL COMMENT 'Nombre completo del cliente.',
	Email varchar(100) NULL COMMENT 'Email del proveedor.', 
	Telefono varchar(20) NOT NULL COMMENT 'N�mero de tel�fono.',

	CONSTRAINT IDENTIF_P_KEY PRIMARY KEY (Identif)

) ENGINE=InnoDB DEFAULT CHARSET='utf8';

CREATE TABLE Notificacion (

	F_Hora timestamp NOT NULL COMMENT 'Clave Primaria (P.K.). Fecha y hora en que el cliente recibi� la notificaci�n.',
	Identif_Cliente varchar(15) NOT NULL COMMENT 'Clave Primaria (P.K.). Clave For�nea (F.K.). Viene de la relaci�n Cliente.',
	Via enum('email, sms') NOT NULL COMMENT 'V�a por la cual se envi� la notificaci�n.',
	Texto varchar(255) NOT NULL COMMENT 'Contenido del mensaje enviado.',

	CONSTRAINT P_KEY PRIMARY KEY (F_Hora, Identif_Cliente),

	FOREIGN KEY (Identif_Cliente) REFERENCES Cliente (Identif)

) ENGINE=InnoDB DEFAULT CHARSET='utf8';

CREATE TABLE Equipo (

	Serial_Eq varchar(20) NOT NULL COMMENT 'Clave Primaria (P.K.). Serial que identifica el equipo.',
	ID_Categoria int NOT NULL COMMENT 'Clave For�nea (F.K.). Viene de la relaci�n Categoria.',
	Identif_Cliente varchar(15) NOT NULL COMMENT 'Clave For�nea (F.K.). Viene de la relaci�n Cliente.',
	Nombre varchar(50) NOT NULL COMMENT 'Nombre del equipo',
	Descripcion varchar(200) NULL COMMENT 'Descripci�n o detalle del equipo.',
	Precio decimal(9,2) NOT NULL DEFAULT 0 COMMENT 'Precio del equipo.',

	CONSTRAINT SERIAL_EQ_P_KEY PRIMARY KEY (Serial_Eq),

	FOREIGN KEY (ID_Categoria) REFERENCES Categoria (ID)
	ON DELETE CASCADE,

	FOREIGN KEY (Identif_Cliente) REFERENCES Cliente (Identif)
	ON DELETE CASCADE

) ENGINE=InnoDB DEFAULT CHARSET='utf8';

CREATE TABLE Orden (

	Numero varchar(5) NOT NULL COMMENT 'Clave Primaria (P.K.). Identifica la orden.',
	F_inicio timestamp NOT NULL COMMENT 'Fecha en que se inici� la orden.',
	F_fin timestamp NOT NULL COMMENT 'Fecha de culminaci�n de la orden.',
	Estado enum('ESP', 'REV', 'REP', 'DEV') NOT NULL COMMENT 'ESP � En espera, REV � Revisado, REP � Reparado, DEV � Devoluci�n.',
	Falla varchar(200) NOT NULL COMMENT 'Descripci�n de la falla que presenta el equipo.',
	Total decimal(9,2) NOT NULL DEFAULT 0 COMMENT 'Costo total de la orden.',

	CONSTRAINT NUMERO_P_KEY PRIMARY KEY (Numero)

) ENGINE=InnoDB DEFAULT CHARSET='utf8';

ALTER TABLE `orden` ADD INDEX `EstadoOrden` (`Estado`);

CREATE TABLE FacturaVenta (

	Numero varchar(5) NOT NULL COMMENT 'Clave Primaria (P.K.). Identifica la factura.',
	Identif_Cliente varchar(15) NOT NULL COMMENT 'Clave For�nea (F.K.). Viene de la relaci�n Cliente.',
	Fecha timestamp NOT NULL COMMENT 'Fecha en que se gener� la factura.',
	Estado varchar(20) NOT NULL COMMENT 'Estado de la factura.',
	Descripcion varchar(200) NULL COMMENT 'Descripci�n o detalle de la factura.',
	Monto decimal(9,2) NOT NULL DEFAULT 0 COMMENT 'Monto total de la factura.',

	CHECK (Monto >= 0),

	CONSTRAINT NUMERO_P_KEY PRIMARY KEY (Numero),

	FOREIGN KEY (Identif_Cliente) REFERENCES Cliente (Identif)
	ON DELETE CASCADE

) ENGINE=InnoDB DEFAULT CHARSET='utf8';

ALTER TABLE `facturaventa` ADD INDEX `FechaVenta` (`Fecha`);

CREATE TABLE VentaWeb (

	Numero varchar(5) NOT NULL COMMENT 'Clave Primaria (P.K.). Identifica la venta web.',
	Identif_Cliente varchar(15) NOT NULL COMMENT 'Clave For�nea (F.K.). Viene de la relaci�n Cliente.',
	Num_Factura varchar(5) NOT NULL COMMENT 'Clave For�nea (F.K). Viene de la relaci�n FacturaVenta.',
	Concepto varchar(200) NOT NULL COMMENT 'Concepto o detalle de la venta.',
	Estado varchar(20) NOT NULL COMMENT 'Estado de la venta web.',

	CONSTRAINT NUMERO_P_KEY PRIMARY KEY (Numero),

	FOREIGN KEY (Identif_Cliente) REFERENCES Cliente (Identif)
	ON DELETE CASCADE,

	FOREIGN KEY (Num_Factura) REFERENCES FacturaVenta (Numero)
	ON DELETE CASCADE


) ENGINE=InnoDB DEFAULT CHARSET='utf8';

ALTER TABLE `VentaWeb` ADD INDEX `StatusVentaWeb` (`Estado`);

CREATE TABLE Reparacion (

	F_asig timestamp NOT NULL COMMENT 'Clave Primaria (P.K.). Fecha de inicio de la reparaci�n.',
	CI_Personal int(9) NOT NULL COMMENT 'Clave Primaria (P.K.). Clave For�nea (F.K.). Viene de la relaci�n Personal.',
	Numero_Orden varchar(5) NOT NULL COMMENT 'Clave Primaria (P.K.). Clave For�nea (F.K.). Viene de la relaci�n Orden.',
	F_rep timestamp NOT NULL COMMENT 'Fecha de culminaci�n de la reparaci�n.',
	Descripcion varchar(200) NULL COMMENT 'Descripci�n o detalle de la reparaci�n.',
	Comision decimal(9,2) NOT NULL DEFAULT 0 COMMENT 'Comisi�n que le corresponde al t�cnico que realiz� la reparaci�n.',
	Total decimal(9,2) NOT NULL DEFAULT 0 COMMENT 'Monto total de la reparaci�n.',
	Estado enum('ESP', 'REV', 'REP', 'DEV') NOT NULL COMMENT 'ESP � En espera, REV � Revisado, REP � Reparado, DEV � Devoluci�n.',

	CHECK (CI_Personal > 0 AND CI_Personal <= 999999999),
	CHECK (Comision >= 0),
	CHECK (Total >= 0),

	CONSTRAINT P_KEY PRIMARY KEY (F_asig, CI_Personal, Numero_Orden),

	FOREIGN KEY (CI_Personal) REFERENCES Personal (CI)
	ON DELETE CASCADE,

	FOREIGN KEY (Numero_Orden) REFERENCES Orden (Numero)
	ON DELETE CASCADE

) ENGINE=InnoDB DEFAULT CHARSET='utf8';

ALTER TABLE `reparacion` ADD INDEX `F_asigReparacion` (`F_asig`, `CI_Personal`);

CREATE TABLE Repuesto_Reparacion (

	Codigo_Repues varchar(5) NOT NULL COMMENT 'Clave Primaria (P.K.). Clave For�nea (F.K.). Viene de la relaci�n Repuesto.',
	F_asig_Rep timestamp NOT NULL COMMENT 'Clave Primaria (P.K.). Clave For�nea (F.K.). Viene de la relaci�n Reparacion.',
	CI_Pers_Rep int(9) NOT NULL COMMENT 'Clave Primaria (P.K.). Clave For�nea (F.K.). Viene de la relaci�n Reparacion.',
	Num_Ord_Rep varchar(5) NOT NULL COMMENT 'Clave Primaria (P.K.). Clave For�nea (F.K.). Viene de la relaci�n Reparacion.',

	CHECK (CI_Pers_Rep > 0 AND CI_Pers_Rep <= 999999999),

	CONSTRAINT P_KEY PRIMARY KEY (Codigo_Repues, F_asig_Rep, CI_Pers_Rep, Num_Ord_Rep),

	FOREIGN KEY (Codigo_Repues) REFERENCES Repuesto (Codigo)
	ON DELETE CASCADE,

	FOREIGN KEY (F_asig_Rep, CI_Pers_Rep, Num_Ord_Rep) REFERENCES Reparacion (F_asig, CI_Personal, Numero_Orden)
	ON DELETE CASCADE

) ENGINE=InnoDB DEFAULT CHARSET='utf8';

CREATE TABLE CuentaCobrar (

	Numero varchar(5) NOT NULL COMMENT 'Clave Primaria (P.K.). Identifica la cuenta.',
	Identif_Cliente varchar(15) NOT NULL COMMENT 'Clave Primaria (P.K.). Clave For�nea (F.K.). Viene de la relaci�n Cliente.',
	Num_Factura varchar(5) NOT NULL COMMENT 'Clave For�nea (F.K.). Viene de la relaci�n FacturaVenta.',
	F_aper timestamp NOT NULL COMMENT 'Fecha de apertura de la cuenta.',
	F_lim date NOT NULL COMMENT 'Fecha l�mite de la cuenta.',
	Estado varchar(20) NOT NULL COMMENT 'Estado de la cuenta.',
	Descripcion varchar(200) NULL COMMENT 'Descripci�n o detalle de la cuenta.',
	Saldo decimal(9,2) NOT NULL DEFAULT 0 COMMENT 'Saldo actual de la cuenta.',
	Monto decimal(9,2) NOT NULL DEFAULT 0 COMMENT 'Monto total de la cuenta.',

	CONSTRAINT P_KEY PRIMARY KEY (Numero, Identif_Cliente),

	FOREIGN KEY (Identif_Cliente) REFERENCES Cliente (Identif)
	ON DELETE CASCADE,

	FOREIGN KEY (Num_Factura) REFERENCES FacturaVenta (Numero)
	ON DELETE CASCADE

) ENGINE=InnoDB DEFAULT CHARSET='utf8';

CREATE TABLE FormaPagoV (

	Fecha timestamp NOT NULL COMMENT 'Clave primaria (P.K.). Fecha y hora en que se efect�a el pago.',
	Banco varchar(20) NULL COMMENT 'Nombre del banco.',
	Nref int(9) NULL COMMENT 'N�mero de referencia.',
	Descripcion varchar(200) NULL COMMENT 'Descripci�n o detalle del pago.',
	Tipo varchar(20) NOT NULL COMMENT 'Modo de pago (transferencia, dep�sito, cheque, etc.).',

	CHECK (Nref > 0 AND Nref <= 999999999),

	CONSTRAINT FECHA_P_KEY PRIMARY KEY (Fecha)

) ENGINE=InnoDB DEFAULT CHARSET='utf8';

CREATE TABLE CuentaCobrar_FormaPagoV (

	Num_Cuenta varchar(5) NOT NULL COMMENT 'Clave Primaria (P.K.). Clave For�nea (F.K.). Viene de la relaci�n CuentaCobrar.',
	Cliente_Cuenta varchar(15) NOT NULL COMMENT 'Clave Primaria (P.K.). Clave For�nea (F.K.). Viene de la relaci�n CuentaCobrar.',
	Fecha_Pago timestamp NOT NULL COMMENT 'Clave primaria (P.K.). Clave For�nea (F.K.). Viene de la relaci�n FormaPagoV.',

	CONSTRAINT P_KEY PRIMARY KEY (Num_Cuenta, Cliente_Cuenta, Fecha_Pago),

	FOREIGN KEY (Num_Cuenta) REFERENCES CuentaCobrar (Numero)
	ON DELETE CASCADE,

	FOREIGN KEY (Cliente_Cuenta) REFERENCES CuentaCobrar (Identif_Cliente)
	ON DELETE CASCADE,

	FOREIGN KEY (Fecha_Pago) REFERENCES FormaPagoV (Fecha)
	ON DELETE CASCADE

) ENGINE=InnoDB DEFAULT CHARSET='utf8';

CREATE TABLE FacturaVenta_FormaPagoV (

	Num_Factura varchar(5) NOT NULL COMMENT 'Clave Primaria (P.K.). Clave For�nea (F.K.). Viene de la relaci�n FacturaVenta.',
	Fecha_Pago timestamp NOT NULL COMMENT 'Clave primaria (P.K.). Clave For�nea (F.K.). Viene de la relaci�n FormaPagoV.',

	CONSTRAINT P_KEY PRIMARY KEY (Num_Factura, Fecha_Pago),

	FOREIGN KEY (Num_Factura) REFERENCES FacturaVenta (Numero)
	ON DELETE CASCADE,

	FOREIGN KEY (Fecha_pago) REFERENCES FormaPagoV (Fecha)
	ON DELETE CASCADE 

) ENGINE=InnoDB DEFAULT CHARSET='utf8';

CREATE TABLE Cuenta_FormaPagoV (

	Num_Cuenta varchar(20) NOT NULL COMMENT 'Clave Primaria (P.K.). Clave For�nea (F.K.). Viene de la relaci�n Cuenta.',
	Fecha_Pago timestamp NOT NULL COMMENT 'Clave primaria (P.K.). Clave For�nea (F.K.). Viene de la relaci�n FormaPagoV.',

	CONSTRAINT P_KEY PRIMARY KEY (Num_Cuenta, Fecha_Pago),

	FOREIGN KEY (Num_Cuenta) REFERENCES Cuenta (Numero)
	ON DELETE CASCADE,

	FOREIGN KEY (Fecha_Pago) REFERENCES FormaPagoV (Fecha)
	ON DELETE CASCADE

) ENGINE=InnoDB DEFAULT CHARSET='utf8';

CREATE TABLE CuentaPagar (

	Numero varchar(5) NOT NULL COMMENT 'Clave Primaria (P.K.). Identifica la cuenta.',
	RIF_Proveed BigInt(10) NOT NULL COMMENT 'Clave Primaria (P.K.). Clave For�nea (F.K.). Viene de la relaci�n Proveedor.',
	Num_Factura varchar(5) NOT NULL COMMENT 'Clave For�nea (F.K.). Viene de la relaci�n FacturaCompra.',
	F_aper timestamp NOT NULL COMMENT 'Fecha de apertura de la cuenta.',
	F_lim date NOT NULL COMMENT 'Fecha l�mite de la cuenta.',
	Estado varchar(20) NOT NULL COMMENT 'Estado de la cuenta.',
	Descripcion varchar(200) NULL COMMENT 'Descripci�n o detalle de la cuenta.',
	Saldo decimal(9,2) NOT NULL DEFAULT 0 COMMENT 'Saldo actual de la cuenta.',
	Monto decimal(9,2) NOT NULL DEFAULT 0 COMMENT 'Monto total de la cuenta.',

	CONSTRAINT P_KEY PRIMARY KEY (Numero, RIF_Proveed),

	FOREIGN KEY (RIF_Proveed) REFERENCES Proveedor (RIF)
	ON DELETE CASCADE,

	FOREIGN KEY (Num_Factura) REFERENCES FacturaCompra (Numero)
	ON DELETE CASCADE

) ENGINE=InnoDB DEFAULT CHARSET='utf8';

CREATE TABLE FormaPagoC (

	Fecha timestamp NOT NULL COMMENT 'Clave primaria (P.K.). Fecha y hora en que se efect�a el pago.',
	Banco varchar(20) NULL COMMENT 'Nombre del banco.',
	Nref int(9) NULL COMMENT 'N�mero de referencia.',
	Descripcion varchar(200) NULL COMMENT 'Descripci�n o detalle del pago.',
	Tipo varchar(20) NOT NULL COMMENT 'Modo de pago (transferencia, dep�sito, cheque, etc.).',

	CHECK (Nref > 0 AND Nref <= 999999999),

	CONSTRAINT FECHA_P_KEY PRIMARY KEY (Fecha)

) ENGINE=InnoDB DEFAULT CHARSET='utf8';

CREATE TABLE CuentaPagar_FormaPagoC (

	Num_Cuenta varchar(5) NOT NULL COMMENT 'Clave Primaria (P.K.). Clave For�nea (F.K.). Viene de la relaci�n CuentaPagar.',
	RIF_Proveed BigInt(10) NOT NULL COMMENT 'Clave Primaria (P.K.). Clave For�nea (F.K.). Viene de la relaci�n CuentaPagar.',
	Fecha_Pago timestamp NOT NULL COMMENT 'Clave primaria (P.K.). Clave For�nea (F.K.). Viene de la relaci�n FormaPagoC.',

	CHECK (RIF_Proveed > 0 AND RIF_Proveed <= 9999999999),

	CONSTRAINT P_KEY PRIMARY KEY (Num_Cuenta, RIF_Proveed, Fecha_Pago),

	FOREIGN KEY (Num_Cuenta) REFERENCES CuentaPagar (Numero)
	ON DELETE CASCADE,

	FOREIGN KEY (RIF_Proveed) REFERENCES CuentaPagar (RIF_Proveed)
	ON DELETE CASCADE,

	FOREIGN KEY (Fecha_Pago) REFERENCES FormaPagoC (Fecha)
	ON DELETE CASCADE

) ENGINE=InnoDB DEFAULT CHARSET='utf8';

CREATE TABLE FacturaCompra_FormaPagoC (

	Num_Factura varchar(5) NOT NULL COMMENT 'Clave Primaria (P.K.). Clave For�nea (F.K.). Viene de la relaci�n FacturaCompra.',
	RIF_Proveed BigInt(10) NOT NULL COMMENT 'Clave Primaria (P.K.). Clave For�nea (F.K.). Viene de la relaci�n FacturaCompra.',
	Fecha_Pago timestamp NOT NULL COMMENT 'Clave primaria (P.K.). Clave For�nea (F.K.). Viene de la relaci�n FormaPagoC.',

	CHECK (RIF_Proveed > 0 AND RIF_Proveed <= 9999999999),

	CONSTRAINT P_KEY PRIMARY KEY (Num_Factura, RIF_Proveed, Fecha_Pago),

	FOREIGN KEY (Num_Factura) REFERENCES FacturaCompra (Numero)
	ON DELETE CASCADE,

	FOREIGN KEY (RIF_Proveed) REFERENCES FacturaCompra (RIF_Proveed)
	ON DELETE CASCADE,

	FOREIGN KEY (Fecha_pago) REFERENCES FormaPagoC (Fecha)
	ON DELETE CASCADE 	

) ENGINE=InnoDB DEFAULT CHARSET='utf8';

CREATE TABLE Cuenta_FormaPagoC (

	Num_Cuenta varchar(20) NOT NULL COMMENT 'Clave Primaria (P.K.). Clave For�nea (F.K.). Viene de la relaci�n Cuenta.',
	Fecha_Pago timestamp NOT NULL COMMENT 'Clave primaria (P.K.). Clave For�nea (F.K.). Viene de la relaci�n FormaPagoC.',

	CONSTRAINT P_KEY PRIMARY KEY (Num_Cuenta, Fecha_Pago),

	FOREIGN KEY (Num_Cuenta) REFERENCES Cuenta (Numero)
	ON DELETE CASCADE,

	FOREIGN KEY (Fecha_Pago) REFERENCES FormaPagoC (Fecha)
	ON DELETE CASCADE

) ENGINE=InnoDB DEFAULT CHARSET='utf8';

CREATE TABLE Orden_FacturaVenta (

	Num_Orden varchar(5) NOT NULL COMMENT 'Clave Primaria (P.K.). Clave For�nea (F.K.). Viene de la relaci�n Orden.',
	Num_Factura varchar(5) NOT NULL COMMENT 'Clave Primaria (P.K.). Clave For�nea (F.K.). Viene de la relaci�n FacturaVenta.',

	CONSTRAINT P_KEY PRIMARY KEY (Num_Orden, Num_Factura),

	FOREIGN KEY (Num_Orden) REFERENCES Orden (Numero)
	ON DELETE CASCADE,

	FOREIGN KEY (Num_Factura) REFERENCES FacturaVenta (Numero)
	ON DELETE CASCADE

) ENGINE=InnoDB DEFAULT CHARSET='utf8';

CREATE TABLE Orden_Equipo (

	Num_Orden varchar(5) NOT NULL COMMENT 'Clave Primaria (P.K.). Clave For�nea (F.K.). Viene de la relaci�n Orden.',
	Serial_Equipo varchar(20) NOT NULL COMMENT 'Clave Primaria (P.K.). Clave For�nea (F.K.). Viene de la relaci�n Equipo.',

	CONSTRAINT P_KEY PRIMARY KEY (Num_Orden, Serial_Equipo),

	FOREIGN KEY (Num_Orden) REFERENCES Orden (Numero)
	ON DELETE CASCADE,

	FOREIGN KEY (Serial_Equipo) REFERENCES Equipo (Serial_Eq)
	ON DELETE CASCADE

) ENGINE=InnoDB DEFAULT CHARSET='utf8';

CREATE TABLE FacturaVenta_EquipoVenta (

	Num_Factura varchar(5) NOT NULL COMMENT 'Clave Primaria (P.K.). Clave For�nea (F.K.). Viene de la relaci�n FacturaVenta.',
	Codigo_Equipo varchar(5) NOT NULL COMMENT 'Clave Primaria (P.K.). Clave For�nea (F.K.). Viene de la relaci�n EquipoVenta.',
	Cantidad int(4) NOT NULL COMMENT 'Cantidad a vender.',
	PVP decimal(9,2) NOT NULL DEFAULT 0 COMMENT 'Precio de venta del equipo.',

	CHECK (Cantidad > 0 AND cantidad <= 9999),
	CHECK (PVP >= 0),

	CONSTRAINT P_KEY PRIMARY KEY (Num_Factura, Codigo_Equipo),

	FOREIGN KEY (Num_Factura) REFERENCES FacturaVenta (Numero)
	ON DELETE CASCADE,

	FOREIGN KEY (Codigo_Equipo) REFERENCES EquipoVenta (Codigo)
	ON DELETE CASCADE

) ENGINE=InnoDB DEFAULT CHARSET='utf8';

CREATE TABLE VentaWeb_EquipoVenta (

	Num_Venta varchar(5) NOT NULL COMMENT 'Clave Primaria (P.K.). Clave For�nea (F.K.). Viene de la relaci�n VentaWeb.',
	Codigo_Equipo varchar(5) NOT NULL COMMENT 'Clave Primaria (P.K.). Clave For�nea (F.K.). Viene de la relaci�n EquipoVenta.',
	Cantidad int(4) NOT NULL COMMENT 'Cantidad a vender.',
	Precio decimal(9,2) NOT NULL DEFAULT 0 COMMENT 'Precio de venta del equipo.',

	CHECK (Cantidad > 0 AND cantidad <= 9999),
	CHECK (PVP >= 0),

	CONSTRAINT P_KEY PRIMARY KEY (Num_Venta, Codigo_Equipo),

	FOREIGN KEY (Num_Venta) REFERENCES VentaWeb (Numero)
	ON DELETE CASCADE,

	FOREIGN KEY (Codigo_Equipo) REFERENCES EquipoVenta (Codigo)
	ON DELETE CASCADE

) ENGINE=InnoDB DEFAULT CHARSET='utf8';

CREATE TABLE FacturaCompra_Inventario (

	Num_Fact varchar(5) NOT NULL COMMENT 'Clave Primaria (P.K.). Clave For�nea (F.K.). Viene de la relaci�n FacturaCompra.',
	Proveed_Fact BigInt(10) NOT NULL COMMENT 'Clave Primaria (P.K.). Clave For�nea (F.K.). Viene de la relaci�n FacturaCompra.',
	Cod_Inv varchar(5) NOT NULL COMMENT 'Clave Primaria (P.K.). Clave For�nea (F.K.). Viene de la relaci�n Inventario.',
	Cantidad int(4) NOT NULL COMMENT 'Cantidad a vender.',
	Costo decimal(9,2) NOT NULL DEFAULT 0 COMMENT 'Precio de costo del producto.',

	CHECK (Proveed_Fact > 0 AND Proveed_Fact <= 9999999999),
	CHECK (Cantidad > 0 AND cantidad <= 9999),
	CHECK (Costo >= 0),

	CONSTRAINT P_KEY PRIMARY KEY (Num_Fact, Proveed_Fact, Cod_Inv),

	FOREIGN KEY (Num_Fact) REFERENCES FacturaCompra (Numero)
	ON DELETE CASCADE,

	FOREIGN KEY (Proveed_Fact) REFERENCES FacturaCompra (RIF_Proveed)
	ON DELETE CASCADE,

	FOREIGN KEY (Cod_Inv) REFERENCES Inventario (Codigo)
	ON DELETE CASCADE

) ENGINE=InnoDB DEFAULT CHARSET='utf8';

DELIMITER //

-- create all functions
CREATE FUNCTION IsNumber (value varchar(255)) RETURNS tinyint
RETURN value REGEXP '^[0-9]+$';//

CREATE FUNCTION IsEmail (value varchar(255)) RETURNS tinyint
RETURN value REGEXP '^[a-zA-Z0-9][a-zA-Z0-9._-]*@[a-zA-Z0-9][a-zA-Z0-9._-]*\\.[a-zA-Z]{2,4}$';//

-- create all triggers
CREATE TRIGGER b_insert_tienda BEFORE INSERT ON Tienda
FOR EACH ROW 
BEGIN
	
	IF NOT IsEmail(NEW.Email) THEN
		BEGIN
			signal sqlstate '45000' SET MESSAGE_TEXT = 'Email invalido';
		END;
	END IF;

	IF NOT IsNumber(NEW.Telefono) THEN
		BEGIN
			signal sqlstate '45000' SET MESSAGE_TEXT = 'Telefono invalido';
		END;
	END IF;

END;//

CREATE TRIGGER b_update_tienda BEFORE UPDATE ON Tienda
FOR EACH ROW 
BEGIN
	
	IF NOT IsEmail(NEW.Email) THEN
		BEGIN
			signal sqlstate '45000' SET MESSAGE_TEXT = 'Email invalido';
		END;
	END IF;

	IF NOT IsNumber(NEW.Telefono) THEN
		BEGIN
			signal sqlstate '45000' SET MESSAGE_TEXT = 'Telefono invalido';
		END;
	END IF;

END;//

CREATE TRIGGER b_insert_cuenta BEFORE INSERT ON Cuenta
FOR EACH ROW 
BEGIN

	IF NOT IsNumber(NEW.Numero) THEN
		BEGIN
			signal sqlstate '45000' SET MESSAGE_TEXT = 'Numero de cuenta invalido';
		END;
	END IF;

END;//

CREATE TRIGGER b_update_cuenta BEFORE UPDATE ON Cuenta
FOR EACH ROW 
BEGIN

	IF NOT IsNumber(NEW.Numero) THEN
		BEGIN
			signal sqlstate '45000' SET MESSAGE_TEXT = 'Numero de cuenta invalido';
		END;
	END IF;

END;//

CREATE TRIGGER b_insert_personal BEFORE INSERT ON Personal
FOR EACH ROW
BEGIN

	IF NOT IsEmail(NEW.Email) THEN
		BEGIN
			signal sqlstate '45000' SET MESSAGE_TEXT = 'Email invalido';
		END;
	END IF;

	IF NOT IsNumber(NEW.Telefono) THEN
		BEGIN
			signal sqlstate '45000' SET MESSAGE_TEXT = 'Telefono invalido';
		END;
	END IF;

END;//

CREATE TRIGGER b_update_personal BEFORE UPDATE ON Personal
FOR EACH ROW
BEGIN

	IF NOT IsEmail(NEW.Email) THEN
		BEGIN
			signal sqlstate '45000' SET MESSAGE_TEXT = 'Email invalido';
		END;
	END IF;

	IF NOT IsNumber(NEW.Telefono) THEN
		BEGIN
			signal sqlstate '45000' SET MESSAGE_TEXT = 'Telefono invalido';
		END;
	END IF;

END;//

CREATE TRIGGER b_insert_proveedor BEFORE INSERT ON Proveedor
FOR EACH ROW
BEGIN

	IF NOT IsEmail(NEW.Email) THEN
		BEGIN
			signal sqlstate '45000' SET MESSAGE_TEXT = 'Email invalido';
		END;
	END IF;

	IF NOT IsNumber(NEW.Telefono) THEN
		BEGIN
			signal sqlstate '45000' SET MESSAGE_TEXT = 'Telefono invalido';
		END;
	END IF;

END;//

CREATE TRIGGER b_update_proveedor BEFORE UPDATE ON Proveedor
FOR EACH ROW
BEGIN

	IF NOT IsEmail(NEW.Email) THEN
		BEGIN
			signal sqlstate '45000' SET MESSAGE_TEXT = 'Email invalido';
		END;
	END IF;

	IF NOT IsNumber(NEW.Telefono) THEN
		BEGIN
			signal sqlstate '45000' SET MESSAGE_TEXT = 'Telefono invalido';
		END;
	END IF;

END;//

CREATE TRIGGER b_insert_factura_compra BEFORE INSERT ON FacturaCompra
FOR EACH ROW
BEGIN

	IF NOT IsNumber(NEW.Numero) THEN
		BEGIN
			signal sqlstate '45000' SET MESSAGE_TEXT = 'Numero de factura invalido';
		END;
	END IF;

END;//

CREATE TRIGGER b_update_factura_compra BEFORE UPDATE ON FacturaCompra
FOR EACH ROW
BEGIN

	IF NOT IsNumber(NEW.Numero) THEN
		BEGIN
			signal sqlstate '45000' SET MESSAGE_TEXT = 'Numero de factura invalido';
		END;
	END IF;

END;//

CREATE TRIGGER b_insert_cliente BEFORE INSERT ON Cliente
FOR EACH ROW
BEGIN

	IF NOT IsNumber(NEW.Identif) THEN
		BEGIN
			signal sqlstate '45000' SET MESSAGE_TEXT = 'Cedula/RIF/Pasaporte invalido';
		END;
	END IF;

	IF NOT IsEmail(NEW.Email) THEN
		BEGIN
			signal sqlstate '45000' SET MESSAGE_TEXT = 'Email invalido';
		END;
	END IF;

	IF NOT IsNumber(NEW.Telefono) THEN
		BEGIN
			signal sqlstate '45000' SET MESSAGE_TEXT = 'Telefono invalido';
		END;
	END IF;

END;//

CREATE TRIGGER b_update_cliente BEFORE UPDATE ON Cliente
FOR EACH ROW
BEGIN

	IF NOT IsNumber(NEW.Identif) THEN
		BEGIN
			signal sqlstate '45000' SET MESSAGE_TEXT = 'Cedula/RIF/Pasaporte invalido';
		END;
	END IF;

	IF NOT IsEmail(NEW.Email) THEN
		BEGIN
			signal sqlstate '45000' SET MESSAGE_TEXT = 'Email invalido';
		END;
	END IF;

	IF NOT IsNumber(NEW.Telefono) THEN
		BEGIN
			signal sqlstate '45000' SET MESSAGE_TEXT = 'Telefono invalido';
		END;
	END IF;

END;//

CREATE TRIGGER b_insert_notificacion BEFORE INSERT ON Notificacion
FOR EACH ROW
BEGIN

	IF NOT IsNumber(NEW.Identif_Cliente) THEN
		BEGIN
			signal sqlstate '45000' SET MESSAGE_TEXT = 'Cedula/RIF/Pasaporte del cliente invalido';
		END;
	END IF;

END;//

CREATE TRIGGER b_update_notificacion BEFORE UPDATE ON Notificacion
FOR EACH ROW
BEGIN

	IF NOT IsNumber(NEW.Identif_Cliente) THEN
		BEGIN
			signal sqlstate '45000' SET MESSAGE_TEXT = 'Cedula/RIF/Pasaporte del cliente invalido';
		END;
	END IF;

END;//

CREATE TRIGGER b_insert_equipo BEFORE INSERT ON Equipo
FOR EACH ROW
BEGIN

	IF NOT IsNumber(NEW.Identif_Cliente) THEN
		BEGIN
			signal sqlstate '45000' SET MESSAGE_TEXT = 'Cedula/RIF/Pasaporte del cliente invalido';
		END;
	END IF;

END;//

CREATE TRIGGER b_update_equipo BEFORE UPDATE ON Equipo
FOR EACH ROW
BEGIN

	IF NOT IsNumber(NEW.Identif_Cliente) THEN
		BEGIN
			signal sqlstate '45000' SET MESSAGE_TEXT = 'Cedula/RIF/Pasaporte del cliente invalido';
		END;
	END IF;

END;//

CREATE TRIGGER b_insert_orden BEFORE INSERT ON Orden
FOR EACH ROW
BEGIN

	IF NOT IsNumber(NEW.Numero) THEN
		BEGIN
			signal sqlstate '45000' SET MESSAGE_TEXT = 'Numero de orden invalido';
		END;
	END IF;

END;//

CREATE TRIGGER b_update_orden BEFORE UPDATE ON Orden
FOR EACH ROW
BEGIN

	IF NOT IsNumber(NEW.Numero) THEN
		BEGIN
			signal sqlstate '45000' SET MESSAGE_TEXT = 'Numero de orden invalido';
		END;
	END IF;

END;//

CREATE TRIGGER b_insert_factura_venta BEFORE INSERT ON FacturaVenta
FOR EACH ROW
BEGIN

	IF NOT IsNumber(NEW.Numero) THEN
		BEGIN
			signal sqlstate '45000' SET MESSAGE_TEXT = 'Numero de factura invalido';
		END;
	END IF;

	IF NOT IsNumber(NEW.Identif_Cliente) THEN
		BEGIN
			signal sqlstate '45000' SET MESSAGE_TEXT = 'Cedula/RIF/Pasaporte del cliente invalido';
		END;
	END IF;

END;//

CREATE TRIGGER b_update_factura_venta BEFORE UPDATE ON FacturaVenta
FOR EACH ROW
BEGIN

	IF NOT IsNumber(NEW.Numero) THEN
		BEGIN
			signal sqlstate '45000' SET MESSAGE_TEXT = 'Numero de factura invalido';
		END;
	END IF;

	IF NOT IsNumber(NEW.Identif_Cliente) THEN
		BEGIN
			signal sqlstate '45000' SET MESSAGE_TEXT = 'Cedula/RIF/Pasaporte del cliente invalido';
		END;
	END IF;

END;//

CREATE TRIGGER b_insert_venta_web BEFORE INSERT ON VentaWeb
FOR EACH ROW
BEGIN

	IF NOT IsNumber(NEW.Numero) THEN
		BEGIN
			signal sqlstate '45000' SET MESSAGE_TEXT = 'Numero de venta invalido';
		END;
	END IF;

	IF NOT IsNumber(NEW.Identif_Cliente) THEN
		BEGIN
			signal sqlstate '45000' SET MESSAGE_TEXT = 'Cedula/RIF/Pasaporte del cliente invalido';
		END;
	END IF;

	IF NOT IsNumber(NEW.Num_Factura) THEN
		BEGIN
			signal sqlstate '45000' SET MESSAGE_TEXT = 'Numero de factura invalido';
		END;
	END IF;

END;//

CREATE TRIGGER b_update_venta_web BEFORE UPDATE ON VentaWeb
FOR EACH ROW
BEGIN

	IF NOT IsNumber(NEW.Numero) THEN
		BEGIN
			signal sqlstate '45000' SET MESSAGE_TEXT = 'Numero de venta invalido';
		END;
	END IF;

	IF NOT IsNumber(NEW.Identif_Cliente) THEN
		BEGIN
			signal sqlstate '45000' SET MESSAGE_TEXT = 'Cedula/RIF/Pasaporte del cliente invalido';
		END;
	END IF;

	IF NOT IsNumber(NEW.Num_Factura) THEN
		BEGIN
			signal sqlstate '45000' SET MESSAGE_TEXT = 'Numero de factura de venta invalido';
		END;
	END IF;

END;//

CREATE TRIGGER b_insert_reparacion BEFORE INSERT ON Reparacion
FOR EACH ROW
BEGIN

	IF NOT IsNumber(NEW.Numero_Orden) THEN
		BEGIN
			signal sqlstate '45000' SET MESSAGE_TEXT = 'Numero de orden invalido';
		END;
	END IF;

END;//

CREATE TRIGGER b_update_reparacion BEFORE UPDATE ON Reparacion
FOR EACH ROW
BEGIN

	IF NOT IsNumber(NEW.Numero_Orden) THEN
		BEGIN
			signal sqlstate '45000' SET MESSAGE_TEXT = 'Numero de orden invalido';
		END;
	END IF;

END;//

CREATE TRIGGER b_insert_repuesto_reparacion BEFORE INSERT ON Repuesto_Reparacion
FOR EACH ROW
BEGIN

	IF NOT IsNumber(NEW.Num_Ord_Rep) THEN
		BEGIN
			signal sqlstate '45000' SET MESSAGE_TEXT = 'Numero de orden de reparacion invalido';
		END;
	END IF;	

END;//

CREATE TRIGGER b_update_repuesto_reparacion BEFORE UPDATE ON Repuesto_Reparacion
FOR EACH ROW
BEGIN

	IF NOT IsNumber(NEW.Num_Ord_Rep) THEN
		BEGIN
			signal sqlstate '45000' SET MESSAGE_TEXT = 'Numero de orden de reparacion invalido';
		END;
	END IF;	

END;//

CREATE TRIGGER b_insert_cuenta_cobrar BEFORE INSERT ON CuentaCobrar
FOR EACH ROW
BEGIN

	IF NOT IsNumber(NEW.Numero) THEN
		BEGIN
			signal sqlstate '45000' SET MESSAGE_TEXT = 'Numero de la cuenta invalido';
		END;
	END IF;	

	IF NOT IsNumber(NEW.Identif_Cliente) THEN
		BEGIN
			signal sqlstate '45000' SET MESSAGE_TEXT = 'Cedula/RIF/Pasaporte del cliente invalido';
		END;
	END IF;

	IF NOT IsNumber(NEW.Num_Factura) THEN
		BEGIN
			signal sqlstate '45000' SET MESSAGE_TEXT = 'Numero de factura de venta invalido';
		END;
	END IF;

END;//

CREATE TRIGGER b_update_cuenta_cobrar BEFORE UPDATE ON CuentaCobrar
FOR EACH ROW
BEGIN

	IF NOT IsNumber(NEW.Numero) THEN
		BEGIN
			signal sqlstate '45000' SET MESSAGE_TEXT = 'Numero de la cuenta invalido';
		END;
	END IF;	

	IF NOT IsNumber(NEW.Identif_Cliente) THEN
		BEGIN
			signal sqlstate '45000' SET MESSAGE_TEXT = 'Cedula/RIF/Pasaporte del cliente invalido';
		END;
	END IF;

	IF NOT IsNumber(NEW.Num_Factura) THEN
		BEGIN
			signal sqlstate '45000' SET MESSAGE_TEXT = 'Numero de factura de venta invalido';
		END;
	END IF;

END;//

CREATE TRIGGER b_insert_cuenta_cobrar_forma_pago_v BEFORE INSERT ON CuentaCobrar_FormaPagoV
FOR EACH ROW
BEGIN

	IF NOT IsNumber(NEW.Num_Cuenta) THEN
		BEGIN
			signal sqlstate '45000' SET MESSAGE_TEXT = 'Numero de la cuenta por cobrar invalido';
		END;
	END IF;

	IF NOT IsNumber(NEW.Cliente_Cuenta) THEN
		BEGIN
			signal sqlstate '45000' SET MESSAGE_TEXT = 'Cedula/RIF/Pasaporte del cliente de la cuenta invalido';
		END;
	END IF;

END;//

CREATE TRIGGER b_update_cuenta_cobrar_forma_pago_v BEFORE UPDATE ON CuentaCobrar_FormaPagoV
FOR EACH ROW
BEGIN

	IF NOT IsNumber(NEW.Num_Cuenta) THEN
		BEGIN
			signal sqlstate '45000' SET MESSAGE_TEXT = 'Numero de la cuenta por cobrar invalido';
		END;
	END IF;

	IF NOT IsNumber(NEW.Cliente_Cuenta) THEN
		BEGIN
			signal sqlstate '45000' SET MESSAGE_TEXT = 'Cedula/RIF/Pasaporte del cliente de la cuenta invalido';
		END;
	END IF;

END;//

CREATE TRIGGER b_insert_factura_venta_forma_pago_v BEFORE INSERT ON FacturaVenta_FormaPagoV
FOR EACH ROW
BEGIN

	IF NOT IsNumber(NEW.Num_Factura) THEN
		BEGIN
			signal sqlstate '45000' SET MESSAGE_TEXT = 'Numero de la factura de venta invalido';
		END;
	END IF;

END;//

CREATE TRIGGER b_update_factura_venta_forma_pago_v BEFORE UPDATE ON FacturaVenta_FormaPagoV
FOR EACH ROW
BEGIN

	IF NOT IsNumber(NEW.Num_Factura) THEN
		BEGIN
			signal sqlstate '45000' SET MESSAGE_TEXT = 'Numero de la factura de venta invalido';
		END;
	END IF;

END;//

CREATE TRIGGER b_insert_cuenta_forma_pago_v BEFORE INSERT ON Cuenta_FormaPagoV
FOR EACH ROW
BEGIN

	IF NOT IsNumber(NEW.Num_Cuenta) THEN
		BEGIN
			signal sqlstate '45000' SET MESSAGE_TEXT = 'Numero de cuenta invalido';
		END;
	END IF;

END;//

CREATE TRIGGER b_update_cuenta_forma_pago_v BEFORE UPDATE ON Cuenta_FormaPagoV
FOR EACH ROW
BEGIN

	IF NOT IsNumber(NEW.Num_Cuenta) THEN
		BEGIN
			signal sqlstate '45000' SET MESSAGE_TEXT = 'Numero de cuenta invalido';
		END;
	END IF;

END;//

CREATE TRIGGER b_insert_cuenta_pagar BEFORE INSERT ON CuentaPagar
FOR EACH ROW
BEGIN

	IF NOT IsNumber(NEW.Numero) THEN
		BEGIN
			signal sqlstate '45000' SET MESSAGE_TEXT = 'Numero de la cuenta invalido';
		END;
	END IF;	

	IF NOT IsNumber(NEW.Num_Factura) THEN
		BEGIN
			signal sqlstate '45000' SET MESSAGE_TEXT = 'Numero de factura de compra invalido';
		END;
	END IF;

END;//

CREATE TRIGGER b_update_cuenta_pagar BEFORE UPDATE ON CuentaPagar
FOR EACH ROW
BEGIN

	IF NOT IsNumber(NEW.Numero) THEN
		BEGIN
			signal sqlstate '45000' SET MESSAGE_TEXT = 'Numero de la cuenta invalido';
		END;
	END IF;	

	IF NOT IsNumber(NEW.Num_Factura) THEN
		BEGIN
			signal sqlstate '45000' SET MESSAGE_TEXT = 'Numero de factura de compra invalido';
		END;
	END IF;

END;//

CREATE TRIGGER b_insert_cuenta_pagar_forma_pago_c BEFORE INSERT ON CuentaPagar_FormaPagoC
FOR EACH ROW
BEGIN

	IF NOT IsNumber(NEW.Num_Cuenta) THEN
		BEGIN
			signal sqlstate '45000' SET MESSAGE_TEXT = 'Numero de la cuenta por pagar invalido';
		END;
	END IF;

END;//

CREATE TRIGGER b_update_cuenta_pagar_forma_pago_c BEFORE UPDATE ON CuentaPagar_FormaPagoC
FOR EACH ROW
BEGIN

	IF NOT IsNumber(NEW.Num_Cuenta) THEN
		BEGIN
			signal sqlstate '45000' SET MESSAGE_TEXT = 'Numero de la cuenta por pagar invalido';
		END;
	END IF;

END;//

CREATE TRIGGER b_insert_factura_compra_forma_pago_c BEFORE INSERT ON FacturaCompra_FormaPagoC
FOR EACH ROW
BEGIN

	IF NOT IsNumber(NEW.Num_Factura) THEN
		BEGIN
			signal sqlstate '45000' SET MESSAGE_TEXT = 'Numero de factura de compra invalido';
		END;
	END IF;

END;//

CREATE TRIGGER b_update_factura_compra_forma_pago_c BEFORE UPDATE ON FacturaCompra_FormaPagoC
FOR EACH ROW
BEGIN

	IF NOT IsNumber(NEW.Num_Factura) THEN
		BEGIN
			signal sqlstate '45000' SET MESSAGE_TEXT = 'Numero de factura de compra invalido';
		END;
	END IF;

END;//

CREATE TRIGGER b_insert_cuenta_forma_pago_c BEFORE INSERT ON Cuenta_FormaPagoC
FOR EACH ROW
BEGIN

	IF NOT IsNumber(NEW.Num_Cuenta) THEN
		BEGIN
			signal sqlstate '45000' SET MESSAGE_TEXT = 'Numero de la cuenta invalido';
		END;
	END IF;

END;//

CREATE TRIGGER b_update_cuenta_forma_pago_c BEFORE UPDATE ON Cuenta_FormaPagoC
FOR EACH ROW
BEGIN

	IF NOT IsNumber(NEW.Num_Cuenta) THEN
		BEGIN
			signal sqlstate '45000' SET MESSAGE_TEXT = 'Numero de la cuenta invalido';
		END;
	END IF;

END;//

CREATE TRIGGER b_insert_orden_factura_venta BEFORE INSERT ON Orden_FacturaVenta
FOR EACH ROW
BEGIN

	IF NOT IsNumber(NEW.Num_Orden) THEN
		BEGIN
			signal sqlstate '45000' SET MESSAGE_TEXT = 'Numero de la orden invalido';
		END;
	END IF;

	IF NOT IsNumber(NEW.Num_Factura) THEN
		BEGIN
			signal sqlstate '45000' SET MESSAGE_TEXT = 'Numero de factura de venta invalido';
		END;
	END IF;

END;//

CREATE TRIGGER b_update_orden_factura_venta BEFORE UPDATE ON Orden_FacturaVenta
FOR EACH ROW
BEGIN

	IF NOT IsNumber(NEW.Num_Orden) THEN
		BEGIN
			signal sqlstate '45000' SET MESSAGE_TEXT = 'Numero de la orden invalido';
		END;
	END IF;

	IF NOT IsNumber(NEW.Num_Factura) THEN
		BEGIN
			signal sqlstate '45000' SET MESSAGE_TEXT = 'Numero de factura de venta invalido';
		END;
	END IF;

END;//

CREATE TRIGGER b_insert_orden_equipo BEFORE INSERT ON Orden_Equipo
FOR EACH ROW
BEGIN

	IF NOT IsNumber(NEW.Num_Orden) THEN
		BEGIN
			signal sqlstate '45000' SET MESSAGE_TEXT = 'Numero de la orden invalido';
		END;
	END IF;

END;//

CREATE TRIGGER b_update_orden_equipo BEFORE UPDATE ON Orden_Equipo
FOR EACH ROW
BEGIN

	IF NOT IsNumber(NEW.Num_Orden) THEN
		BEGIN
			signal sqlstate '45000' SET MESSAGE_TEXT = 'Numero de la orden invalido';
		END;
	END IF;

END;//

CREATE TRIGGER b_insert_factura_venta_equipo_venta BEFORE INSERT ON FacturaVenta_EquipoVenta
FOR EACH ROW
BEGIN

	IF NOT IsNumber(NEW.Num_Factura) THEN
		BEGIN
			signal sqlstate '45000' SET MESSAGE_TEXT = 'Numero de factura de venta invalido';
		END;
	END IF;

END;//

CREATE TRIGGER b_update_factura_venta_equipo_venta BEFORE UPDATE ON FacturaVenta_EquipoVenta
FOR EACH ROW
BEGIN

	IF NOT IsNumber(NEW.Num_Factura) THEN
		BEGIN
			signal sqlstate '45000' SET MESSAGE_TEXT = 'Numero de factura de venta invalido';
		END;
	END IF;

END;//

CREATE TRIGGER b_insert_venta_web_equipo_venta BEFORE INSERT ON VentaWeb_EquipoVenta
FOR EACH ROW
BEGIN

	IF NOT IsNumber(NEW.Num_Venta) THEN
		BEGIN
			signal sqlstate '45000' SET MESSAGE_TEXT = 'Numero de la venta invalido';
		END;
	END IF;

END;//

CREATE TRIGGER b_update_venta_web_equipo_venta BEFORE UPDATE ON VentaWeb_EquipoVenta
FOR EACH ROW
BEGIN

	IF NOT IsNumber(NEW.Num_Venta) THEN
		BEGIN
			signal sqlstate '45000' SET MESSAGE_TEXT = 'Numero de la venta invalido';
		END;
	END IF;

END;//

CREATE TRIGGER b_insert_factura_compra_inventario BEFORE INSERT ON FacturaCompra_Inventario
FOR EACH ROW
BEGIN

	IF NOT IsNumber(NEW.Num_Fact) THEN
		BEGIN
			signal sqlstate '45000' SET MESSAGE_TEXT = 'Numero de factura de compra invalido';
		END;
	END IF;	

END;//

CREATE VIEW `Reporte_Diario_Equipos` as
SELECT Equipo.Nombre, Equipo.Serial_Eq, Orden.Estado 
From ((Orden INNER JOIN Orden_Equipo on Orden.Numero = Orden_Equipo.Num_Orden) 
      INNER JOIN Equipo on Orden_Equipo.Serial_Equipo = Equipo.Serial_Eq) 
WHERE (date(Orden.F_inicio) = CURRENT_DATE)
ORDER BY (Orden.Estado);

CREATE VIEW `Equipos_Tecnicos_Diarios` as 
SELECT Personal.Nombre as tecnico, Personal.CI, Equipo.Serial_Eq, Equipo.Nombre, Reparacion.Estado as estado_especifico, Orden.Estado 
FROM ((((Orden INNER JOIN Orden_Equipo on Orden.Numero = Orden_Equipo.Num_Orden) 
               INNER JOIN Equipo on Orden_Equipo.Serial_Equipo = Equipo.Serial_Eq) 
               INNER JOIN Reparacion on Orden.Numero = Reparacion.Numero_Orden)  
               INNER JOIN Personal on Personal.CI = Reparacion.CI_Personal) 
WHERE (date(Orden.F_inicio) = CURRENT_DATE)  
ORDER BY (Personal.CI and Orden.F_inicio);


CREATE VIEW `Pagos_Semanal` as 
SELECT Cliente.Identif, Cliente.Nombre, Cliente.Direccion, Cliente.Telefono, Cliente.Email, CuentaCobrar.Monto, FormaPagoV.Descripcion 
FROM (((Cliente INNER JOIN CuentaCobrar on Cliente.Identif = CuentaCobrar.Identif_Cliente) 
                INNER JOIN CuentaCobrar_FormaPagoV on CuentaCobrar.Numero = CuentaCobrar_FormaPagoV.Num_Cuenta) 
                INNER JOIN FormaPagoV on CuentaCobrar_FormaPagoV.Fecha_Pago = FormaPagoV.Fecha);
