-- drop all indexes
DROP INDEX IF EXISTS NombreRepuesto ON Repuesto;
DROP INDEX IF EXISTS NombreCliente ON Cliente;
DROP INDEX IF EXISTS OrdenDiaria ON Orden;
DROP INDEX IF EXISTS FechaFormaPagoV ON FormaPagoV;
DROP INDEX IF EXISTS FechaCompra ON FacturaCompra;
DROP INDEX IF EXISTS FechaVenta ON FacturaVenta;
DROP INDEX IF EXISTS StatusVentaWeb ON VentaWeb;
DROP INDEX IF EXISTS F_asigReparacion ON Reparacion;

-- create all indexes
CREATE UNIQUE INDEX NombreRepuesto ON Repuesto(Nombre);
CREATE UNIQUE INDEX NombreCliente ON Cliente(Nombre);
CREATE INDEX OrdenDiaria ON Orden(F_inicio, F_fin, Estado, Total);
CREATE INDEX FechaFormaPagoV ON FormaPagoV(Fecha);
CREATE INDEX FechaCompra ON FacturaCompra(Fecha);
CREATE INDEX FechaVenta ON FacturaVenta(Fecha);
CREATE INDEX StatusVentaWeb ON VentaWeb(Estado);
CREATE INDEX F_asigReparacion ON Reparacion(F_asig, CI_Personal);