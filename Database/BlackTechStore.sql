
-- Authors:
-- 1. Diego Castellano C.I.: 24885991.
-- 2. Laura Larez C.I.: 24108504.
-- 3. Rafael Di Gregorio C.I.: 25235931.
-- 4. Nestor Juarez C.I.: 26163348.
-- 5. Wendy Rivas C.I.: 26469959.

DELIMITER ;

USE mysql;

DROP DATABASE IF EXISTS BlackTechStore;

CREATE DATABASE BlackTechStore COLLATE='utf8_bin';

USE BlackTechStore;

--
-- TABLES STRUCTURE
--

-- drop all tables
DROP TABLE IF EXISTS FacturaCompra_Inventario;
DROP TABLE IF EXISTS VentaWeb_EquipoVenta;
DROP TABLE IF EXISTS FacturaVenta_EquipoVenta;
DROP TABLE IF EXISTS Orden_Equipo;
DROP TABLE IF EXISTS Orden_FacturaVenta;
DROP TABLE IF EXISTS Cuenta_FormaPagoC;
DROP TABLE IF EXISTS FacturaCompra_FormaPagoC;
DROP TABLE IF EXISTS CuentaPagar_FormaPagoC;
DROP TABLE IF EXISTS FormaPagoC;
DROP TABLE IF EXISTS CuentaPagar;
DROP TABLE IF EXISTS Cuenta_FormaPagoV;
DROP TABLE IF EXISTS FacturaVenta_FormaPagoV;
DROP TABLE IF EXISTS CuentaCobrar_FormaPagoV;
DROP TABLE IF EXISTS FormaPagoV;
DROP TABLE IF EXISTS CuentaCobrar;
DROP TABLE IF EXISTS Repuesto_Reparacion;
DROP TABLE IF EXISTS Reparacion;
DROP TABLE IF EXISTS VentaWeb;
DROP TABLE IF EXISTS FacturaVenta;
DROP TABLE IF EXISTS Orden;
DROP TABLE IF EXISTS Equipo;
DROP TABLE IF EXISTS Notificacion;
DROP TABLE IF EXISTS Cliente;
DROP TABLE IF EXISTS FacturaCompra;
DROP TABLE IF EXISTS Proveedor;
DROP TABLE IF EXISTS EquipoVenta;
DROP TABLE IF EXISTS Herramienta;
DROP TABLE IF EXISTS Repuesto;
DROP TABLE IF EXISTS Inventario;
DROP TABLE IF EXISTS Personal;
DROP TABLE IF EXISTS Categoria;
DROP TABLE IF EXISTS Modelo;
DROP TABLE IF EXISTS Marca;
DROP TABLE IF EXISTS Cuenta;
DROP TABLE IF EXISTS Tienda;

-- create all tables
CREATE TABLE Tienda (

	RIF BigInt(10) NOT NULL COMMENT 'Clave primaria (P.K.). RIF de la tienda.',
	Tipo_RIF enum('J', 'G') NOT NULL COMMENT 'J - Empresa privada, G - Empresa del Gobierno.',
	Direccion varchar(50) NOT NULL COMMENT 'Direccion o ubicacion de la tienda.',
	Nombre varchar(50) UNIQUE NOT NULL COMMENT 'Nombre que identifica la tienda (unico).',
	Email varchar(100) UNIQUE NOT NULL COMMENT 'Email de la tienda (unico).',
	Telefono varchar(20) NOT NULL COMMENT 'Numero telefonico de contacto.',

	CHECK (RIF > 0 AND RIF <= 9999999999),

	CONSTRAINT RIF_P_KEY PRIMARY KEY (RIF) 

) ENGINE=InnoDB DEFAULT CHARSET='utf8';

CREATE TABLE Cuenta (

	Numero varchar(20) NOT NULL COMMENT 'Clave Primaria (P.K.). Identifica la cuenta.',
	RIF_tienda BigInt(10) NOT NULL COMMENT 'Clave Foranea (F.K.). Viene de la relacion Tienda.',
	Tipo enum('Ahorro', 'Corriente') NOT NULL COMMENT 'Representa el tipo de cuenta.',
	Banco varchar(20) NOT NULL COMMENT 'Representa el nombre del banco al que corresponde la cuenta.',

	CHECK (RIF_tienda > 0 AND RIF_tienda <= 9999999999),

	CONSTRAINT NUMERO_P_KEY PRIMARY KEY (Numero),

	FOREIGN KEY (RIF_tienda) REFERENCES Tienda (RIF)
	ON UPDATE RESTRICT
	ON DELETE CASCADE

) ENGINE=InnoDB DEFAULT CHARSET='utf8';

CREATE TABLE Marca (

	ID int NOT NULL AUTO_INCREMENT COMMENT 'Clave Primaria (P.K.).',
	Nombre varchar(50) NOT NULL COMMENT 'Nombre de la marca.',

	CONSTRAINT ID_P_KEY PRIMARY KEY (ID)

) ENGINE=InnoDB DEFAULT CHARSET='utf8';

CREATE TABLE Modelo (

	ID int NOT NULL AUTO_INCREMENT COMMENT 'Clave Primaria (P.K.).',
	ID_Marca int NOT NULL COMMENT 'Clave Foranea (F.K.). Viene de la relacion Marca.',
	Nombre varchar(50) NOT NULL COMMENT 'Nombre del modelo.',

	CONSTRAINT ID_P_KEY PRIMARY KEY (ID),

	FOREIGN KEY (ID_Marca) REFERENCES Marca (ID)
	ON UPDATE CASCADE
	ON DELETE CASCADE

) ENGINE=InnoDB DEFAULT CHARSET='utf8';

CREATE TABLE Categoria (

	ID int NOT NULL AUTO_INCREMENT COMMENT 'Clave Primaria (P.K.).',
	ID_Modelo int NOT NULL COMMENT 'Clave Foranea (F.K.). Viene de la relacion Modelo.',
	Nombre varchar(50) NOT NULL COMMENT 'Nombre de la categoria.',

	CONSTRAINT ID_P_KEY PRIMARY KEY (ID),

	FOREIGN KEY (ID_Modelo) REFERENCES Modelo (ID)
	ON UPDATE CASCADE
	ON DELETE CASCADE

) ENGINE=InnoDB DEFAULT CHARSET='utf8';

CREATE TABLE Personal (

	CI int(9) NOT NULL COMMENT 'Clave primaria (P.K.). Identificacion del personal.',
	RIF BigInt(10) UNIQUE NOT NULL COMMENT 'RIF que lo identifica.',
	Tipo_RIF enum('J', 'G') NOT NULL COMMENT 'J - Empresa privada, G - Empresa del Gobierno.',
	Direccion varchar(50) NOT NULL COMMENT 'Direccion de vivienda.',
	Nombre varchar(50) NOT NULL COMMENT 'Nombre completo.',
	Email varchar(100) UNIQUE NOT NULL COMMENT 'Email del personal (unico).', 
	Telefono varchar(20) UNIQUE NOT NULL COMMENT 'Numero de telefono (unico).',
	Especialidad varchar(50) NULL COMMENT 'Especialidad del personal (hardware, software, reparaciones de micas, etc.), en caso de ser un tecnico.',
	Tipo enum('GR', 'TE', 'AS') NOT NULL COMMENT 'GR - Gerente, TC - Tecnico, AS - Asistente Administrativo.',

	CONSTRAINT CI_VALUE CHECK (CI > 0 AND CI <= 999999999),
	CONSTRAINT RIF_VALUE CHECK (RIF > 0 AND RIF <= 9999999999),

	CONSTRAINT CI_P_KEY PRIMARY KEY (CI)

) ENGINE=InnoDB DEFAULT CHARSET='utf8';

CREATE TABLE Inventario (

	Codigo varchar(5) NOT NULL COMMENT 'Clave Primaria (P.K.). Identifica el inventario.',
	F_Registro timestamp NOT NULL COMMENT 'Fecha y hora en que se registra el inventario.',
	Cantidad int(4) NOT NULL COMMENT 'Cantidad en inventario.',

	CHECK (Cantidad > 0 AND Cantidad <= 9999),

	CONSTRAINT CODIGO_P_KEY PRIMARY KEY (Codigo)

) ENGINE=InnoDB DEFAULT CHARSET='utf8';

CREATE TABLE Repuesto (

	Codigo varchar(5) NOT NULL COMMENT 'Clave Primaria (P.K.). Clave Foranea (F.K.). Viene de la relacion Inventario.',
	Nombre varchar(50) NOT NULL COMMENT 'Nombre del repuesto.',
	Descripcion varchar(200) NULL COMMENT 'Descripcion o detalle del repuesto.',
	Cantidad int(4) NOT NULL COMMENT 'Cantidad en existencia.',
	Precio decimal(9,2) NOT NULL DEFAULT 0 COMMENT 'Precio del repuesto.',

	CHECK (Cantidad > 0 AND Cantidad <= 9999),
	CHECK (Precio >= 0),

	CONSTRAINT CODIGO_P_KEY PRIMARY KEY (Codigo),

	FOREIGN KEY (Codigo) REFERENCES Inventario (Codigo)
	ON UPDATE CASCADE
	ON DELETE CASCADE

) ENGINE=InnoDB DEFAULT CHARSET='utf8';

CREATE TABLE Herramienta (

	Codigo varchar(5) NOT NULL COMMENT 'Clave Primaria (P.K.). Clave Foranea (F.K.). Viene de la relacion Inventario.',
	CI_Personal int(9) NULL COMMENT 'Clave Foranea (F.K.). Viene de la relacion Personal.',
	Nombre varchar(50) NOT NULL COMMENT 'Nombre de la herramienta.',
	Descripcion varchar(200) NULL COMMENT 'Descripcion o detalle de la herramienta.',
	Cantidad int(4) NOT NULL COMMENT 'Cantidad en existencia.',
	Precio decimal(9,2) NOT NULL DEFAULT 0 COMMENT 'Precio de la herramienta.',
	Cant_Uso int(4) NULL DEFAULT 0 COMMENT 'Cantidad utilizada por el Personal.',
	Precio_Uso decimal(9,2) NULL DEFAULT 0 COMMENT 'Precio de la herramienta para su uso por el Personal.',

	CHECK (CI_Personal > 0 AND CI_Personal <= 999999999),
	CHECK (Cantidad > 0 AND Cantidad <= 9999),
	CHECK (Precio >= 0),
	CHECK (Cant_Uso > 0 AND Cant_Uso <= 9999),
	CHECK (Precio_Uso >= 0),

	CONSTRAINT CODIGO_P_KEY PRIMARY KEY (Codigo),

	FOREIGN KEY (Codigo) REFERENCES Inventario (Codigo)
	ON UPDATE CASCADE
	ON DELETE CASCADE,

	FOREIGN KEY (CI_Personal) REFERENCES Personal (CI)
	ON UPDATE CASCADE
	ON DELETE CASCADE

) ENGINE=InnoDB DEFAULT CHARSET='utf8';

CREATE TABLE EquipoVenta (

	Codigo varchar(5) NOT NULL COMMENT 'Clave Primaria (P.K.). Clave Foranea (F.K.). Viene de la relacion Inventario.',
	ID_Categoria int NOT NULL COMMENT 'Clave Foranea (F.K.). Viene de la relacion Categoria.',
	Nombre varchar(50) NOT NULL COMMENT 'Nombre del equipo.',
	Descripcion varchar(200) NULL COMMENT 'Descripcion o detalle del equipo.',
	Cantidad int(4) NOT NULL COMMENT 'Cantidad en existencia.',
	Precio decimal(9,2) NOT NULL DEFAULT 0 COMMENT 'Precio del equipo.',

	CHECK (Cantidad > 0 AND Cantidad <= 9999),
	CHECK (Precio >= 0),

	CONSTRAINT CODIGO_P_KEY PRIMARY KEY (Codigo),

	FOREIGN KEY (Codigo) REFERENCES Inventario (Codigo)
	ON UPDATE CASCADE
	ON DELETE CASCADE,

	FOREIGN KEY (ID_Categoria) REFERENCES Categoria (ID)
	ON UPDATE CASCADE
	ON DELETE CASCADE	

) ENGINE=InnoDB DEFAULT CHARSET='utf8';

CREATE TABLE Proveedor (

	RIF BigInt(10) NOT NULL COMMENT 'RIF que lo identifica.',
	Tipo_RIF enum('J', 'G') NOT NULL COMMENT 'J - Empresa privada, G - Empresa del Gobierno.',
	Direccion varchar(50) NOT NULL COMMENT 'Direccion del proveedor.',
	Nombre varchar(50) NOT NULL COMMENT 'Nombre del proveedor.',
	Email varchar(100) UNIQUE NULL COMMENT 'Email del proveedor (unico).', 
	Telefono varchar(20) UNIQUE NOT NULL COMMENT 'Numero de telefono (unico).',

	CHECK (RIF > 0 AND RIF <= 9999999999),

	CONSTRAINT RIF_P_KEY PRIMARY KEY (RIF)

) ENGINE=InnoDB DEFAULT CHARSET='utf8';

CREATE TABLE FacturaCompra (

	Numero varchar(5) NOT NULL COMMENT 'Clave Primaria (P.K.). Identifica la factura.',
	RIF_Proveed BigInt(10) NOT NULL COMMENT 'Clave Primaria (P.K.). Clave Foranea (F.K.). Viene de la relacion Proveedor.',
	Fecha timestamp NOT NULL COMMENT 'Fecha en que se genera la factura.',
	Estado varchar(20) NOT NULL COMMENT 'Estado de la factura.',
	Descripcion varchar(200) NULL COMMENT 'Descripcion o detalle de la factura.',
	Monto decimal(9,2) NOT NULL DEFAULT 0 COMMENT 'Monto total de la factura.',

	CHECK (RIF_Proveed > 0 AND RIF_Proveed <= 9999999999),
	CHECK (Monto >= 0),

	CONSTRAINT P_KEY PRIMARY KEY (Numero, RIF_Proveed),

	FOREIGN KEY (RIF_Proveed) REFERENCES Proveedor (RIF)
	ON UPDATE NO ACTION
	ON DELETE NO ACTION

) ENGINE=InnoDB DEFAULT CHARSET='utf8';

CREATE TABLE Cliente (

	Identif varchar(15) NOT NULL COMMENT 'Clave Primaria (P.K.). Identificador del cliente.',
	Tipo_Identif enum('V', 'E', 'J', 'G') NOT NULL COMMENT 'V - Venezolano (cedula), E - Extranjero (Pasaporte), J - Empresa privada, G - Empresa del Gobierno.',
	Direccion varchar(50) NOT NULL COMMENT 'Direccion del cliente.',
	Nombre varchar(50) NOT NULL COMMENT 'Nombre completo del cliente.',
	Email varchar(100) UNIQUE NULL COMMENT 'Email del cliente (unico).', 
	Telefono varchar(20) UNIQUE NOT NULL COMMENT 'Numero de telefono (unico).',

	CONSTRAINT IDENTIF_P_KEY PRIMARY KEY (Identif)

) ENGINE=InnoDB DEFAULT CHARSET='utf8';

CREATE TABLE Notificacion (

	F_Hora timestamp NOT NULL COMMENT 'Clave Primaria (P.K.). Fecha y hora en que el cliente recibio la notificacion.',
	Identif_Cliente varchar(15) NOT NULL COMMENT 'Clave Primaria (P.K.). Clave Foranea (F.K.). Viene de la relacion Cliente.',
	Via enum('email', 'sms') NOT NULL COMMENT 'Via por la cual se envia la notificacion.',
	Texto varchar(255) NOT NULL COMMENT 'Contenido del mensaje enviado.',

	CONSTRAINT P_KEY PRIMARY KEY (F_Hora, Identif_Cliente),

	FOREIGN KEY (Identif_Cliente) REFERENCES Cliente (Identif)
	ON UPDATE NO ACTION
	ON DELETE NO ACTION

) ENGINE=InnoDB DEFAULT CHARSET='utf8';

CREATE TABLE Equipo (

	Serial_Eq varchar(20) NOT NULL COMMENT 'Clave Primaria (P.K.). Serial que identifica el equipo.',
	ID_Categoria int NOT NULL COMMENT 'Clave Foranea (F.K.). Viene de la relacion Categoria.',
	Identif_Cliente varchar(15) NOT NULL COMMENT 'Clave Foranea (F.K.). Viene de la relacion Cliente.',
	Nombre varchar(50) NOT NULL COMMENT 'Nombre del equipo',
	Descripcion varchar(200) NULL COMMENT 'Descripcion o detalle del equipo.',
	Precio decimal(9,2) NOT NULL DEFAULT 0 COMMENT 'Precio del equipo.',

	CHECK (Precio >= 0),

	CONSTRAINT SERIAL_EQ_P_KEY PRIMARY KEY (Serial_Eq),

	FOREIGN KEY (ID_Categoria) REFERENCES Categoria (ID)
	ON UPDATE CASCADE
	ON DELETE CASCADE,

	FOREIGN KEY (Identif_Cliente) REFERENCES Cliente (Identif)
	ON UPDATE RESTRICT
	ON DELETE NO ACTION

) ENGINE=InnoDB DEFAULT CHARSET='utf8';

CREATE TABLE Orden (

	Numero varchar(5) NOT NULL COMMENT 'Clave Primaria (P.K.). Identifica la orden.',
	F_inicio timestamp NOT NULL COMMENT 'Fecha en que se inicio la orden.',
	F_fin timestamp NOT NULL COMMENT 'Fecha de culminacion de la orden.',
	Estado enum('ESP', 'REV', 'REP', 'DEV') NOT NULL COMMENT 'ESP - En espera, REV - Revisado, REP - Reparado, DEV - Devolucion.',
	Total decimal(9,2) NOT NULL DEFAULT 0 COMMENT 'Costo total de la orden.',

	CHECK (Total >= 0),

	CONSTRAINT NUMERO_P_KEY PRIMARY KEY (Numero)

) ENGINE=InnoDB DEFAULT CHARSET='utf8';

CREATE TABLE FacturaVenta (

	Numero varchar(5) NOT NULL COMMENT 'Clave Primaria (P.K.). Identifica la factura.',
	Identif_Cliente varchar(15) NOT NULL COMMENT 'Clave Foranea (F.K.). Viene de la relacion Cliente.',
	Fecha timestamp NOT NULL COMMENT 'Fecha en que se genera la factura.',
	Estado varchar(20) NOT NULL COMMENT 'Estado de la factura.',
	Descripcion varchar(200) NULL COMMENT 'Descripcion o detalle de la factura.',
	Monto decimal(9,2) NOT NULL DEFAULT 0 COMMENT 'Monto total de la factura.',

	CHECK (Monto >= 0),

	CONSTRAINT NUMERO_P_KEY PRIMARY KEY (Numero),

	FOREIGN KEY (Identif_Cliente) REFERENCES Cliente (Identif)
	ON UPDATE NO ACTION
	ON DELETE NO ACTION

) ENGINE=InnoDB DEFAULT CHARSET='utf8';

CREATE TABLE VentaWeb (

	Numero varchar(5) NOT NULL COMMENT 'Clave Primaria (P.K.). Identifica la venta web.',
	Identif_Cliente varchar(15) NOT NULL COMMENT 'Clave Foranea (F.K.). Viene de la relacion Cliente.',
	Num_Factura varchar(5) NOT NULL COMMENT 'Clave Foranea (F.K). Viene de la relacion FacturaVenta.',
	Concepto varchar(200) NOT NULL COMMENT 'Concepto o detalle de la venta.',
	Estado enum('PAPB', 'APROB', 'RECHAZ') NOT NULL COMMENT 'PAPB - Por aprobar, APROB - Aprobada, RECHAZ - Rechazada.',

	CONSTRAINT NUMERO_P_KEY PRIMARY KEY (Numero),

	FOREIGN KEY (Identif_Cliente) REFERENCES Cliente (Identif)
	ON UPDATE RESTRICT
	ON DELETE RESTRICT,

	FOREIGN KEY (Num_Factura) REFERENCES FacturaVenta (Numero)
	ON UPDATE NO ACTION
	ON DELETE NO ACTION

) ENGINE=InnoDB DEFAULT CHARSET='utf8';

CREATE TABLE Reparacion (

	F_asig timestamp NOT NULL COMMENT 'Clave Primaria (P.K.). Fecha de inicio de la reparacion.',
	CI_Personal int(9) NOT NULL COMMENT 'Clave Primaria (P.K.). Clave Foranea (F.K.). Viene de la relacion Personal.',
	Numero_Orden varchar(5) NOT NULL COMMENT 'Clave Primaria (P.K.). Clave Foranea (F.K.). Viene de la relacion Orden.',
	F_rep timestamp NOT NULL COMMENT 'Fecha de culminacion de la reparacion.',
	Descripcion varchar(200) NULL COMMENT 'Descripcion o detalle de la reparacion.',
	Comision decimal(9,2) NOT NULL DEFAULT 0 COMMENT 'Comision que le corresponde al tecnico que realizo la reparacion.',
	Total decimal(9,2) NOT NULL DEFAULT 0 COMMENT 'Monto total de la reparacion.',
	Estado enum('ESP', 'REV', 'REP', 'DEV') NOT NULL COMMENT 'ESP - En espera, REV - Revisado, REP - Reparado, DEV - Devolucion.',

	CHECK (CI_Personal > 0 AND CI_Personal <= 999999999),
	CHECK (Comision >= 0),
	CHECK (Total >= 0),

	CONSTRAINT P_KEY PRIMARY KEY (F_asig, CI_Personal, Numero_Orden),

	FOREIGN KEY (CI_Personal) REFERENCES Personal (CI)
	ON UPDATE RESTRICT
	ON DELETE NO ACTION,

	FOREIGN KEY (Numero_Orden) REFERENCES Orden (Numero)
	ON UPDATE RESTRICT
	ON DELETE NO ACTION

) ENGINE=InnoDB DEFAULT CHARSET='utf8';

CREATE TABLE Repuesto_Reparacion (

	Codigo_Repues varchar(5) NOT NULL COMMENT 'Clave Primaria (P.K.). Clave Foranea (F.K.). Viene de la relacion Repuesto.',
	F_asig_Rep timestamp NOT NULL COMMENT 'Clave Primaria (P.K.). Clave Foranea (F.K.). Viene de la relacion Reparacion.',
	CI_Pers_Rep int(9) NOT NULL COMMENT 'Clave Primaria (P.K.). Clave Foranea (F.K.). Viene de la relacion Reparacion.',
	Num_Ord_Rep varchar(5) NOT NULL COMMENT 'Clave Primaria (P.K.). Clave Foranea (F.K.). Viene de la relacion Reparacion.',
    Cant_Rep int(4) NOT NULL COMMENT 'Cantidad utilizada del repuesto para la reparacion.',
	Precio_Rep decimal(9,2) NOT NULL DEFAULT 0 COMMENT 'Precio del repuesto en el momento en que se hizo la reparacion.',

	CHECK (CI_Pers_Rep > 0 AND CI_Pers_Rep <= 999999999),
	CHECK (Cant_Rep > 0 AND Cant_Rep <= 9999),
	CHECK (Precio_Rep >= 0),

	CONSTRAINT P_KEY PRIMARY KEY (Codigo_Repues, F_asig_Rep, CI_Pers_Rep, Num_Ord_Rep),

	FOREIGN KEY (Codigo_Repues) REFERENCES Repuesto (Codigo)
	ON UPDATE RESTRICT
	ON DELETE CASCADE,

	FOREIGN KEY (F_asig_Rep, CI_Pers_Rep, Num_Ord_Rep) REFERENCES Reparacion (F_asig, CI_Personal, Numero_Orden)
	ON UPDATE RESTRICT
	ON DELETE CASCADE

) ENGINE=InnoDB DEFAULT CHARSET='utf8';

CREATE TABLE CuentaCobrar (

	Numero varchar(5) NOT NULL COMMENT 'Clave Primaria (P.K.). Identifica la cuenta.',
	Identif_Cliente varchar(15) NOT NULL COMMENT 'Clave Primaria (P.K.). Clave Foranea (F.K.). Viene de la relacion Cliente.',
	Num_Factura varchar(5) NOT NULL COMMENT 'Clave Foranea (F.K.). Viene de la relacion FacturaVenta.',
	F_aper timestamp NOT NULL COMMENT 'Fecha de apertura de la cuenta.',
	F_lim date NOT NULL COMMENT 'Fecha limite de la cuenta.',
	Estado varchar(20) NOT NULL COMMENT 'Estado de la cuenta.',
	Descripcion varchar(200) NULL COMMENT 'Descripcion o detalle de la cuenta.',
	Saldo decimal(9,2) NOT NULL DEFAULT 0 COMMENT 'Saldo actual de la cuenta.',
	Monto decimal(9,2) NOT NULL DEFAULT 0 COMMENT 'Monto total de la cuenta.',

	CHECK (Saldo >= 0),
	CHECK (Monto >= 0),

	CONSTRAINT P_KEY PRIMARY KEY (Numero, Identif_Cliente),

	FOREIGN KEY (Identif_Cliente) REFERENCES Cliente (Identif)
	ON UPDATE NO ACTION
	ON DELETE CASCADE,

	FOREIGN KEY (Num_Factura) REFERENCES FacturaVenta (Numero)
	ON UPDATE NO ACTION
	ON DELETE CASCADE

) ENGINE=InnoDB DEFAULT CHARSET='utf8';

CREATE TABLE FormaPagoV (

	Fecha timestamp NOT NULL COMMENT 'Clave primaria (P.K.). Fecha y hora en que se efectua el pago.',
	Banco varchar(20) NULL COMMENT 'Nombre del banco.',
	Nref int(9) NULL COMMENT 'Numero de referencia.',
	Descripcion varchar(200) NULL COMMENT 'Descripcion o detalle del pago.',
	Tipo enum('EF', 'TRF', 'DEP', 'CH', 'TD', 'TC') NOT NULL COMMENT 'Modo de pago (EF – Efectivo, TRF – Transferencia, DEP – Deposito, CH – Cheque, TD – Tarjeta Debito, TC – Tarjeta Credito).',
	Monto decimal(9,2) NOT NULL DEFAULT 0 COMMENT 'Monto a pagar.',

	CHECK (Nref > 0 AND Nref <= 999999999),
	CHECK (Monto >= 0),

	CONSTRAINT FECHA_P_KEY PRIMARY KEY (Fecha)

) ENGINE=InnoDB DEFAULT CHARSET='utf8';

CREATE TABLE CuentaCobrar_FormaPagoV (

	Num_Cuenta varchar(5) NOT NULL COMMENT 'Clave Primaria (P.K.). Clave Foranea (F.K.). Viene de la relacion CuentaCobrar.',
	Cliente_Cuenta varchar(15) NOT NULL COMMENT 'Clave Primaria (P.K.). Clave Foranea (F.K.). Viene de la relacion CuentaCobrar.',
	Fecha_Pago timestamp NOT NULL COMMENT 'Clave primaria (P.K.). Clave Foranea (F.K.). Viene de la relacion FormaPagoV.',

	CONSTRAINT P_KEY PRIMARY KEY (Num_Cuenta, Cliente_Cuenta, Fecha_Pago),

	FOREIGN KEY (Num_Cuenta) REFERENCES CuentaCobrar (Numero)
	ON UPDATE RESTRICT
	ON DELETE CASCADE,

	FOREIGN KEY (Cliente_Cuenta) REFERENCES CuentaCobrar (Identif_Cliente)
	ON UPDATE RESTRICT
	ON DELETE CASCADE,

	FOREIGN KEY (Fecha_Pago) REFERENCES FormaPagoV (Fecha)
	ON UPDATE RESTRICT
	ON DELETE CASCADE

) ENGINE=InnoDB DEFAULT CHARSET='utf8';

CREATE TABLE FacturaVenta_FormaPagoV (

	Num_Factura varchar(5) NOT NULL COMMENT 'Clave Primaria (P.K.). Clave Foranea (F.K.). Viene de la relacion FacturaVenta.',
	Fecha_Pago timestamp NOT NULL COMMENT 'Clave primaria (P.K.). Clave Foranea (F.K.). Viene de la relacion FormaPagoV.',

	CONSTRAINT P_KEY PRIMARY KEY (Num_Factura, Fecha_Pago),

	FOREIGN KEY (Num_Factura) REFERENCES FacturaVenta (Numero)
	ON UPDATE RESTRICT
	ON DELETE CASCADE,

	FOREIGN KEY (Fecha_pago) REFERENCES FormaPagoV (Fecha)
	ON UPDATE RESTRICT
	ON DELETE CASCADE 

) ENGINE=InnoDB DEFAULT CHARSET='utf8';

CREATE TABLE Cuenta_FormaPagoV (

	Num_Cuenta varchar(20) NOT NULL COMMENT 'Clave Primaria (P.K.). Clave Foranea (F.K.). Viene de la relacion Cuenta.',
	Fecha_Pago timestamp NOT NULL COMMENT 'Clave primaria (P.K.). Clave Foranea (F.K.). Viene de la relacion FormaPagoV.',

	CONSTRAINT P_KEY PRIMARY KEY (Num_Cuenta, Fecha_Pago),

	FOREIGN KEY (Num_Cuenta) REFERENCES Cuenta (Numero)
	ON UPDATE RESTRICT
	ON DELETE CASCADE,

	FOREIGN KEY (Fecha_Pago) REFERENCES FormaPagoV (Fecha)
	ON UPDATE RESTRICT
	ON DELETE CASCADE

) ENGINE=InnoDB DEFAULT CHARSET='utf8';

CREATE TABLE CuentaPagar (

	Numero varchar(5) NOT NULL COMMENT 'Clave Primaria (P.K.). Identifica la cuenta.',
	RIF_Proveed BigInt(10) NOT NULL COMMENT 'Clave Primaria (P.K.). Clave Foranea (F.K.). Viene de la relacion Proveedor.',
	Num_Factura varchar(5) NOT NULL COMMENT 'Clave Foranea (F.K.). Viene de la relacion FacturaCompra.',
	F_aper timestamp NOT NULL COMMENT 'Fecha de apertura de la cuenta.',
	F_lim date NOT NULL COMMENT 'Fecha limite de la cuenta.',
	Estado varchar(20) NOT NULL COMMENT 'Estado de la cuenta.',
	Descripcion varchar(200) NULL COMMENT 'Descripcion o detalle de la cuenta.',
	Saldo decimal(9,2) NOT NULL DEFAULT 0 COMMENT 'Saldo actual de la cuenta.',
	Monto decimal(9,2) NOT NULL DEFAULT 0 COMMENT 'Monto total de la cuenta.',

	CHECK (Saldo >= 0),
	CHECK (Monto >= 0),

	CONSTRAINT P_KEY PRIMARY KEY (Numero, RIF_Proveed),

	FOREIGN KEY (RIF_Proveed) REFERENCES Proveedor (RIF)
	ON UPDATE NO ACTION
	ON DELETE CASCADE,

	FOREIGN KEY (Num_Factura) REFERENCES FacturaCompra (Numero)
	ON UPDATE NO ACTION
	ON DELETE CASCADE

) ENGINE=InnoDB DEFAULT CHARSET='utf8';

CREATE TABLE FormaPagoC (

	Fecha timestamp NOT NULL COMMENT 'Clave primaria (P.K.). Fecha y hora en que se efectua el pago.',
	Banco varchar(20) NULL COMMENT 'Nombre del banco.',
	Nref int(9) NULL COMMENT 'Numero de referencia.',
	Descripcion varchar(200) NULL COMMENT 'Descripcion o detalle del pago.',
	Tipo enum('EF', 'TRF', 'DEP', 'CH', 'TD', 'TC') NOT NULL COMMENT 'Modo de pago (EF – Efectivo, TRF – Transferencia, DEP – Deposito, CH – Cheque, TD – Tarjeta Debito, TC – Tarjeta Credito).',
	Monto decimal(9,2) NOT NULL DEFAULT 0 COMMENT 'Monto a pagar.',

	CHECK (Nref > 0 AND Nref <= 999999999),
	CHECK (Monto >= 0),

	CONSTRAINT FECHA_P_KEY PRIMARY KEY (Fecha)

) ENGINE=InnoDB DEFAULT CHARSET='utf8';

CREATE TABLE CuentaPagar_FormaPagoC (

	Num_Cuenta varchar(5) NOT NULL COMMENT 'Clave Primaria (P.K.). Clave Foranea (F.K.). Viene de la relacion CuentaPagar.',
	RIF_Proveed BigInt(10) NOT NULL COMMENT 'Clave Primaria (P.K.). Clave Foranea (F.K.). Viene de la relacion CuentaPagar.',
	Fecha_Pago timestamp NOT NULL COMMENT 'Clave primaria (P.K.). Clave Foranea (F.K.). Viene de la relacion FormaPagoC.',

	CHECK (RIF_Proveed > 0 AND RIF_Proveed <= 9999999999),

	CONSTRAINT P_KEY PRIMARY KEY (Num_Cuenta, RIF_Proveed, Fecha_Pago),

	FOREIGN KEY (Num_Cuenta) REFERENCES CuentaPagar (Numero)
	ON UPDATE NO ACTION
	ON DELETE NO ACTION,

	FOREIGN KEY (RIF_Proveed) REFERENCES CuentaPagar (RIF_Proveed)
	ON UPDATE NO ACTION
	ON DELETE NO ACTION,

	FOREIGN KEY (Fecha_Pago) REFERENCES FormaPagoC (Fecha)
	ON UPDATE NO ACTION
	ON DELETE NO ACTION

) ENGINE=InnoDB DEFAULT CHARSET='utf8';

CREATE TABLE FacturaCompra_FormaPagoC (

	Num_Factura varchar(5) NOT NULL COMMENT 'Clave Primaria (P.K.). Clave Foranea (F.K.). Viene de la relacion FacturaCompra.',
	RIF_Proveed BigInt(10) NOT NULL COMMENT 'Clave Primaria (P.K.). Clave Foranea (F.K.). Viene de la relacion FacturaCompra.',
	Fecha_Pago timestamp NOT NULL COMMENT 'Clave primaria (P.K.). Clave Foranea (F.K.). Viene de la relacion FormaPagoC.',

	CHECK (RIF_Proveed > 0 AND RIF_Proveed <= 9999999999),

	CONSTRAINT P_KEY PRIMARY KEY (Num_Factura, RIF_Proveed, Fecha_Pago),

	FOREIGN KEY (Num_Factura) REFERENCES FacturaCompra (Numero)
	ON UPDATE NO ACTION
	ON DELETE NO ACTION,

	FOREIGN KEY (RIF_Proveed) REFERENCES FacturaCompra (RIF_Proveed)
	ON UPDATE NO ACTION
	ON DELETE NO ACTION,

	FOREIGN KEY (Fecha_pago) REFERENCES FormaPagoC (Fecha)
	ON UPDATE NO ACTION
	ON DELETE NO ACTION 	

) ENGINE=InnoDB DEFAULT CHARSET='utf8';

CREATE TABLE Cuenta_FormaPagoC (

	Num_Cuenta varchar(20) NOT NULL COMMENT 'Clave Primaria (P.K.). Clave Foranea (F.K.). Viene de la relacion Cuenta.',
	Fecha_Pago timestamp NOT NULL COMMENT 'Clave primaria (P.K.). Clave Foranea (F.K.). Viene de la relacion FormaPagoC.',

	CONSTRAINT P_KEY PRIMARY KEY (Num_Cuenta, Fecha_Pago),

	FOREIGN KEY (Num_Cuenta) REFERENCES Cuenta (Numero)
	ON UPDATE RESTRICT
	ON DELETE CASCADE,

	FOREIGN KEY (Fecha_Pago) REFERENCES FormaPagoC (Fecha)
	ON UPDATE RESTRICT
	ON DELETE CASCADE

) ENGINE=InnoDB DEFAULT CHARSET='utf8';

CREATE TABLE Orden_FacturaVenta (

	Num_Orden varchar(5) NOT NULL COMMENT 'Clave Primaria (P.K.). Clave Foranea (F.K.). Viene de la relacion Orden.',
	Num_Factura varchar(5) NOT NULL COMMENT 'Clave Primaria (P.K.). Clave Foranea (F.K.). Viene de la relacion FacturaVenta.',

	CONSTRAINT P_KEY PRIMARY KEY (Num_Orden, Num_Factura),

	FOREIGN KEY (Num_Orden) REFERENCES Orden (Numero)
	ON UPDATE NO ACTION
	ON DELETE NO ACTION,

	FOREIGN KEY (Num_Factura) REFERENCES FacturaVenta (Numero)
	ON UPDATE NO ACTION
	ON DELETE NO ACTION

) ENGINE=InnoDB DEFAULT CHARSET='utf8';

CREATE TABLE Orden_Equipo (

	Num_Orden varchar(5) NOT NULL COMMENT 'Clave Primaria (P.K.). Clave Foranea (F.K.). Viene de la relacion Orden.',
	Serial_Equipo varchar(20) NOT NULL COMMENT 'Clave Primaria (P.K.). Clave Foranea (F.K.). Viene de la relacion Equipo.',
	Falla_Equipo varchar(200) NOT NULL COMMENT 'Descripcion de la falla que presenta el equipo.',

	CONSTRAINT P_KEY PRIMARY KEY (Num_Orden, Serial_Equipo),

	FOREIGN KEY (Num_Orden) REFERENCES Orden (Numero)
	ON UPDATE NO ACTION
	ON DELETE NO ACTION,

	FOREIGN KEY (Serial_Equipo) REFERENCES Equipo (Serial_Eq)
	ON UPDATE NO ACTION
	ON DELETE NO ACTION

) ENGINE=InnoDB DEFAULT CHARSET='utf8';

CREATE TABLE FacturaVenta_EquipoVenta (

	Num_Factura varchar(5) NOT NULL COMMENT 'Clave Primaria (P.K.). Clave Foranea (F.K.). Viene de la relacion FacturaVenta.',
	Codigo_Equipo varchar(5) NOT NULL COMMENT 'Clave Primaria (P.K.). Clave Foranea (F.K.). Viene de la relacion EquipoVenta.',
	Cantidad int(4) NOT NULL COMMENT 'Cantidad a vender.',
	PVP decimal(9,2) NOT NULL DEFAULT 0 COMMENT 'Precio de venta del equipo.',

	CHECK (Cantidad > 0 AND Cantidad <= 9999),
	CHECK (PVP >= 0),

	CONSTRAINT P_KEY PRIMARY KEY (Num_Factura, Codigo_Equipo),

	FOREIGN KEY (Num_Factura) REFERENCES FacturaVenta (Numero)
	ON UPDATE NO ACTION
	ON DELETE NO ACTION,

	FOREIGN KEY (Codigo_Equipo) REFERENCES EquipoVenta (Codigo)
	ON UPDATE NO ACTION
	ON DELETE NO ACTION

) ENGINE=InnoDB DEFAULT CHARSET='utf8';

CREATE TABLE VentaWeb_EquipoVenta (

	Num_Venta varchar(5) NOT NULL COMMENT 'Clave Primaria (P.K.). Clave Foranea (F.K.). Viene de la relacion VentaWeb.',
	Codigo_Equipo varchar(5) NOT NULL COMMENT 'Clave Primaria (P.K.). Clave Foranea (F.K.). Viene de la relacion EquipoVenta.',
	Cantidad int(4) NOT NULL COMMENT 'Cantidad a vender.',
	Precio decimal(9,2) NOT NULL DEFAULT 0 COMMENT 'Precio de venta del equipo.',

	CHECK (Cantidad > 0 AND Cantidad <= 9999),
	CHECK (Precio >= 0),

	CONSTRAINT P_KEY PRIMARY KEY (Num_Venta, Codigo_Equipo),

	FOREIGN KEY (Num_Venta) REFERENCES VentaWeb (Numero)
	ON UPDATE NO ACTION
	ON DELETE NO ACTION,

	FOREIGN KEY (Codigo_Equipo) REFERENCES EquipoVenta (Codigo)
	ON UPDATE NO ACTION
	ON DELETE NO ACTION

) ENGINE=InnoDB DEFAULT CHARSET='utf8';

CREATE TABLE FacturaCompra_Inventario (

	Num_Fact varchar(5) NOT NULL COMMENT 'Clave Primaria (P.K.). Clave Foranea (F.K.). Viene de la relacion FacturaCompra.',
	Proveed_Fact BigInt(10) NOT NULL COMMENT 'Clave Primaria (P.K.). Clave Foranea (F.K.). Viene de la relacion FacturaCompra.',
	Cod_Inv varchar(5) NOT NULL COMMENT 'Clave Primaria (P.K.). Clave Foranea (F.K.). Viene de la relacion Inventario.',
	Cantidad int(4) NOT NULL COMMENT 'Cantidad a vender.',
	Costo decimal(9,2) NOT NULL DEFAULT 0 COMMENT 'Precio de costo del producto.',

	CHECK (Proveed_Fact > 0 AND Proveed_Fact <= 9999999999),
	CHECK (Cantidad > 0 AND Cantidad <= 9999),
	CHECK (Costo >= 0),

	CONSTRAINT P_KEY PRIMARY KEY (Num_Fact, Proveed_Fact, Cod_Inv),

	FOREIGN KEY (Num_Fact) REFERENCES FacturaCompra (Numero)
	ON UPDATE NO ACTION
	ON DELETE NO ACTION,

	FOREIGN KEY (Proveed_Fact) REFERENCES FacturaCompra (RIF_Proveed)
	ON UPDATE NO ACTION
	ON DELETE NO ACTION,

	FOREIGN KEY (Cod_Inv) REFERENCES Inventario (Codigo)
	ON DELETE NO ACTION

) ENGINE=InnoDB DEFAULT CHARSET='utf8';

--
-- INDEXES STRUCTURE
--

-- drop all indexes
DROP INDEX IF EXISTS NombreRepuesto ON Repuesto;
DROP INDEX IF EXISTS NombreCliente ON Cliente;
DROP INDEX IF EXISTS OrdenDiaria ON Orden;
DROP INDEX IF EXISTS FechaFormaPagoV ON FormaPagoV;
DROP INDEX IF EXISTS FechaCompra ON FacturaCompra;
DROP INDEX IF EXISTS FechaVenta ON FacturaVenta;
DROP INDEX IF EXISTS StatusVentaWeb ON VentaWeb;
DROP INDEX IF EXISTS F_asigReparacion ON Reparacion;

-- create all indexes
CREATE UNIQUE INDEX NombreRepuesto ON Repuesto(Nombre);
CREATE UNIQUE INDEX NombreCliente ON Cliente(Nombre);
CREATE INDEX OrdenDiaria ON Orden(F_inicio, F_fin, Estado, Total);
CREATE INDEX FechaFormaPagoV ON FormaPagoV(Fecha);
CREATE INDEX FechaCompra ON FacturaCompra(Fecha);
CREATE INDEX FechaVenta ON FacturaVenta(Fecha);
CREATE INDEX StatusVentaWeb ON VentaWeb(Estado);
CREATE INDEX F_asigReparacion ON Reparacion(F_asig, CI_Personal);


--
-- VIEWS STRUCTURE
--

-- drop all views
DROP VIEW IF EXISTS Reporte_Diario_Equipos;
DROP VIEW IF EXISTS Ventas_Web_Diarias;
DROP VIEW IF EXISTS Equipos_Tecnicos_Diarios;
DROP VIEW IF EXISTS Pagos_Semanal;
DROP VIEW IF EXISTS Ingresos_Obtenidos;
DROP VIEW IF EXISTS Ventas_Realizadas;
DROP VIEW IF EXISTS Repuestos_Utilizados;
DROP VIEW IF EXISTS Existencia_Repuestos;
DROP VIEW IF EXISTS Estado_Venta_Web_Diaria;

-- create all views
CREATE VIEW Reporte_Diario_Equipos AS
SELECT Equipo.Nombre AS nombre_equipo, Equipo.Serial_Eq AS serial_equipo, Orden.Estado AS estado_orden 
FROM ((Orden INNER JOIN Orden_Equipo ON Orden.Numero = Orden_Equipo.Num_Orden) 
      INNER JOIN Equipo ON Orden_Equipo.Serial_Equipo = Equipo.Serial_Eq) 
WHERE (DATE(Orden.F_inicio) = CURRENT_DATE)
ORDER BY Orden.Estado, Orden.F_inicio DESC;

CREATE VIEW Ventas_Web_Diarias AS
SELECT VentaWeb.Numero AS num_venta, VentaWeb.Concepto AS concepto_venta, VentaWeb.Estado
AS estado_venta_web, VentaWeb.Num_Factura AS num_factura_venta, VentaWeb.Identif_Cliente 
AS identif_cliente, Cliente.Tipo_Identif AS tipo_identif_cliente, Cliente.Nombre AS nombre_cliente,
FacturaVenta.Fecha AS fecha_venta, FacturaVenta_EquipoVenta.Cantidad AS cantidad_equipo_venta,
FacturaVenta_EquipoVenta.PVP AS pvp_equipo_venta FROM 
(((VentaWeb INNER JOIN FacturaVenta ON VentaWeb.Num_Factura = FacturaVenta.Numero)
		   INNER JOIN Cliente ON VentaWeb.Identif_Cliente = Cliente.Identif)
		   INNER JOIN FacturaVenta_EquipoVenta ON FacturaVenta.Numero = FacturaVenta_EquipoVenta.Num_Factura)
WHERE (DATE(FacturaVenta.Fecha) = CURRENT_DATE)
ORDER BY FacturaVenta.Fecha, VentaWeb.Estado DESC;

CREATE VIEW Equipos_Tecnicos_Diarios AS 
SELECT Personal.Nombre AS tecnico, Personal.CI AS cedula_personal, Equipo.Serial_Eq AS serial_equipo,
Equipo.Nombre AS nombre_equipo, Reparacion.Estado AS estado_reparacion, Orden.Estado AS estado_orden 
FROM ((((Orden INNER JOIN Orden_Equipo ON Orden.Numero = Orden_Equipo.Num_Orden) 
               INNER JOIN Equipo ON Orden_Equipo.Serial_Equipo = Equipo.Serial_Eq) 
               INNER JOIN Reparacion ON Orden.Numero = Reparacion.Numero_Orden)  
               INNER JOIN Personal ON Personal.CI = Reparacion.CI_Personal) 
WHERE (DATE(Orden.F_inicio) = CURRENT_DATE)  
ORDER BY Personal.CI, Orden.F_inicio DESC;

CREATE VIEW Pagos_Semanal AS 
SELECT Cliente.Identif AS identif_cliente, Cliente.Nombre AS nombre_cliente, Cliente.Direccion
AS direccion_cliente, Cliente.Telefono AS telefono_cliente, Cliente.Email AS email_cliente,
CuentaCobrar.Monto AS monto_cuenta_cobrar, FormaPagoV.Descripcion AS descripcion_forma_pago_v
FROM (((Cliente INNER JOIN CuentaCobrar ON Cliente.Identif = CuentaCobrar.Identif_Cliente) 
                INNER JOIN CuentaCobrar_FormaPagoV ON CuentaCobrar.Numero = CuentaCobrar_FormaPagoV.Num_Cuenta) 
                INNER JOIN FormaPagoV ON CuentaCobrar_FormaPagoV.Fecha_Pago = FormaPagoV.Fecha)
WHERE (DATE(FormaPagoV.Fecha) >= DATE_SUB(CURRENT_DATE, INTERVAL 6 DAY) AND DATE(FormaPagoV.Fecha) <= (CURRENT_DATE))
ORDER BY FormaPagoV.Fecha DESC;

CREATE VIEW Ingresos_Obtenidos AS
SELECT Categoria.ID AS id_categoria, Categoria.Nombre AS nombre_categoria, SUM(Orden.Total) AS monto_total FROM
(((Equipo INNER JOIN Orden_Equipo ON Equipo.Serial_Eq = Orden_Equipo.Serial_Equipo)
		 INNER JOIN Orden ON Orden_Equipo.Num_Orden = Orden.Numero)
     INNER JOIN Categoria ON Equipo.ID_Categoria = Categoria.ID)
WHERE (Orden.Estado = 'REP' AND DATE(Orden.F_fin) >= DATE_SUB(CURRENT_DATE, INTERVAL 6 DAY) AND DATE(Orden.F_fin) <= (CURRENT_DATE))
GROUP BY Equipo.ID_Categoria
ORDER BY Categoria.ID, Orden.F_fin DESC;

CREATE VIEW Ventas_Realizadas AS
SELECT Cliente.Identif AS identif_cliente, Cliente.Nombre AS nombre_cliente, Cliente.Direccion
AS direccion_cliente, Cliente.Telefono AS telefono_cliente, Cliente.Email AS email_cliente,
EquipoVenta.Nombre AS articulo, FacturaVenta.Monto AS monto, FacturaVenta_EquipoVenta.Cantidad AS cantidad,
FacturaVenta.Fecha AS fecha FROM
(((Cliente INNER JOIN FacturaVenta ON Cliente.Identif = FacturaVenta.Identif_Cliente)
	     INNER JOIN FacturaVenta_EquipoVenta ON FacturaVenta.Numero = FacturaVenta_EquipoVenta.Num_Factura)
	     INNER JOIN EquipoVenta ON EquipoVenta.Codigo = FacturaVenta_EquipoVenta.Codigo_Equipo)
WHERE (DATE(FacturaVenta.Fecha) >= DATE_SUB(CURRENT_DATE, INTERVAL 6 DAY) AND DATE(FacturaVenta.Fecha) <= (CURRENT_DATE))
ORDER BY FacturaVenta.Fecha DESC;

CREATE VIEW Repuestos_Utilizados AS
SELECT Repuesto.Nombre AS nombre_repuesto, Repuesto.Precio AS precio_repuesto_actual,
Repuesto_Reparacion.Cant_Rep AS cantidad_usada, Repuesto_Reparacion.Precio_Rep AS precio_repuesto_uso,
Orden.Numero AS numero_orden, Orden.F_inicio AS fecha_inicio_orden,
Orden.F_fin AS fecha_fin_orden, Orden.Estado AS estado_orden, Orden.Total AS total_orden FROM
(((Repuesto INNER JOIN Repuesto_Reparacion ON Repuesto.Codigo = Repuesto_Reparacion.Codigo_Repues)
          INNER JOIN Reparacion ON Repuesto_Reparacion.F_asig_Rep = Reparacion.F_asig AND
          Repuesto_Reparacion.CI_Pers_Rep = Reparacion.CI_Personal AND
          Repuesto_Reparacion.Num_Ord_Rep = Reparacion.Numero_Orden)
          INNER JOIN Orden ON Reparacion.Numero_Orden = Orden.Numero)
WHERE (DATE(Reparacion.F_asig) >= DATE_SUB(CURRENT_DATE, INTERVAL 6 DAY) AND DATE(Reparacion.F_asig) <= (CURRENT_DATE))
ORDER BY Reparacion.F_asig DESC;

CREATE VIEW Existencia_Repuestos AS 
SELECT Codigo, Nombre, Cantidad FROM Repuesto WHERE (Cantidad > 0)
ORDER BY Nombre;

CREATE VIEW Estado_Venta_Web_Diaria AS
SELECT VentaWeb.Numero, VentaWeb.Concepto, VentaWeb.Estado FROM
VentaWeb INNER JOIN FacturaVenta ON VentaWeb.Num_Factura = FacturaVenta.Numero
WHERE (DATE(FacturaVenta.Numero) >= DATE_SUB(CURRENT_DATE, INTERVAL 6 DAY) AND DATE(FacturaVenta.Numero) <= (CURRENT_DATE))
ORDER BY VentaWeb.Estado;

--
-- FUNCTIONS STRUCTURE
--

-- drop all functions
DROP FUNCTION IF EXISTS IsNumber;
DROP FUNCTION IF EXISTS IsEmail;
DROP FUNCTION IF EXISTS NoSpaces;
DROP FUNCTION IF EXISTS SplitString;

DELIMITER //

-- create all functions
CREATE FUNCTION IsNumber (str varchar(255)) RETURNS tinyint
RETURN str REGEXP '^[0-9]+$';//

CREATE FUNCTION IsEmail (str varchar(255)) RETURNS tinyint
RETURN str REGEXP '^[a-zA-Z0-9][a-zA-Z0-9._-]*@[a-zA-Z0-9][a-zA-Z0-9._-]*\\.[a-zA-Z]{2,4}$';//

CREATE FUNCTION NoSpaces (str varchar(255)) RETURNS tinyint
RETURN str NOT REGEXP '[[:space:]]';//

CREATE FUNCTION SplitString (str text, delim char(1), pos int) RETURNS text
RETURN REPLACE(
	SUBSTRING(
		SUBSTRING_INDEX(str, delim, pos),
		CHAR_LENGTH(
			SUBSTRING_INDEX(str, delim, pos - 1)
		) + 1
	),
	delim,
	''
)//

DELIMITER ;

--
-- PROCEDURES STRUCTURE
--

-- drop all procedures
DROP PROCEDURE IF EXISTS Generar_Nueva_Orden_Cliente;
DROP PROCEDURE IF EXISTS Reporte_Diario_Equipos;
DROP PROCEDURE IF EXISTS Ventas_Web_Diarias;
DROP PROCEDURE IF EXISTS Equipos_Tecnicos_Diarios;
DROP PROCEDURE IF EXISTS Pagos_Semanal;
DROP PROCEDURE IF EXISTS Ingresos_Obtenidos;
DROP PROCEDURE IF EXISTS Ventas_Realizadas;
DROP PROCEDURE IF EXISTS Repuestos_Utilizados;

-- create all procedures

DELIMITER //

-- transaction A
CREATE PROCEDURE Generar_Nueva_Orden_Cliente(
Tipo_Identif_Cliente enum('V', 'E', 'J', 'G'),
Identif_Cliente varchar(15),
Nombre_Cliente varchar(50),
Email_Cliente varchar(100),
Direccion_Cliente varchar(50),
Telefono_Cliente varchar(20),
Numero_Orden varchar(5),
F_inicio_Orden timestamp,
F_fin_Orden timestamp,
Estado_Orden enum('ESP', 'REV', 'REP', 'DEV'),
Total_Orden decimal(9,2),
IN IDs_Categorias_Equipos text,
IN Seriales_Equipos text,
IN Nombres_Equipos text,
IN Descripcion_Equipos text,
IN Precios_Equipos text,
IN Fallas_Equipos text
)
Proc_Generar_Nueva_Orden_Cliente:BEGIN

	DECLARE CONTINUE HANDLER FOR SQLEXCEPTION SET @code = '45000';

	START TRANSACTION;

		SET @code = '00000';

		INSERT INTO Cliente (Tipo_Identif, Identif, Nombre, Email, Direccion, Telefono) 
		VALUES (Tipo_Identif_Cliente, Identif_Cliente, Nombre_Cliente, Email_Cliente, Direccion_Cliente, Telefono_Cliente);
		
		IF @code = '45000' THEN
			ROLLBACK;
			SELECT 'Error insertando los datos del cliente' AS ERROR_MESSAGE;
			LEAVE Proc_Generar_Nueva_Orden_Cliente;
		END IF;

		SET @code = '00000';

		INSERT INTO Orden (Numero, F_inicio, F_fin, Estado, Total)
		VALUES (Numero_Orden, F_inicio_Orden, F_fin_Orden, Estado_Orden, Total_Orden);

		IF @code = '45000' THEN
		   	ROLLBACK;
			SELECT 'Error insertando los datos de la orden' AS ERROR_MESSAGE;
			LEAVE Proc_Generar_Nueva_Orden_Cliente;
		END IF;

        -- validate data consistence
		SET @CantIdsCategorias = CHAR_LENGTH(IDs_Categorias_Equipos) - CHAR_LENGTH(REPLACE(IDs_Categorias_Equipos, ',', ''));
		SET @CantSerialesEquipos = CHAR_LENGTH(Seriales_Equipos) - CHAR_LENGTH(REPLACE(Seriales_Equipos, ',', ''));
		SET @CantNombresEquipos = CHAR_LENGTH(Nombres_Equipos) - CHAR_LENGTH(REPLACE(Nombres_Equipos, ',', ''));
		SET @CantDescripcionEquipos = CHAR_LENGTH(Descripcion_Equipos) - CHAR_LENGTH(REPLACE(Descripcion_Equipos, ',', ''));
		SET @CantPreciosEquipos = CHAR_LENGTH(Precios_Equipos) - CHAR_LENGTH(REPLACE(Precios_Equipos, ',', ''));
		SET @CantFallasEquipos = CHAR_LENGTH(Fallas_Equipos) - CHAR_LENGTH(REPLACE(Fallas_Equipos, ',', ''));

		IF NOT(@CantIdsCategorias = @CantSerialesEquipos AND @CantSerialesEquipos = @CantNombresEquipos AND
			@CantNombresEquipos = @CantDescripcionEquipos AND @CantDescripcionEquipos = @CantPreciosEquipos AND
			@CantPreciosEquipos = @CantFallasEquipos) THEN
		       ROLLBACK;
		       SELECT 'Error con los datos del(los) equipo(s) que no coincide(n)' AS ERROR_MESSAGE;
		       LEAVE Proc_Generar_Nueva_Orden_Cliente;
		END IF;

		SET @Datos = @CantIdsCategorias;
		SET @Pos = 0;
		SET @signToSplit = ',';

		IF @Datos > 0 THEN
		    SET @Datos = @Datos + 1;
		END IF;

        -- while data is available
		WHILE (@Datos) DO

			-- set current pos for data
			SET @Pos = @Pos + 1;

		    -- first get the data to insert in current transaction
			SET @ID_Categoria = SplitString(IDs_Categorias_Equipos, @signToSplit, @Pos);
			SET @Serial_Equipo = SplitString(Seriales_Equipos, @signToSplit, @Pos);
			SET @Nombre_Equipo = SplitString(Nombres_Equipos, @signToSplit, @Pos);
			SET @Descripcion_Equipo = SplitString(Descripcion_Equipos, @signToSplit, @Pos);
			SET @Precio_Equipo = SplitString(Precios_Equipos, @signToSplit, @Pos);
			SET @Falla_Equipo = SplitString(Fallas_Equipos, @signToSplit, @Pos);

			SET @code = '00000';

			-- now insert the data in respective tables
			INSERT INTO Equipo (Serial_Eq, ID_Categoria, Identif_Cliente, Nombre, Descripcion, Precio) VALUES
			(@Serial_Equipo, @ID_Categoria, Identif_Cliente, @Nombre_Equipo, @Descripcion_Equipo, @Precio_Equipo);

			IF @code = '45000' THEN
		    	ROLLBACK;
				SELECT CONCAT('Error insertando los datos del equipo ', @Pos) AS ERROR_MESSAGE;
				LEAVE Proc_Generar_Nueva_Orden_Cliente;
			END IF;

			SET @code = '00000';

			INSERT INTO Orden_Equipo (Num_Orden, Serial_Equipo, Falla_Equipo) VALUES
			(Numero_Orden, @Serial_Equipo, @Falla_Equipo);

			IF @code = '45000' THEN
		    	ROLLBACK;
		    	SELECT CONCAT('Error insertando los datos del equipo ', @Pos, ', asociado a la orden') AS ERROR_MESSAGE;
		    	LEAVE Proc_Generar_Nueva_Orden_Cliente;
			END IF;

			-- remove item from data
			SET @Datos = @Datos - 1;

		END WHILE;

	COMMIT;

END;//

-- transaction B
CREATE PROCEDURE Reporte_Diario_Equipos()
BEGIN

	START TRANSACTION;

		SELECT Equipo.Nombre AS nombre_equipo, Equipo.Serial_Eq AS serial_equipo, Orden.Estado AS estado_orden 
		FROM ((Orden INNER JOIN Orden_Equipo ON Orden.Numero = Orden_Equipo.Num_Orden) 
	      INNER JOIN Equipo ON Orden_Equipo.Serial_Equipo = Equipo.Serial_Eq) 
		WHERE (DATE(Orden.F_inicio) = CURRENT_DATE)
		ORDER BY Orden.Estado, Orden.F_inicio DESC;

	COMMIT;

END;//

-- transaction C
CREATE PROCEDURE Ventas_Web_Diarias()
BEGIN

	START TRANSACTION;

		SELECT VentaWeb.Numero AS num_venta, VentaWeb.Concepto AS concepto_venta, VentaWeb.Estado
		AS estado_venta_web, VentaWeb.Num_Factura AS num_factura_venta, VentaWeb.Identif_Cliente 
		AS identif_cliente, Cliente.Tipo_Identif AS tipo_identif_cliente, Cliente.Nombre AS nombre_cliente,
		FacturaVenta.Fecha AS fecha_venta, FacturaVenta_EquipoVenta.Cantidad AS cantidad_equipo_venta,
		FacturaVenta_EquipoVenta.PVP AS pvp_equipo_venta FROM 
		(((VentaWeb INNER JOIN FacturaVenta ON VentaWeb.Num_Factura = FacturaVenta.Numero)
				   INNER JOIN Cliente ON VentaWeb.Identif_Cliente = Cliente.Identif)
				   INNER JOIN FacturaVenta_EquipoVenta ON FacturaVenta.Numero = FacturaVenta_EquipoVenta.Num_Factura)
		WHERE (DATE(FacturaVenta.Fecha) = CURRENT_DATE)
		ORDER BY FacturaVenta.Fecha, VentaWeb.Estado DESC;

	COMMIT;

END;//

-- transaction D
CREATE PROCEDURE Equipos_Tecnicos_Diarios() 
BEGIN

	START TRANSACTION;

		SELECT Personal.Nombre AS tecnico, Personal.CI AS cedula_personal, Equipo.Serial_Eq AS serial_equipo,
		Equipo.Nombre AS nombre_equipo, Reparacion.Estado AS estado_reparacion, Orden.Estado AS estado_orden 
		FROM ((((Orden INNER JOIN Orden_Equipo ON Orden.Numero = Orden_Equipo.Num_Orden) 
		               INNER JOIN Equipo ON Orden_Equipo.Serial_Equipo = Equipo.Serial_Eq) 
		               INNER JOIN Reparacion ON Orden.Numero = Reparacion.Numero_Orden)  
		               INNER JOIN Personal ON Personal.CI = Reparacion.CI_Personal) 
		WHERE (DATE(Orden.F_inicio) = CURRENT_DATE)  
		ORDER BY Personal.CI, Orden.F_inicio DESC;

	COMMIT;

END;//

-- transaction E
CREATE PROCEDURE Pagos_Semanal()
BEGIN

	START TRANSACTION;

		SELECT Cliente.Identif AS identif_cliente, Cliente.Nombre AS nombre_cliente, Cliente.Direccion
		AS direccion_cliente, Cliente.Telefono AS telefono_cliente, Cliente.Email AS email_cliente,
		CuentaCobrar.Monto AS monto_cuenta_cobrar, FormaPagoV.Descripcion AS descripcion_forma_pago_v
		FROM (((Cliente INNER JOIN CuentaCobrar ON Cliente.Identif = CuentaCobrar.Identif_Cliente) 
	                INNER JOIN CuentaCobrar_FormaPagoV ON CuentaCobrar.Numero = CuentaCobrar_FormaPagoV.Num_Cuenta) 
	                INNER JOIN FormaPagoV ON CuentaCobrar_FormaPagoV.Fecha_Pago = FormaPagoV.Fecha)
		WHERE (DATE(FormaPagoV.Fecha) >= DATE_SUB(CURRENT_DATE, INTERVAL 6 DAY) AND DATE(FormaPagoV.Fecha) <= (CURRENT_DATE))
		ORDER BY FormaPagoV.Fecha DESC;

	COMMIT;

END;//

-- transaction F
CREATE PROCEDURE Ingresos_Obtenidos()
BEGIN

	START TRANSACTION;

		SELECT Categoria.ID AS id_categoria, Categoria.Nombre AS nombre_categoria, SUM(Orden.Total) AS monto_total FROM
		(((Equipo INNER JOIN Orden_Equipo ON Equipo.Serial_Eq = Orden_Equipo.Serial_Equipo)
				 INNER JOIN Orden ON Orden_Equipo.Num_Orden = Orden.Numero)
		         INNER JOIN Categoria ON Equipo.ID_Categoria = Categoria.ID)
		WHERE (Orden.Estado = 'REP' AND DATE(Orden.F_fin) >= DATE_SUB(CURRENT_DATE, INTERVAL 6 DAY) AND DATE(Orden.F_fin) <= (CURRENT_DATE))
		GROUP BY Equipo.ID_Categoria
		ORDER BY Categoria.ID, Orden.F_fin DESC;

	COMMIT;

END;//

-- transaction G
CREATE PROCEDURE Ventas_Realizadas()
BEGIN
	
	START TRANSACTION;

		SELECT Cliente.Identif AS identif_cliente, Cliente.Nombre AS nombre_cliente, Cliente.Direccion
		AS direccion_cliente, Cliente.Telefono AS telefono_cliente, Cliente.Email AS email_cliente,
		EquipoVenta.Nombre AS articulo, FacturaVenta.Monto AS monto, FacturaVenta_EquipoVenta.Cantidad AS cantidad,
		FacturaVenta.Fecha AS fecha FROM
		(((Cliente INNER JOIN FacturaVenta ON Cliente.Identif = FacturaVenta.Identif_Cliente)
			     INNER JOIN FacturaVenta_EquipoVenta ON FacturaVenta.Numero = FacturaVenta_EquipoVenta.Num_Factura)
			     INNER JOIN EquipoVenta ON EquipoVenta.Codigo = FacturaVenta_EquipoVenta.Codigo_Equipo)
		WHERE (DATE(FacturaVenta.Fecha) >= DATE_SUB(CURRENT_DATE, INTERVAL 6 DAY) AND DATE(FacturaVenta.Fecha) <= (CURRENT_DATE))
		ORDER BY FacturaVenta.Fecha DESC;

	COMMIT;

END;//

-- transaction H
CREATE PROCEDURE Repuestos_Utilizados()
BEGIN

	START TRANSACTION;

		SELECT Repuesto.Nombre AS nombre_repuesto, Repuesto.Precio AS precio_repuesto_actual,
		Repuesto_Reparacion.Cant_Rep AS cantidad_usada, Repuesto_Reparacion.Precio_Rep AS precio_repuesto_uso,
		Orden.Numero AS numero_orden, Orden.F_inicio AS fecha_inicio_orden,
		Orden.F_fin AS fecha_fin_orden, Orden.Estado AS estado_orden, Orden.Total AS total_orden FROM
		(((Repuesto INNER JOIN Repuesto_Reparacion ON Repuesto.Codigo = Repuesto_Reparacion.Codigo_Repues)
		          INNER JOIN Reparacion ON Repuesto_Reparacion.F_asig_Rep = Reparacion.F_asig AND
		          Repuesto_Reparacion.CI_Pers_Rep = Reparacion.CI_Personal AND
		          Repuesto_Reparacion.Num_Ord_Rep = Reparacion.Numero_Orden)
		          INNER JOIN Orden ON Reparacion.Numero_Orden = Orden.Numero)
		WHERE (DATE(Reparacion.F_asig) >= DATE_SUB(CURRENT_DATE, INTERVAL 6 DAY) AND DATE(Reparacion.F_asig) <= (CURRENT_DATE))
		ORDER BY Reparacion.F_asig DESC;

	COMMIT;

END;//

DELIMITER ;

--
-- TRIGGERS STRUCTURE
--

-- drop all triggers
DROP TRIGGER IF EXISTS b_insert_tienda;
DROP TRIGGER IF EXISTS b_update_tienda;
DROP TRIGGER IF EXISTS b_insert_cuenta;
DROP TRIGGER IF EXISTS b_update_cuenta;
DROP TRIGGER IF EXISTS b_insert_personal;
DROP TRIGGER IF EXISTS b_update_personal;
DROP TRIGGER IF EXISTS b_insert_inventario;
DROP TRIGGER IF EXISTS b_update_inventario;
DROP TRIGGER IF EXISTS b_insert_repuesto;
DROP TRIGGER IF EXISTS b_update_repuesto;
DROP TRIGGER IF EXISTS b_insert_herramienta;
DROP TRIGGER IF EXISTS b_update_herramienta;
DROP TRIGGER IF EXISTS b_insert_equipo_venta;
DROP TRIGGER IF EXISTS b_update_equipo_venta;
DROP TRIGGER IF EXISTS b_insert_proveedor;
DROP TRIGGER IF EXISTS b_update_proveedor;
DROP TRIGGER IF EXISTS b_insert_factura_compra;
DROP TRIGGER IF EXISTS b_update_factura_compra;
DROP TRIGGER IF EXISTS b_insert_cliente;
DROP TRIGGER IF EXISTS b_update_cliente;
DROP TRIGGER IF EXISTS b_insert_notificacion;
DROP TRIGGER IF EXISTS b_update_notificacion;
DROP TRIGGER IF EXISTS b_insert_equipo;
DROP TRIGGER IF EXISTS b_update_equipo;
DROP TRIGGER IF EXISTS b_insert_orden;
DROP TRIGGER IF EXISTS b_update_orden;
DROP TRIGGER IF EXISTS b_insert_factura_venta;
DROP TRIGGER IF EXISTS b_update_factura_venta;
DROP TRIGGER IF EXISTS b_insert_venta_web;
DROP TRIGGER IF EXISTS b_update_venta_web;
DROP TRIGGER IF EXISTS b_insert_reparacion;
DROP TRIGGER IF EXISTS b_update_reparacion;
DROP TRIGGER IF EXISTS b_insert_repuesto_reparacion;
DROP TRIGGER IF EXISTS b_update_repuesto_reparacion;
DROP TRIGGER IF EXISTS b_insert_cuenta_cobrar;
DROP TRIGGER IF EXISTS b_update_cuenta_cobrar;
DROP TRIGGER IF EXISTS b_insert_forma_pago_v;
DROP TRIGGER IF EXISTS b_update_forma_pago_v;
DROP TRIGGER IF EXISTS b_insert_cuenta_cobrar_forma_pago_v;
DROP TRIGGER IF EXISTS b_update_cuenta_cobrar_forma_pago_v;
DROP TRIGGER IF EXISTS b_insert_factura_venta_forma_pago_v;
DROP TRIGGER IF EXISTS b_update_factura_venta_forma_pago_v;
DROP TRIGGER IF EXISTS b_insert_cuenta_forma_pago_v;
DROP TRIGGER IF EXISTS b_update_cuenta_forma_pago_v;
DROP TRIGGER IF EXISTS b_insert_cuenta_pagar;
DROP TRIGGER IF EXISTS b_update_cuenta_pagar;
DROP TRIGGER IF EXISTS b_insert_forma_pago_c;
DROP TRIGGER IF EXISTS b_update_forma_pago_c;
DROP TRIGGER IF EXISTS b_insert_cuenta_pagar_forma_pago_c;
DROP TRIGGER IF EXISTS b_update_cuenta_pagar_forma_pago_c;
DROP TRIGGER IF EXISTS b_insert_factura_compra_forma_pago_c;
DROP TRIGGER IF EXISTS b_update_factura_compra_forma_pago_c;
DROP TRIGGER IF EXISTS b_insert_cuenta_forma_pago_c;
DROP TRIGGER IF EXISTS b_update_cuenta_forma_pago_c;
DROP TRIGGER IF EXISTS b_insert_orden_factura_venta;
DROP TRIGGER IF EXISTS b_update_orden_factura_venta;
DROP TRIGGER IF EXISTS b_insert_orden_equipo;
DROP TRIGGER IF EXISTS b_update_orden_equipo;
DROP TRIGGER IF EXISTS b_insert_factura_venta_equipo_venta;
DROP TRIGGER IF EXISTS b_update_factura_venta_equipo_venta;
DROP TRIGGER IF EXISTS b_insert_venta_web_equipo_venta;
DROP TRIGGER IF EXISTS b_update_venta_web_equipo_venta;
DROP TRIGGER IF EXISTS b_insert_factura_compra_inventario;
DROP TRIGGER IF EXISTS b_update_factura_compra_inventario;

DELIMITER //

-- create all triggers
CREATE TRIGGER b_insert_tienda BEFORE INSERT ON Tienda
FOR EACH ROW 
BEGIN

	IF NOT (NEW.RIF > 0 AND NEW.RIF <= 9999999999) THEN
		BEGIN
			SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'RIF invalido';
		END;
	END IF;
	
	IF NOT IsEmail(NEW.Email) THEN
		BEGIN
			SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Email invalido';
		END;
	END IF;

	IF NOT IsNumber(NEW.Telefono) THEN
		BEGIN
			SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Telefono invalido';
		END;
	END IF;

END;//

CREATE TRIGGER b_update_tienda BEFORE UPDATE ON Tienda
FOR EACH ROW 
BEGIN

	IF NOT (NEW.RIF > 0 AND NEW.RIF <= 9999999999) THEN
		BEGIN
			SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'RIF invalido';
		END;
	END IF;

	IF NOT IsEmail(NEW.Email) THEN
		BEGIN
			SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Email invalido';
		END;
	END IF;

	IF NOT IsNumber(NEW.Telefono) THEN
		BEGIN
			SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Telefono invalido';
		END;
	END IF;

END;//

CREATE TRIGGER b_insert_cuenta BEFORE INSERT ON Cuenta
FOR EACH ROW 
BEGIN

	IF NOT (NEW.RIF_tienda > 0 AND NEW.RIF_tienda <= 9999999999) THEN
		BEGIN
			SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'RIF de la tienda invalido';
		END;
	END IF;

	IF NOT IsNumber(NEW.Numero) THEN
		BEGIN
			SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Numero de cuenta invalido';
		END;
	END IF;

END;//

CREATE TRIGGER b_update_cuenta BEFORE UPDATE ON Cuenta
FOR EACH ROW 
BEGIN

	IF NOT (NEW.RIF_tienda > 0 AND NEW.RIF_tienda <= 9999999999) THEN
		BEGIN
			SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'RIF de la tienda invalido';
		END;
	END IF;

	IF NOT IsNumber(NEW.Numero) THEN
		BEGIN
			SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Numero de cuenta invalido';
		END;
	END IF;

END;//

CREATE TRIGGER b_insert_personal BEFORE INSERT ON Personal
FOR EACH ROW
BEGIN

	IF NOT (NEW.CI > 0 AND NEW.CI <= 999999999) THEN
		BEGIN
			SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Cedula invalida';
		END;
	END IF;

	IF NOT (NEW.RIF > 0 AND NEW.RIF <= 9999999999) THEN
		BEGIN
			SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'RIF invalido';
		END;
	END IF;

	IF NOT IsEmail(NEW.Email) THEN
		BEGIN
			SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Email invalido';
		END;
	END IF;

	IF NOT IsNumber(NEW.Telefono) THEN
		BEGIN
			SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Telefono invalido';
		END;
	END IF;

	IF NOT (NEW.Tipo = 'TE') THEN
		BEGIN
			SET NEW.Especialidad = NULL;
		END;
	END IF;

END;//

CREATE TRIGGER b_update_personal BEFORE UPDATE ON Personal
FOR EACH ROW
BEGIN

	IF NOT (NEW.CI > 0 AND NEW.CI <= 999999999) THEN
		BEGIN
			SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Cedula invalida';
		END;
	END IF;

	IF NOT (NEW.RIF > 0 AND NEW.RIF <= 9999999999) THEN
		BEGIN
			SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'RIF invalido';
		END;
	END IF;

	IF NOT IsEmail(NEW.Email) THEN
		BEGIN
			SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Email invalido';
		END;
	END IF;

	IF NOT IsNumber(NEW.Telefono) THEN
		BEGIN
			SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Telefono invalido';
		END;
	END IF;

	IF NOT (NEW.Tipo = 'TE') THEN
		BEGIN
			SET NEW.Especialidad = NULL;
		END;
	END IF;

END;//

CREATE TRIGGER b_insert_inventario BEFORE INSERT ON Inventario
FOR EACH ROW
BEGIN

	IF NOT NoSpaces(NEW.Codigo) THEN
		BEGIN
			SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Codigo invalido';
		END;
	END IF;

	IF NOT (NEW.Cantidad > 0 AND NEW.Cantidad <= 9999) THEN
		BEGIN
			SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Cantidad invalida';
		END;
	END IF;

END;//

CREATE TRIGGER b_update_inventario BEFORE UPDATE ON Inventario
FOR EACH ROW
BEGIN

	IF NOT NoSpaces(NEW.Codigo) THEN
		BEGIN
			SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Codigo invalido';
		END;
	END IF;

	IF NOT (NEW.Cantidad > 0 AND NEW.Cantidad <= 9999) THEN
		BEGIN
			SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Cantidad invalida';
		END;
	END IF;

	UPDATE Repuesto SET Cantidad = NEW.Cantidad WHERE Codigo = NEW.Codigo;
	UPDATE Herramienta SET Cantidad = NEW.Cantidad WHERE Codigo = NEW.Codigo;
	UPDATE EquipoVenta SET Cantidad = NEW.Cantidad WHERE Codigo = NEW.Codigo;

END;//

CREATE TRIGGER b_insert_repuesto BEFORE INSERT ON Repuesto
FOR EACH ROW
BEGIN

	IF NOT NoSpaces(NEW.Codigo) THEN
		BEGIN
			SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Codigo invalido';
		END;
	END IF;

	IF NOT (NEW.Cantidad > 0 AND NEW.Cantidad <= 9999) THEN
		BEGIN
			SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Cantidad invalida';
		END;
	END IF;

	IF NOT (NEW.Precio >= 0) THEN
		BEGIN
			SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Precio invalido';
		END;
	END IF;

	SELECT COUNT(Codigo) INTO @CantHerramienta FROM Herramienta WHERE Codigo = NEW.Codigo LIMIT 1;

	IF @CantHerramienta > 0 THEN
		SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'El codigo ya se encuentra registrado como Herramienta';
	END IF;

	SELECT COUNT(Codigo) INTO @CantEquipVenta FROM EquipoVenta WHERE Codigo = NEW.Codigo LIMIT 1;

	IF @CantEquipVenta > 0 THEN
		SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'El codigo ya se encuentra registrado como EquipoVenta';
	END IF;

	SELECT Cantidad INTO @CantExists FROM Inventario WHERE Codigo = NEW.Codigo LIMIT 1;

	IF @CantExists THEN
		BEGIN
			SET NEW.Cantidad = @CantExists;
		END;
	END IF;

END;//

CREATE TRIGGER b_update_repuesto BEFORE UPDATE ON Repuesto
FOR EACH ROW
BEGIN

	IF NOT NoSpaces(NEW.Codigo) THEN
		BEGIN
			SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Codigo invalido';
		END;
	END IF;

	IF NOT (NEW.Cantidad > 0 AND NEW.Cantidad <= 9999) THEN
		BEGIN
			SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Cantidad invalida';
		END;
	END IF;

	IF NOT (NEW.Precio >= 0) THEN
		BEGIN
			SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Precio invalido';
		END;
	END IF;

	SELECT COUNT(Codigo) INTO @CantHerramienta FROM Herramienta WHERE Codigo = NEW.Codigo LIMIT 1;

	IF @CantHerramienta > 0 THEN
		SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'El codigo ya se encuentra registrado como Herramienta';
	END IF;

	SELECT COUNT(Codigo) INTO @CantEquipVenta FROM EquipoVenta WHERE Codigo = NEW.Codigo LIMIT 1;

	IF @CantEquipVenta > 0 THEN
		SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'El codigo ya se encuentra registrado como EquipoVenta';
	END IF;

	SET NEW.Cantidad = OLD.Cantidad;

END;//

CREATE TRIGGER b_insert_herramienta BEFORE INSERT ON Herramienta
FOR EACH ROW
BEGIN

	IF NOT NoSpaces(NEW.Codigo) THEN
		BEGIN
			SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Codigo invalido';
		END;
	END IF;

	IF NOT (NEW.CI_Personal > 0 AND NEW.CI_Personal <= 999999999) THEN
		BEGIN
			SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Cedula del personal invalida';
		END;
	END IF;

	IF NOT (NEW.Cantidad > 0 AND NEW.Cantidad <= 9999) THEN
		BEGIN
			SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Cantidad invalida';
		END;
	END IF;

	IF NOT (NEW.Precio >= 0) THEN
		BEGIN
			SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Precio invalido';
		END;
	END IF;

	IF NOT (NEW.Cant_Uso > 0 AND NEW.Cant_Uso <= 9999) THEN
		BEGIN
			SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Cantidad de uso invalida';
		END;
	END IF;

	IF NOT (NEW.Precio_Uso >= 0) THEN
		BEGIN
			SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Precio de uso invalido';
		END;
	END IF;

	IF NOT (NEW.Cant_Uso <= NEW.Cantidad) THEN
		BEGIN
			SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'La cantidad de uso supera la cantidad en existencia';
		END;
	END IF;

	SELECT COUNT(Codigo) INTO @CantRep FROM Repuesto WHERE Codigo = NEW.Codigo LIMIT 1;

	IF @CantRep > 0 THEN
		SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'El codigo ya se encuentra registrado como Repuesto';
	END IF;

	SELECT COUNT(Codigo) INTO @CantEquipVenta FROM EquipoVenta WHERE Codigo = NEW.Codigo LIMIT 1;

	IF @CantEquipVenta > 0 THEN
		SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'El codigo ya se encuentra registrado como EquipoVenta';
	END IF;
 
	SELECT Cantidad INTO @CantExists FROM Inventario WHERE Codigo = NEW.Codigo LIMIT 1;

	IF @CantExists THEN
		BEGIN
			SET NEW.Cantidad = @CantExists;
		END;
	END IF;

END;//

CREATE TRIGGER b_update_herramienta BEFORE UPDATE ON Herramienta
FOR EACH ROW
BEGIN

	IF NOT NoSpaces(NEW.Codigo) THEN
		BEGIN
			SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Codigo invalido';
		END;
	END IF;

	IF NOT (NEW.CI_Personal > 0 AND NEW.CI_Personal <= 999999999) THEN
		BEGIN
			SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Cedula del personal invalida';
		END;
	END IF;

	IF NOT (NEW.Cantidad > 0 AND NEW.Cantidad <= 9999) THEN
		BEGIN
			SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Cantidad invalida';
		END;
	END IF;

	IF NOT (NEW.Precio >= 0) THEN
		BEGIN
			SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Precio invalido';
		END;
	END IF;

	IF NOT (NEW.Cant_Uso > 0 AND NEW.Cant_Uso <= 9999) THEN
		BEGIN
			SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Cantidad de uso invalida';
		END;
	END IF;

	IF NOT (NEW.Precio_Uso >= 0) THEN
		BEGIN
			SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Precio de uso invalido';
		END;
	END IF;

	IF NOT (NEW.Cant_Uso <= NEW.Cantidad) THEN
		BEGIN
			SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'La cantidad de uso supera la cantidad en existencia';
		END;
	END IF;

	SELECT COUNT(Codigo) INTO @CantRep FROM Repuesto WHERE Codigo = NEW.Codigo LIMIT 1;

	IF @CantRep > 0 THEN
		SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'El codigo ya se encuentra registrado como Repuesto';
	END IF;

	SELECT COUNT(Codigo) INTO @CantEquipVenta FROM EquipoVenta WHERE Codigo = NEW.Codigo LIMIT 1;

	IF @CantEquipVenta > 0 THEN
		SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'El codigo ya se encuentra registrado como EquipoVenta';
	END IF;

	SET NEW.Cantidad = OLD.Cantidad;

END;//

CREATE TRIGGER b_insert_equipo_venta BEFORE INSERT ON EquipoVenta
FOR EACH ROW
BEGIN

	IF NOT NoSpaces(NEW.Codigo) THEN
		BEGIN
			SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Codigo invalido';
		END;
	END IF;

	IF NOT (NEW.Cantidad > 0 AND NEW.Cantidad <= 9999) THEN
		BEGIN
			SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Cantidad invalida';
		END;
	END IF;

	IF NOT (NEW.Precio >= 0) THEN
		BEGIN
			SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Precio invalido';
		END;
	END IF;

	SELECT COUNT(Codigo) INTO @CantRep FROM Repuesto WHERE Codigo = NEW.Codigo LIMIT 1;

	IF @CantRep > 0 THEN
		SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'El codigo ya se encuentra registrado como Repuesto';
	END IF;

	SELECT COUNT(Codigo) INTO @CantHerramienta FROM Herramienta WHERE Codigo = NEW.Codigo LIMIT 1;

	IF @CantHerramienta > 0 THEN
		SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'El codigo ya se encuentra registrado como Herramienta';
	END IF;

	SELECT Cantidad INTO @CantExists FROM Inventario WHERE Codigo = NEW.Codigo LIMIT 1;

	IF @CantExists THEN
		BEGIN
			SET NEW.Cantidad = @CantExists;
		END;
	END IF;

END;//

CREATE TRIGGER b_update_equipo_venta BEFORE UPDATE ON EquipoVenta
FOR EACH ROW
BEGIN

	IF NOT NoSpaces(NEW.Codigo) THEN
		BEGIN
			SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Codigo invalido';
		END;
	END IF;

	IF NOT (NEW.Cantidad > 0 AND NEW.Cantidad <= 9999) THEN
		BEGIN
			SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Cantidad invalida';
		END;
	END IF;

	IF NOT (NEW.Precio >= 0) THEN
		BEGIN
			SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Precio invalido';
		END;
	END IF;

	SELECT COUNT(Codigo) INTO @CantRep FROM Repuesto WHERE Codigo = NEW.Codigo LIMIT 1;

	IF @CantRep > 0 THEN
		SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'El codigo ya se encuentra registrado como Repuesto';
	END IF;

	SELECT COUNT(Codigo) INTO @CantHerramienta FROM Herramienta WHERE Codigo = NEW.Codigo LIMIT 1;

	IF @CantHerramienta > 0 THEN
		SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'El codigo ya se encuentra registrado como Herramienta';
	END IF;

	SET NEW.Cantidad = OLD.Cantidad;

END;//

CREATE TRIGGER b_insert_proveedor BEFORE INSERT ON Proveedor
FOR EACH ROW
BEGIN

	IF NOT (NEW.RIF > 0 AND NEW.RIF <= 9999999999) THEN
		BEGIN
			SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'RIF invalido';
		END;
	END IF;

	IF NOT IsEmail(NEW.Email) THEN
		BEGIN
			SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Email invalido';
		END;
	END IF;

	IF NOT IsNumber(NEW.Telefono) THEN
		BEGIN
			SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Telefono invalido';
		END;
	END IF;

END;//

CREATE TRIGGER b_update_proveedor BEFORE UPDATE ON Proveedor
FOR EACH ROW
BEGIN

	IF NOT (NEW.RIF > 0 AND NEW.RIF <= 9999999999) THEN
		BEGIN
			SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'RIF invalido';
		END;
	END IF;

	IF NOT IsEmail(NEW.Email) THEN
		BEGIN
			SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Email invalido';
		END;
	END IF;

	IF NOT IsNumber(NEW.Telefono) THEN
		BEGIN
			SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Telefono invalido';
		END;
	END IF;

END;//

CREATE TRIGGER b_insert_factura_compra BEFORE INSERT ON FacturaCompra
FOR EACH ROW
BEGIN

	IF NOT IsNumber(NEW.Numero) THEN
		BEGIN
			SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Numero de factura invalido';
		END;
	END IF;

	IF NOT (NEW.RIF_Proveed > 0 AND NEW.RIF_Proveed <= 9999999999) THEN
		BEGIN
			SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'RIF del proveedor invalido';	
		END;
	END IF;

	IF NOT (NEW.Monto >= 0) THEN
		BEGIN
			SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Monto invalido';
		END;
	END IF;

END;//

CREATE TRIGGER b_update_factura_compra BEFORE UPDATE ON FacturaCompra
FOR EACH ROW
BEGIN

	IF NOT IsNumber(NEW.Numero) THEN
		BEGIN
			SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Numero de factura invalido';
		END;
	END IF;

	IF NOT (NEW.RIF_Proveed > 0 AND NEW.RIF_Proveed <= 9999999999) THEN
		BEGIN
			SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'RIF del proveedor invalido';	
		END;
	END IF;

	IF NOT (NEW.Monto >= 0) THEN
		BEGIN
			SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Monto invalido';
		END;
	END IF;

END;//

CREATE TRIGGER b_insert_cliente BEFORE INSERT ON Cliente
FOR EACH ROW
BEGIN

	IF NOT IsNumber(NEW.Identif) THEN
		BEGIN
			SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Cedula/RIF/Pasaporte invalido';
		END;
	END IF;

	IF NOT IsEmail(NEW.Email) THEN
		BEGIN
			SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Email invalido';
		END;
	END IF;

	IF NOT IsNumber(NEW.Telefono) THEN
		BEGIN
			SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Telefono invalido';
		END;
	END IF;

END;//

CREATE TRIGGER b_update_cliente BEFORE UPDATE ON Cliente
FOR EACH ROW
BEGIN

	IF NOT IsNumber(NEW.Identif) THEN
		BEGIN
			SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Cedula/RIF/Pasaporte invalido';
		END;
	END IF;

	IF NOT IsEmail(NEW.Email) THEN
		BEGIN
			SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Email invalido';
		END;
	END IF;

	IF NOT IsNumber(NEW.Telefono) THEN
		BEGIN
			SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Telefono invalido';
		END;
	END IF;

END;//

CREATE TRIGGER b_insert_notificacion BEFORE INSERT ON Notificacion
FOR EACH ROW
BEGIN

	IF NOT IsNumber(NEW.Identif_Cliente) THEN
		BEGIN
			SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Cedula/RIF/Pasaporte del cliente invalido';
		END;
	END IF;

END;//

CREATE TRIGGER b_update_notificacion BEFORE UPDATE ON Notificacion
FOR EACH ROW
BEGIN

	IF NOT IsNumber(NEW.Identif_Cliente) THEN
		BEGIN
			SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Cedula/RIF/Pasaporte del cliente invalido';
		END;
	END IF;

END;//

CREATE TRIGGER b_insert_equipo BEFORE INSERT ON Equipo
FOR EACH ROW
BEGIN

	IF NOT NoSpaces(NEW.Serial_Eq) THEN
		BEGIN
			SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Serial invalido';
		END;
	END IF;

	IF NOT IsNumber(NEW.Identif_Cliente) THEN
		BEGIN
			SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Cedula/RIF/Pasaporte del cliente invalido';
		END;
	END IF;

	IF NOT (NEW.Precio >= 0) THEN
		BEGIN
			SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Precio invalido';
		END;
	END IF;

END;//

CREATE TRIGGER b_update_equipo BEFORE UPDATE ON Equipo
FOR EACH ROW
BEGIN

	IF NOT NoSpaces(NEW.Serial_Eq) THEN
		BEGIN
			SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Serial invalido';
		END;
	END IF;

	IF NOT IsNumber(NEW.Identif_Cliente) THEN
		BEGIN
			SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Cedula/RIF/Pasaporte del cliente invalido';
		END;
	END IF;

	IF NOT (NEW.Precio >= 0) THEN
		BEGIN
			SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Precio invalido';
		END;
	END IF;

END;//

CREATE TRIGGER b_insert_orden BEFORE INSERT ON Orden
FOR EACH ROW
BEGIN

	IF NOT IsNumber(NEW.Numero) THEN
		BEGIN
			SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Numero de orden invalido';
		END;
	END IF;

	IF NOT (NEW.Total >= 0) THEN
		BEGIN
			SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Total invalido';
		END;
	END IF;

	IF NOT (NEW.F_inicio <= NEW.F_fin) THEN
		BEGIN
			SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'La fecha de inicio debe ser menor o igual a la fecha fin';
		END;
	END IF;

END;//

CREATE TRIGGER b_update_orden BEFORE UPDATE ON Orden
FOR EACH ROW
BEGIN

	IF NOT IsNumber(NEW.Numero) THEN
		BEGIN
			SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Numero de orden invalido';
		END;
	END IF;

	IF NOT (NEW.Total >= 0) THEN
		BEGIN
			SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Total invalido';
		END;
	END IF;

	IF NOT (NEW.F_inicio <= NEW.F_fin) THEN
		BEGIN
			SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'La fecha de inicio debe ser menor o igual a la fecha fin';
		END;
	END IF;

END;//

CREATE TRIGGER b_insert_factura_venta BEFORE INSERT ON FacturaVenta
FOR EACH ROW
BEGIN

	IF NOT IsNumber(NEW.Numero) THEN
		BEGIN
			SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Numero de factura invalido';
		END;
	END IF;

	IF NOT IsNumber(NEW.Identif_Cliente) THEN
		BEGIN
			SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Cedula/RIF/Pasaporte del cliente invalido';
		END;
	END IF;

	IF NOT (NEW.Monto >= 0) THEN
		BEGIN
			SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Monto invalido';
		END;
	END IF;

END;//

CREATE TRIGGER b_update_factura_venta BEFORE UPDATE ON FacturaVenta
FOR EACH ROW
BEGIN

	IF NOT IsNumber(NEW.Numero) THEN
		BEGIN
			SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Numero de factura invalido';
		END;
	END IF;

	IF NOT IsNumber(NEW.Identif_Cliente) THEN
		BEGIN
			SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Cedula/RIF/Pasaporte del cliente invalido';
		END;
	END IF;

	IF NOT (NEW.Monto >= 0) THEN
		BEGIN
			SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Monto invalido';
		END;
	END IF;

END;//

CREATE TRIGGER b_insert_venta_web BEFORE INSERT ON VentaWeb
FOR EACH ROW
BEGIN

	IF NOT IsNumber(NEW.Numero) THEN
		BEGIN
			SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Numero de venta invalido';
		END;
	END IF;

	IF NOT IsNumber(NEW.Identif_Cliente) THEN
		BEGIN
			SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Cedula/RIF/Pasaporte del cliente invalido';
		END;
	END IF;

	IF NOT IsNumber(NEW.Num_Factura) THEN
		BEGIN
			SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Numero de factura invalido';
		END;
	END IF;

END;//

CREATE TRIGGER b_update_venta_web BEFORE UPDATE ON VentaWeb
FOR EACH ROW
BEGIN

	IF NOT IsNumber(NEW.Numero) THEN
		BEGIN
			SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Numero de venta invalido';
		END;
	END IF;

	IF NOT IsNumber(NEW.Identif_Cliente) THEN
		BEGIN
			SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Cedula/RIF/Pasaporte del cliente invalido';
		END;
	END IF;

	IF NOT IsNumber(NEW.Num_Factura) THEN
		BEGIN
			SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Numero de factura de venta invalido';
		END;
	END IF;

END;//

CREATE TRIGGER b_insert_reparacion BEFORE INSERT ON Reparacion
FOR EACH ROW
BEGIN

	IF NOT IsNumber(NEW.Numero_Orden) THEN
		BEGIN
			SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Numero de orden invalido';
		END;
	END IF;

	IF NOT (NEW.CI_Personal > 0 AND NEW.CI_Personal <= 999999999) THEN
		BEGIN
			SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Cedula del personal invalida';
		END;
	END IF;

	IF NOT (NEW.Comision >= 0) THEN
		BEGIN
			SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Comision invalida';
		END;
	END IF;

	IF NOT (NEW.Total >= 0) THEN
		BEGIN
			SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Total invalido';
		END;
	END IF;

	IF NOT (NEW.F_asig <= NEW.F_rep) THEN
		BEGIN
			SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'La fecha de inicio debe ser menor o igual a la fecha fin';
		END;
	END IF;

END;//

CREATE TRIGGER b_update_reparacion BEFORE UPDATE ON Reparacion
FOR EACH ROW
BEGIN

	IF NOT IsNumber(NEW.Numero_Orden) THEN
		BEGIN
			SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Numero de orden invalido';
		END;
	END IF;

	IF NOT (NEW.CI_Personal > 0 AND NEW.CI_Personal <= 999999999) THEN
		BEGIN
			SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Cedula del personal invalida';
		END;
	END IF;

	IF NOT (NEW.Comision >= 0) THEN
		BEGIN
			SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Comision invalida';
		END;
	END IF;

	IF NOT (NEW.Total >= 0) THEN
		BEGIN
			SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Total invalido';
		END;
	END IF;

	IF NOT (NEW.F_asig <= NEW.F_rep) THEN
		BEGIN
			SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'La fecha de inicio debe ser menor o igual a la fecha fin';
		END;
	END IF;

END;//

CREATE TRIGGER b_insert_repuesto_reparacion BEFORE INSERT ON Repuesto_Reparacion
FOR EACH ROW
BEGIN

	IF NOT NoSpaces(NEW.Codigo_Repues) THEN
		BEGIN
			SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Codigo del repuesto invalido';
		END;
	END IF;

	IF NOT IsNumber(NEW.Num_Ord_Rep) THEN
		BEGIN
			SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Numero de orden de reparacion invalido';
		END;
	END IF;

	IF NOT (NEW.CI_Pers_Rep > 0 AND NEW.CI_Pers_Rep <= 999999999) THEN
		BEGIN
			SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Cedula invalida';
		END;
	END IF;

	IF NOT (NEW.Cant_Rep > 0 AND NEW.Cant_Rep <= 9999) THEN
		BEGIN
			SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Cantidad invalida';
		END;
	END IF;

	IF NOT (NEW.Precio_Rep >= 0) THEN
		BEGIN
			SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Precio invalido';
		END;
	END IF;

END;//

CREATE TRIGGER b_update_repuesto_reparacion BEFORE UPDATE ON Repuesto_Reparacion
FOR EACH ROW
BEGIN

	IF NOT NoSpaces(NEW.Codigo_Repues) THEN
		BEGIN
			SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Codigo del repuesto invalido';
		END;
	END IF;

	IF NOT IsNumber(NEW.Num_Ord_Rep) THEN
		BEGIN
			SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Numero de orden de reparacion invalido';
		END;
	END IF;	

	IF NOT (NEW.CI_Pers_Rep > 0 AND NEW.CI_Pers_Rep <= 999999999) THEN
		BEGIN
			SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Cedula invalida';
		END;
	END IF;

	IF NOT (NEW.Cant_Rep > 0 AND NEW.Cant_Rep <= 9999) THEN
		BEGIN
			SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Cantidad invalida';
		END;
	END IF;

	IF NOT (NEW.Precio_Rep >= 0) THEN
		BEGIN
			SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Precio invalido';
		END;
	END IF;

END;//

CREATE TRIGGER b_insert_cuenta_cobrar BEFORE INSERT ON CuentaCobrar
FOR EACH ROW
BEGIN

	IF NOT IsNumber(NEW.Numero) THEN
		BEGIN
			SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Numero de la cuenta invalido';
		END;
	END IF;	

	IF NOT IsNumber(NEW.Identif_Cliente) THEN
		BEGIN
			SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Cedula/RIF/Pasaporte del cliente invalido';
		END;
	END IF;

	IF NOT IsNumber(NEW.Num_Factura) THEN
		BEGIN
			SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Numero de factura de venta invalido';
		END;
	END IF;

	IF NOT (NEW.Saldo >= 0) THEN
		BEGIN
			SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Saldo invalido';
		END;
	END IF;

	IF NOT (NEW.Monto >= 0) THEN
		BEGIN
			SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Monto invalido';
		END;
	END IF;

	IF NOT (DATE(NEW.F_aper) <= NEW.F_lim) THEN
		BEGIN
			SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'La fecha de inicio debe ser menor o igual a la fecha fin';
		END;
	END IF;

END;//

CREATE TRIGGER b_update_cuenta_cobrar BEFORE UPDATE ON CuentaCobrar
FOR EACH ROW
BEGIN

	IF NOT IsNumber(NEW.Numero) THEN
		BEGIN
			SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Numero de la cuenta invalido';
		END;
	END IF;	

	IF NOT IsNumber(NEW.Identif_Cliente) THEN
		BEGIN
			SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Cedula/RIF/Pasaporte del cliente invalido';
		END;
	END IF;

	IF NOT IsNumber(NEW.Num_Factura) THEN
		BEGIN
			SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Numero de factura de venta invalido';
		END;
	END IF;

	IF NOT (NEW.Saldo >= 0) THEN
		BEGIN
			SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Saldo invalido';
		END;
	END IF;

	IF NOT (NEW.Monto >= 0) THEN
		BEGIN
			SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Monto invalido';
		END;
	END IF;

	IF NOT (DATE(NEW.F_aper) <= NEW.F_lim) THEN
		BEGIN
			SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'La fecha de inicio debe ser menor o igual a la fecha fin';
		END;
	END IF;

END;//

CREATE TRIGGER b_insert_forma_pago_v BEFORE INSERT ON FormaPagoV
FOR EACH ROW
BEGIN

	IF NOT (NEW.Nref > 0 AND NEW.Nref <= 999999999) THEN
		BEGIN
			SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Numero de referencia invalido';
		END;
	END IF;

	IF NOT (NEW.Monto >= 0) THEN
		BEGIN
			SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Monto invalido';
		END;
	END IF;

	IF NEW.Tipo = 'EF' THEN
		BEGIN
			SET NEW.Nref = NULL;
		END;
	END IF;

END;//

CREATE TRIGGER b_update_forma_pago_v BEFORE UPDATE ON FormaPagoV
FOR EACH ROW
BEGIN

	IF NOT (NEW.Nref > 0 AND NEW.Nref <= 999999999) THEN
		BEGIN
			SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Numero de referencia invalido';
		END;
	END IF;

	IF NOT (NEW.Monto >= 0) THEN
		BEGIN
			SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Monto invalido';
		END;
	END IF;

	IF NEW.Tipo = 'EF' THEN
		BEGIN
			SET NEW.Nref = NULL;
		END;
	END IF;

END;//

CREATE TRIGGER b_insert_cuenta_cobrar_forma_pago_v BEFORE INSERT ON CuentaCobrar_FormaPagoV
FOR EACH ROW
BEGIN

	IF NOT IsNumber(NEW.Num_Cuenta) THEN
		BEGIN
			SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Numero de la cuenta por cobrar invalido';
		END;
	END IF;

	IF NOT IsNumber(NEW.Cliente_Cuenta) THEN
		BEGIN
			SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Cedula/RIF/Pasaporte del cliente de la cuenta invalido';
		END;
	END IF;

	SELECT Saldo INTO @SaldoCuenta FROM CuentaCobrar WHERE Numero = NEW.Num_Cuenta LIMIT 1;

	SELECT Monto INTO @MontoPago FROM FormaPagoV WHERE Fecha = NEW.Fecha_Pago LIMIT 1;

	SET @SaldoActual = @SaldoCuenta - @MontoPago;

	IF @SaldoActual < 0 THEN
		SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'El pago de la cuenta por cobrar supera el saldo actual de la cuenta';
	ELSE
		UPDATE CuentaCobrar SET Saldo = @SaldoActual, F_aper = F_aper WHERE Numero = NEW.Num_Cuenta;
	END IF;

END;//

CREATE TRIGGER b_update_cuenta_cobrar_forma_pago_v BEFORE UPDATE ON CuentaCobrar_FormaPagoV
FOR EACH ROW
BEGIN

	IF NOT IsNumber(NEW.Num_Cuenta) THEN
		BEGIN
			SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Numero de la cuenta por cobrar invalido';
		END;
	END IF;

	IF NOT IsNumber(NEW.Cliente_Cuenta) THEN
		BEGIN
			SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Cedula/RIF/Pasaporte del cliente de la cuenta invalido';
		END;
	END IF;

END;//

CREATE TRIGGER b_insert_factura_venta_forma_pago_v BEFORE INSERT ON FacturaVenta_FormaPagoV
FOR EACH ROW
BEGIN

	IF NOT IsNumber(NEW.Num_Factura) THEN
		BEGIN
			SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Numero de la factura de venta invalido';
		END;
	END IF;

END;//

CREATE TRIGGER b_update_factura_venta_forma_pago_v BEFORE UPDATE ON FacturaVenta_FormaPagoV
FOR EACH ROW
BEGIN

	IF NOT IsNumber(NEW.Num_Factura) THEN
		BEGIN
			SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Numero de la factura de venta invalido';
		END;
	END IF;

END;//

CREATE TRIGGER b_insert_cuenta_forma_pago_v BEFORE INSERT ON Cuenta_FormaPagoV
FOR EACH ROW
BEGIN

	IF NOT IsNumber(NEW.Num_Cuenta) THEN
		BEGIN
			SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Numero de cuenta invalido';
		END;
	END IF;

END;//

CREATE TRIGGER b_update_cuenta_forma_pago_v BEFORE UPDATE ON Cuenta_FormaPagoV
FOR EACH ROW
BEGIN

	IF NOT IsNumber(NEW.Num_Cuenta) THEN
		BEGIN
			SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Numero de cuenta invalido';
		END;
	END IF;

END;//

CREATE TRIGGER b_insert_cuenta_pagar BEFORE INSERT ON CuentaPagar
FOR EACH ROW
BEGIN

	IF NOT IsNumber(NEW.Numero) THEN
		BEGIN
			SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Numero de la cuenta invalido';
		END;
	END IF;	

	IF NOT IsNumber(NEW.Num_Factura) THEN
		BEGIN
			SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Numero de factura de compra invalido';
		END;
	END IF;

	IF NOT (NEW.Saldo >= 0) THEN
		BEGIN
			SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Saldo invalido';
		END;
	END IF;

	IF NOT (NEW.Monto >= 0) THEN
		BEGIN
			SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Monto invalido';
		END;
	END IF;

	IF NOT (DATE(NEW.F_aper) <= NEW.F_lim) THEN
		BEGIN
			SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'La fecha de inicio debe ser menor o igual a la fecha fin';
		END;
	END IF;

END;//

CREATE TRIGGER b_update_cuenta_pagar BEFORE UPDATE ON CuentaPagar
FOR EACH ROW
BEGIN

	IF NOT IsNumber(NEW.Numero) THEN
		BEGIN
			SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Numero de la cuenta invalido';
		END;
	END IF;	

	IF NOT IsNumber(NEW.Num_Factura) THEN
		BEGIN
			SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Numero de factura de compra invalido';
		END;
	END IF;

	IF NOT (NEW.Saldo >= 0) THEN
		BEGIN
			SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Saldo invalido';
		END;
	END IF;

	IF NOT (NEW.Monto >= 0) THEN
		BEGIN
			SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Monto invalido';
		END;
	END IF;

	IF NOT (DATE(NEW.F_aper) <= NEW.F_lim) THEN
		BEGIN
			SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'La fecha de inicio debe ser menor o igual a la fecha fin';
		END;
	END IF;

END;//

CREATE TRIGGER b_insert_forma_pago_c BEFORE INSERT ON FormaPagoC
FOR EACH ROW
BEGIN

	IF NOT (NEW.Nref > 0 AND NEW.Nref <= 999999999) THEN
		BEGIN
			SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Numero de referencia invalido';
		END;
	END IF;

	IF NOT (NEW.Monto >= 0) THEN
		BEGIN
			SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Monto invalido';
		END;
	END IF;

	IF NEW.Tipo = 'EF' THEN
		BEGIN
			SET NEW.Nref = NULL;
		END;
	END IF;

END;//

CREATE TRIGGER b_update_forma_pago_c BEFORE UPDATE ON FormaPagoC
FOR EACH ROW
BEGIN

	IF NOT (NEW.Nref > 0 AND NEW.Nref <= 999999999) THEN
		BEGIN
			SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Numero de referencia invalido';
		END;
	END IF;

	IF NOT (NEW.Monto >= 0) THEN
		BEGIN
			SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Monto invalido';
		END;
	END IF;

	IF NEW.Tipo = 'EF' THEN
		BEGIN
			SET NEW.Nref = NULL;
		END;
	END IF;

END;//

CREATE TRIGGER b_insert_cuenta_pagar_forma_pago_c BEFORE INSERT ON CuentaPagar_FormaPagoC
FOR EACH ROW
BEGIN

	IF NOT IsNumber(NEW.Num_Cuenta) THEN
		BEGIN
			SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Numero de la cuenta por pagar invalido';
		END;
	END IF;

	IF NOT (NEW.RIF_Proveed > 0 AND NEW.RIF_Proveed <= 9999999999) THEN
		BEGIN
			SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'RIF del proveedor invalido';
		END;
	END IF;

	SELECT Saldo INTO @SaldoCuenta FROM CuentaPagar WHERE Numero = NEW.Num_Cuenta LIMIT 1;

	SELECT Monto INTO @MontoPago FROM FormaPagoC WHERE Fecha = NEW.Fecha_Pago LIMIT 1;

	SET @SaldoActual = @SaldoCuenta - @MontoPago;

	IF @SaldoActual < 0 THEN
		SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'El pago de la cuenta por pagar supera el saldo actual de la cuenta';
	ELSE
		UPDATE CuentaPagar SET Saldo = @SaldoActual, F_aper = F_aper WHERE Numero = NEW.Num_Cuenta;
	END IF;

END;//

CREATE TRIGGER b_update_cuenta_pagar_forma_pago_c BEFORE UPDATE ON CuentaPagar_FormaPagoC
FOR EACH ROW
BEGIN

	IF NOT IsNumber(NEW.Num_Cuenta) THEN
		BEGIN
			SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Numero de la cuenta por pagar invalido';
		END;
	END IF;

	IF NOT (NEW.RIF_Proveed > 0 AND NEW.RIF_Proveed <= 9999999999) THEN
		BEGIN
			SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'RIF del proveedor invalido';
		END;
	END IF;

END;//

CREATE TRIGGER b_insert_factura_compra_forma_pago_c BEFORE INSERT ON FacturaCompra_FormaPagoC
FOR EACH ROW
BEGIN

	IF NOT IsNumber(NEW.Num_Factura) THEN
		BEGIN
			SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Numero de factura de compra invalido';
		END;
	END IF;

	IF NOT (NEW.RIF_Proveed > 0 AND NEW.RIF_Proveed <= 9999999999) THEN
		BEGIN
			SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'RIF del proveedor invalido';
		END;
	END IF;

END;//

CREATE TRIGGER b_update_factura_compra_forma_pago_c BEFORE UPDATE ON FacturaCompra_FormaPagoC
FOR EACH ROW
BEGIN

	IF NOT IsNumber(NEW.Num_Factura) THEN
		BEGIN
			SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Numero de factura de compra invalido';
		END;
	END IF;

	IF NOT (NEW.RIF_Proveed > 0 AND NEW.RIF_Proveed <= 9999999999) THEN
		BEGIN
			SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'RIF del proveedor invalido';
		END;
	END IF;

END;//

CREATE TRIGGER b_insert_cuenta_forma_pago_c BEFORE INSERT ON Cuenta_FormaPagoC
FOR EACH ROW
BEGIN

	IF NOT IsNumber(NEW.Num_Cuenta) THEN
		BEGIN
			SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Numero de la cuenta invalido';
		END;
	END IF;

END;//

CREATE TRIGGER b_update_cuenta_forma_pago_c BEFORE UPDATE ON Cuenta_FormaPagoC
FOR EACH ROW
BEGIN

	IF NOT IsNumber(NEW.Num_Cuenta) THEN
		BEGIN
			SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Numero de la cuenta invalido';
		END;
	END IF;

END;//

CREATE TRIGGER b_insert_orden_factura_venta BEFORE INSERT ON Orden_FacturaVenta
FOR EACH ROW
BEGIN

	IF NOT IsNumber(NEW.Num_Orden) THEN
		BEGIN
			SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Numero de la orden invalido';
		END;
	END IF;

	IF NOT IsNumber(NEW.Num_Factura) THEN
		BEGIN
			SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Numero de factura de venta invalido';
		END;
	END IF;

END;//

CREATE TRIGGER b_update_orden_factura_venta BEFORE UPDATE ON Orden_FacturaVenta
FOR EACH ROW
BEGIN

	IF NOT IsNumber(NEW.Num_Orden) THEN
		BEGIN
			SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Numero de la orden invalido';
		END;
	END IF;

	IF NOT IsNumber(NEW.Num_Factura) THEN
		BEGIN
			SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Numero de factura de venta invalido';
		END;
	END IF;

END;//

CREATE TRIGGER b_insert_orden_equipo BEFORE INSERT ON Orden_Equipo
FOR EACH ROW
BEGIN

	IF NOT NoSpaces(NEW.Serial_Equipo) THEN
		BEGIN
			SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Serial del equipo invalido';
		END;
	END IF;

	IF NOT IsNumber(NEW.Num_Orden) THEN
		BEGIN
			SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Numero de la orden invalido';
		END;
	END IF;

END;//

CREATE TRIGGER b_update_orden_equipo BEFORE UPDATE ON Orden_Equipo
FOR EACH ROW
BEGIN

	IF NOT NoSpaces(NEW.Serial_Equipo) THEN
		BEGIN
			SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Serial del equipo invalido';
		END;
	END IF;

	IF NOT IsNumber(NEW.Num_Orden) THEN
		BEGIN
			SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Numero de la orden invalido';
		END;
	END IF;

END;//

CREATE TRIGGER b_insert_factura_venta_equipo_venta BEFORE INSERT ON FacturaVenta_EquipoVenta
FOR EACH ROW
BEGIN

	IF NOT NoSpaces(NEW.Codigo_Equipo) THEN
		BEGIN
			SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Codigo del equipo invalido';
		END;
	END IF;

	IF NOT IsNumber(NEW.Num_Factura) THEN
		BEGIN
			SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Numero de factura de venta invalido';
		END;
	END IF;

	IF NOT (NEW.Cantidad > 0 AND NEW.Cantidad <= 9999) THEN
		BEGIN
			SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Cantidad invalida';
		END;
	END IF;

	IF NOT (NEW.PVP >= 0) THEN
		BEGIN
			SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'PVP invalido';
		END;
	END IF;

	SELECT Cantidad INTO @Cant FROM EquipoVenta WHERE Codigo = NEW.Codigo_Equipo LIMIT 1;

	SET @CantActual = @Cant - NEW.Cantidad;

	IF @CantActual < 0 THEN
		SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'La cantidad del equipo que se desea vender supera la cantidad actual';
	ELSE
		UPDATE Inventario SET Cantidad = @CantActual WHERE Codigo = NEW.Codigo_Equipo;
	END IF;

END;//

CREATE TRIGGER b_update_factura_venta_equipo_venta BEFORE UPDATE ON FacturaVenta_EquipoVenta
FOR EACH ROW
BEGIN

	IF NOT NoSpaces(NEW.Codigo_Equipo) THEN
		BEGIN
			SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Codigo del equipo invalido';
		END;
	END IF;

	IF NOT IsNumber(NEW.Num_Factura) THEN
		BEGIN
			SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Numero de factura de venta invalido';
		END;
	END IF;

	IF NOT (NEW.Cantidad > 0 AND NEW.Cantidad <= 9999) THEN
		BEGIN
			SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Cantidad invalida';
		END;
	END IF;

	IF NOT (NEW.PVP >= 0) THEN
		BEGIN
			SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'PVP invalido';
		END;
	END IF;

	SELECT Cantidad INTO @CantEquipo FROM EquipoVenta WHERE Codigo = NEW.Codigo_Equipo LIMIT 1;

	SET @CantActual = 0;

	IF NEW.Cantidad > OLD.Cantidad THEN
		SET @CantActual = NEW.Cantidad - OLD.Cantidad;
		SET @CantEquipo = @CantEquipo - @CantActual;
	ELSE
		IF OLD.Cantidad > NEW.Cantidad THEN
			SET @CantActual = OLD.Cantidad - NEW.Cantidad;
			SET @CantEquipo = @CantEquipo + @CantActual;
		END IF;
	END IF;

	IF @CantEquipo < 0 THEN
		SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'La cantidad del equipo presenta inconsistencia en cuanto a la cantidad en existencia';
	ELSE
		IF @CantActual > 0 THEN
			UPDATE Inventario SET Cantidad = @CantEquipo WHERE Codigo = NEW.Codigo_Equipo;
		END IF;
	END IF;

END;//

CREATE TRIGGER b_insert_venta_web_equipo_venta BEFORE INSERT ON VentaWeb_EquipoVenta
FOR EACH ROW
BEGIN

	IF NOT NoSpaces(NEW.Codigo_Equipo) THEN
		BEGIN
			SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Codigo del equipo invalido';
		END;
	END IF;

	IF NOT IsNumber(NEW.Num_Venta) THEN
		BEGIN
			SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Numero de la venta invalido';
		END;
	END IF;

	IF NOT (NEW.Cantidad > 0 AND NEW.Cantidad <= 9999) THEN
		BEGIN
			SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Cantidad invalida';
		END;
	END IF;

	IF NOT (NEW.Precio >= 0) THEN
		BEGIN
			SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'PVP invalido';
		END;
	END IF;

	SELECT Cantidad INTO @Cant FROM EquipoVenta WHERE Codigo = NEW.Codigo_Equipo LIMIT 1;

	SET @CantActual = @Cant - NEW.Cantidad;

	IF @CantActual < 0 THEN
		SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'La cantidad del equipo que se desea vender supera la cantidad actual';
	ELSE
		UPDATE Inventario SET Cantidad = @CantActual WHERE Codigo = NEW.Codigo_Equipo;
	END IF;

END;//

CREATE TRIGGER b_update_venta_web_equipo_venta BEFORE UPDATE ON VentaWeb_EquipoVenta
FOR EACH ROW
BEGIN

	IF NOT NoSpaces(NEW.Codigo_Equipo) THEN
		BEGIN
			SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Codigo del equipo invalido';
		END;
	END IF;

	IF NOT IsNumber(NEW.Num_Venta) THEN
		BEGIN
			SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Numero de la venta invalido';
		END;
	END IF;

	IF NOT (NEW.Cantidad > 0 AND NEW.Cantidad <= 9999) THEN
		BEGIN
			SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Cantidad invalida';
		END;
	END IF;

	IF NOT (NEW.Precio >= 0) THEN
		BEGIN
			SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'PVP invalido';
		END;
	END IF;

	SELECT Cantidad INTO @CantEquipo FROM EquipoVenta WHERE Codigo = NEW.Codigo_Equipo LIMIT 1;

	SET @CantActual = 0;

	IF NEW.Cantidad > OLD.Cantidad THEN
		SET @CantActual = NEW.Cantidad - OLD.Cantidad;
		SET @CantEquipo = @CantEquipo - @CantActual;
	ELSE
		IF OLD.Cantidad > NEW.Cantidad THEN
			SET @CantActual = OLD.Cantidad - NEW.Cantidad;
			SET @CantEquipo = @CantEquipo + @CantActual;
		END IF;
	END IF;

	IF @CantEquipo < 0 THEN
		SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'La cantidad del equipo presenta inconsistencia en cuanto a la cantidad en existencia';
	ELSE
		IF @CantActual > 0 THEN
			UPDATE Inventario SET Cantidad = @CantEquipo WHERE Codigo = NEW.Codigo_Equipo;
		END IF;
	END IF;

END;//

CREATE TRIGGER b_insert_factura_compra_inventario BEFORE INSERT ON FacturaCompra_Inventario
FOR EACH ROW
BEGIN

	IF NOT NoSpaces(NEW.Cod_Inv) THEN
		BEGIN
			SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Codigo del inventario invalido';
		END;
	END IF;

	IF NOT IsNumber(NEW.Num_Fact) THEN
		BEGIN
			SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Numero de factura de compra invalido';
		END;
	END IF;

	IF NOT (NEW.Proveed_Fact > 0 AND NEW.Proveed_Fact <= 9999999999) THEN
		BEGIN
			SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'RIF del proveedor invalido';
		END;
	END IF;

	IF NOT (NEW.Cantidad > 0 AND NEW.Cantidad <= 9999) THEN
		BEGIN
			SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Cantidad invalida';
		END;
	END IF;

	IF NOT (NEW.Costo >= 0) THEN
		BEGIN
			SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Costo invalido';
		END;
	END IF;

	UPDATE Inventario SET Cantidad = Cantidad + NEW.Cantidad, F_Registro = F_Registro WHERE Codigo = NEW.Cod_Inv;

END;//

CREATE TRIGGER b_update_factura_compra_inventario BEFORE UPDATE ON FacturaCompra_Inventario
FOR EACH ROW
BEGIN

	IF NOT NoSpaces(NEW.Cod_Inv) THEN
		BEGIN
			SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Codigo del inventario invalido';
		END;
	END IF;

	IF NOT IsNumber(NEW.Num_Fact) THEN
		BEGIN
			SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Numero de factura de compra invalido';
		END;
	END IF;

	IF NOT (NEW.Proveed_Fact > 0 AND NEW.Proveed_Fact <= 9999999999) THEN
		BEGIN
			SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'RIF del proveedor invalido';
		END;
	END IF;

	IF NOT (NEW.Cantidad > 0 AND NEW.Cantidad <= 9999) THEN
		BEGIN
			SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Cantidad invalida';
		END;
	END IF;

	IF NOT (NEW.Costo >= 0) THEN
		BEGIN
			SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Costo invalido';
		END;
	END IF;

	SELECT Cantidad INTO @CantInv FROM Inventario WHERE Codigo = NEW.Cod_Inv LIMIT 1;

	SET @CantActual = 0;

	IF NEW.Cantidad > OLD.Cantidad THEN
		SET @CantActual = NEW.Cantidad - OLD.Cantidad;
		SET @CantInv = @CantInv + @CantActual;
	ELSE
		IF OLD.Cantidad > NEW.Cantidad THEN
			SET @CantActual = OLD.Cantidad - NEW.Cantidad;
			SET @CantInv = @CantInv - @CantActual;
		END IF;
	END IF;

	IF @CantInv < 0 THEN
		SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'La cantidad del inventario presenta inconsistencia en cuanto a la cantidad en existencia';
	ELSE
		IF @CantActual > 0 THEN
			UPDATE Inventario SET Cantidad = @CantInv, F_Registro = F_Registro WHERE Codigo = NEW.Cod_Inv;
		END IF;
	END IF;

END;//

DELIMITER ;

--
-- DUMP TABLE Tienda
--

LOCK TABLES Tienda WRITE;

	INSERT INTO Tienda (RIF, Tipo_RIF, Direccion, Nombre, Email, Telefono) VALUES ('406863254', 'J', 'Avenida 4 de mayo con calle Campos Jumbo local 84', 'Black Tech Store C.A', 'blacktechstore@blacktechstore.com', '584265965920');

UNLOCK TABLES;

--
-- DUMP TABLE Cuenta
--

LOCK TABLES Cuenta WRITE;

	INSERT INTO Cuenta (Numero, RIF_tienda, Tipo, Banco) VALUES ('4239543749192393100', '406863254', 'Corriente', 'BANESCO');
	INSERT INTO Cuenta (Numero, RIF_tienda, Tipo, Banco) VALUES ('9239543649192293100', '406863254', 'Ahorro', 'Mercantil');
	INSERT INTO Cuenta (Numero, RIF_tienda, Tipo, Banco) VALUES ('42395432491923931000', '406863254', 'Ahorro', 'Nacional de Crédito');

UNLOCK TABLES;

--
-- DUMP TABLE Marca
--

LOCK TABLES Marca WRITE;

	INSERT INTO Marca (ID, Nombre) VALUES (1, 'ACER');
	INSERT INTO Marca (ID, Nombre) VALUES (2, 'GATEWAY');
	INSERT INTO Marca (ID, Nombre) VALUES (3, 'SONY');
	INSERT INTO Marca (ID, Nombre) VALUES (4, 'HP ');
	INSERT INTO Marca (ID, Nombre) VALUES (5, 'Compac');
	INSERT INTO Marca (ID, Nombre) VALUES (6, 'Lenovo');
	INSERT INTO Marca (ID, Nombre) VALUES (7, 'DELL');
	INSERT INTO Marca (ID, Nombre) VALUES (8, 'LG');
	INSERT INTO Marca (ID, Nombre) VALUES (9, 'Toshiba');
	INSERT INTO Marca (ID, Nombre) VALUES (10, 'Samsung');

UNLOCK TABLES;

--
-- DUMP TABLE Modelo
--

LOCK TABLES Modelo WRITE;

	INSERT INTO Modelo (ID, ID_Marca, Nombre) VALUES (1, 6, 'LEN32');
	INSERT INTO Modelo (ID, ID_Marca, Nombre) VALUES (2, 10, 'S2');
	INSERT INTO Modelo (ID, ID_Marca, Nombre) VALUES (3, 4, 'H5');
	INSERT INTO Modelo (ID, ID_Marca, Nombre) VALUES (4, 4, 'H5');
	INSERT INTO Modelo (ID, ID_Marca, Nombre) VALUES (5, 3, 'SON');
	INSERT INTO Modelo (ID, ID_Marca, Nombre) VALUES (6, 3, 'SON');
	INSERT INTO Modelo (ID, ID_Marca, Nombre) VALUES (7, 8, 'L6');
	INSERT INTO Modelo (ID, ID_Marca, Nombre) VALUES (8, 5, 'CA3');
	INSERT INTO Modelo (ID, ID_Marca, Nombre) VALUES (9, 3, 'SON');
	INSERT INTO Modelo (ID, ID_Marca, Nombre) VALUES (10, 1, 'AC232');
	INSERT INTO Modelo (ID, ID_Marca, Nombre) VALUES (11, 2, 'GAT');
	INSERT INTO Modelo (ID, ID_Marca, Nombre) VALUES (12, 7, 'D32K');
	INSERT INTO Modelo (ID, ID_Marca, Nombre) VALUES (13, 9, 'TOSH12');

UNLOCK TABLES;

--
-- DUMP TABLE Categoria
--

LOCK TABLES Categoria WRITE;

	INSERT INTO Categoria (ID, ID_Modelo, Nombre) VALUES (1, 13, 'Celular');
	INSERT INTO Categoria (ID, ID_Modelo, Nombre) VALUES (2, 5, 'Celular');
	INSERT INTO Categoria (ID, ID_Modelo, Nombre) VALUES (3, 11, 'Laptops/Pc’s');
	INSERT INTO Categoria (ID, ID_Modelo, Nombre) VALUES (4, 3, 'Tablet/Ds');
	INSERT INTO Categoria (ID, ID_Modelo, Nombre) VALUES (5, 4, 'Laptops/Pc’s');
	INSERT INTO Categoria (ID, ID_Modelo, Nombre) VALUES (6, 6, 'Celular');
	INSERT INTO Categoria (ID, ID_Modelo, Nombre) VALUES (7, 5, 'Celular');
	INSERT INTO Categoria (ID, ID_Modelo, Nombre) VALUES (8, 12, 'Laptops/Pc’s');
	INSERT INTO Categoria (ID, ID_Modelo, Nombre) VALUES (9, 4, 'Celular');
	INSERT INTO Categoria (ID, ID_Modelo, Nombre) VALUES (10, 1, 'Tablet/Ds');

UNLOCK TABLES;

--
-- DUMP TABLE Personal
--

LOCK TABLES Personal WRITE;

	INSERT INTO Personal (CI, RIF, Tipo_RIF, Direccion, Nombre, Email, Telefono, Especialidad, Tipo) VALUES (27423439, '467555271', 'J', '019 Gunner Trail Apt. 459', 'Prof. Thurman Considine III', 'timothy.jaskolski@larson.com', '46853438792', 'Hardware', 'TE');
	INSERT INTO Personal (CI, RIF, Tipo_RIF, Direccion, Nombre, Email, Telefono, Especialidad, Tipo) VALUES (36145388, '959940145', 'J', '4638 Mekhi Parkway', 'Erika Sanford', 'hodkiewicz.madisyn@hotmail.com', '71954507632', 'Software', 'TE');
	INSERT INTO Personal (CI, RIF, Tipo_RIF, Direccion, Nombre, Email, Telefono, Especialidad, Tipo) VALUES (53290784, '166516585', 'G', '5797 Holly Circle Suite 784', 'Arlene Rolfson MD', 'hannah.zemlak@yahoo.com', '35952763073', NULL, 'GR');
	INSERT INTO Personal (CI, RIF, Tipo_RIF, Direccion, Nombre, Email, Telefono, Especialidad, Tipo) VALUES (56973278, '677560247', 'G', '660 Phyllis Passage', 'Arno Klein', 'merritt46@yahoo.com', '63619681000', NULL, 'AS');

UNLOCK TABLES;

--
-- DUMP TABLE Inventario
--

LOCK TABLES Inventario WRITE;

	INSERT INTO Inventario (Codigo, F_Registro, Cantidad) VALUES ('10478', '2018-04-05 14:10:47', 7350);
	INSERT INTO Inventario (Codigo, F_Registro, Cantidad) VALUES ('16351', '2018-04-06 05:43:39', 1718);
	INSERT INTO Inventario (Codigo, F_Registro, Cantidad) VALUES ('18269', '2018-04-10 14:03:03', 4645);
	INSERT INTO Inventario (Codigo, F_Registro, Cantidad) VALUES ('18334', '2018-04-06 11:37:36', 6416);
	INSERT INTO Inventario (Codigo, F_Registro, Cantidad) VALUES ('19625', '2018-04-01 20:44:19', 6826);
	INSERT INTO Inventario (Codigo, F_Registro, Cantidad) VALUES ('20151', '2018-04-07 02:22:08', 453);
	INSERT INTO Inventario (Codigo, F_Registro, Cantidad) VALUES ('22671', '2018-04-09 08:21:09', 2222);
	INSERT INTO Inventario (Codigo, F_Registro, Cantidad) VALUES ('23751', '2018-04-05 08:12:03', 8306);
	INSERT INTO Inventario (Codigo, F_Registro, Cantidad) VALUES ('25264', '2018-03-29 04:20:31', 926);
	INSERT INTO Inventario (Codigo, F_Registro, Cantidad) VALUES ('2564', '2018-04-12 17:28:13', 4594);
	INSERT INTO Inventario (Codigo, F_Registro, Cantidad) VALUES ('2688', '2018-03-30 17:25:42', 9232);
	INSERT INTO Inventario (Codigo, F_Registro, Cantidad) VALUES ('30620', '2018-04-05 20:14:07', 3396);
	INSERT INTO Inventario (Codigo, F_Registro, Cantidad) VALUES ('33489', '2018-04-13 01:14:34', 876);
	INSERT INTO Inventario (Codigo, F_Registro, Cantidad) VALUES ('36674', '2018-03-29 20:48:27', 4063);
	INSERT INTO Inventario (Codigo, F_Registro, Cantidad) VALUES ('37661', '2018-04-09 19:42:09', 4425);
	INSERT INTO Inventario (Codigo, F_Registro, Cantidad) VALUES ('38621', '2018-04-13 03:49:13', 930);
	INSERT INTO Inventario (Codigo, F_Registro, Cantidad) VALUES ('91839', '2018-04-12 02:45:05', 4906);
	INSERT INTO Inventario (Codigo, F_Registro, Cantidad) VALUES ('92712', '2018-04-08 00:10:16', 4866);
	INSERT INTO Inventario (Codigo, F_Registro, Cantidad) VALUES ('94747', '2018-03-28 17:54:44', 9098);
	INSERT INTO Inventario (Codigo, F_Registro, Cantidad) VALUES ('40315', '2018-04-04 12:05:19', 9711);
	INSERT INTO Inventario (Codigo, F_Registro, Cantidad) VALUES ('4146', '2018-03-31 07:46:02', 2931);
	INSERT INTO Inventario (Codigo, F_Registro, Cantidad) VALUES ('42391', '2018-04-14 08:29:33', 7396);
	INSERT INTO Inventario (Codigo, F_Registro, Cantidad) VALUES ('43240', '2018-03-30 18:32:45', 4219);
	INSERT INTO Inventario (Codigo, F_Registro, Cantidad) VALUES ('47404', '2018-04-07 05:29:46', 1718);
	INSERT INTO Inventario (Codigo, F_Registro, Cantidad) VALUES ('48027', '2018-04-10 09:38:29', 8697);
	INSERT INTO Inventario (Codigo, F_Registro, Cantidad) VALUES ('51846', '2018-04-15 15:22:35', 814);
	INSERT INTO Inventario (Codigo, F_Registro, Cantidad) VALUES ('52713', '2018-03-29 21:18:51', 1505);
	INSERT INTO Inventario (Codigo, F_Registro, Cantidad) VALUES ('52844', '2018-04-03 17:20:17', 1128);
	INSERT INTO Inventario (Codigo, F_Registro, Cantidad) VALUES ('52914', '2018-03-27 22:36:26', 4372);
	INSERT INTO Inventario (Codigo, F_Registro, Cantidad) VALUES ('5497', '2018-04-09 09:37:07', 8139);
	INSERT INTO Inventario (Codigo, F_Registro, Cantidad) VALUES ('5728', '2018-04-12 08:05:59', 1965);
	INSERT INTO Inventario (Codigo, F_Registro, Cantidad) VALUES ('59291', '2018-03-27 16:36:53', 920);
	INSERT INTO Inventario (Codigo, F_Registro, Cantidad) VALUES ('61563', '2018-04-03 23:23:07', 8216);
	INSERT INTO Inventario (Codigo, F_Registro, Cantidad) VALUES ('62596', '2018-04-10 11:26:26', 9867);
	INSERT INTO Inventario (Codigo, F_Registro, Cantidad) VALUES ('64073', '2018-04-13 00:38:42', 9391);
	INSERT INTO Inventario (Codigo, F_Registro, Cantidad) VALUES ('71498', '2018-04-08 20:22:07', 9819);
	INSERT INTO Inventario (Codigo, F_Registro, Cantidad) VALUES ('71501', '2018-04-15 15:20:13', 4065);
	INSERT INTO Inventario (Codigo, F_Registro, Cantidad) VALUES ('73107', '2018-04-14 03:00:01', 6106);
	INSERT INTO Inventario (Codigo, F_Registro, Cantidad) VALUES ('75408', '2018-03-25 19:25:55', 7322);
	INSERT INTO Inventario (Codigo, F_Registro, Cantidad) VALUES ('81495', '2018-03-26 20:24:34', 4601);
	INSERT INTO Inventario (Codigo, F_Registro, Cantidad) VALUES ('82568', '2018-04-10 11:59:57', 7996);
	INSERT INTO Inventario (Codigo, F_Registro, Cantidad) VALUES ('82820', '2018-04-11 08:21:35', 7620);
	INSERT INTO Inventario (Codigo, F_Registro, Cantidad) VALUES ('83060', '2018-03-31 15:08:41', 3477);
	INSERT INTO Inventario (Codigo, F_Registro, Cantidad) VALUES ('83364', '2018-03-27 22:16:24', 1990);
	INSERT INTO Inventario (Codigo, F_Registro, Cantidad) VALUES ('83593', '2018-04-14 14:58:26', 6254);
	INSERT INTO Inventario (Codigo, F_Registro, Cantidad) VALUES ('83611', '2018-04-10 02:27:04', 5234);
	INSERT INTO Inventario (Codigo, F_Registro, Cantidad) VALUES ('86136', '2018-04-03 22:20:45', 994);
	INSERT INTO Inventario (Codigo, F_Registro, Cantidad) VALUES ('8888', '2018-04-11 17:18:58', 2508);
	INSERT INTO Inventario (Codigo, F_Registro, Cantidad) VALUES ('89009', '2018-04-13 14:16:38', 9591);
	INSERT INTO Inventario (Codigo, F_Registro, Cantidad) VALUES ('91201', '2018-03-31 16:22:21', 8207);

UNLOCK TABLES;

--
-- DUMP TABLE Repuesto
--

LOCK TABLES Repuesto WRITE;

	INSERT INTO Repuesto (Codigo, Nombre, Descripcion, Cantidad, Precio) VALUES ('10478', 'CABLES PARA INTERFACES DE CONEXION PERIFERICO P1', 'Repuesto para consistencia del equipo', 7350, '487989.72');
	INSERT INTO Repuesto (Codigo, Nombre, Descripcion, Cantidad, Precio) VALUES ('16351', 'CABLE HDMI', 'Repuesto para equipo', 1718, '4898387.41');
	INSERT INTO Repuesto (Codigo, Nombre, Descripcion, Cantidad, Precio) VALUES ('18269', 'MICA MI', NULL, 4645, '6292429.51');
	INSERT INTO Repuesto (Codigo, Nombre, Descripcion, Cantidad, Precio) VALUES ('18334', 'CABLES PARA INTERFACES DE CONEXION PERIFERICO P2', NULL, 6416, '2732829.51');
	INSERT INTO Repuesto (Codigo, Nombre, Descripcion, Cantidad, Precio) VALUES ('19625', 'CABLE HDMI HD', NULL, 6826, '8839832.55');
	INSERT INTO Repuesto (Codigo, Nombre, Descripcion, Cantidad, Precio) VALUES ('20151', 'TARJETA MADRE RC5', 'Repuesto para consistencia del equipo', 453, '1953448.03');
	INSERT INTO Repuesto (Codigo, Nombre, Descripcion, Cantidad, Precio) VALUES ('22671', 'BATERIA', NULL, 2222, '1848531.88');
	INSERT INTO Repuesto (Codigo, Nombre, Descripcion, Cantidad, Precio) VALUES ('23751', 'BATERIA RS3', 'Repuesto para equipo', 8306, '8966383.35');
	INSERT INTO Repuesto (Codigo, Nombre, Descripcion, Cantidad, Precio) VALUES ('25264', 'CABLE HDMI HD-2', NULL, 926, '1330399.76');
	INSERT INTO Repuesto (Codigo, Nombre, Descripcion, Cantidad, Precio) VALUES ('2564', 'CABLE HDMI HD-3', 'Repuesto para equipo', 4594, '5229303.51');
	INSERT INTO Repuesto (Codigo, Nombre, Descripcion, Cantidad, Precio) VALUES ('2688', 'TARJETA MADRE RC6', 'Repuesto para equipo', 9232, '2300340.33');
	INSERT INTO Repuesto (Codigo, Nombre, Descripcion, Cantidad, Precio) VALUES ('30620', 'CABLES PARA INTERFACES DE CONEXION PERIFERICO P3', 'Repuesto para consistencia del equipo', 3396, '7936374.45');
	INSERT INTO Repuesto (Codigo, Nombre, Descripcion, Cantidad, Precio) VALUES ('33489', 'TARJETA MADRE RC7', 'Repuesto para equipo', 876, '5783557.28');
	INSERT INTO Repuesto (Codigo, Nombre, Descripcion, Cantidad, Precio) VALUES ('36674', 'CABLE HDMI HD-4', 'Repuesto para equipo', 4063, '9036567.73');
	INSERT INTO Repuesto (Codigo, Nombre, Descripcion, Cantidad, Precio) VALUES ('37661', 'TARJETA MADRE RC8', 'Repuesto para equipo', 4425, '9580644.42');
	INSERT INTO Repuesto (Codigo, Nombre, Descripcion, Cantidad, Precio) VALUES ('38621', 'CABLES PARA INTERFACES DE CONEXION PERIFERICO P4', NULL, 930, '5435773.88');
	INSERT INTO Repuesto (Codigo, Nombre, Descripcion, Cantidad, Precio) VALUES ('91839', 'CABLES PARA INTERFACES DE CONEXION PERIFERICO P5', NULL, 4906, '5435773.88');
	INSERT INTO Repuesto (Codigo, Nombre, Descripcion, Cantidad, Precio) VALUES ('92712', 'DUAL PERIFERICO E/S 32C', NULL, 4866, '5435773.88');
	INSERT INTO Repuesto (Codigo, Nombre, Descripcion, Cantidad, Precio) VALUES ('94747', 'CABLE PUERTO E/S', NULL, 9098, '5435773.88');

UNLOCK TABLES;

--
-- DUMP TABLE Herramienta
-- 

LOCK TABLES Herramienta WRITE;

	INSERT INTO Herramienta (Codigo, CI_Personal, Nombre, Descripcion, Cantidad, Precio, Cant_Uso, Precio_Uso) VALUES ('40315', 36145388, 'CABLE COAXIAL', 'Herramienta de reparación de equipo', 9711, '1359401.39', 1953, '599367.19');
	INSERT INTO Herramienta (Codigo, CI_Personal, Nombre, Descripcion, Cantidad, Precio, Cant_Uso, Precio_Uso) VALUES ('4146', 27423439, 'DESTORNILLADOR', 'Herramienta de reparación de equipo', 2931, '7331484.89', 742, '8949441.51');
	INSERT INTO Herramienta (Codigo, CI_Personal, Nombre, Descripcion, Cantidad, Precio, Cant_Uso, Precio_Uso) VALUES ('42391', 53290784, 'PEGA 3', NULL, 7396, '6312477.56', 757, '7904060.77');
	INSERT INTO Herramienta (Codigo, CI_Personal, Nombre, Descripcion, Cantidad, Precio, Cant_Uso, Precio_Uso) VALUES ('43240', 27423439, 'DESTORNILLADOR D3', 'Herramienta para equipo', 4219, '3263305.72', 762, '1206366.56');
	INSERT INTO Herramienta (Codigo, CI_Personal, Nombre, Descripcion, Cantidad, Precio, Cant_Uso, Precio_Uso) VALUES ('47404', 27423439, 'PEGA 4', 'Herramienta para equipo', 1718, '1225175.94', 50, '7488598.17');
	INSERT INTO Herramienta (Codigo, CI_Personal, Nombre, Descripcion, Cantidad, Precio, Cant_Uso, Precio_Uso) VALUES ('48027', 56973278, 'LINTERNA', NULL, 7836, '3954529.52', 10, '4834465.82');
	INSERT INTO Herramienta (Codigo, CI_Personal, Nombre, Descripcion, Cantidad, Precio, Cant_Uso, Precio_Uso) VALUES ('51846', 56973278, 'CABLE UTP', 'Herramienta para equipo', 814, '7868563.60', 35, '4772225.33');
	INSERT INTO Herramienta (Codigo, CI_Personal, Nombre, Descripcion, Cantidad, Precio, Cant_Uso, Precio_Uso) VALUES ('52713', 53290784, 'PEGA P-3', 'Herramienta para equipo', 1505, '5785938.86', 1322, '2548267.51');
	INSERT INTO Herramienta (Codigo, CI_Personal, Nombre, Descripcion, Cantidad, Precio, Cant_Uso, Precio_Uso) VALUES ('52844', 53290784, 'PEGA P-4', 'Herramienta para equipo', 1128, '6694041.02', 1128, '2179260.24');

UNLOCK TABLES;

--
-- DUMP TABLE EquipoVenta
-- 

LOCK TABLES EquipoVenta WRITE;

	INSERT INTO EquipoVenta (Codigo, ID_Categoria, Nombre, Descripcion, Cantidad, Precio) VALUES ('52914', 5, 'AMGOO', 'Equipo computacional', 4372, '2607797.54');
	INSERT INTO EquipoVenta (Codigo, ID_Categoria, Nombre, Descripcion, Cantidad, Precio) VALUES ('5497', 2, 'LG L6', NULL, 8139, '6907951.47');
	INSERT INTO EquipoVenta (Codigo, ID_Categoria, Nombre, Descripcion, Cantidad, Precio) VALUES ('5728', 10, 'ACER GATEWAY 1411', NULL, 1965, '1353107.13');
	INSERT INTO EquipoVenta (Codigo, ID_Categoria, Nombre, Descripcion, Cantidad, Precio) VALUES ('59291', 7, 'SAMSUNG S5', 'Equipo computacional', 920, '7458311.43');
	INSERT INTO EquipoVenta (Codigo, ID_Categoria, Nombre, Descripcion, Cantidad, Precio) VALUES ('61563', 4, 'LENOVO', 'Equipo a la venta', 8216, '7164995.72');
	INSERT INTO EquipoVenta (Codigo, ID_Categoria, Nombre, Descripcion, Cantidad, Precio) VALUES ('62596', 10, 'INTEL CELERON INSIDE', 'Equipo computacional', 9867, '9727060.11');
	INSERT INTO EquipoVenta (Codigo, ID_Categoria, Nombre, Descripcion, Cantidad, Precio) VALUES ('64073', 6, 'LG L4', 'Equipo a la venta', 9391, '2927323.86');
	INSERT INTO EquipoVenta (Codigo, ID_Categoria, Nombre, Descripcion, Cantidad, Precio) VALUES ('71498', 4, 'SAMSUNG S4', 'Equipo computacional', 9819, '1323405.40');
	INSERT INTO EquipoVenta (Codigo, ID_Categoria, Nombre, Descripcion, Cantidad, Precio) VALUES ('71501', 5, 'LG L4', 'Equipo a la venta', 4065, '9819285.66');
	INSERT INTO EquipoVenta (Codigo, ID_Categoria, Nombre, Descripcion, Cantidad, Precio) VALUES ('73107', 1, 'SAMSUNG S5', 'Equipo computacional', 6106, '7007831.21');
	INSERT INTO EquipoVenta (Codigo, ID_Categoria, Nombre, Descripcion, Cantidad, Precio) VALUES ('75408', 9, 'ACER GATEWAY 1411', NULL, 7322, '2485.65');
	INSERT INTO EquipoVenta (Codigo, ID_Categoria, Nombre, Descripcion, Cantidad, Precio) VALUES ('81495', 6, 'SAMSUNG S6', 'Equipo computacional', 4601, '2119824.17');
	INSERT INTO EquipoVenta (Codigo, ID_Categoria, Nombre, Descripcion, Cantidad, Precio) VALUES ('82568', 7, 'LENOVO', 'Equipo computacional', 7996, '1391103.80');
	INSERT INTO EquipoVenta (Codigo, ID_Categoria, Nombre, Descripcion, Cantidad, Precio) VALUES ('82820', 7, 'LG L6', 'Equipo a la venta', 7620, '5175242.43');
	INSERT INTO EquipoVenta (Codigo, ID_Categoria, Nombre, Descripcion, Cantidad, Precio) VALUES ('83060', 4, 'SAMSUNG S7', 'Equipo a la venta', 3477, '5180010.53');
	INSERT INTO EquipoVenta (Codigo, ID_Categoria, Nombre, Descripcion, Cantidad, Precio) VALUES ('83364', 5, 'LG L7', 'Equipo a la venta', 1990, '7819285.21');
	INSERT INTO EquipoVenta (Codigo, ID_Categoria, Nombre, Descripcion, Cantidad, Precio) VALUES ('83593', 1, 'SAMSUNG S8', 'Equipo computacional', 6254, '7017831.22');
	INSERT INTO EquipoVenta (Codigo, ID_Categoria, Nombre, Descripcion, Cantidad, Precio) VALUES ('83611', 9, 'MAC', NULL, 5234, '2485.62');
	INSERT INTO EquipoVenta (Codigo, ID_Categoria, Nombre, Descripcion, Cantidad, Precio) VALUES ('86136', 6, 'BlackBerry', 'Equipo computacional', 994, '1629824.16');
	INSERT INTO EquipoVenta (Codigo, ID_Categoria, Nombre, Descripcion, Cantidad, Precio) VALUES ('8888', 7, 'LENOVO PENTIUM 4', 'Equipo computacional', 2508, '1411103.10');
	INSERT INTO EquipoVenta (Codigo, ID_Categoria, Nombre, Descripcion, Cantidad, Precio) VALUES ('89009', 7, 'HUAWEI', 'Equipo a la venta', 9591, '5275242.43');
	INSERT INTO EquipoVenta (Codigo, ID_Categoria, Nombre, Descripcion, Cantidad, Precio) VALUES ('91201', 4, 'IPAD', 'Equipo a la venta', 8207, '5230010.51');

UNLOCK TABLES;

--
-- DUMP TABLE Proveedor
--

LOCK TABLES Proveedor WRITE;

	INSERT INTO Proveedor (RIF, Tipo_RIF, Direccion, Nombre, Email, Telefono) VALUES ('1541887123', 'G', '538 Priscilla Manors Apt. 057', 'Marvin, Ryan and Johns', 'mkulas@mclaughlin.com', '79428568673');
	INSERT INTO Proveedor (RIF, Tipo_RIF, Direccion, Nombre, Email, Telefono) VALUES ('1904015615', 'J', '084 Rubye Unions', 'Hilll Inc', 'keshaun53@blickpagac.com', '17901239581');
	INSERT INTO Proveedor (RIF, Tipo_RIF, Direccion, Nombre, Email, Telefono) VALUES ('2084380257', 'G', '639 Spinka Isle', 'Schneider-Schumm', 'mcdermott.adolphus@runolfsson.biz', '66714873844');
	INSERT INTO Proveedor (RIF, Tipo_RIF, Direccion, Nombre, Email, Telefono) VALUES ('2259089403', 'J', '9920 Herzog Divide Apt. 448', 'Hermann, Boyle and Schowalter', 'wmorar@jonesgaylord.org', '17058971049');
	INSERT INTO Proveedor (RIF, Tipo_RIF, Direccion, Nombre, Email, Telefono) VALUES ('2312558255', 'J', '484 Francesco Greens', 'Jaskolski-Pouros', 'htremblay@yundt.com', '13779907664');
	INSERT INTO Proveedor (RIF, Tipo_RIF, Direccion, Nombre, Email, Telefono) VALUES ('2607662921', 'G', '1974 Marquise Ford', 'Hamill and Sons', 'violette14@yost.com', '64496989157');
	INSERT INTO Proveedor (RIF, Tipo_RIF, Direccion, Nombre, Email, Telefono) VALUES ('2736012909', 'G', '97529 Lueilwitz Islands Apt. 903', 'DuBuque, Ebert and Witting', 'renner.vladimir@schulist.info', '91644525513');
	INSERT INTO Proveedor (RIF, Tipo_RIF, Direccion, Nombre, Email, Telefono) VALUES ('2802982995', 'J', '102 McCullough Expressway Apt. 347', 'Lebsack Group', 'jesse.schinner@walker.com', '90950855761');
	INSERT INTO Proveedor (RIF, Tipo_RIF, Direccion, Nombre, Email, Telefono) VALUES ('3306600502', 'J', '498 Lulu Port Suite 398', 'Schmitt Group', 'koepp.vinnie@green.net', '54567915950');
	INSERT INTO Proveedor (RIF, Tipo_RIF, Direccion, Nombre, Email, Telefono) VALUES ('3557837648', 'J', '758 Barton Ridge Suite 629', 'Schaden-Bode', 'mtoy@boscocummerata.info', '31707453839');
	INSERT INTO Proveedor (RIF, Tipo_RIF, Direccion, Nombre, Email, Telefono) VALUES ('3604887211', 'J', '5358 Lia Shoals Suite 212', 'Bernier Inc', 'kuhn.jamir@price.com', '59625550038');
	INSERT INTO Proveedor (RIF, Tipo_RIF, Direccion, Nombre, Email, Telefono) VALUES ('3680972675', 'J', '1368 Carlee Terrace Apt. 435', 'Marks-Heaney', 'braden19@kerlukeauer.com', '86512803700');
	INSERT INTO Proveedor (RIF, Tipo_RIF, Direccion, Nombre, Email, Telefono) VALUES ('3766016693', 'J', '26709 Bernard Well Suite 512', 'Cummings Group', 'hnienow@oreillykemmer.com', '79185451027');
	INSERT INTO Proveedor (RIF, Tipo_RIF, Direccion, Nombre, Email, Telefono) VALUES ('4179571988', 'G', '9257 Adelia Dale', 'Corkery-Schroeder', 'williamson.noe@simonis.org', '49315893542');
	INSERT INTO Proveedor (RIF, Tipo_RIF, Direccion, Nombre, Email, Telefono) VALUES ('4229885128', 'J', '2043 Andrew Roads', 'White, Kuhic and Gerhold', 'lulu.heaney@mueller.net', '85291374246');
	INSERT INTO Proveedor (RIF, Tipo_RIF, Direccion, Nombre, Email, Telefono) VALUES ('4299484232', 'J', '70653 Nathen Park Suite 830', 'Hartmann-Monahan', 'hugh82@kub.info', '64408372407');
	INSERT INTO Proveedor (RIF, Tipo_RIF, Direccion, Nombre, Email, Telefono) VALUES ('4334347836', 'G', '0903 Anderson Isle', 'Johnson Ltd', 'hillard66@von.biz', '34870066647');
	INSERT INTO Proveedor (RIF, Tipo_RIF, Direccion, Nombre, Email, Telefono) VALUES ('4874382655', 'G', '43009 Noble Groves Apt. 347', 'Wisoky-Monahan', 'crona.sven@medhurstnitzsche.com', '19288415974');
	INSERT INTO Proveedor (RIF, Tipo_RIF, Direccion, Nombre, Email, Telefono) VALUES ('5252675292', 'G', '5609 Ledner Pine Suite 591', 'Paucek, Hansen and Kuhlman', 'ffeest@runolfsson.com', '72999793905');
	INSERT INTO Proveedor (RIF, Tipo_RIF, Direccion, Nombre, Email, Telefono) VALUES ('5354966181', 'J', '0850 Amie Cove', 'Wiegand, Pouros and Auer', 'zwaters@borertromp.com', '17978789662');
	INSERT INTO Proveedor (RIF, Tipo_RIF, Direccion, Nombre, Email, Telefono) VALUES ('5477514320', 'G', '7945 Hackett Fords', 'Walsh-Halvorson', 'cole.dach@paucek.com', '74164528921');
	INSERT INTO Proveedor (RIF, Tipo_RIF, Direccion, Nombre, Email, Telefono) VALUES ('5503040112', 'G', '130 Langosh Coves Suite 171', 'Schaden, Lueilwitz and Roob', 'rocky.abshire@brekkemorissette.org', '93201305023');
	INSERT INTO Proveedor (RIF, Tipo_RIF, Direccion, Nombre, Email, Telefono) VALUES ('5544671420', 'G', '612 Green Extension', 'Barton and Sons', 'rolfson.dortha@collins.com', '29344745790');
	INSERT INTO Proveedor (RIF, Tipo_RIF, Direccion, Nombre, Email, Telefono) VALUES ('5587593285', 'G', '3808 Dominique Plains', 'Roberts Inc', 'jamie.kub@terry.biz', '15209417277');
	INSERT INTO Proveedor (RIF, Tipo_RIF, Direccion, Nombre, Email, Telefono) VALUES ('5614337859', 'J', '5658 Kunze Mill', 'Langosh-Grimes', 'schimmel.brannon@kuhn.com', '38520009759');
	INSERT INTO Proveedor (RIF, Tipo_RIF, Direccion, Nombre, Email, Telefono) VALUES ('5637769851', 'G', '029 Corwin Plain', 'Fritsch-Lehner', 'alang@koepp.com', '49690148327');
	INSERT INTO Proveedor (RIF, Tipo_RIF, Direccion, Nombre, Email, Telefono) VALUES ('5775683946', 'J', '819 Kuphal Creek Suite 537', 'Hoppe, Bartoletti and Borer', 'dorris75@keeling.com', '19998518908');
	INSERT INTO Proveedor (RIF, Tipo_RIF, Direccion, Nombre, Email, Telefono) VALUES ('6339617126', 'G', '1080 Christina Mall Suite 354', 'Dibbert-Weber', 'immanuel64@mayert.com', '95820017266');
	INSERT INTO Proveedor (RIF, Tipo_RIF, Direccion, Nombre, Email, Telefono) VALUES ('6409739906', 'G', '93558 Corwin Gardens', 'Kiehn, Abbott and Kessler', 'yesenia98@wehner.com', '38089131782');
	INSERT INTO Proveedor (RIF, Tipo_RIF, Direccion, Nombre, Email, Telefono) VALUES ('6460615468', 'J', '5717 Gerald Trail', 'Rice and Sons', 'melody00@howe.com', '40159371006');
	INSERT INTO Proveedor (RIF, Tipo_RIF, Direccion, Nombre, Email, Telefono) VALUES ('6503882827', 'J', '303 Athena Forge Apt. 367', 'Feest LLC', 'reichert.clemens@hilll.com', '64903574660');
	INSERT INTO Proveedor (RIF, Tipo_RIF, Direccion, Nombre, Email, Telefono) VALUES ('6546809290', 'J', '739 VonRueden Plain', 'Kovacek-Lindgren', 'tina27@jakubowski.net', '79266794798');
	INSERT INTO Proveedor (RIF, Tipo_RIF, Direccion, Nombre, Email, Telefono) VALUES ('6613845882', 'G', '5533 Baby Forge Apt. 907', 'Hamill, Skiles and Mueller', 'alexis48@hamill.biz', '28164075016');
	INSERT INTO Proveedor (RIF, Tipo_RIF, Direccion, Nombre, Email, Telefono) VALUES ('6852379868', 'J', '61659 Daugherty Trafficway', 'Raynor PLC', 'maximus.zieme@krajcik.com', '34784970339');
	INSERT INTO Proveedor (RIF, Tipo_RIF, Direccion, Nombre, Email, Telefono) VALUES ('7027146087', 'J', '1621 Ola Station Apt. 665', 'Blanda-Emmerich', 'bode.jimmie@christiansen.info', '34813434025');
	INSERT INTO Proveedor (RIF, Tipo_RIF, Direccion, Nombre, Email, Telefono) VALUES ('7084027404', 'G', '6333 Kreiger Field', 'Nikolaus, Hettinger and Strosin', 'sidney82@walker.com', '19749911786');
	INSERT INTO Proveedor (RIF, Tipo_RIF, Direccion, Nombre, Email, Telefono) VALUES ('7122454935', 'J', '42621 Keeling Lake Apt. 832', 'Stehr, Dare and Schamberger', 'ibradtke@volkmanweber.com', '27801240491');
	INSERT INTO Proveedor (RIF, Tipo_RIF, Direccion, Nombre, Email, Telefono) VALUES ('7468380684', 'J', '1980 Renner Spurs', 'Langosh Ltd', 'brandt.stiedemann@cristbailey.com', '34258576300');
	INSERT INTO Proveedor (RIF, Tipo_RIF, Direccion, Nombre, Email, Telefono) VALUES ('7622061974', 'G', '9498 Stiedemann Mall', 'Lind-Flatley', 'gwendolyn.crona@marvindavis.org', '70913620390');
	INSERT INTO Proveedor (RIF, Tipo_RIF, Direccion, Nombre, Email, Telefono) VALUES ('7725879862', 'J', '117 Lavern Turnpike', 'Daniel-Jerde', 'kendrick32@emardpfeffer.com', '65273555135');
	INSERT INTO Proveedor (RIF, Tipo_RIF, Direccion, Nombre, Email, Telefono) VALUES ('7951989013', 'G', '11511 Clementine Mission', 'Schneider, Altenwerth and Dare', 'christiansen.vivianne@doylewill.com', '26647801264');
	INSERT INTO Proveedor (RIF, Tipo_RIF, Direccion, Nombre, Email, Telefono) VALUES ('8390533217', 'G', '8232 Hermina Centers', 'Morissette-Lesch', 'ferry.twila@rogahnhilpert.com', '96935644219');
	INSERT INTO Proveedor (RIF, Tipo_RIF, Direccion, Nombre, Email, Telefono) VALUES ('8627721015', 'G', '959 Reginald Lane Suite 079', 'Balistreri and Sons', 'waelchi.frederic@kessler.net', '16047061891');
	INSERT INTO Proveedor (RIF, Tipo_RIF, Direccion, Nombre, Email, Telefono) VALUES ('8694958056', 'J', '36853 Serena Rest', 'Mayert, Yundt and Volkman', 'lcrooks@larkinrenner.com', '59700594893');
	INSERT INTO Proveedor (RIF, Tipo_RIF, Direccion, Nombre, Email, Telefono) VALUES ('9035177997', 'G', '4425 Stamm Grove', 'Fahey-Ebert', 'jerde.maegan@olson.com', '15567990527');
	INSERT INTO Proveedor (RIF, Tipo_RIF, Direccion, Nombre, Email, Telefono) VALUES ('9071168377', 'G', '65764 Schamberger Points', 'Adams-Ritchie', 'beatty.bella@wuckert.com', '11257442301');
	INSERT INTO Proveedor (RIF, Tipo_RIF, Direccion, Nombre, Email, Telefono) VALUES ('9339956821', 'J', '96013 Schowalter Shoal', 'Mueller-Rempel', 'mara28@murphy.net', '95946151455');
	INSERT INTO Proveedor (RIF, Tipo_RIF, Direccion, Nombre, Email, Telefono) VALUES ('9609266972', 'G', '374 Metz Motorway Suite 955', 'Orn Inc', 'shanelle40@langjohnston.info', '34371339888');
	INSERT INTO Proveedor (RIF, Tipo_RIF, Direccion, Nombre, Email, Telefono) VALUES ('9734091711', 'G', '42169 Katarina Walks Suite 243', 'Bailey-Watsica', 'heller.gina@schmitt.biz', '97683986444');
	INSERT INTO Proveedor (RIF, Tipo_RIF, Direccion, Nombre, Email, Telefono) VALUES ('9818162875', 'J', '2298 Jasmin Rapid', 'Abbott LLC', 'webster21@grimesmcdermott.com', '42607753882');

UNLOCK TABLES;

--
-- DUMP TABLE FacturaCompra
--

LOCK TABLES FacturaCompra WRITE;

	INSERT INTO FacturaCompra (Numero, RIF_Proveed, Fecha, Estado, Descripcion, Monto) VALUES ('14534', '7122454935', '2018-04-08 10:38:31', 'Pagado', 'Pago efectuado', '2633622.05');
	INSERT INTO FacturaCompra (Numero, RIF_Proveed, Fecha, Estado, Descripcion, Monto) VALUES ('16168', '6339617126', '2018-04-03 15:03:08', 'Procesado', 'Pago efectuado', '4400546.18');
	INSERT INTO FacturaCompra (Numero, RIF_Proveed, Fecha, Estado, Descripcion, Monto) VALUES ('19074', '3766016693', '2018-04-08 02:32:44', 'Facturado', 'Pago efectuado', '2169169.78');
	INSERT INTO FacturaCompra (Numero, RIF_Proveed, Fecha, Estado, Descripcion, Monto) VALUES ('19141', '5775683946', '2018-03-20 03:37:09', 'Facturado', 'Pago efectuado', '5053038.00');
	INSERT INTO FacturaCompra (Numero, RIF_Proveed, Fecha, Estado, Descripcion, Monto) VALUES ('19668', '7468380684', '2018-04-10 10:53:28', 'Procesado', NULL, '7132840.70');
	INSERT INTO FacturaCompra (Numero, RIF_Proveed, Fecha, Estado, Descripcion, Monto) VALUES ('20842', '5587593285', '2018-03-31 09:52:05', 'Pagado', NULL, '9171681.16');
	INSERT INTO FacturaCompra (Numero, RIF_Proveed, Fecha, Estado, Descripcion, Monto) VALUES ('21358', '3766016693', '2018-03-30 12:07:05', 'Procesado', NULL, '5754961.15');
	INSERT INTO FacturaCompra (Numero, RIF_Proveed, Fecha, Estado, Descripcion, Monto) VALUES ('25488', '6503882827', '2018-04-13 13:36:36', 'Facturado', 'Pago efectuado', '5070144.13');
	INSERT INTO FacturaCompra (Numero, RIF_Proveed, Fecha, Estado, Descripcion, Monto) VALUES ('26685', '5587593285', '2018-04-08 01:58:26', 'Facturado', 'Pago efectuado', '6270928.06');
	INSERT INTO FacturaCompra (Numero, RIF_Proveed, Fecha, Estado, Descripcion, Monto) VALUES ('28552', '6503882827', '2018-03-18 18:49:36', 'Procesado', NULL, '8817798.08');
	INSERT INTO FacturaCompra (Numero, RIF_Proveed, Fecha, Estado, Descripcion, Monto) VALUES ('31036', '2312558255', '2018-03-21 10:04:09', 'Pagado', 'Pago realizado para Proveedor', '7582051.43');
	INSERT INTO FacturaCompra (Numero, RIF_Proveed, Fecha, Estado, Descripcion, Monto) VALUES ('37649', '7622061974', '2018-03-22 08:15:41', 'Procesado', NULL, '4701545.27');
	INSERT INTO FacturaCompra (Numero, RIF_Proveed, Fecha, Estado, Descripcion, Monto) VALUES ('4003', '6852379868', '2018-03-25 00:15:19', 'Facturado', NULL, '4548365.54');
	INSERT INTO FacturaCompra (Numero, RIF_Proveed, Fecha, Estado, Descripcion, Monto) VALUES ('44111', '3557837648', '2018-03-17 06:28:47', 'Facturado', 'Pago realizado para Proveedor', '9101667.25');
	INSERT INTO FacturaCompra (Numero, RIF_Proveed, Fecha, Estado, Descripcion, Monto) VALUES ('44637', '7122454935', '2018-04-15 12:19:10', 'Facturado', 'Pago realizado para Proveedor', '9223003.19');
	INSERT INTO FacturaCompra (Numero, RIF_Proveed, Fecha, Estado, Descripcion, Monto) VALUES ('46553', '1541887123', '2018-04-06 23:22:11', 'Pagado', NULL, '1503063.69');
	INSERT INTO FacturaCompra (Numero, RIF_Proveed, Fecha, Estado, Descripcion, Monto) VALUES ('46874', '6852379868', '2018-03-27 00:10:42', 'Procesado', 'Pago realizado para Proveedor', '334869.66');
	INSERT INTO FacturaCompra (Numero, RIF_Proveed, Fecha, Estado, Descripcion, Monto) VALUES ('46981', '5477514320', '2018-03-24 02:11:23', 'Pagado', 'Pago efectuado', '4652106.51');
	INSERT INTO FacturaCompra (Numero, RIF_Proveed, Fecha, Estado, Descripcion, Monto) VALUES ('49672', '1541887123', '2018-04-01 18:08:36', 'Pagado', 'Pago efectuado', '747972.67');
	INSERT INTO FacturaCompra (Numero, RIF_Proveed, Fecha, Estado, Descripcion, Monto) VALUES ('5080', '7622061974', '2018-04-08 11:41:39', 'Facturado', 'Pago efectuado', '96094.16');
	INSERT INTO FacturaCompra (Numero, RIF_Proveed, Fecha, Estado, Descripcion, Monto) VALUES ('51376', '3557837648', '2018-03-22 23:54:55', 'Pagado', 'Pago efectuado', '4273030.11');
	INSERT INTO FacturaCompra (Numero, RIF_Proveed, Fecha, Estado, Descripcion, Monto) VALUES ('52299', '2607662921', '2018-03-18 14:33:24', 'Procesado', 'Pago realizado para Proveedor', '6326272.85');
	INSERT INTO FacturaCompra (Numero, RIF_Proveed, Fecha, Estado, Descripcion, Monto) VALUES ('53750', '9734091711', '2018-04-09 09:09:34', 'Procesado', 'Pago efectuado', '8884531.61');
	INSERT INTO FacturaCompra (Numero, RIF_Proveed, Fecha, Estado, Descripcion, Monto) VALUES ('56044', '6546809290', '2018-04-14 09:39:36', 'Procesado', NULL, '5312518.90');
	INSERT INTO FacturaCompra (Numero, RIF_Proveed, Fecha, Estado, Descripcion, Monto) VALUES ('56859', '7468380684', '2018-03-23 07:47:59', 'Facturado', 'Pago realizado para Proveedor', '7767687.61');
	INSERT INTO FacturaCompra (Numero, RIF_Proveed, Fecha, Estado, Descripcion, Monto) VALUES ('57636', '7122454935', '2018-03-22 07:45:34', 'Procesado', NULL, '3435329.63');
	INSERT INTO FacturaCompra (Numero, RIF_Proveed, Fecha, Estado, Descripcion, Monto) VALUES ('5803', '6409739906', '2018-04-04 12:29:20', 'Pagado', 'Pago efectuado', '1257805.32');
	INSERT INTO FacturaCompra (Numero, RIF_Proveed, Fecha, Estado, Descripcion, Monto) VALUES ('60423', '7725879862', '2018-03-28 18:19:28', 'Facturado', NULL, '7156469.70');
	INSERT INTO FacturaCompra (Numero, RIF_Proveed, Fecha, Estado, Descripcion, Monto) VALUES ('62707', '6460615468', '2018-03-23 16:52:56', 'Facturado', 'Pago efectuado', '6436485.98');
	INSERT INTO FacturaCompra (Numero, RIF_Proveed, Fecha, Estado, Descripcion, Monto) VALUES ('63958', '9609266972', '2018-04-09 22:03:21', 'Procesado', 'Pago realizado para Proveedor', '6557722.98');
	INSERT INTO FacturaCompra (Numero, RIF_Proveed, Fecha, Estado, Descripcion, Monto) VALUES ('67581', '6460615468', '2018-03-29 20:11:00', 'Pagado', 'Pago efectuado', '4541832.86');
	INSERT INTO FacturaCompra (Numero, RIF_Proveed, Fecha, Estado, Descripcion, Monto) VALUES ('72995', '4874382655', '2018-03-22 04:37:38', 'Procesado', 'Pago realizado para Proveedor', '234591.29');
	INSERT INTO FacturaCompra (Numero, RIF_Proveed, Fecha, Estado, Descripcion, Monto) VALUES ('73349', '6852379868', '2018-04-13 05:25:34', 'Facturado', 'Pago realizado para Proveedor', '1343318.47');
	INSERT INTO FacturaCompra (Numero, RIF_Proveed, Fecha, Estado, Descripcion, Monto) VALUES ('77111', '7027146087', '2018-03-19 15:44:42', 'Pagado', 'Pago realizado para Proveedor', '2517842.46');
	INSERT INTO FacturaCompra (Numero, RIF_Proveed, Fecha, Estado, Descripcion, Monto) VALUES ('78622', '7622061974', '2018-03-16 16:37:58', 'Procesado', 'Pago efectuado', '5383246.30');
	INSERT INTO FacturaCompra (Numero, RIF_Proveed, Fecha, Estado, Descripcion, Monto) VALUES ('83441', '3306600502', '2018-03-28 16:40:18', 'Pagado', 'Pago efectuado', '8821705.05');
	INSERT INTO FacturaCompra (Numero, RIF_Proveed, Fecha, Estado, Descripcion, Monto) VALUES ('86933', '2259089403', '2018-03-26 05:20:09', 'Pagado', NULL, '9590594.58');
	INSERT INTO FacturaCompra (Numero, RIF_Proveed, Fecha, Estado, Descripcion, Monto) VALUES ('87903', '3680972675', '2018-03-20 21:19:00', 'Facturado', NULL, '6516252.56');
	INSERT INTO FacturaCompra (Numero, RIF_Proveed, Fecha, Estado, Descripcion, Monto) VALUES ('89176', '9818162875', '2018-04-11 11:33:04', 'Facturado', NULL, '3660265.01');
	INSERT INTO FacturaCompra (Numero, RIF_Proveed, Fecha, Estado, Descripcion, Monto) VALUES ('90129', '7622061974', '2018-04-02 20:10:47', 'Pagado', NULL, '131757.59');
	INSERT INTO FacturaCompra (Numero, RIF_Proveed, Fecha, Estado, Descripcion, Monto) VALUES ('90288', '3680972675', '2018-04-13 17:10:42', 'Pagado', 'Pago efectuado', '386592.66');
	INSERT INTO FacturaCompra (Numero, RIF_Proveed, Fecha, Estado, Descripcion, Monto) VALUES ('90836', '7951989013', '2018-03-30 17:50:46', 'Pagado', 'Pago realizado para Proveedor', '9340495.28');
	INSERT INTO FacturaCompra (Numero, RIF_Proveed, Fecha, Estado, Descripcion, Monto) VALUES ('91334', '6339617126', '2018-03-29 15:52:19', 'Procesado', 'Pago realizado para Proveedor', '5290266.70');
	INSERT INTO FacturaCompra (Numero, RIF_Proveed, Fecha, Estado, Descripcion, Monto) VALUES ('91873', '5614337859', '2018-03-18 07:47:28', 'Pagado', 'Pago realizado para Proveedor', '5107193.34');
	INSERT INTO FacturaCompra (Numero, RIF_Proveed, Fecha, Estado, Descripcion, Monto) VALUES ('9437', '2084380257', '2018-03-16 19:45:09', 'Facturado', 'Pago efectuado', '2154268.89');
	INSERT INTO FacturaCompra (Numero, RIF_Proveed, Fecha, Estado, Descripcion, Monto) VALUES ('94922', '5477514320', '2018-03-25 23:09:15', 'Procesado', NULL, '9347883.95');
	INSERT INTO FacturaCompra (Numero, RIF_Proveed, Fecha, Estado, Descripcion, Monto) VALUES ('96311', '7951989013', '2018-04-02 04:58:51', 'Pagado', 'Pago efectuado', '9932331.23');
	INSERT INTO FacturaCompra (Numero, RIF_Proveed, Fecha, Estado, Descripcion, Monto) VALUES ('98322', '3604887211', '2018-04-11 02:17:32', 'Pagado', NULL, '1744098.95');
	INSERT INTO FacturaCompra (Numero, RIF_Proveed, Fecha, Estado, Descripcion, Monto) VALUES ('99296', '7468380684', '2018-03-19 00:09:05', 'Procesado', 'Pago realizado para Proveedor', '9837877.70');
	INSERT INTO FacturaCompra (Numero, RIF_Proveed, Fecha, Estado, Descripcion, Monto) VALUES ('99652', '2312558255', '2018-04-03 03:05:47', 'Pagado', 'Pago realizado para Proveedor', '3899325.68');

UNLOCK TABLES;

--
-- DUMP TABLE Cliente
--

LOCK TABLES Cliente WRITE;

	INSERT INTO Cliente (Identif, Tipo_Identif, Direccion, Nombre, Email, Telefono) VALUES ('106191025', 'E', '83721 Walsh Gateway', 'Breanne Bartoletti', 'jacobi.carley@yahoo.com', '29553129207');
	INSERT INTO Cliente (Identif, Tipo_Identif, Direccion, Nombre, Email, Telefono) VALUES ('1107166870', 'E', '8586 O\'Kon Grove', 'Dr. Wilfredo Vandervort', 'kyla55@kulas.com', '35242578415');
	INSERT INTO Cliente (Identif, Tipo_Identif, Direccion, Nombre, Email, Telefono) VALUES ('1140901638', 'G', '2877 Tristian Street', 'Werner Schmitt', 'laney62@hotmail.com', '87611253703');
	INSERT INTO Cliente (Identif, Tipo_Identif, Direccion, Nombre, Email, Telefono) VALUES ('1248296668', 'G', '336 Hoppe Springs', 'Karson Ward', 'vickie74@balistreri.com', '18907023118');
	INSERT INTO Cliente (Identif, Tipo_Identif, Direccion, Nombre, Email, Telefono) VALUES ('1379583121', 'E', '453 Howell Causeway Suite 232', 'Adrianna Bailey', 'bferry@hotmail.com', '82014959920');
	INSERT INTO Cliente (Identif, Tipo_Identif, Direccion, Nombre, Email, Telefono) VALUES ('1447121725', 'V', '45497 Sadie Bypass', 'Sherman Jenkins DDS', 'chalvorson@hotmail.com', '96647943062');
	INSERT INTO Cliente (Identif, Tipo_Identif, Direccion, Nombre, Email, Telefono) VALUES ('1549385540', 'V', '575 Koch Lock', 'Dr. Darby Hand DVM', 'kyra.mosciski@croninlang.com', '35599346454');
	INSERT INTO Cliente (Identif, Tipo_Identif, Direccion, Nombre, Email, Telefono) VALUES ('1580326440', 'E', '438 Katarina Lakes', 'Everardo Schuppe Jr.', 'koepp.wilburn@mills.biz', '72244946402');
	INSERT INTO Cliente (Identif, Tipo_Identif, Direccion, Nombre, Email, Telefono) VALUES ('1894378886', 'E', '37691 Little Divide Suite 503', 'Ibrahim Littel', 'river29@ebertbahringer.biz', '12396260304');
	INSERT INTO Cliente (Identif, Tipo_Identif, Direccion, Nombre, Email, Telefono) VALUES ('1895222812', 'G', '09391 Kuhn Locks', 'Dayne Nolan', 'frami.julien@gmail.com', '85435787020');
	INSERT INTO Cliente (Identif, Tipo_Identif, Direccion, Nombre, Email, Telefono) VALUES ('1933073566', 'G', '031 Batz Loop Apt. 292', 'Adeline Dooley', 'jeanette.hammes@yahoo.com', '65423991186');
	INSERT INTO Cliente (Identif, Tipo_Identif, Direccion, Nombre, Email, Telefono) VALUES ('2250770075', 'V', '12252 Dortha Curve', 'Alfonso Prohaska', 'mchamplin@powlowskihyatt.net', '23299915203');
	INSERT INTO Cliente (Identif, Tipo_Identif, Direccion, Nombre, Email, Telefono) VALUES ('2429767043', 'G', '08172 Mckenna Roads Suite 820', 'Prof. Stone Bosco', 'klarson@gmail.com', '36812242795');
	INSERT INTO Cliente (Identif, Tipo_Identif, Direccion, Nombre, Email, Telefono) VALUES ('3103281006', 'V', '024 Iliana Throughway', 'Miss Zoey Dickinson', 'madyson21@hotmail.com', '79618943613');
	INSERT INTO Cliente (Identif, Tipo_Identif, Direccion, Nombre, Email, Telefono) VALUES ('3334690165', 'V', '7279 Ubaldo Cliffs Apt. 236', 'Jocelyn Rodriguez', 'russel.daija@reilly.biz', '53464720449');
	INSERT INTO Cliente (Identif, Tipo_Identif, Direccion, Nombre, Email, Telefono) VALUES ('3658549168', 'G', '0289 Corkery Well Apt. 976', 'Karli Thiel', 'name.boyle@paucekauer.biz', '64618438743');
	INSERT INTO Cliente (Identif, Tipo_Identif, Direccion, Nombre, Email, Telefono) VALUES ('4042473736', 'G', '2526 Fae Grove', 'Carmella Goodwin', 'drew66@wunschking.biz', '61432179915');
	INSERT INTO Cliente (Identif, Tipo_Identif, Direccion, Nombre, Email, Telefono) VALUES ('420040739', 'J', '02250 Sandra Cape Apt. 214', 'Breanne Corkery', 'eauer@yahoo.com', '36617280063');
	INSERT INTO Cliente (Identif, Tipo_Identif, Direccion, Nombre, Email, Telefono) VALUES ('4344068043', 'V', '939 Zachery Wall Suite 152', 'Monica Ziemann II', 'breitenberg.assunta@gmail.com', '28545981515');
	INSERT INTO Cliente (Identif, Tipo_Identif, Direccion, Nombre, Email, Telefono) VALUES ('4648459618', 'J', '225 Laverne Ranch Apt. 550', 'Ophelia Bahringer', 'klocko.erling@stracke.biz', '10349337314');
	INSERT INTO Cliente (Identif, Tipo_Identif, Direccion, Nombre, Email, Telefono) VALUES ('4700790442', 'E', '70023 Hintz Parkway Apt. 193', 'Shanny Willms', 'harrison.beahan@hotmail.com', '10711508579');
	INSERT INTO Cliente (Identif, Tipo_Identif, Direccion, Nombre, Email, Telefono) VALUES ('489783770', 'G', '98312 Mertz Mountains', 'Mr. Lavon Koss IV', 'icummings@ullrich.com', '83026184192');
	INSERT INTO Cliente (Identif, Tipo_Identif, Direccion, Nombre, Email, Telefono) VALUES ('4924162942', 'J', '08444 Shana Lane Suite 426', 'Dr. Tom Mitchell', 'udenesik@botsfordarmstrong.com', '18327202871');
	INSERT INTO Cliente (Identif, Tipo_Identif, Direccion, Nombre, Email, Telefono) VALUES ('5021133496', 'J', '3287 Veda Ferry Apt. 735', 'Yesenia Deckow', 'tara.feeney@yahoo.com', '63147723944');
	INSERT INTO Cliente (Identif, Tipo_Identif, Direccion, Nombre, Email, Telefono) VALUES ('5034358240', 'E', '596 Olaf Street Suite 415', 'Blair Torp', 'larissa.carroll@gmail.com', '15378652638');
	INSERT INTO Cliente (Identif, Tipo_Identif, Direccion, Nombre, Email, Telefono) VALUES ('5072576596', 'G', '2685 Prosacco Knolls', 'Gerda Lehner', 'miller.edd@reichel.net', '55602728114');
	INSERT INTO Cliente (Identif, Tipo_Identif, Direccion, Nombre, Email, Telefono) VALUES ('5219232517', 'J', '629 Fay Flat', 'Dr. Alejandrin Langosh DVM', 'darius.muller@romagueracummings.com', '86972925257');
	INSERT INTO Cliente (Identif, Tipo_Identif, Direccion, Nombre, Email, Telefono) VALUES ('5600934749', 'J', '36846 Roberta Harbor', 'Susanna Nicolas', 'elliott42@hotmail.com', '19118030741');
	INSERT INTO Cliente (Identif, Tipo_Identif, Direccion, Nombre, Email, Telefono) VALUES ('5623462726', 'V', '95564 Ethan Bridge Suite 934', 'Reginald Hane', 'swaniawski.carol@schiller.net', '62400358798');
	INSERT INTO Cliente (Identif, Tipo_Identif, Direccion, Nombre, Email, Telefono) VALUES ('5750068707', 'J', '940 Jefferey Parks', 'Brett Emmerich', 'gust64@okeefe.com', '50903428853');
	INSERT INTO Cliente (Identif, Tipo_Identif, Direccion, Nombre, Email, Telefono) VALUES ('5962500047', 'E', '258 Cole Garden Apt. 538', 'Keven Swaniawski', 'cristobal49@balistreribosco.com', '30838428889');
	INSERT INTO Cliente (Identif, Tipo_Identif, Direccion, Nombre, Email, Telefono) VALUES ('6114924311', 'J', '46999 Logan Valley Suite 621', 'Celestine Runolfsdottir', 'fsatterfield@zemlakwatsica.com', '19124293741');
	INSERT INTO Cliente (Identif, Tipo_Identif, Direccion, Nombre, Email, Telefono) VALUES ('6160975585', 'V', '19957 Stiedemann Burg Apt. 448', 'Hailie Stamm Sr.', 'ccrooks@gmail.com', '58349617803');
	INSERT INTO Cliente (Identif, Tipo_Identif, Direccion, Nombre, Email, Telefono) VALUES ('6279233983', 'J', '17959 Keeley Well Apt. 345', 'Annie Witting', 'kemmer.sasha@hintzboyle.com', '40825040591');
	INSERT INTO Cliente (Identif, Tipo_Identif, Direccion, Nombre, Email, Telefono) VALUES ('6361535682', 'G', '597 Natalia Camp Apt. 057', 'Tanner Mohr', 'tromp.simeon@gmail.com', '12143383775');
	INSERT INTO Cliente (Identif, Tipo_Identif, Direccion, Nombre, Email, Telefono) VALUES ('6484006930', 'G', '34577 Assunta Route Apt. 531', 'Kenneth Kohler', 'karina59@hegmann.com', '58621891690');
	INSERT INTO Cliente (Identif, Tipo_Identif, Direccion, Nombre, Email, Telefono) VALUES ('6512770808', 'J', '21098 Mraz Extensions Apt. 880', 'Ms. Deborah Zboncak IV', 'aleffler@treutel.net', '85921772909');
	INSERT INTO Cliente (Identif, Tipo_Identif, Direccion, Nombre, Email, Telefono) VALUES ('6828971550', 'V', '93571 Ernest Way Apt. 517', 'Flossie Schuppe', 'yschroeder@hotmail.com', '45842139292');
	INSERT INTO Cliente (Identif, Tipo_Identif, Direccion, Nombre, Email, Telefono) VALUES ('6897390523', 'V', '797 Gutkowski Wells', 'Julie Koelpin', 'leila22@yahoo.com', '89471598505');
	INSERT INTO Cliente (Identif, Tipo_Identif, Direccion, Nombre, Email, Telefono) VALUES ('7188356091', 'G', '1718 Koepp Center Suite 859', 'Dr. Harmony Jaskolski III', 'hahn.adonis@walsh.com', '91808262304');
	INSERT INTO Cliente (Identif, Tipo_Identif, Direccion, Nombre, Email, Telefono) VALUES ('7200315903', 'E', '5751 Corkery Manor Apt. 030', 'Arne Becker', 'eichmann.hazel@towne.com', '99484765795');
	INSERT INTO Cliente (Identif, Tipo_Identif, Direccion, Nombre, Email, Telefono) VALUES ('731294052', 'G', '4800 Emory Road Apt. 248', 'Krystel Keeling', 'jeanette.schiller@beckerbode.org', '39404878979');
	INSERT INTO Cliente (Identif, Tipo_Identif, Direccion, Nombre, Email, Telefono) VALUES ('7459084697', 'E', '806 Marjolaine Hill', 'Godfrey Howell', 'hilma.bruen@dooleywiza.net', '39440526333');
	INSERT INTO Cliente (Identif, Tipo_Identif, Direccion, Nombre, Email, Telefono) VALUES ('801275844', 'E', '60694 Kautzer Oval', 'Carroll Wyman', 'lehner.victor@russelhermann.com', '11206500446');
	INSERT INTO Cliente (Identif, Tipo_Identif, Direccion, Nombre, Email, Telefono) VALUES ('8556054366', 'G', '13039 Samara Tunnel', 'Dr. Jillian Kassulke', 'angel86@mullerfeeney.com', '69290705653');
	INSERT INTO Cliente (Identif, Tipo_Identif, Direccion, Nombre, Email, Telefono) VALUES ('8588786154', 'J', '663 Steuber Walks Apt. 245', 'Ms. Rosanna Kessler', 'dakota36@toycorkery.com', '94016984440');
	INSERT INTO Cliente (Identif, Tipo_Identif, Direccion, Nombre, Email, Telefono) VALUES ('8714711800', 'E', '6041 Terry Villages', 'Milton Dibbert', 'qdenesik@kuphal.com', '34573753965');
	INSERT INTO Cliente (Identif, Tipo_Identif, Direccion, Nombre, Email, Telefono) VALUES ('8990920321', 'G', '2558 Turcotte Forges', 'Dr. Lexus Littel', 'vmiller@zemlakpacocha.com', '43485134751');
	INSERT INTO Cliente (Identif, Tipo_Identif, Direccion, Nombre, Email, Telefono) VALUES ('903137637', 'J', '6275 Lesch Shores Suite 091', 'Pearlie Gibson MD', 'heath.mraz@gmail.com', '53722867644');
	INSERT INTO Cliente (Identif, Tipo_Identif, Direccion, Nombre, Email, Telefono) VALUES ('9203584013', 'E', '3599 Krajcik Haven Apt. 893', 'Ms. Icie Bechtelar', 'curtis30@schmidt.biz', '18217865219');

UNLOCK TABLES;

--
-- DUMP TABLE Notificacion
--

LOCK TABLES Notificacion WRITE;

	INSERT INTO Notificacion (F_Hora, Identif_Cliente, Via, Texto) VALUES ('2018-03-16 03:20:05', '5219232517', 'email', 'Se le ha enviado las especificaciones necesarias para procesar su solicitud de equipo');
	INSERT INTO Notificacion (F_Hora, Identif_Cliente, Via, Texto) VALUES ('2018-03-16 21:00:54', '6828971550', 'sms', 'Los detalles del equipo se encuentran adjuntos en la página Web de la compañía Black Tech Store C.A.');
	INSERT INTO Notificacion (F_Hora, Identif_Cliente, Via, Texto) VALUES ('2018-03-18 13:47:20', '8556054366', 'sms', 'Envío de equipo computacional exitoso');
	INSERT INTO Notificacion (F_Hora, Identif_Cliente, Via, Texto) VALUES ('2018-03-18 16:18:51', '6484006930', 'email', 'Se le ha enviado las especificaciones necesarias para procesar su solicitud de equipo');
	INSERT INTO Notificacion (F_Hora, Identif_Cliente, Via, Texto) VALUES ('2018-03-19 04:32:25', '4344068043', 'email', 'Envío de equipo computacional exitoso');
	INSERT INTO Notificacion (F_Hora, Identif_Cliente, Via, Texto) VALUES ('2018-03-22 10:27:03', '106191025', 'email', 'Se le ha enviado las especificaciones necesarias para procesar su solicitud de equipo');
	INSERT INTO Notificacion (F_Hora, Identif_Cliente, Via, Texto) VALUES ('2018-03-23 09:02:25', '1549385540', 'sms', 'Confirmada transacción de envío de equipo');
	INSERT INTO Notificacion (F_Hora, Identif_Cliente, Via, Texto) VALUES ('2018-03-23 10:05:47', '3658549168', 'sms', 'Envío de equipo computacional exitoso');
	INSERT INTO Notificacion (F_Hora, Identif_Cliente, Via, Texto) VALUES ('2018-03-24 09:50:14', '8556054366', 'email', 'Se le ha enviado las especificaciones necesarias para procesar su solicitud de equipo');
	INSERT INTO Notificacion (F_Hora, Identif_Cliente, Via, Texto) VALUES ('2018-03-24 12:12:24', '4344068043', 'sms', 'Solicitud recibida');
	INSERT INTO Notificacion (F_Hora, Identif_Cliente, Via, Texto) VALUES ('2018-03-24 21:47:13', '5219232517', 'email', 'Se le ha enviado las especificaciones necesarias para procesar su solicitud de equipo');
	INSERT INTO Notificacion (F_Hora, Identif_Cliente, Via, Texto) VALUES ('2018-03-26 05:02:32', '4924162942', 'sms', 'Solicitud recibida');
	INSERT INTO Notificacion (F_Hora, Identif_Cliente, Via, Texto) VALUES ('2018-03-26 16:11:23', '1549385540', 'email', 'Se le ha enviado las especificaciones necesarias para procesar su solicitud de equipo');
	INSERT INTO Notificacion (F_Hora, Identif_Cliente, Via, Texto) VALUES ('2018-03-27 05:26:18', '801275844', 'email', 'Se le ha enviado las especificaciones necesarias para procesar su solicitud de equipo');
	INSERT INTO Notificacion (F_Hora, Identif_Cliente, Via, Texto) VALUES ('2018-03-27 15:07:57', '5962500047', 'sms', 'Solicitud recibida');
	INSERT INTO Notificacion (F_Hora, Identif_Cliente, Via, Texto) VALUES ('2018-03-28 03:26:28', '6484006930', 'sms', 'Solicitud recibida');
	INSERT INTO Notificacion (F_Hora, Identif_Cliente, Via, Texto) VALUES ('2018-03-28 08:52:17', '106191025', 'email', 'Se le ha enviado las especificaciones necesarias para procesar su solicitud de equipo');
	INSERT INTO Notificacion (F_Hora, Identif_Cliente, Via, Texto) VALUES ('2018-03-28 11:55:21', '489783770', 'sms', 'Envío de equipo computacional exitoso');
	INSERT INTO Notificacion (F_Hora, Identif_Cliente, Via, Texto) VALUES ('2018-03-28 20:05:39', '1447121725', 'email', 'Se le ha enviado las especificaciones necesarias para procesar su solicitud de equipo');
	INSERT INTO Notificacion (F_Hora, Identif_Cliente, Via, Texto) VALUES ('2018-03-29 08:57:20', '3334690165', 'email', 'Los detalles del equipo se encuentran adjuntos en la página Web de la compañía Black Tech Store C.A.');
	INSERT INTO Notificacion (F_Hora, Identif_Cliente, Via, Texto) VALUES ('2018-04-03 03:48:00', '6279233983', 'sms', 'Solicitud recibida');
	INSERT INTO Notificacion (F_Hora, Identif_Cliente, Via, Texto) VALUES ('2018-04-05 02:22:42', '420040739', 'sms', 'Envío de equipo computacional exitoso');
	INSERT INTO Notificacion (F_Hora, Identif_Cliente, Via, Texto) VALUES ('2018-04-05 13:05:50', '1447121725', 'email', 'Se le ha enviado las especificaciones necesarias para procesar su solicitud de equipo');
	INSERT INTO Notificacion (F_Hora, Identif_Cliente, Via, Texto) VALUES ('2018-04-07 15:34:06', '1107166870', 'email', 'Se le ha enviado las especificaciones necesarias para procesar su solicitud de equipo');
	INSERT INTO Notificacion (F_Hora, Identif_Cliente, Via, Texto) VALUES ('2018-04-08 14:41:19', '6512770808', 'sms', 'Los detalles del equipo se encuentran adjuntos en la página Web de la compañía Black Tech Store C.A.');
	INSERT INTO Notificacion (F_Hora, Identif_Cliente, Via, Texto) VALUES ('2018-04-08 15:42:30', '5219232517', 'sms', 'Envío de equipo computacional exitoso');
	INSERT INTO Notificacion (F_Hora, Identif_Cliente, Via, Texto) VALUES ('2018-04-09 11:25:18', '1580326440', 'sms', 'Solicitud recibida');
	INSERT INTO Notificacion (F_Hora, Identif_Cliente, Via, Texto) VALUES ('2018-04-09 11:26:29', '5219232517', 'email', 'Se le ha enviado las especificaciones necesarias para procesar su solicitud de equipo');
	INSERT INTO Notificacion (F_Hora, Identif_Cliente, Via, Texto) VALUES ('2018-04-10 03:55:12', '1580326440', 'sms', 'Confirmada transacción de envío de equipo');
	INSERT INTO Notificacion (F_Hora, Identif_Cliente, Via, Texto) VALUES ('2018-04-10 14:51:12', '8588786154', 'sms', 'Solicitud recibida');
	INSERT INTO Notificacion (F_Hora, Identif_Cliente, Via, Texto) VALUES ('2018-04-10 17:20:24', '1447121725', 'sms', 'Envío de equipo computacional exitoso');
	INSERT INTO Notificacion (F_Hora, Identif_Cliente, Via, Texto) VALUES ('2018-04-11 12:08:14', '6279233983', 'email', 'Los detalles del equipo se encuentran adjuntos en la página Web de la compañía Black Tech Store C.A.');
	INSERT INTO Notificacion (F_Hora, Identif_Cliente, Via, Texto) VALUES ('2018-04-13 03:00:51', '9203584013', 'sms', 'Envío de equipo computacional exitoso');
	INSERT INTO Notificacion (F_Hora, Identif_Cliente, Via, Texto) VALUES ('2018-04-14 03:09:57', '5750068707', 'sms', 'Confirmada transacción de envío de equipo');
	INSERT INTO Notificacion (F_Hora, Identif_Cliente, Via, Texto) VALUES ('2018-04-14 21:30:05', '6484006930', 'email', 'Se le ha enviado las especificaciones necesarias para procesar su solicitud de equipo');

UNLOCK TABLES;

--
-- DUMP TABLE Equipo
--

LOCK TABLES Equipo WRITE;

	INSERT INTO Equipo (Serial_Eq, ID_Categoria, Identif_Cliente, Nombre, Descripcion, Precio) VALUES ('12594272624', 6, '6114924311', 'IPAD', 'Equipo a reparar', '9754047.02');
	INSERT INTO Equipo (Serial_Eq, ID_Categoria, Identif_Cliente, Nombre, Descripcion, Precio) VALUES ('13294877773', 1, '4924162942', 'LG L7', NULL, '7821107.36');
	INSERT INTO Equipo (Serial_Eq, ID_Categoria, Identif_Cliente, Nombre, Descripcion, Precio) VALUES ('14908596640', 7, '1933073566', 'NEXUS 7', 'Equipo a reparar', '8794811.46');
	INSERT INTO Equipo (Serial_Eq, ID_Categoria, Identif_Cliente, Nombre, Descripcion, Precio) VALUES ('19545683613', 5, '2429767043', 'HP', 'Equipo a reparar', '6214061.62');
	INSERT INTO Equipo (Serial_Eq, ID_Categoria, Identif_Cliente, Nombre, Descripcion, Precio) VALUES ('20233641602', 6, '106191025', 'MAC 3', 'Equipo que necesita ser atendido a la brevedad posible', '8666993.88');
	INSERT INTO Equipo (Serial_Eq, ID_Categoria, Identif_Cliente, Nombre, Descripcion, Precio) VALUES ('23168190116', 7, '4344068043', 'HP', 'Equipo que necesita ser atendido a la brevedad posible', '7191866.80');
	INSERT INTO Equipo (Serial_Eq, ID_Categoria, Identif_Cliente, Nombre, Descripcion, Precio) VALUES ('26811031578', 1, '6897390523', 'ACER GATEWAY 1411', 'Equipo que necesita ser atendido a la brevedad posible', '2180728.32');
	INSERT INTO Equipo (Serial_Eq, ID_Categoria, Identif_Cliente, Nombre, Descripcion, Precio) VALUES ('28498599412', 5, '3103281006', 'BLU DASH 5', 'Equipo que necesita ser atendido a la brevedad posible', '4372150.29');
	INSERT INTO Equipo (Serial_Eq, ID_Categoria, Identif_Cliente, Nombre, Descripcion, Precio) VALUES ('29290275196', 5, '903137637', 'LG L6', NULL, '7985401.48');
	INSERT INTO Equipo (Serial_Eq, ID_Categoria, Identif_Cliente, Nombre, Descripcion, Precio) VALUES ('29298720215', 5, '1895222812', 'NEXUS 5', 'Equipo que necesita ser atendido a la brevedad posible', '3314876.31');
	INSERT INTO Equipo (Serial_Eq, ID_Categoria, Identif_Cliente, Nombre, Descripcion, Precio) VALUES ('30155539647', 8, '8556054366', 'BLU DASH 5', 'Equipo que necesita ser atendido a la brevedad posible', '881354.10');
	INSERT INTO Equipo (Serial_Eq, ID_Categoria, Identif_Cliente, Nombre, Descripcion, Precio) VALUES ('36715684193', 3, '2429767043', 'LENOVO', NULL, '886639.29');
	INSERT INTO Equipo (Serial_Eq, ID_Categoria, Identif_Cliente, Nombre, Descripcion, Precio) VALUES ('38480738280', 9, '106191025', 'LG L6', 'Equipo a reparar', '9577207.64');
	INSERT INTO Equipo (Serial_Eq, ID_Categoria, Identif_Cliente, Nombre, Descripcion, Precio) VALUES ('44264630707', 2, '1895222812', 'BLU 6', 'Equipo a reparar', '8776996.81');
	INSERT INTO Equipo (Serial_Eq, ID_Categoria, Identif_Cliente, Nombre, Descripcion, Precio) VALUES ('55912880911', 8, '1107166870', 'AMGOO', 'Equipo a reparar', '4881721.98');
	INSERT INTO Equipo (Serial_Eq, ID_Categoria, Identif_Cliente, Nombre, Descripcion, Precio) VALUES ('59377323873', 10, '5962500047', 'INTEL CELERON INSIDE', NULL, '3079921.28');
	INSERT INTO Equipo (Serial_Eq, ID_Categoria, Identif_Cliente, Nombre, Descripcion, Precio) VALUES ('59682743619', 3, '3658549168', 'BlackBerry', 'Equipo que necesita ser atendido a la brevedad posible', '6542130.73');
	INSERT INTO Equipo (Serial_Eq, ID_Categoria, Identif_Cliente, Nombre, Descripcion, Precio) VALUES ('60009572757', 7, '4924162942', 'HUAWEI H2', 'Equipo que necesita ser atendido a la brevedad posible', '7897393.72');
	INSERT INTO Equipo (Serial_Eq, ID_Categoria, Identif_Cliente, Nombre, Descripcion, Precio) VALUES ('63909253040', 10, '5219232517', 'NEXUS 6', 'Equipo que necesita ser atendido a la brevedad posible', '3565511.81');
	INSERT INTO Equipo (Serial_Eq, ID_Categoria, Identif_Cliente, Nombre, Descripcion, Precio) VALUES ('64754137145', 6, '6828971550', 'MAC 3', NULL, '3619674.52');
	INSERT INTO Equipo (Serial_Eq, ID_Categoria, Identif_Cliente, Nombre, Descripcion, Precio) VALUES ('65524353412', 5, '801275844', 'LG L5', 'Equipo que necesita ser atendido a la brevedad posible', '8249508.23');
	INSERT INTO Equipo (Serial_Eq, ID_Categoria, Identif_Cliente, Nombre, Descripcion, Precio) VALUES ('67284067641', 5, '5962500047', 'SAMSUNG S8', 'Equipo que necesita ser atendido a la brevedad posible', '8790250.19');
	INSERT INTO Equipo (Serial_Eq, ID_Categoria, Identif_Cliente, Nombre, Descripcion, Precio) VALUES ('67374456156', 7, '3334690165', 'NEXUS 5', 'Equipo a reparar', '1759918.33');
	INSERT INTO Equipo (Serial_Eq, ID_Categoria, Identif_Cliente, Nombre, Descripcion, Precio) VALUES ('68013114221', 8, '7200315903', 'IPAD', 'Equipo a reparar', '1253157.91');
	INSERT INTO Equipo (Serial_Eq, ID_Categoria, Identif_Cliente, Nombre, Descripcion, Precio) VALUES ('70951436101', 5, '1894378886', 'GATEWAY', 'Equipo que necesita ser atendido a la brevedad posible', '9169994.75');
	INSERT INTO Equipo (Serial_Eq, ID_Categoria, Identif_Cliente, Nombre, Descripcion, Precio) VALUES ('74603323983', 6, '801275844', 'BLU 6', 'Equipo a reparar', '7582212.62');
	INSERT INTO Equipo (Serial_Eq, ID_Categoria, Identif_Cliente, Nombre, Descripcion, Precio) VALUES ('76442463491', 1, '6160975585', 'SAMSUNG S7', 'Equipo a reparar', '2477751.50');
	INSERT INTO Equipo (Serial_Eq, ID_Categoria, Identif_Cliente, Nombre, Descripcion, Precio) VALUES ('78513924377', 4, '420040739', 'LG L5', 'Equipo que necesita ser atendido a la brevedad posible', '7862288.81');
	INSERT INTO Equipo (Serial_Eq, ID_Categoria, Identif_Cliente, Nombre, Descripcion, Precio) VALUES ('82362963380', 2, '6512770808', 'SAMSUNG S8', 'Equipo a reparar', '1958089.80');
	INSERT INTO Equipo (Serial_Eq, ID_Categoria, Identif_Cliente, Nombre, Descripcion, Precio) VALUES ('88274474851', 10, '5021133496', 'BlackBerry', 'Equipo que necesita ser atendido a la brevedad posible', '9335913.16');
	INSERT INTO Equipo (Serial_Eq, ID_Categoria, Identif_Cliente, Nombre, Descripcion, Precio) VALUES ('92220518081', 8, '5600934749', 'ACER GATEWAY 1411', 'Equipo que necesita ser atendido a la brevedad posible', '8169898.87');
	INSERT INTO Equipo (Serial_Eq, ID_Categoria, Identif_Cliente, Nombre, Descripcion, Precio) VALUES ('92720296224', 2, '731294052', 'LENOVO', 'Equipo a reparar', '9240059.63');
	INSERT INTO Equipo (Serial_Eq, ID_Categoria, Identif_Cliente, Nombre, Descripcion, Precio) VALUES ('94248035834', 1, '1107166870', 'LG L7', 'Equipo a reparar', '825152.62');
	INSERT INTO Equipo (Serial_Eq, ID_Categoria, Identif_Cliente, Nombre, Descripcion, Precio) VALUES ('95617644218', 2, '3334690165', 'GATEWAY', 'Equipo a reparar', '1087494.17');
	INSERT INTO Equipo (Serial_Eq, ID_Categoria, Identif_Cliente, Nombre, Descripcion, Precio) VALUES ('98575835945', 5, '4700790442', 'INTEL CELERON INSIDE', NULL, '3046037.76');
	INSERT INTO Equipo (Serial_Eq, ID_Categoria, Identif_Cliente, Nombre, Descripcion, Precio) VALUES ('99389043413', 6, '5750068707', 'SAMSUNG S7', NULL, '9103903.65');
	INSERT INTO Equipo (Serial_Eq, ID_Categoria, Identif_Cliente, Nombre, Descripcion, Precio) VALUES ('99892292586', 6, '1580326440', 'AMGOO', 'Equipo que necesita ser atendido a la brevedad posible', '8898599.45');
	INSERT INTO Equipo (Serial_Eq, ID_Categoria, Identif_Cliente, Nombre, Descripcion, Precio) VALUES ('67744470159', 6, '5600934749', 'BLU DASH 2', NULL, '3112706.19');
	INSERT INTO Equipo (Serial_Eq, ID_Categoria, Identif_Cliente, Nombre, Descripcion, Precio) VALUES ('72540120738', 2, '801275844', 'LG L2', 'Equipo a reparar', '624543.98');
	INSERT INTO Equipo (Serial_Eq, ID_Categoria, Identif_Cliente, Nombre, Descripcion, Precio) VALUES ('76477632280', 7, '8588786154', 'HUAWEI HU 1', NULL, '3172550.05');
	INSERT INTO Equipo (Serial_Eq, ID_Categoria, Identif_Cliente, Nombre, Descripcion, Precio) VALUES ('77115054475', 8, '6361535682', 'IPHONE 3', NULL, '6716062.99');
	INSERT INTO Equipo (Serial_Eq, ID_Categoria, Identif_Cliente, Nombre, Descripcion, Precio) VALUES ('81087981192', 8, '1107166870', 'IPHONE 4', 'Equipo que necesita ser atendido a la brevedad posible', '9206034.48');
	INSERT INTO Equipo (Serial_Eq, ID_Categoria, Identif_Cliente, Nombre, Descripcion, Precio) VALUES ('85876605515', 3, '1379583121', 'BlackBerry PERL', NULL, '7026457.17');
	INSERT INTO Equipo (Serial_Eq, ID_Categoria, Identif_Cliente, Nombre, Descripcion, Precio) VALUES ('86429658909', 6, '7200315903', 'NEXUS LG', 'Equipo que necesita ser atendido a la brevedad posible', '562875.36');
	INSERT INTO Equipo (Serial_Eq, ID_Categoria, Identif_Cliente, Nombre, Descripcion, Precio) VALUES ('87356153037', 6, '1895222812', 'AMGOO 3V', NULL, '7113355.96');
	INSERT INTO Equipo (Serial_Eq, ID_Categoria, Identif_Cliente, Nombre, Descripcion, Precio) VALUES ('93115759948', 6, '1894378886', 'AMGOO 3S', 'Equipo que necesita ser atendido a la brevedad posible', '1762688.35');
	INSERT INTO Equipo (Serial_Eq, ID_Categoria, Identif_Cliente, Nombre, Descripcion, Precio) VALUES ('94726056023', 5, '5600934749', 'PENTIUM CELERON', NULL, '1158686.77');
	INSERT INTO Equipo (Serial_Eq, ID_Categoria, Identif_Cliente, Nombre, Descripcion, Precio) VALUES ('95214270325', 6, '5034358240', 'AMGOO 3D', NULL, '2929208.51');
	INSERT INTO Equipo (Serial_Eq, ID_Categoria, Identif_Cliente, Nombre, Descripcion, Precio) VALUES ('99420089940', 10, '5219232517', 'ACER 1120', 'Equipo a reparar', '9090323.84');
	INSERT INTO Equipo (Serial_Eq, ID_Categoria, Identif_Cliente, Nombre, Descripcion, Precio) VALUES ('99828031593', 4, '489783770', 'BLU DASH 5D', 'Equipo que necesita ser atendido a la brevedad posible', '1165901.29');

UNLOCK TABLES;

--
-- DUMP TABLE Orden
--

LOCK TABLES Orden WRITE;

	INSERT INTO Orden (Numero, F_inicio, F_fin, Estado, Total) VALUES ('12063', '2018-03-18 14:29:34', '2018-03-23 13:57:07', 'REP', '334967.95');
	INSERT INTO Orden (Numero, F_inicio, F_fin, Estado, Total) VALUES ('15703', '2018-03-20 01:36:31', '2018-03-21 15:16:07', 'DEV', '8815492.48');
	INSERT INTO Orden (Numero, F_inicio, F_fin, Estado, Total) VALUES ('2117', '2018-04-14 06:10:27', '2018-04-24 10:07:56', 'ESP', '4662743.79');
	INSERT INTO Orden (Numero, F_inicio, F_fin, Estado, Total) VALUES ('25755', '2018-03-20 03:52:28', '2018-03-25 19:11:03', 'REV', '9669434.57');
	INSERT INTO Orden (Numero, F_inicio, F_fin, Estado, Total) VALUES ('27540', '2018-04-15 04:36:22', '2018-04-17 04:08:28', 'ESP', '445074.87');
	INSERT INTO Orden (Numero, F_inicio, F_fin, Estado, Total) VALUES ('30263', '2018-04-06 23:13:05', '2018-04-30 08:14:01', 'REP', '5003714.54');
	INSERT INTO Orden (Numero, F_inicio, F_fin, Estado, Total) VALUES ('31559', '2018-03-18 06:45:18', '2018-03-24 02:31:30', 'REP', '1014449.60');
	INSERT INTO Orden (Numero, F_inicio, F_fin, Estado, Total) VALUES ('33459', '2018-03-30 05:46:26', '2018-04-11 02:55:14', 'DEV', '3216914.76');
	INSERT INTO Orden (Numero, F_inicio, F_fin, Estado, Total) VALUES ('35763', '2018-04-04 02:00:38', '2018-04-05 06:28:39', 'REV', '4920518.50');
	INSERT INTO Orden (Numero, F_inicio, F_fin, Estado, Total) VALUES ('37608', '2018-04-03 17:01:29', '2018-04-08 17:10:57', 'REP', '4964836.21');
	INSERT INTO Orden (Numero, F_inicio, F_fin, Estado, Total) VALUES ('3983', '2018-03-27 05:44:58', '2018-03-28 18:05:51', 'DEV', '8176905.01');
	INSERT INTO Orden (Numero, F_inicio, F_fin, Estado, Total) VALUES ('40089', '2018-03-20 21:41:57', '2018-04-04 02:24:31', 'REV', '3362884.99');
	INSERT INTO Orden (Numero, F_inicio, F_fin, Estado, Total) VALUES ('41086', '2018-03-29 14:53:54', '2018-04-08 11:59:13', 'REV', '223099.92');
	INSERT INTO Orden (Numero, F_inicio, F_fin, Estado, Total) VALUES ('41382', '2018-03-16 14:02:20', '2018-04-07 23:56:33', 'REV', '9282349.86');
	INSERT INTO Orden (Numero, F_inicio, F_fin, Estado, Total) VALUES ('41558', '2018-03-22 04:56:23', '2018-04-06 15:10:46', 'REP', '240416.36');
	INSERT INTO Orden (Numero, F_inicio, F_fin, Estado, Total) VALUES ('4553', '2018-04-01 12:40:25', '2018-04-14 12:56:12', 'REV', '2529532.10');
	INSERT INTO Orden (Numero, F_inicio, F_fin, Estado, Total) VALUES ('48684', '2018-04-04 18:37:27', '2018-04-19 21:24:11', 'REV', '7365994.32');
	INSERT INTO Orden (Numero, F_inicio, F_fin, Estado, Total) VALUES ('50991', '2018-03-19 16:50:12', '2018-04-30 06:04:32', 'REV', '897916.35');
	INSERT INTO Orden (Numero, F_inicio, F_fin, Estado, Total) VALUES ('53564', '2018-03-24 04:28:16', '2018-04-18 05:04:06', 'DEV', '5111945.33');
	INSERT INTO Orden (Numero, F_inicio, F_fin, Estado, Total) VALUES ('54775', '2018-04-01 07:05:42', '2018-04-15 19:20:15', 'DEV', '60411.03');
	INSERT INTO Orden (Numero, F_inicio, F_fin, Estado, Total) VALUES ('54992', '2018-04-01 10:33:57', '2018-04-30 00:26:40', 'REP', '2496761.50');
	INSERT INTO Orden (Numero, F_inicio, F_fin, Estado, Total) VALUES ('55269', '2018-03-21 09:10:50', '2018-04-01 10:05:57', 'DEV', '5852729.03');
	INSERT INTO Orden (Numero, F_inicio, F_fin, Estado, Total) VALUES ('55807', '2018-04-09 20:11:42', '2018-04-15 09:12:09', 'DEV', '3769756.81');
	INSERT INTO Orden (Numero, F_inicio, F_fin, Estado, Total) VALUES ('5607', '2018-04-15 12:56:08', '2018-04-15 17:04:54', 'ESP', '3482153.38');
	INSERT INTO Orden (Numero, F_inicio, F_fin, Estado, Total) VALUES ('56739', '2018-04-10 00:19:18', '2018-04-20 11:07:05', 'REV', '5032274.42');
	INSERT INTO Orden (Numero, F_inicio, F_fin, Estado, Total) VALUES ('58672', '2018-04-08 02:41:51', '2018-04-27 07:14:21', 'REV', '5106255.21');
	INSERT INTO Orden (Numero, F_inicio, F_fin, Estado, Total) VALUES ('63474', '2018-04-09 16:22:41', '2018-04-30 17:00:44', 'REP', '5572890.45');
	INSERT INTO Orden (Numero, F_inicio, F_fin, Estado, Total) VALUES ('63485', '2018-03-20 13:06:31', '2018-03-22 14:39:07', 'REP', '2691260.50');
	INSERT INTO Orden (Numero, F_inicio, F_fin, Estado, Total) VALUES ('63714', '2018-04-10 20:47:31', '2018-04-15 10:02:12', 'DEV', '5705702.26');
	INSERT INTO Orden (Numero, F_inicio, F_fin, Estado, Total) VALUES ('64158', '2018-04-05 00:42:03', '2018-04-20 15:54:37', 'REP', '8561455.79');
	INSERT INTO Orden (Numero, F_inicio, F_fin, Estado, Total) VALUES ('64500', '2018-04-12 07:26:32', '2018-04-14 14:28:45', 'ESP', '4630586.47');
	INSERT INTO Orden (Numero, F_inicio, F_fin, Estado, Total) VALUES ('684', '2018-04-14 10:33:09', '2018-04-15 23:14:46', 'ESP', '8817202.75');
	INSERT INTO Orden (Numero, F_inicio, F_fin, Estado, Total) VALUES ('70509', '2018-04-03 13:59:42', '2018-04-14 08:55:25', 'REP', '253784.27');
	INSERT INTO Orden (Numero, F_inicio, F_fin, Estado, Total) VALUES ('71205', '2018-04-04 04:58:00', '2018-04-15 01:49:41', 'ESP', '9273967.11');
	INSERT INTO Orden (Numero, F_inicio, F_fin, Estado, Total) VALUES ('7718', '2018-03-26 15:22:10', '2018-04-15 14:18:44', 'REP', '7383989.84');
	INSERT INTO Orden (Numero, F_inicio, F_fin, Estado, Total) VALUES ('77257', '2018-04-05 20:56:35', '2018-04-06 01:36:26', 'REV', '9386976.47');
	INSERT INTO Orden (Numero, F_inicio, F_fin, Estado, Total) VALUES ('7888', '2018-03-17 09:27:53', '2018-03-29 14:41:39', 'REV', '6153309.01');
	INSERT INTO Orden (Numero, F_inicio, F_fin, Estado, Total) VALUES ('80644', '2018-03-28 17:37:15', '2018-03-28 21:28:41', 'ESP', '5366199.08');
	INSERT INTO Orden (Numero, F_inicio, F_fin, Estado, Total) VALUES ('81136', '2018-03-30 02:41:29', '2018-03-30 19:27:31', 'ESP', '8941108.77');
	INSERT INTO Orden (Numero, F_inicio, F_fin, Estado, Total) VALUES ('81430', '2018-03-27 07:38:51', '2018-04-14 03:50:44', 'REV', '9415120.43');
	INSERT INTO Orden (Numero, F_inicio, F_fin, Estado, Total) VALUES ('83998', '2018-04-06 07:21:50', '2018-04-22 11:07:11', 'DEV', '3411680.66');
	INSERT INTO Orden (Numero, F_inicio, F_fin, Estado, Total) VALUES ('85009', '2018-04-15 05:20:32', '2018-04-17 21:21:44', 'DEV', '3179483.67');
	INSERT INTO Orden (Numero, F_inicio, F_fin, Estado, Total) VALUES ('85427', '2018-04-11 20:50:26', '2018-04-13 00:43:11', 'ESP', '6368783.25');
	INSERT INTO Orden (Numero, F_inicio, F_fin, Estado, Total) VALUES ('85981', '2018-03-31 13:00:56', '2018-04-21 00:20:15', 'ESP', '5576637.37');
	INSERT INTO Orden (Numero, F_inicio, F_fin, Estado, Total) VALUES ('87838', '2018-04-02 06:41:03', '2018-04-04 13:06:20', 'DEV', '639998.66');
	INSERT INTO Orden (Numero, F_inicio, F_fin, Estado, Total) VALUES ('89768', '2018-04-01 18:36:08', '2018-04-21 12:46:14', 'REP', '9222185.51');
	INSERT INTO Orden (Numero, F_inicio, F_fin, Estado, Total) VALUES ('9105', '2018-04-02 08:47:14', '2018-04-27 09:57:13', 'REP', '1522612.61');
	INSERT INTO Orden (Numero, F_inicio, F_fin, Estado, Total) VALUES ('9144', '2018-03-17 03:32:55', '2018-03-28 05:36:48', 'REP', '1967341.37');
	INSERT INTO Orden (Numero, F_inicio, F_fin, Estado, Total) VALUES ('93269', '2018-04-14 02:08:50', '2018-04-15 04:19:22', 'ESP', '5028163.71');
	INSERT INTO Orden (Numero, F_inicio, F_fin, Estado, Total) VALUES ('96982', '2018-03-17 08:12:10', '2018-04-01 17:23:45', 'ESP', '5613448.98');

UNLOCK TABLES;

--
-- DUMP TABLE FacturaVenta
--

LOCK TABLES FacturaVenta WRITE;

	INSERT INTO FacturaVenta (Numero, Identif_Cliente, Fecha, Estado, Descripcion, Monto) VALUES ('13239', '1107166870', '2018-04-15 09:07:50', 'Pagado', 'Pago efectuado', '3488298.06');
	INSERT INTO FacturaVenta (Numero, Identif_Cliente, Fecha, Estado, Descripcion, Monto) VALUES ('13525', '4700790442', '2018-04-15 17:48:23', 'Pagado', 'Pago efectuado', '6249325.25');
	INSERT INTO FacturaVenta (Numero, Identif_Cliente, Fecha, Estado, Descripcion, Monto) VALUES ('1667', '6828971550', '2018-04-14 06:45:41', 'Procesado', 'Pago efectuado', '8558269.28');
	INSERT INTO FacturaVenta (Numero, Identif_Cliente, Fecha, Estado, Descripcion, Monto) VALUES ('18079', '5034358240', '2018-04-02 11:36:09', 'Procesado', 'Pago efectuado', '6400693.54');
	INSERT INTO FacturaVenta (Numero, Identif_Cliente, Fecha, Estado, Descripcion, Monto) VALUES ('19310', '8990920321', '2018-04-02 14:08:04', 'Procesado', 'Pago efectuado', '714544.02');
	INSERT INTO FacturaVenta (Numero, Identif_Cliente, Fecha, Estado, Descripcion, Monto) VALUES ('23174', '1580326440', '2018-04-07 02:41:34', 'Pagado', 'Pago efectuado', '6532988.50');
	INSERT INTO FacturaVenta (Numero, Identif_Cliente, Fecha, Estado, Descripcion, Monto) VALUES ('23604', '731294052', '2018-04-08 04:58:58', 'Facturado', 'Pago realizado', '6947995.63');
	INSERT INTO FacturaVenta (Numero, Identif_Cliente, Fecha, Estado, Descripcion, Monto) VALUES ('25991', '6361535682', '2018-04-06 17:51:37', 'Facturado', 'Pago efectuado', '4572420.03');
	INSERT INTO FacturaVenta (Numero, Identif_Cliente, Fecha, Estado, Descripcion, Monto) VALUES ('2797', '6160975585', '2018-04-14 12:15:06', 'Pagado', 'Pago realizado', '3069014.56');
	INSERT INTO FacturaVenta (Numero, Identif_Cliente, Fecha, Estado, Descripcion, Monto) VALUES ('34262', '4344068043', '2018-04-04 11:38:15', 'Pagado', NULL, '8850894.93');
	INSERT INTO FacturaVenta (Numero, Identif_Cliente, Fecha, Estado, Descripcion, Monto) VALUES ('35514', '3658549168', '2018-04-03 18:34:21', 'Pagado', NULL, '8234536.75');
	INSERT INTO FacturaVenta (Numero, Identif_Cliente, Fecha, Estado, Descripcion, Monto) VALUES ('36004', '731294052', '2018-04-07 05:36:46', 'Procesado', 'Pago realizado', '205415.37');
	INSERT INTO FacturaVenta (Numero, Identif_Cliente, Fecha, Estado, Descripcion, Monto) VALUES ('36346', '903137637', '2018-04-04 11:35:12', 'Facturado', NULL, '9574735.31');
	INSERT INTO FacturaVenta (Numero, Identif_Cliente, Fecha, Estado, Descripcion, Monto) VALUES ('3656', '6114924311', '2018-04-06 19:40:58', 'Facturado', 'Pago efectuado', '2607757.66');
	INSERT INTO FacturaVenta (Numero, Identif_Cliente, Fecha, Estado, Descripcion, Monto) VALUES ('39089', '4344068043', '2018-04-03 07:39:31', 'Pagado', NULL, '6842497.55');
	INSERT INTO FacturaVenta (Numero, Identif_Cliente, Fecha, Estado, Descripcion, Monto) VALUES ('41572', '1933073566', '2018-04-04 00:23:43', 'Procesado', NULL, '5015405.52');
	INSERT INTO FacturaVenta (Numero, Identif_Cliente, Fecha, Estado, Descripcion, Monto) VALUES ('43697', '4648459618', '2018-04-10 06:32:19', 'Facturado', 'Pago realizado', '2521328.61');
	INSERT INTO FacturaVenta (Numero, Identif_Cliente, Fecha, Estado, Descripcion, Monto) VALUES ('4386', '420040739', '2018-04-02 10:17:28', 'Procesado', 'Pago efectuado', '5958881.50');
	INSERT INTO FacturaVenta (Numero, Identif_Cliente, Fecha, Estado, Descripcion, Monto) VALUES ('44779', '5623462726', '2018-04-11 01:24:20', 'Procesado', NULL, '3294793.53');
	INSERT INTO FacturaVenta (Numero, Identif_Cliente, Fecha, Estado, Descripcion, Monto) VALUES ('47167', '1580326440', '2018-04-03 06:51:19', 'Pagado', 'Pago efectuado', '7476447.94');
	INSERT INTO FacturaVenta (Numero, Identif_Cliente, Fecha, Estado, Descripcion, Monto) VALUES ('49040', '5021133496', '2018-04-04 16:03:56', 'Facturado', NULL, '9423337.06');
	INSERT INTO FacturaVenta (Numero, Identif_Cliente, Fecha, Estado, Descripcion, Monto) VALUES ('52647', '1895222812', '2018-04-11 15:28:41', 'Pagado', 'Pago efectuado', '1034727.70');
	INSERT INTO FacturaVenta (Numero, Identif_Cliente, Fecha, Estado, Descripcion, Monto) VALUES ('55801', '3334690165', '2018-04-07 04:33:44', 'Pagado', 'Pago realizado', '9938546.95');
	INSERT INTO FacturaVenta (Numero, Identif_Cliente, Fecha, Estado, Descripcion, Monto) VALUES ('56472', '6828971550', '2018-04-06 14:03:52', 'Pagado', 'Pago realizado', '4041880.59');
	INSERT INTO FacturaVenta (Numero, Identif_Cliente, Fecha, Estado, Descripcion, Monto) VALUES ('60726', '6160975585', '2018-04-08 11:09:40', 'Procesado', 'Pago efectuado', '4365638.02');
	INSERT INTO FacturaVenta (Numero, Identif_Cliente, Fecha, Estado, Descripcion, Monto) VALUES ('61657', '4042473736', '2018-04-05 18:02:49', 'Facturado', NULL, '1900438.42');
	INSERT INTO FacturaVenta (Numero, Identif_Cliente, Fecha, Estado, Descripcion, Monto) VALUES ('62343', '1447121725', '2018-04-11 07:15:41', 'Pagado', 'Pago realizado', '5651741.72');
	INSERT INTO FacturaVenta (Numero, Identif_Cliente, Fecha, Estado, Descripcion, Monto) VALUES ('63893', '6279233983', '2018-04-10 17:01:11', 'Facturado', 'Pago efectuado', '7679594.93');
	INSERT INTO FacturaVenta (Numero, Identif_Cliente, Fecha, Estado, Descripcion, Monto) VALUES ('64006', '4042473736', '2018-04-07 17:13:24', 'Procesado', 'Pago realizado', '144424.97');
	INSERT INTO FacturaVenta (Numero, Identif_Cliente, Fecha, Estado, Descripcion, Monto) VALUES ('6449', '2429767043', '2018-04-08 13:04:45', 'Pagado', 'Pago realizado', '9590775.19');
	INSERT INTO FacturaVenta (Numero, Identif_Cliente, Fecha, Estado, Descripcion, Monto) VALUES ('64515', '6512770808', '2018-04-13 06:09:10', 'Procesado', 'Pago realizado', '5567548.84');
	INSERT INTO FacturaVenta (Numero, Identif_Cliente, Fecha, Estado, Descripcion, Monto) VALUES ('64788', '7200315903', '2018-04-11 09:50:40', 'Pagado', 'Pago realizado', '9777862.68');
	INSERT INTO FacturaVenta (Numero, Identif_Cliente, Fecha, Estado, Descripcion, Monto) VALUES ('65998', '6484006930', '2018-04-03 13:19:41', 'Procesado', 'Pago realizado', '7886507.72');
	INSERT INTO FacturaVenta (Numero, Identif_Cliente, Fecha, Estado, Descripcion, Monto) VALUES ('68784', '8588786154', '2018-04-09 02:35:31', 'Procesado', NULL, '5164474.91');
	INSERT INTO FacturaVenta (Numero, Identif_Cliente, Fecha, Estado, Descripcion, Monto) VALUES ('70808', '5600934749', '2018-04-03 00:01:47', 'Pagado', NULL, '7292727.60');
	INSERT INTO FacturaVenta (Numero, Identif_Cliente, Fecha, Estado, Descripcion, Monto) VALUES ('71133', '6897390523', '2018-04-11 01:59:02', 'Facturado', NULL, '7817063.41');
	INSERT INTO FacturaVenta (Numero, Identif_Cliente, Fecha, Estado, Descripcion, Monto) VALUES ('74290', '5623462726', '2018-04-03 10:30:16', 'Procesado', 'Pago realizado', '558844.45');
	INSERT INTO FacturaVenta (Numero, Identif_Cliente, Fecha, Estado, Descripcion, Monto) VALUES ('75458', '801275844', '2018-04-11 23:12:06', 'Facturado', NULL, '7398217.37');
	INSERT INTO FacturaVenta (Numero, Identif_Cliente, Fecha, Estado, Descripcion, Monto) VALUES ('76289', '5600934749', '2018-04-10 05:30:24', 'Procesado', 'Pago efectuado', '821143.93');
	INSERT INTO FacturaVenta (Numero, Identif_Cliente, Fecha, Estado, Descripcion, Monto) VALUES ('7649', '6512770808', '2018-04-12 12:05:28', 'Facturado', 'Pago realizado', '5131324.16');
	INSERT INTO FacturaVenta (Numero, Identif_Cliente, Fecha, Estado, Descripcion, Monto) VALUES ('76705', '1580326440', '2018-04-09 01:16:29', 'Procesado', NULL, '6976243.71');
	INSERT INTO FacturaVenta (Numero, Identif_Cliente, Fecha, Estado, Descripcion, Monto) VALUES ('8273', '7200315903', '2018-04-07 14:01:41', 'Procesado', 'Pago efectuado', '4329177.27');
	INSERT INTO FacturaVenta (Numero, Identif_Cliente, Fecha, Estado, Descripcion, Monto) VALUES ('8382', '6484006930', '2018-04-04 23:10:47', 'Facturado', 'Pago realizado', '2658017.63');
	INSERT INTO FacturaVenta (Numero, Identif_Cliente, Fecha, Estado, Descripcion, Monto) VALUES ('8903', '8588786154', '2018-04-14 03:45:23', 'Procesado', 'Pago realizado', '7777157.74');
	INSERT INTO FacturaVenta (Numero, Identif_Cliente, Fecha, Estado, Descripcion, Monto) VALUES ('89840', '5219232517', '2018-04-07 16:24:21', 'Pagado', NULL, '9865947.37');
	INSERT INTO FacturaVenta (Numero, Identif_Cliente, Fecha, Estado, Descripcion, Monto) VALUES ('91174', '5623462726', '2018-04-07 18:20:18', 'Pagado', 'Pago efectuado', '9903488.49');
	INSERT INTO FacturaVenta (Numero, Identif_Cliente, Fecha, Estado, Descripcion, Monto) VALUES ('91472', '6279233983', '2018-04-12 13:36:03', 'Procesado', 'Pago efectuado', '4637831.21');
	INSERT INTO FacturaVenta (Numero, Identif_Cliente, Fecha, Estado, Descripcion, Monto) VALUES ('95910', '9203584013', '2018-04-07 14:16:42', 'Pagado', 'Pago efectuado', '7478051.67');
	INSERT INTO FacturaVenta (Numero, Identif_Cliente, Fecha, Estado, Descripcion, Monto) VALUES ('96013', '6361535682', '2018-04-02 18:22:37', 'Facturado', 'Pago realizado', '8635113.02');
	INSERT INTO FacturaVenta (Numero, Identif_Cliente, Fecha, Estado, Descripcion, Monto) VALUES ('98687', '4700790442', '2018-04-13 09:14:18', 'Pagado', 'Pago realizado', '3195771.38');

UNLOCK TABLES;

--
-- DUMP TABLE VentaWeb
--

LOCK TABLES VentaWeb WRITE;

	INSERT INTO VentaWeb (Numero, Identif_Cliente, Num_Factura, Concepto, Estado) VALUES ('13853', '6828971550', '25991', 'Compra de equipo computacional', 'PAPB');
	INSERT INTO VentaWeb (Numero, Identif_Cliente, Num_Factura, Concepto, Estado) VALUES ('14445', '1379583121', '3656', 'Pago efectuado', 'PAPB');
	INSERT INTO VentaWeb (Numero, Identif_Cliente, Num_Factura, Concepto, Estado) VALUES ('20953', '7459084697', '25991', 'Pago efectuado', 'PAPB');
	INSERT INTO VentaWeb (Numero, Identif_Cliente, Num_Factura, Concepto, Estado) VALUES ('21835', '6484006930', '52647', 'Pago efectuado', 'PAPB');
	INSERT INTO VentaWeb (Numero, Identif_Cliente, Num_Factura, Concepto, Estado) VALUES ('22295', '903137637', '71133', 'Compra de equipo computacional', 'RECHAZ');
	INSERT INTO VentaWeb (Numero, Identif_Cliente, Num_Factura, Concepto, Estado) VALUES ('23351', '8588786154', '6449', 'Pago efectuado', 'RECHAZ');
	INSERT INTO VentaWeb (Numero, Identif_Cliente, Num_Factura, Concepto, Estado) VALUES ('24966', '903137637', '60726', 'Pago de equipo', 'PAPB');
	INSERT INTO VentaWeb (Numero, Identif_Cliente, Num_Factura, Concepto, Estado) VALUES ('26016', '3103281006', '76705', 'Compra de equipo computacional', 'RECHAZ');
	INSERT INTO VentaWeb (Numero, Identif_Cliente, Num_Factura, Concepto, Estado) VALUES ('28813', '1248296668', '23604', 'Pago de equipo', 'PAPB');
	INSERT INTO VentaWeb (Numero, Identif_Cliente, Num_Factura, Concepto, Estado) VALUES ('31476', '4344068043', '18079', 'Pago efectuado', 'RECHAZ');
	INSERT INTO VentaWeb (Numero, Identif_Cliente, Num_Factura, Concepto, Estado) VALUES ('32288', '7188356091', '95910', 'Pago efectuado', 'APROB');
	INSERT INTO VentaWeb (Numero, Identif_Cliente, Num_Factura, Concepto, Estado) VALUES ('34066', '5600934749', '75458', 'Compra de equipo computacional', 'APROB');
	INSERT INTO VentaWeb (Numero, Identif_Cliente, Num_Factura, Concepto, Estado) VALUES ('4496', '1379583121', '3656', 'Pago efectuado', 'PAPB');
	INSERT INTO VentaWeb (Numero, Identif_Cliente, Num_Factura, Concepto, Estado) VALUES ('45650', '7200315903', '3656', 'Pago efectuado', 'RECHAZ');
	INSERT INTO VentaWeb (Numero, Identif_Cliente, Num_Factura, Concepto, Estado) VALUES ('47959', '8714711800', '91174', 'Compra de equipo computacional', 'RECHAZ');
	INSERT INTO VentaWeb (Numero, Identif_Cliente, Num_Factura, Concepto, Estado) VALUES ('48433', '489783770', '95910', 'Pago de equipo', 'RECHAZ');
	INSERT INTO VentaWeb (Numero, Identif_Cliente, Num_Factura, Concepto, Estado) VALUES ('48860', '5034358240', '61657', 'Pago efectuado', 'PAPB');
	INSERT INTO VentaWeb (Numero, Identif_Cliente, Num_Factura, Concepto, Estado) VALUES ('48946', '8714711800', '23174', 'Pago de equipo', 'RECHAZ');
	INSERT INTO VentaWeb (Numero, Identif_Cliente, Num_Factura, Concepto, Estado) VALUES ('49191', '3103281006', '44779', 'Compra de equipo computacional', 'RECHAZ');
	INSERT INTO VentaWeb (Numero, Identif_Cliente, Num_Factura, Concepto, Estado) VALUES ('49911', '2250770075', '55801', 'Pago de equipo', 'RECHAZ');
	INSERT INTO VentaWeb (Numero, Identif_Cliente, Num_Factura, Concepto, Estado) VALUES ('51080', '4042473736', '95910', 'Compra de equipo computacional', 'RECHAZ');
	INSERT INTO VentaWeb (Numero, Identif_Cliente, Num_Factura, Concepto, Estado) VALUES ('5123', '5750068707', '13525', 'Pago de equipo', 'RECHAZ');
	INSERT INTO VentaWeb (Numero, Identif_Cliente, Num_Factura, Concepto, Estado) VALUES ('52479', '5072576596', '23174', 'Compra de equipo computacional', 'APROB');
	INSERT INTO VentaWeb (Numero, Identif_Cliente, Num_Factura, Concepto, Estado) VALUES ('52980', '106191025', '64788', 'Pago de equipo', 'RECHAZ');
	INSERT INTO VentaWeb (Numero, Identif_Cliente, Num_Factura, Concepto, Estado) VALUES ('55067', '6160975585', '96013', 'Pago efectuado', 'PAPB');
	INSERT INTO VentaWeb (Numero, Identif_Cliente, Num_Factura, Concepto, Estado) VALUES ('5915', '903137637', '64788', 'Pago de equipo', 'PAPB');
	INSERT INTO VentaWeb (Numero, Identif_Cliente, Num_Factura, Concepto, Estado) VALUES ('59505', '1447121725', '75458', 'Pago efectuado', 'PAPB');
	INSERT INTO VentaWeb (Numero, Identif_Cliente, Num_Factura, Concepto, Estado) VALUES ('60803', '6279233983', '3656', 'Pago de equipo', 'RECHAZ');
	INSERT INTO VentaWeb (Numero, Identif_Cliente, Num_Factura, Concepto, Estado) VALUES ('61038', '1248296668', '34262', 'Pago efectuado', 'PAPB');
	INSERT INTO VentaWeb (Numero, Identif_Cliente, Num_Factura, Concepto, Estado) VALUES ('61481', '2429767043', '64006', 'Pago de equipo', 'PAPB');
	INSERT INTO VentaWeb (Numero, Identif_Cliente, Num_Factura, Concepto, Estado) VALUES ('62090', '731294052', '70808', 'Pago efectuado', 'PAPB');
	INSERT INTO VentaWeb (Numero, Identif_Cliente, Num_Factura, Concepto, Estado) VALUES ('65522', '8556054366', '36004', 'Pago de equipo', 'PAPB');
	INSERT INTO VentaWeb (Numero, Identif_Cliente, Num_Factura, Concepto, Estado) VALUES ('66167', '6897390523', '36004', 'Pago de equipo', 'RECHAZ');
	INSERT INTO VentaWeb (Numero, Identif_Cliente, Num_Factura, Concepto, Estado) VALUES ('66858', '4924162942', '43697', 'Pago efectuado', 'PAPB');
	INSERT INTO VentaWeb (Numero, Identif_Cliente, Num_Factura, Concepto, Estado) VALUES ('68351', '6484006930', '52647', 'Pago efectuado', 'PAPB');
	INSERT INTO VentaWeb (Numero, Identif_Cliente, Num_Factura, Concepto, Estado) VALUES ('68602', '1379583121', '23174', 'Pago de equipo', 'RECHAZ');
	INSERT INTO VentaWeb (Numero, Identif_Cliente, Num_Factura, Concepto, Estado) VALUES ('7269', '4344068043', '19310', 'Pago de equipo', 'APROB');
	INSERT INTO VentaWeb (Numero, Identif_Cliente, Num_Factura, Concepto, Estado) VALUES ('74230', '489783770', '96013', 'Compra de equipo computacional', 'PAPB');
	INSERT INTO VentaWeb (Numero, Identif_Cliente, Num_Factura, Concepto, Estado) VALUES ('74784', '1140901638', '35514', 'Pago efectuado', 'PAPB');
	INSERT INTO VentaWeb (Numero, Identif_Cliente, Num_Factura, Concepto, Estado) VALUES ('75479', '731294052', '63893', 'Pago efectuado', 'RECHAZ');
	INSERT INTO VentaWeb (Numero, Identif_Cliente, Num_Factura, Concepto, Estado) VALUES ('77019', '801275844', '19310', 'Pago efectuado', 'APROB');
	INSERT INTO VentaWeb (Numero, Identif_Cliente, Num_Factura, Concepto, Estado) VALUES ('77786', '8588786154', '2797', 'Pago de equipo', 'PAPB');
	INSERT INTO VentaWeb (Numero, Identif_Cliente, Num_Factura, Concepto, Estado) VALUES ('78215', '6512770808', '3656', 'Compra de equipo computacional', 'PAPB');
	INSERT INTO VentaWeb (Numero, Identif_Cliente, Num_Factura, Concepto, Estado) VALUES ('78930', '1140901638', '1667', 'Pago efectuado', 'PAPB');
	INSERT INTO VentaWeb (Numero, Identif_Cliente, Num_Factura, Concepto, Estado) VALUES ('80056', '4924162942', '76705', 'Pago de equipo', 'PAPB');
	INSERT INTO VentaWeb (Numero, Identif_Cliente, Num_Factura, Concepto, Estado) VALUES ('80866', '1549385540', '43697', 'Pago de equipo', 'RECHAZ');
	INSERT INTO VentaWeb (Numero, Identif_Cliente, Num_Factura, Concepto, Estado) VALUES ('8187', '1447121725', '36004', 'Pago de equipo', 'APROB');
	INSERT INTO VentaWeb (Numero, Identif_Cliente, Num_Factura, Concepto, Estado) VALUES ('81884', '1248296668', '36004', 'Pago de equipo', 'PAPB');
	INSERT INTO VentaWeb (Numero, Identif_Cliente, Num_Factura, Concepto, Estado) VALUES ('82882', '5034358240', '60726', 'Pago efectuado', 'PAPB');
	INSERT INTO VentaWeb (Numero, Identif_Cliente, Num_Factura, Concepto, Estado) VALUES ('83479', '7188356091', '3656', 'Compra de equipo computacional', 'PAPB');
	INSERT INTO VentaWeb (Numero, Identif_Cliente, Num_Factura, Concepto, Estado) VALUES ('84299', '3658549168', '3656', 'Compra de equipo computacional', 'PAPB');
	INSERT INTO VentaWeb (Numero, Identif_Cliente, Num_Factura, Concepto, Estado) VALUES ('84343', '5623462726', '47167', 'Pago de equipo', 'RECHAZ');
	INSERT INTO VentaWeb (Numero, Identif_Cliente, Num_Factura, Concepto, Estado) VALUES ('84595', '4700790442', '13239', 'Pago efectuado', 'PAPB');
	INSERT INTO VentaWeb (Numero, Identif_Cliente, Num_Factura, Concepto, Estado) VALUES ('84913', '6160975585', '89840', 'Pago de equipo', 'APROB');
	INSERT INTO VentaWeb (Numero, Identif_Cliente, Num_Factura, Concepto, Estado) VALUES ('86136', '3334690165', '95910', 'Pago efectuado', 'APROB');
	INSERT INTO VentaWeb (Numero, Identif_Cliente, Num_Factura, Concepto, Estado) VALUES ('87052', '6484006930', '7649', 'Compra de equipo computacional', 'RECHAZ');
	INSERT INTO VentaWeb (Numero, Identif_Cliente, Num_Factura, Concepto, Estado) VALUES ('89906', '5962500047', '8903', 'Pago de equipo', 'RECHAZ');
	INSERT INTO VentaWeb (Numero, Identif_Cliente, Num_Factura, Concepto, Estado) VALUES ('91588', '1140901638', '76289', 'Pago de equipo', 'RECHAZ');
	INSERT INTO VentaWeb (Numero, Identif_Cliente, Num_Factura, Concepto, Estado) VALUES ('9287', '4924162942', '8382', 'Pago efectuado', 'PAPB');
	INSERT INTO VentaWeb (Numero, Identif_Cliente, Num_Factura, Concepto, Estado) VALUES ('93227', '1549385540', '64006', 'Pago efectuado', 'PAPB');
	INSERT INTO VentaWeb (Numero, Identif_Cliente, Num_Factura, Concepto, Estado) VALUES ('96274', '8714711800', '65998', 'Compra de equipo computacional', 'PAPB');
	INSERT INTO VentaWeb (Numero, Identif_Cliente, Num_Factura, Concepto, Estado) VALUES ('96647', '5219232517', '47167', 'Pago de equipo', 'APROB');
	INSERT INTO VentaWeb (Numero, Identif_Cliente, Num_Factura, Concepto, Estado) VALUES ('96855', '1140901638', '75458', 'Pago efectuado', 'PAPB');
	INSERT INTO VentaWeb (Numero, Identif_Cliente, Num_Factura, Concepto, Estado) VALUES ('98138', '4648459618', '34262', 'Pago efectuado', 'PAPB');
	INSERT INTO VentaWeb (Numero, Identif_Cliente, Num_Factura, Concepto, Estado) VALUES ('99229', '1895222812', '49040', 'Pago de equipo', 'PAPB');

UNLOCK TABLES;

--
-- DUMP TABLE Reparacion
--

LOCK TABLES Reparacion WRITE;

	INSERT INTO Reparacion (F_asig, CI_Personal, Numero_Orden, F_rep, Descripcion, Comision, Total, Estado) VALUES ('2018-04-02 11:12:41', 56973278, '83998', '2018-04-06 17:14:18', NULL, '26410.43', '4461590.27', 'REP');
	INSERT INTO Reparacion (F_asig, CI_Personal, Numero_Orden, F_rep, Descripcion, Comision, Total, Estado) VALUES ('2018-04-02 15:29:39', 53290784, '15703', '2018-04-14 18:39:13', 'Equipo computacional con fallas internas', '77571.61', '46887.00', 'REV');
	INSERT INTO Reparacion (F_asig, CI_Personal, Numero_Orden, F_rep, Descripcion, Comision, Total, Estado) VALUES ('2018-04-03 23:22:38', 27423439, '89768', '2018-04-05 16:33:01', NULL, '11522.77', '1292747.47', 'REP');
	INSERT INTO Reparacion (F_asig, CI_Personal, Numero_Orden, F_rep, Descripcion, Comision, Total, Estado) VALUES ('2018-04-04 21:11:52', 56973278, '93269', '2018-04-14 21:14:18', 'Equipo computacional con fallas internas', '15961.33', '7967390.47', 'DEV');
	INSERT INTO Reparacion (F_asig, CI_Personal, Numero_Orden, F_rep, Descripcion, Comision, Total, Estado) VALUES ('2018-04-05 10:47:49', 53290784, '58672', '2018-04-06 05:15:39', 'Equipo computacional con fallas internas', '83641.91', '3531939.21', 'REV');
	INSERT INTO Reparacion (F_asig, CI_Personal, Numero_Orden, F_rep, Descripcion, Comision, Total, Estado) VALUES ('2018-04-05 11:16:33', 56973278, '9144', '2018-04-06 11:47:30', 'Falla de equipo', '681.48', '311588.46', 'REP');
	INSERT INTO Reparacion (F_asig, CI_Personal, Numero_Orden, F_rep, Descripcion, Comision, Total, Estado) VALUES ('2018-04-05 22:53:26', 56973278, '55269', '2018-04-07 05:25:35', 'Error en equipo', '83318.57', '8115554.76', 'ESP');
	INSERT INTO Reparacion (F_asig, CI_Personal, Numero_Orden, F_rep, Descripcion, Comision, Total, Estado) VALUES ('2018-04-07 01:42:52', 53290784, '55269', '2018-04-14 05:48:26', NULL, '58980.96', '7058354.38', 'REP');
	INSERT INTO Reparacion (F_asig, CI_Personal, Numero_Orden, F_rep, Descripcion, Comision, Total, Estado) VALUES ('2018-04-07 01:45:07', 53290784, '40089', '2018-04-08 15:07:10', 'Equipo computacional con fallas internas', '84174.58', '5671034.79', 'DEV');
	INSERT INTO Reparacion (F_asig, CI_Personal, Numero_Orden, F_rep, Descripcion, Comision, Total, Estado) VALUES ('2018-04-07 12:31:55', 56973278, '2117', '2018-04-09 01:38:24', 'Error en equipo', '98791.47', '1010923.36', 'REV');
	INSERT INTO Reparacion (F_asig, CI_Personal, Numero_Orden, F_rep, Descripcion, Comision, Total, Estado) VALUES ('2018-04-07 20:16:49', 27423439, '37608', '2018-04-09 18:59:55', NULL, '91945.65', '168614.29', 'ESP');
	INSERT INTO Reparacion (F_asig, CI_Personal, Numero_Orden, F_rep, Descripcion, Comision, Total, Estado) VALUES ('2018-04-07 22:34:54', 53290784, '80644', '2018-04-12 16:01:19', 'Error en equipo', '39416.84', '9828389.30', 'REP');
	INSERT INTO Reparacion (F_asig, CI_Personal, Numero_Orden, F_rep, Descripcion, Comision, Total, Estado) VALUES ('2018-04-08 00:16:16', 36145388, '3983', '2018-04-10 08:52:02', NULL, '70256.34', '7141858.59', 'REP');
	INSERT INTO Reparacion (F_asig, CI_Personal, Numero_Orden, F_rep, Descripcion, Comision, Total, Estado) VALUES ('2018-04-08 05:51:45', 36145388, '55269', '2018-04-15 21:28:07', 'Error en equipo', '53906.44', '1896985.37', 'REV');
	INSERT INTO Reparacion (F_asig, CI_Personal, Numero_Orden, F_rep, Descripcion, Comision, Total, Estado) VALUES ('2018-04-08 08:16:12', 36145388, '85009', '2018-04-08 23:59:38', 'Error en equipo', '72482.10', '7992039.87', 'REV');
	INSERT INTO Reparacion (F_asig, CI_Personal, Numero_Orden, F_rep, Descripcion, Comision, Total, Estado) VALUES ('2018-04-08 16:13:27', 56973278, '63714', '2018-04-09 11:02:06', NULL, '89956.69', '6635605.42', 'REV');
	INSERT INTO Reparacion (F_asig, CI_Personal, Numero_Orden, F_rep, Descripcion, Comision, Total, Estado) VALUES ('2018-04-10 11:16:23', 27423439, '87838', '2018-04-12 16:23:39', 'Falla de equipo', '82266.98', '640856.00', 'DEV');
	INSERT INTO Reparacion (F_asig, CI_Personal, Numero_Orden, F_rep, Descripcion, Comision, Total, Estado) VALUES ('2018-04-11 06:59:05', 56973278, '31559', '2018-04-12 20:22:56', 'Falla de equipo', '96827.10', '963265.96', 'REV');
	INSERT INTO Reparacion (F_asig, CI_Personal, Numero_Orden, F_rep, Descripcion, Comision, Total, Estado) VALUES ('2018-04-11 08:30:56', 56973278, '9105', '2018-04-13 10:08:48', 'Falla de equipo', '81783.39', '2349114.71', 'REV');
	INSERT INTO Reparacion (F_asig, CI_Personal, Numero_Orden, F_rep, Descripcion, Comision, Total, Estado) VALUES ('2018-04-11 10:42:42', 36145388, '85981', '2018-04-15 17:13:51', 'Error en equipo', '83582.39', '7729845.25', 'ESP');
	INSERT INTO Reparacion (F_asig, CI_Personal, Numero_Orden, F_rep, Descripcion, Comision, Total, Estado) VALUES ('2018-04-11 15:23:24', 36145388, '55807', '2018-04-16 02:12:39', 'Falla de equipo', '13821.28', '4994017.75', 'REP');
	INSERT INTO Reparacion (F_asig, CI_Personal, Numero_Orden, F_rep, Descripcion, Comision, Total, Estado) VALUES ('2018-04-12 01:16:11', 53290784, '87838', '2018-04-17 12:04:58', 'Falla de equipo', '58130.88', '371279.13', 'ESP');
	INSERT INTO Reparacion (F_asig, CI_Personal, Numero_Orden, F_rep, Descripcion, Comision, Total, Estado) VALUES ('2018-04-12 06:59:13', 27423439, '81430', '2018-04-13 08:56:40', 'Error en equipo', '66888.43', '3269495.28', 'ESP');
	INSERT INTO Reparacion (F_asig, CI_Personal, Numero_Orden, F_rep, Descripcion, Comision, Total, Estado) VALUES ('2018-04-14 03:05:45', 27423439, '53564', '2018-04-15 08:32:37', 'Error en equipo', '98541.52', '2089359.93', 'DEV');
	INSERT INTO Reparacion (F_asig, CI_Personal, Numero_Orden, F_rep, Descripcion, Comision, Total, Estado) VALUES ('2018-04-14 14:26:49', 27423439, '35763', '2018-04-15 15:45:22', 'Error en equipo', '20186.40', '6420819.17', 'ESP');
	INSERT INTO Reparacion (F_asig, CI_Personal, Numero_Orden, F_rep, Descripcion, Comision, Total, Estado) VALUES ('2018-04-15 04:35:04', 27423439, '41086', '2018-04-16 23:04:53', 'Equipo computacional con fallas internas', '60238.01', '8064548.09', 'DEV');
	INSERT INTO Reparacion (F_asig, CI_Personal, Numero_Orden, F_rep, Descripcion, Comision, Total, Estado) VALUES ('2018-04-15 08:17:27', 56973278, '53564', '2018-04-20 02:02:20', 'Error en equipo', '41124.29', '7736835.91', 'ESP');
	INSERT INTO Reparacion (F_asig, CI_Personal, Numero_Orden, F_rep, Descripcion, Comision, Total, Estado) VALUES ('2018-04-15 13:29:57', 36145388, '64158', '2018-04-23 02:08:24', 'Error en equipo', '50644.38', '7933659.29', 'REP');
	INSERT INTO Reparacion (F_asig, CI_Personal, Numero_Orden, F_rep, Descripcion, Comision, Total, Estado) VALUES ('2018-04-15 14:20:47', 53290784, '25755', '2018-04-28 19:00:28', NULL, '44421.57', '6886174.51', 'REP');
	INSERT INTO Reparacion (F_asig, CI_Personal, Numero_Orden, F_rep, Descripcion, Comision, Total, Estado) VALUES ('2018-04-15 18:54:01', 53290784, '684', '2018-04-15 19:03:19', NULL, '14683.80', '5989067.78', 'DEV');

UNLOCK TABLES;

--
-- DUMP TABLE Repuesto_Reparacion
--

LOCK TABLES Repuesto_Reparacion WRITE;

	INSERT INTO Repuesto_Reparacion (Codigo_Repues, F_asig_Rep, CI_Pers_Rep, Num_Ord_Rep, Cant_Rep, Precio_Rep) VALUES ('10478', '2018-04-08 05:51:45', 36145388, '55269', 72, '2605310.22');
	INSERT INTO Repuesto_Reparacion (Codigo_Repues, F_asig_Rep, CI_Pers_Rep, Num_Ord_Rep, Cant_Rep, Precio_Rep) VALUES ('10478', '2018-04-14 03:05:45', 27423439, '53564', 68, '9820809.28');
	INSERT INTO Repuesto_Reparacion (Codigo_Repues, F_asig_Rep, CI_Pers_Rep, Num_Ord_Rep, Cant_Rep, Precio_Rep) VALUES ('16351', '2018-04-05 11:16:33', 56973278, '9144', 7, '8916900.35');
	INSERT INTO Repuesto_Reparacion (Codigo_Repues, F_asig_Rep, CI_Pers_Rep, Num_Ord_Rep, Cant_Rep, Precio_Rep) VALUES ('16351', '2018-04-10 11:16:23', 27423439, '87838', 93, '1914144.79');
	INSERT INTO Repuesto_Reparacion (Codigo_Repues, F_asig_Rep, CI_Pers_Rep, Num_Ord_Rep, Cant_Rep, Precio_Rep) VALUES ('16351', '2018-04-15 08:17:27', 56973278, '53564', 15, '3549361.34');
	INSERT INTO Repuesto_Reparacion (Codigo_Repues, F_asig_Rep, CI_Pers_Rep, Num_Ord_Rep, Cant_Rep, Precio_Rep) VALUES ('18269', '2018-04-02 15:29:39', 53290784, '15703', 92, '2949279.30');
	INSERT INTO Repuesto_Reparacion (Codigo_Repues, F_asig_Rep, CI_Pers_Rep, Num_Ord_Rep, Cant_Rep, Precio_Rep) VALUES ('18269', '2018-04-15 08:17:27', 56973278, '53564', 12, '2194822.91');
	INSERT INTO Repuesto_Reparacion (Codigo_Repues, F_asig_Rep, CI_Pers_Rep, Num_Ord_Rep, Cant_Rep, Precio_Rep) VALUES ('18334', '2018-04-07 01:42:52', 53290784, '55269', 61, '1634376.07');
	INSERT INTO Repuesto_Reparacion (Codigo_Repues, F_asig_Rep, CI_Pers_Rep, Num_Ord_Rep, Cant_Rep, Precio_Rep) VALUES ('18334', '2018-04-07 22:34:54', 53290784, '80644', 22, '5164726.28');
	INSERT INTO Repuesto_Reparacion (Codigo_Repues, F_asig_Rep, CI_Pers_Rep, Num_Ord_Rep, Cant_Rep, Precio_Rep) VALUES ('19625', '2018-04-07 01:45:07', 53290784, '40089', 2, '1432905.88');
	INSERT INTO Repuesto_Reparacion (Codigo_Repues, F_asig_Rep, CI_Pers_Rep, Num_Ord_Rep, Cant_Rep, Precio_Rep) VALUES ('19625', '2018-04-07 12:31:55', 56973278, '2117', 1, '5835920.27');
	INSERT INTO Repuesto_Reparacion (Codigo_Repues, F_asig_Rep, CI_Pers_Rep, Num_Ord_Rep, Cant_Rep, Precio_Rep) VALUES ('20151', '2018-04-08 05:51:45', 36145388, '55269', 50, '7837174.01');
	INSERT INTO Repuesto_Reparacion (Codigo_Repues, F_asig_Rep, CI_Pers_Rep, Num_Ord_Rep, Cant_Rep, Precio_Rep) VALUES ('20151', '2018-04-15 08:17:27', 56973278, '53564', 37, '2804247.88');
	INSERT INTO Repuesto_Reparacion (Codigo_Repues, F_asig_Rep, CI_Pers_Rep, Num_Ord_Rep, Cant_Rep, Precio_Rep) VALUES ('20151', '2018-04-15 18:54:01', 53290784, '684', 52, '2914020.11');
	INSERT INTO Repuesto_Reparacion (Codigo_Repues, F_asig_Rep, CI_Pers_Rep, Num_Ord_Rep, Cant_Rep, Precio_Rep) VALUES ('23751', '2018-04-05 10:47:49', 53290784, '58672', 5, '2042683.11');
	INSERT INTO Repuesto_Reparacion (Codigo_Repues, F_asig_Rep, CI_Pers_Rep, Num_Ord_Rep, Cant_Rep, Precio_Rep) VALUES ('23751', '2018-04-05 11:16:33', 56973278, '9144', 31, '6638598.65');
	INSERT INTO Repuesto_Reparacion (Codigo_Repues, F_asig_Rep, CI_Pers_Rep, Num_Ord_Rep, Cant_Rep, Precio_Rep) VALUES ('23751', '2018-04-07 01:42:52', 53290784, '55269', 69, '3798557.92');
	INSERT INTO Repuesto_Reparacion (Codigo_Repues, F_asig_Rep, CI_Pers_Rep, Num_Ord_Rep, Cant_Rep, Precio_Rep) VALUES ('23751', '2018-04-07 01:45:07', 53290784, '40089', 47, '846174.92');
	INSERT INTO Repuesto_Reparacion (Codigo_Repues, F_asig_Rep, CI_Pers_Rep, Num_Ord_Rep, Cant_Rep, Precio_Rep) VALUES ('23751', '2018-04-14 14:26:49', 27423439, '35763', 27, '4347653.01');
	INSERT INTO Repuesto_Reparacion (Codigo_Repues, F_asig_Rep, CI_Pers_Rep, Num_Ord_Rep, Cant_Rep, Precio_Rep) VALUES ('25264', '2018-04-11 10:42:42', 36145388, '85981', 65, '6496650.01');
	INSERT INTO Repuesto_Reparacion (Codigo_Repues, F_asig_Rep, CI_Pers_Rep, Num_Ord_Rep, Cant_Rep, Precio_Rep) VALUES ('25264', '2018-04-14 03:05:45', 27423439, '53564', 93, '2314611.10');
	INSERT INTO Repuesto_Reparacion (Codigo_Repues, F_asig_Rep, CI_Pers_Rep, Num_Ord_Rep, Cant_Rep, Precio_Rep) VALUES ('25264', '2018-04-15 18:54:01', 53290784, '684', 93, '4796648.54');
	INSERT INTO Repuesto_Reparacion (Codigo_Repues, F_asig_Rep, CI_Pers_Rep, Num_Ord_Rep, Cant_Rep, Precio_Rep) VALUES ('2564', '2018-04-07 20:16:49', 27423439, '37608', 33, '7776468.48');
	INSERT INTO Repuesto_Reparacion (Codigo_Repues, F_asig_Rep, CI_Pers_Rep, Num_Ord_Rep, Cant_Rep, Precio_Rep) VALUES ('2564', '2018-04-08 05:51:45', 36145388, '55269', 80, '931759.42');
	INSERT INTO Repuesto_Reparacion (Codigo_Repues, F_asig_Rep, CI_Pers_Rep, Num_Ord_Rep, Cant_Rep, Precio_Rep) VALUES ('2688', '2018-04-08 16:13:27', 56973278, '63714', 86, '783184.07');
	INSERT INTO Repuesto_Reparacion (Codigo_Repues, F_asig_Rep, CI_Pers_Rep, Num_Ord_Rep, Cant_Rep, Precio_Rep) VALUES ('30620', '2018-04-05 22:53:26', 56973278, '55269', 85, '9984336.03');
	INSERT INTO Repuesto_Reparacion (Codigo_Repues, F_asig_Rep, CI_Pers_Rep, Num_Ord_Rep, Cant_Rep, Precio_Rep) VALUES ('30620', '2018-04-07 20:16:49', 27423439, '37608', 96, '1371001.59');
	INSERT INTO Repuesto_Reparacion (Codigo_Repues, F_asig_Rep, CI_Pers_Rep, Num_Ord_Rep, Cant_Rep, Precio_Rep) VALUES ('30620', '2018-04-14 14:26:49', 27423439, '35763', 45, '6261559.30');
	INSERT INTO Repuesto_Reparacion (Codigo_Repues, F_asig_Rep, CI_Pers_Rep, Num_Ord_Rep, Cant_Rep, Precio_Rep) VALUES ('33489', '2018-04-07 01:42:52', 53290784, '55269', 89, '2366604.91');
	INSERT INTO Repuesto_Reparacion (Codigo_Repues, F_asig_Rep, CI_Pers_Rep, Num_Ord_Rep, Cant_Rep, Precio_Rep) VALUES ('36674', '2018-04-02 15:29:39', 53290784, '15703', 6, '3345640.77');
	INSERT INTO Repuesto_Reparacion (Codigo_Repues, F_asig_Rep, CI_Pers_Rep, Num_Ord_Rep, Cant_Rep, Precio_Rep) VALUES ('36674', '2018-04-08 08:16:12', 36145388, '85009', 90, '419787.49');
	INSERT INTO Repuesto_Reparacion (Codigo_Repues, F_asig_Rep, CI_Pers_Rep, Num_Ord_Rep, Cant_Rep, Precio_Rep) VALUES ('36674', '2018-04-15 08:17:27', 56973278, '53564', 74, '1075041.57');
	INSERT INTO Repuesto_Reparacion (Codigo_Repues, F_asig_Rep, CI_Pers_Rep, Num_Ord_Rep, Cant_Rep, Precio_Rep) VALUES ('37661', '2018-04-04 21:11:52', 56973278, '93269', 19, '1965905.77');
	INSERT INTO Repuesto_Reparacion (Codigo_Repues, F_asig_Rep, CI_Pers_Rep, Num_Ord_Rep, Cant_Rep, Precio_Rep) VALUES ('37661', '2018-04-07 01:42:52', 53290784, '55269', 36, '1767716.24');
	INSERT INTO Repuesto_Reparacion (Codigo_Repues, F_asig_Rep, CI_Pers_Rep, Num_Ord_Rep, Cant_Rep, Precio_Rep) VALUES ('37661', '2018-04-07 12:31:55', 56973278, '2117', 98, '5000944.00');
	INSERT INTO Repuesto_Reparacion (Codigo_Repues, F_asig_Rep, CI_Pers_Rep, Num_Ord_Rep, Cant_Rep, Precio_Rep) VALUES ('37661', '2018-04-12 01:16:11', 53290784, '87838', 75, '5388351.43');
	INSERT INTO Repuesto_Reparacion (Codigo_Repues, F_asig_Rep, CI_Pers_Rep, Num_Ord_Rep, Cant_Rep, Precio_Rep) VALUES ('37661', '2018-04-15 08:17:27', 56973278, '53564', 92, '4211957.23');
	INSERT INTO Repuesto_Reparacion (Codigo_Repues, F_asig_Rep, CI_Pers_Rep, Num_Ord_Rep, Cant_Rep, Precio_Rep) VALUES ('38621', '2018-04-04 21:11:52', 56973278, '93269', 96, '1300022.34');
	INSERT INTO Repuesto_Reparacion (Codigo_Repues, F_asig_Rep, CI_Pers_Rep, Num_Ord_Rep, Cant_Rep, Precio_Rep) VALUES ('38621', '2018-04-07 01:42:52', 53290784, '55269', 72, '8668436.97');
	INSERT INTO Repuesto_Reparacion (Codigo_Repues, F_asig_Rep, CI_Pers_Rep, Num_Ord_Rep, Cant_Rep, Precio_Rep) VALUES ('38621', '2018-04-15 18:54:01', 53290784, '684', 73, '2621803.97');
	INSERT INTO Repuesto_Reparacion (Codigo_Repues, F_asig_Rep, CI_Pers_Rep, Num_Ord_Rep, Cant_Rep, Precio_Rep) VALUES ('91839', '2018-04-04 21:11:52', 56973278, '93269', 56, '9774265.96');
	INSERT INTO Repuesto_Reparacion (Codigo_Repues, F_asig_Rep, CI_Pers_Rep, Num_Ord_Rep, Cant_Rep, Precio_Rep) VALUES ('91839', '2018-04-08 05:51:45', 36145388, '55269', 77, '2016802.57');
	INSERT INTO Repuesto_Reparacion (Codigo_Repues, F_asig_Rep, CI_Pers_Rep, Num_Ord_Rep, Cant_Rep, Precio_Rep) VALUES ('92712', '2018-04-05 10:47:49', 53290784, '58672', 85, '6827121.71');
	INSERT INTO Repuesto_Reparacion (Codigo_Repues, F_asig_Rep, CI_Pers_Rep, Num_Ord_Rep, Cant_Rep, Precio_Rep) VALUES ('92712', '2018-04-05 11:16:33', 56973278, '9144', 81, '308432.95');
	INSERT INTO Repuesto_Reparacion (Codigo_Repues, F_asig_Rep, CI_Pers_Rep, Num_Ord_Rep, Cant_Rep, Precio_Rep) VALUES ('94747', '2018-04-05 11:16:33', 56973278, '9144', 83, '1605923.77');
	INSERT INTO Repuesto_Reparacion (Codigo_Repues, F_asig_Rep, CI_Pers_Rep, Num_Ord_Rep, Cant_Rep, Precio_Rep) VALUES ('94747', '2018-04-07 01:45:07', 53290784, '40089', 82, '7291172.09');
	INSERT INTO Repuesto_Reparacion (Codigo_Repues, F_asig_Rep, CI_Pers_Rep, Num_Ord_Rep, Cant_Rep, Precio_Rep) VALUES ('94747', '2018-04-07 12:31:55', 56973278, '2117', 95, '8293618.20');
	INSERT INTO Repuesto_Reparacion (Codigo_Repues, F_asig_Rep, CI_Pers_Rep, Num_Ord_Rep, Cant_Rep, Precio_Rep) VALUES ('94747', '2018-04-08 00:16:16', 36145388, '3983', 25, '8822520.70');

UNLOCK TABLES;

--
-- DUMP TABLE CuentaCobrar
--

LOCK TABLES CuentaCobrar WRITE;

	INSERT INTO CuentaCobrar (Numero, Identif_Cliente, Num_Factura, F_aper, F_lim, Estado, Descripcion, Saldo, Monto) VALUES ('10508', '3334690165', '64515', '2018-04-02 23:09:05', '2018-04-14', 'Abierta', NULL, '8524599.64', '8524599.64');
	INSERT INTO CuentaCobrar (Numero, Identif_Cliente, Num_Factura, F_aper, F_lim, Estado, Descripcion, Saldo, Monto) VALUES ('12925', '6114924311', '52647', '2018-04-04 18:09:52', '2018-04-06', 'Activa', 'Cuenta pendiente', '878779.29', '878779.29');
	INSERT INTO CuentaCobrar (Numero, Identif_Cliente, Num_Factura, F_aper, F_lim, Estado, Descripcion, Saldo, Monto) VALUES ('30249', '8990920321', '76705', '2018-04-06 16:42:42', '2018-04-08', 'Activa', 'Pago pendiente', '9460869.10', '9460869.10');
	INSERT INTO CuentaCobrar (Numero, Identif_Cliente, Num_Factura, F_aper, F_lim, Estado, Descripcion, Saldo, Monto) VALUES ('31228', '801275844', '8903', '2018-04-08 01:11:05', '2018-04-13', 'Por procesar', NULL, '469519.12', '469519.12');
	INSERT INTO CuentaCobrar (Numero, Identif_Cliente, Num_Factura, F_aper, F_lim, Estado, Descripcion, Saldo, Monto) VALUES ('38181', '1580326440', '75458', '2018-04-11 08:42:33', '2018-04-12', 'Abierta', 'Pago pendiente', '289918.28', '289918.28');
	INSERT INTO CuentaCobrar (Numero, Identif_Cliente, Num_Factura, F_aper, F_lim, Estado, Descripcion, Saldo, Monto) VALUES ('38528', '3103281006', '1667', '2018-04-07 13:20:51', '2018-04-09', 'Activa', NULL, '832922.00', '832922.00');
	INSERT INTO CuentaCobrar (Numero, Identif_Cliente, Num_Factura, F_aper, F_lim, Estado, Descripcion, Saldo, Monto) VALUES ('39198', '5072576596', '64006', '2018-04-04 17:02:57', '2018-04-15', 'Activa', 'Cuenta pendiente a Cliente', '8298479.80', '8298479.80');
	INSERT INTO CuentaCobrar (Numero, Identif_Cliente, Num_Factura, F_aper, F_lim, Estado, Descripcion, Saldo, Monto) VALUES ('54617', '6361535682', '68784', '2018-04-09 05:10:22', '2018-04-13', 'Por procesar', 'Cuenta pendiente a Cliente', '1603200.80', '1603200.80');
	INSERT INTO CuentaCobrar (Numero, Identif_Cliente, Num_Factura, F_aper, F_lim, Estado, Descripcion, Saldo, Monto) VALUES ('56476', '5072576596', '36346', '2018-04-05 09:58:38', '2018-04-06', 'Abierta', 'Cuenta pendiente', '7709572.91', '7709572.91');
	INSERT INTO CuentaCobrar (Numero, Identif_Cliente, Num_Factura, F_aper, F_lim, Estado, Descripcion, Saldo, Monto) VALUES ('59291', '801275844', '25991', '2018-04-15 10:36:40', '2018-04-17', 'Abierta', 'Cuenta pendiente', '1654603.57', '1654603.57');
	INSERT INTO CuentaCobrar (Numero, Identif_Cliente, Num_Factura, F_aper, F_lim, Estado, Descripcion, Saldo, Monto) VALUES ('61732', '5962500047', '64788', '2018-04-04 16:50:15', '2018-04-04', 'Abierta', 'Cuenta pendiente a Cliente', '9111203.45', '9111203.45');
	INSERT INTO CuentaCobrar (Numero, Identif_Cliente, Num_Factura, F_aper, F_lim, Estado, Descripcion, Saldo, Monto) VALUES ('62762', '9203584013', '19310', '2018-04-10 11:08:53', '2018-04-13', 'Por procesar', NULL, '2802999.15', '2802999.15');
	INSERT INTO CuentaCobrar (Numero, Identif_Cliente, Num_Factura, F_aper, F_lim, Estado, Descripcion, Saldo, Monto) VALUES ('65785', '5962500047', '68784', '2018-04-15 11:34:53', '2018-04-18', 'Por procesar', NULL, '5782965.51', '5782965.51');
	INSERT INTO CuentaCobrar (Numero, Identif_Cliente, Num_Factura, F_aper, F_lim, Estado, Descripcion, Saldo, Monto) VALUES ('67988', '420040739', '13525', '2018-04-02 07:38:45', '2018-04-05', 'Activa', NULL, '8572205.29', '8572205.29');
	INSERT INTO CuentaCobrar (Numero, Identif_Cliente, Num_Factura, F_aper, F_lim, Estado, Descripcion, Saldo, Monto) VALUES ('75287', '9203584013', '55801', '2018-04-04 17:30:41', '2018-04-15', 'Activa', 'Cuenta pendiente a Cliente', '4308905.09', '4308905.09');
	INSERT INTO CuentaCobrar (Numero, Identif_Cliente, Num_Factura, F_aper, F_lim, Estado, Descripcion, Saldo, Monto) VALUES ('79048', '7188356091', '55801', '2018-04-06 08:49:54', '2018-04-07', 'Por procesar', 'Cuenta pendiente a Cliente', '7887500.88', '7887500.88');
	INSERT INTO CuentaCobrar (Numero, Identif_Cliente, Num_Factura, F_aper, F_lim, Estado, Descripcion, Saldo, Monto) VALUES ('79915', '6160975585', '34262', '2018-04-14 22:51:05', '2018-04-15', 'Por procesar', NULL, '1524473.44', '1524473.44');
	INSERT INTO CuentaCobrar (Numero, Identif_Cliente, Num_Factura, F_aper, F_lim, Estado, Descripcion, Saldo, Monto) VALUES ('90756', '6279233983', '13525', '2018-04-10 11:28:03', '2018-04-11', 'Abierta', 'Pago pendiente', '909999.71', '909999.71');
	INSERT INTO CuentaCobrar (Numero, Identif_Cliente, Num_Factura, F_aper, F_lim, Estado, Descripcion, Saldo, Monto) VALUES ('92655', '1248296668', '25991', '2018-04-11 12:43:57', '2018-04-15', 'Por procesar', 'Pago pendiente', '550499.45', '550499.45');
	INSERT INTO CuentaCobrar (Numero, Identif_Cliente, Num_Factura, F_aper, F_lim, Estado, Descripcion, Saldo, Monto) VALUES ('9984', '2429767043', '55801', '2018-04-13 08:55:01', '2018-04-14', 'Por procesar', 'Cuenta pendiente a Cliente', '6527223.74', '6527223.74');

UNLOCK TABLES;

--
-- DUMP TABLE FormaPagoV
--

LOCK TABLES FormaPagoV WRITE;

	INSERT INTO FormaPagoV (Fecha, Banco, Nref, Descripcion, Tipo, Monto) VALUES ('2018-04-02 00:11:15', 'BANESCO', 406721590, 'Pago efectuado', 'DEP', '61277.78');
	INSERT INTO FormaPagoV (Fecha, Banco, Nref, Descripcion, Tipo, Monto) VALUES ('2018-04-02 07:41:26', 'Mercantil', 520379129, 'Pago realizado', 'TRF', '45510.77');
	INSERT INTO FormaPagoV (Fecha, Banco, Nref, Descripcion, Tipo, Monto) VALUES ('2018-04-04 10:14:58', 'Venezuela', 616647683, 'Pago realizado', 'TC', '48843.39');
	INSERT INTO FormaPagoV (Fecha, Banco, Nref, Descripcion, Tipo, Monto) VALUES ('2018-04-04 10:51:13', 'BANESCO', 497295567, 'Pago efectuado', 'TRF', '68169.97');
	INSERT INTO FormaPagoV (Fecha, Banco, Nref, Descripcion, Tipo, Monto) VALUES ('2018-04-05 09:26:09', 'Mercantil', 854475228, 'Pago efectuado', 'TD', '33362.36');
	INSERT INTO FormaPagoV (Fecha, Banco, Nref, Descripcion, Tipo, Monto) VALUES ('2018-04-06 05:38:13', 'Bicentenario', 498976863, 'Pago realizado', 'DEP', '51643.23');
	INSERT INTO FormaPagoV (Fecha, Banco, Nref, Descripcion, Tipo, Monto) VALUES ('2018-04-06 19:04:37', 'BANESCO', 637576725, 'Pago realizado', 'DEP', '25810.35');
	INSERT INTO FormaPagoV (Fecha, Banco, Nref, Descripcion, Tipo, Monto) VALUES ('2018-04-08 01:19:58', 'Venezuela', 286978030, 'Pago realizado', 'TD', '45114.82');
	INSERT INTO FormaPagoV (Fecha, Banco, Nref, Descripcion, Tipo, Monto) VALUES ('2018-04-08 02:52:58', 'Nacional de Crédito', NULL, 'Pago realizado', 'EF', '30186.37');
	INSERT INTO FormaPagoV (Fecha, Banco, Nref, Descripcion, Tipo, Monto) VALUES ('2018-04-08 09:47:41', 'Bicentenario', 394603862, 'Pago efectuado', 'TD', '55954.04');
	INSERT INTO FormaPagoV (Fecha, Banco, Nref, Descripcion, Tipo, Monto) VALUES ('2018-04-09 17:06:50', 'Venezuela', NULL, 'Pago realizado', 'EF', '19979.17');
	INSERT INTO FormaPagoV (Fecha, Banco, Nref, Descripcion, Tipo, Monto) VALUES ('2018-04-10 03:52:33', 'BANESCO', 195176458, 'Pago efectuado', 'CH', '64219.55');
	INSERT INTO FormaPagoV (Fecha, Banco, Nref, Descripcion, Tipo, Monto) VALUES ('2018-04-10 19:20:49', 'Mercantil', NULL, NULL, 'EF', '82819.12');
	INSERT INTO FormaPagoV (Fecha, Banco, Nref, Descripcion, Tipo, Monto) VALUES ('2018-04-12 13:33:10', 'Bicentenario', 859664574, 'Pago efectuado', 'CH', '30177.84');
	INSERT INTO FormaPagoV (Fecha, Banco, Nref, Descripcion, Tipo, Monto) VALUES ('2018-04-13 10:17:39', 'Mercantil', 679261394, NULL, 'TRF', '48674.58');
	INSERT INTO FormaPagoV (Fecha, Banco, Nref, Descripcion, Tipo, Monto) VALUES ('2018-04-13 17:54:17', 'BANESCO', 164568694, 'Pago realizado', 'TD', '67557.86');
	INSERT INTO FormaPagoV (Fecha, Banco, Nref, Descripcion, Tipo, Monto) VALUES ('2018-04-14 05:50:42', 'Venezuela', 495026680, NULL, 'DEP', '77032.77');
	INSERT INTO FormaPagoV (Fecha, Banco, Nref, Descripcion, Tipo, Monto) VALUES ('2018-04-14 12:24:06', 'Nacional de Crédito', 608230926, 'Pago efectuado', 'DEP', '10341.89');
	INSERT INTO FormaPagoV (Fecha, Banco, Nref, Descripcion, Tipo, Monto) VALUES ('2018-04-15 05:02:10', 'Nacional de Crédito', NULL, 'Pago efectuado', 'EF', '939.65');
	INSERT INTO FormaPagoV (Fecha, Banco, Nref, Descripcion, Tipo, Monto) VALUES ('2018-04-15 11:01:15', 'Bicentenario', 390631621, 'Pago realizado', 'TRF', '1074.23');

UNLOCK TABLES;

--
-- DUMP TABLE CuentaCobrar_FormaPagoV
--

LOCK TABLES CuentaCobrar_FormaPagoV WRITE;

	INSERT INTO CuentaCobrar_FormaPagoV (Num_Cuenta, Cliente_Cuenta, Fecha_Pago) VALUES ('10508', '6114924311', '2018-04-08 09:47:41');
	INSERT INTO CuentaCobrar_FormaPagoV (Num_Cuenta, Cliente_Cuenta, Fecha_Pago) VALUES ('12925', '1248296668', '2018-04-15 11:01:15');
	INSERT INTO CuentaCobrar_FormaPagoV (Num_Cuenta, Cliente_Cuenta, Fecha_Pago) VALUES ('12925', '5072576596', '2018-04-15 05:02:10');
	INSERT INTO CuentaCobrar_FormaPagoV (Num_Cuenta, Cliente_Cuenta, Fecha_Pago) VALUES ('12925', '6114924311', '2018-04-02 07:41:26');
	INSERT INTO CuentaCobrar_FormaPagoV (Num_Cuenta, Cliente_Cuenta, Fecha_Pago) VALUES ('12925', '801275844', '2018-04-15 05:02:10');
	INSERT INTO CuentaCobrar_FormaPagoV (Num_Cuenta, Cliente_Cuenta, Fecha_Pago) VALUES ('12925', '9203584013', '2018-04-10 03:52:33');
	INSERT INTO CuentaCobrar_FormaPagoV (Num_Cuenta, Cliente_Cuenta, Fecha_Pago) VALUES ('31228', '3103281006', '2018-04-09 17:06:50');
	INSERT INTO CuentaCobrar_FormaPagoV (Num_Cuenta, Cliente_Cuenta, Fecha_Pago) VALUES ('31228', '6361535682', '2018-04-15 05:02:10');
	INSERT INTO CuentaCobrar_FormaPagoV (Num_Cuenta, Cliente_Cuenta, Fecha_Pago) VALUES ('31228', '801275844', '2018-04-14 12:24:06');
	INSERT INTO CuentaCobrar_FormaPagoV (Num_Cuenta, Cliente_Cuenta, Fecha_Pago) VALUES ('31228', '9203584013', '2018-04-04 10:14:58');
	INSERT INTO CuentaCobrar_FormaPagoV (Num_Cuenta, Cliente_Cuenta, Fecha_Pago) VALUES ('38181', '801275844', '2018-04-14 05:50:42');
	INSERT INTO CuentaCobrar_FormaPagoV (Num_Cuenta, Cliente_Cuenta, Fecha_Pago) VALUES ('38528', '5962500047', '2018-04-14 05:50:42');
	INSERT INTO CuentaCobrar_FormaPagoV (Num_Cuenta, Cliente_Cuenta, Fecha_Pago) VALUES ('38528', '6160975585', '2018-04-09 17:06:50');
	INSERT INTO CuentaCobrar_FormaPagoV (Num_Cuenta, Cliente_Cuenta, Fecha_Pago) VALUES ('38528', '801275844', '2018-04-12 13:33:10');
	INSERT INTO CuentaCobrar_FormaPagoV (Num_Cuenta, Cliente_Cuenta, Fecha_Pago) VALUES ('38528', '9203584013', '2018-04-12 13:33:10');
	INSERT INTO CuentaCobrar_FormaPagoV (Num_Cuenta, Cliente_Cuenta, Fecha_Pago) VALUES ('39198', '6361535682', '2018-04-10 03:52:33');
	INSERT INTO CuentaCobrar_FormaPagoV (Num_Cuenta, Cliente_Cuenta, Fecha_Pago) VALUES ('39198', '801275844', '2018-04-05 09:26:09');
	INSERT INTO CuentaCobrar_FormaPagoV (Num_Cuenta, Cliente_Cuenta, Fecha_Pago) VALUES ('54617', '1248296668', '2018-04-13 17:54:17');
	INSERT INTO CuentaCobrar_FormaPagoV (Num_Cuenta, Cliente_Cuenta, Fecha_Pago) VALUES ('54617', '420040739', '2018-04-12 13:33:10');
	INSERT INTO CuentaCobrar_FormaPagoV (Num_Cuenta, Cliente_Cuenta, Fecha_Pago) VALUES ('54617', '5072576596', '2018-04-15 11:01:15');
	INSERT INTO CuentaCobrar_FormaPagoV (Num_Cuenta, Cliente_Cuenta, Fecha_Pago) VALUES ('56476', '5072576596', '2018-04-02 00:11:15');
	INSERT INTO CuentaCobrar_FormaPagoV (Num_Cuenta, Cliente_Cuenta, Fecha_Pago) VALUES ('59291', '3103281006', '2018-04-10 19:20:49');
	INSERT INTO CuentaCobrar_FormaPagoV (Num_Cuenta, Cliente_Cuenta, Fecha_Pago) VALUES ('59291', '5962500047', '2018-04-15 11:01:15');
	INSERT INTO CuentaCobrar_FormaPagoV (Num_Cuenta, Cliente_Cuenta, Fecha_Pago) VALUES ('59291', '801275844', '2018-04-06 19:04:37');
	INSERT INTO CuentaCobrar_FormaPagoV (Num_Cuenta, Cliente_Cuenta, Fecha_Pago) VALUES ('61732', '3334690165', '2018-04-06 05:38:13');
	INSERT INTO CuentaCobrar_FormaPagoV (Num_Cuenta, Cliente_Cuenta, Fecha_Pago) VALUES ('62762', '6279233983', '2018-04-14 05:50:42');
	INSERT INTO CuentaCobrar_FormaPagoV (Num_Cuenta, Cliente_Cuenta, Fecha_Pago) VALUES ('62762', '9203584013', '2018-04-10 03:52:33');
	INSERT INTO CuentaCobrar_FormaPagoV (Num_Cuenta, Cliente_Cuenta, Fecha_Pago) VALUES ('65785', '5072576596', '2018-04-08 01:19:58');
	INSERT INTO CuentaCobrar_FormaPagoV (Num_Cuenta, Cliente_Cuenta, Fecha_Pago) VALUES ('65785', '5962500047', '2018-04-08 02:52:58');
	INSERT INTO CuentaCobrar_FormaPagoV (Num_Cuenta, Cliente_Cuenta, Fecha_Pago) VALUES ('65785', '7188356091', '2018-04-13 17:54:17');
	INSERT INTO CuentaCobrar_FormaPagoV (Num_Cuenta, Cliente_Cuenta, Fecha_Pago) VALUES ('65785', '801275844', '2018-04-06 19:04:37');
	INSERT INTO CuentaCobrar_FormaPagoV (Num_Cuenta, Cliente_Cuenta, Fecha_Pago) VALUES ('67988', '5962500047', '2018-04-08 01:19:58');
	INSERT INTO CuentaCobrar_FormaPagoV (Num_Cuenta, Cliente_Cuenta, Fecha_Pago) VALUES ('67988', '9203584013', '2018-04-15 05:02:10');
	INSERT INTO CuentaCobrar_FormaPagoV (Num_Cuenta, Cliente_Cuenta, Fecha_Pago) VALUES ('79915', '1580326440', '2018-04-10 03:52:33');
	INSERT INTO CuentaCobrar_FormaPagoV (Num_Cuenta, Cliente_Cuenta, Fecha_Pago) VALUES ('79915', '6279233983', '2018-04-13 17:54:17');
	INSERT INTO CuentaCobrar_FormaPagoV (Num_Cuenta, Cliente_Cuenta, Fecha_Pago) VALUES ('79915', '801275844', '2018-04-04 10:51:13');
	INSERT INTO CuentaCobrar_FormaPagoV (Num_Cuenta, Cliente_Cuenta, Fecha_Pago) VALUES ('90756', '5962500047', '2018-04-08 02:52:58');
	INSERT INTO CuentaCobrar_FormaPagoV (Num_Cuenta, Cliente_Cuenta, Fecha_Pago) VALUES ('90756', '8990920321', '2018-04-10 19:20:49');
	INSERT INTO CuentaCobrar_FormaPagoV (Num_Cuenta, Cliente_Cuenta, Fecha_Pago) VALUES ('92655', '5072576596', '2018-04-10 19:20:49');
	INSERT INTO CuentaCobrar_FormaPagoV (Num_Cuenta, Cliente_Cuenta, Fecha_Pago) VALUES ('92655', '6361535682', '2018-04-10 03:52:33');

UNLOCK TABLES;

--
-- DUMP TABLE FacturaVenta_FormaPagoV
--

LOCK TABLES FacturaVenta_FormaPagoV WRITE;

	INSERT INTO FacturaVenta_FormaPagoV (Num_Factura, Fecha_Pago) VALUES ('13239', '2018-04-13 17:54:17');
	INSERT INTO FacturaVenta_FormaPagoV (Num_Factura, Fecha_Pago) VALUES ('13525', '2018-04-02 00:11:15');
	INSERT INTO FacturaVenta_FormaPagoV (Num_Factura, Fecha_Pago) VALUES ('18079', '2018-04-10 19:20:49');
	INSERT INTO FacturaVenta_FormaPagoV (Num_Factura, Fecha_Pago) VALUES ('23604', '2018-04-14 05:50:42');
	INSERT INTO FacturaVenta_FormaPagoV (Num_Factura, Fecha_Pago) VALUES ('2797', '2018-04-08 01:19:58');
	INSERT INTO FacturaVenta_FormaPagoV (Num_Factura, Fecha_Pago) VALUES ('34262', '2018-04-15 05:02:10');
	INSERT INTO FacturaVenta_FormaPagoV (Num_Factura, Fecha_Pago) VALUES ('35514', '2018-04-06 05:38:13');
	INSERT INTO FacturaVenta_FormaPagoV (Num_Factura, Fecha_Pago) VALUES ('36004', '2018-04-04 10:51:13');
	INSERT INTO FacturaVenta_FormaPagoV (Num_Factura, Fecha_Pago) VALUES ('36004', '2018-04-14 05:50:42');
	INSERT INTO FacturaVenta_FormaPagoV (Num_Factura, Fecha_Pago) VALUES ('36346', '2018-04-04 10:14:58');
	INSERT INTO FacturaVenta_FormaPagoV (Num_Factura, Fecha_Pago) VALUES ('41572', '2018-04-15 11:01:15');
	INSERT INTO FacturaVenta_FormaPagoV (Num_Factura, Fecha_Pago) VALUES ('4386', '2018-04-02 07:41:26');
	INSERT INTO FacturaVenta_FormaPagoV (Num_Factura, Fecha_Pago) VALUES ('44779', '2018-04-10 19:20:49');
	INSERT INTO FacturaVenta_FormaPagoV (Num_Factura, Fecha_Pago) VALUES ('47167', '2018-04-02 00:11:15');
	INSERT INTO FacturaVenta_FormaPagoV (Num_Factura, Fecha_Pago) VALUES ('47167', '2018-04-05 09:26:09');
	INSERT INTO FacturaVenta_FormaPagoV (Num_Factura, Fecha_Pago) VALUES ('47167', '2018-04-08 09:47:41');
	INSERT INTO FacturaVenta_FormaPagoV (Num_Factura, Fecha_Pago) VALUES ('52647', '2018-04-08 02:52:58');
	INSERT INTO FacturaVenta_FormaPagoV (Num_Factura, Fecha_Pago) VALUES ('55801', '2018-04-06 05:38:13');
	INSERT INTO FacturaVenta_FormaPagoV (Num_Factura, Fecha_Pago) VALUES ('55801', '2018-04-15 11:01:15');
	INSERT INTO FacturaVenta_FormaPagoV (Num_Factura, Fecha_Pago) VALUES ('60726', '2018-04-02 00:11:15');
	INSERT INTO FacturaVenta_FormaPagoV (Num_Factura, Fecha_Pago) VALUES ('60726', '2018-04-05 09:26:09');
	INSERT INTO FacturaVenta_FormaPagoV (Num_Factura, Fecha_Pago) VALUES ('61657', '2018-04-08 02:52:58');
	INSERT INTO FacturaVenta_FormaPagoV (Num_Factura, Fecha_Pago) VALUES ('61657', '2018-04-13 10:17:39');
	INSERT INTO FacturaVenta_FormaPagoV (Num_Factura, Fecha_Pago) VALUES ('63893', '2018-04-02 07:41:26');
	INSERT INTO FacturaVenta_FormaPagoV (Num_Factura, Fecha_Pago) VALUES ('63893', '2018-04-12 13:33:10');
	INSERT INTO FacturaVenta_FormaPagoV (Num_Factura, Fecha_Pago) VALUES ('64006', '2018-04-14 12:24:06');
	INSERT INTO FacturaVenta_FormaPagoV (Num_Factura, Fecha_Pago) VALUES ('64515', '2018-04-08 01:19:58');
	INSERT INTO FacturaVenta_FormaPagoV (Num_Factura, Fecha_Pago) VALUES ('64788', '2018-04-10 03:52:33');
	INSERT INTO FacturaVenta_FormaPagoV (Num_Factura, Fecha_Pago) VALUES ('64788', '2018-04-13 17:54:17');
	INSERT INTO FacturaVenta_FormaPagoV (Num_Factura, Fecha_Pago) VALUES ('68784', '2018-04-13 17:54:17');
	INSERT INTO FacturaVenta_FormaPagoV (Num_Factura, Fecha_Pago) VALUES ('70808', '2018-04-15 05:02:10');
	INSERT INTO FacturaVenta_FormaPagoV (Num_Factura, Fecha_Pago) VALUES ('71133', '2018-04-14 12:24:06');
	INSERT INTO FacturaVenta_FormaPagoV (Num_Factura, Fecha_Pago) VALUES ('74290', '2018-04-06 05:38:13');
	INSERT INTO FacturaVenta_FormaPagoV (Num_Factura, Fecha_Pago) VALUES ('76705', '2018-04-15 11:01:15');
	INSERT INTO FacturaVenta_FormaPagoV (Num_Factura, Fecha_Pago) VALUES ('8273', '2018-04-15 11:01:15');
	INSERT INTO FacturaVenta_FormaPagoV (Num_Factura, Fecha_Pago) VALUES ('8382', '2018-04-09 17:06:50');
	INSERT INTO FacturaVenta_FormaPagoV (Num_Factura, Fecha_Pago) VALUES ('8382', '2018-04-13 10:17:39');
	INSERT INTO FacturaVenta_FormaPagoV (Num_Factura, Fecha_Pago) VALUES ('8903', '2018-04-12 13:33:10');
	INSERT INTO FacturaVenta_FormaPagoV (Num_Factura, Fecha_Pago) VALUES ('91472', '2018-04-10 19:20:49');
	INSERT INTO FacturaVenta_FormaPagoV (Num_Factura, Fecha_Pago) VALUES ('96013', '2018-04-08 02:52:58');

UNLOCK TABLES;

--
-- DUMP TABLE Cuenta_FormaPagoV
--

LOCK TABLES Cuenta_FormaPagoV WRITE;

	INSERT INTO Cuenta_FormaPagoV (Num_Cuenta, Fecha_Pago) VALUES ('42395432491923931000', '2018-04-04 10:51:13');
	INSERT INTO Cuenta_FormaPagoV (Num_Cuenta, Fecha_Pago) VALUES ('42395432491923931000', '2018-04-05 09:26:09');
	INSERT INTO Cuenta_FormaPagoV (Num_Cuenta, Fecha_Pago) VALUES ('42395432491923931000', '2018-04-06 05:38:13');
	INSERT INTO Cuenta_FormaPagoV (Num_Cuenta, Fecha_Pago) VALUES ('42395432491923931000', '2018-04-08 01:19:58');
	INSERT INTO Cuenta_FormaPagoV (Num_Cuenta, Fecha_Pago) VALUES ('42395432491923931000', '2018-04-08 09:47:41');
	INSERT INTO Cuenta_FormaPagoV (Num_Cuenta, Fecha_Pago) VALUES ('42395432491923931000', '2018-04-13 10:17:39');
	INSERT INTO Cuenta_FormaPagoV (Num_Cuenta, Fecha_Pago) VALUES ('4239543749192393100', '2018-04-06 05:38:13');
	INSERT INTO Cuenta_FormaPagoV (Num_Cuenta, Fecha_Pago) VALUES ('4239543749192393100', '2018-04-08 02:52:58');
	INSERT INTO Cuenta_FormaPagoV (Num_Cuenta, Fecha_Pago) VALUES ('4239543749192393100', '2018-04-09 17:06:50');
	INSERT INTO Cuenta_FormaPagoV (Num_Cuenta, Fecha_Pago) VALUES ('4239543749192393100', '2018-04-10 03:52:33');
	INSERT INTO Cuenta_FormaPagoV (Num_Cuenta, Fecha_Pago) VALUES ('4239543749192393100', '2018-04-12 13:33:10');
	INSERT INTO Cuenta_FormaPagoV (Num_Cuenta, Fecha_Pago) VALUES ('4239543749192393100', '2018-04-13 17:54:17');
	INSERT INTO Cuenta_FormaPagoV (Num_Cuenta, Fecha_Pago) VALUES ('4239543749192393100', '2018-04-15 11:01:15');
	INSERT INTO Cuenta_FormaPagoV (Num_Cuenta, Fecha_Pago) VALUES ('9239543649192293100', '2018-04-02 00:11:15');
	INSERT INTO Cuenta_FormaPagoV (Num_Cuenta, Fecha_Pago) VALUES ('9239543649192293100', '2018-04-08 09:47:41');
	INSERT INTO Cuenta_FormaPagoV (Num_Cuenta, Fecha_Pago) VALUES ('9239543649192293100', '2018-04-15 05:02:10');

UNLOCK TABLES;

--
-- DUMP TABLE CuentaPagar
--

LOCK TABLES CuentaPagar WRITE;

	INSERT INTO CuentaPagar (Numero, RIF_Proveed, Num_Factura, F_aper, F_lim, Estado, Descripcion, Saldo, Monto) VALUES ('18823', '9734091711', '53750', '2018-04-15 12:39:06', '2018-04-16', 'Activa', 'Pago efectuado', '4033476.97', '4033476.97');
	INSERT INTO CuentaPagar (Numero, RIF_Proveed, Num_Factura, F_aper, F_lim, Estado, Descripcion, Saldo, Monto) VALUES ('23571', '3306600502', '16168', '2018-04-05 15:06:57', '2018-04-11', 'Activa', 'Pago efectuado', '348163.25', '348163.25');
	INSERT INTO CuentaPagar (Numero, RIF_Proveed, Num_Factura, F_aper, F_lim, Estado, Descripcion, Saldo, Monto) VALUES ('34147', '2607662921', '25488', '2018-04-12 10:55:40', '2018-04-12', 'Abierta', 'Pago efectuado', '5686999.15', '5686999.15');
	INSERT INTO CuentaPagar (Numero, RIF_Proveed, Num_Factura, F_aper, F_lim, Estado, Descripcion, Saldo, Monto) VALUES ('37248', '3604887211', '51376', '2018-04-05 22:07:53', '2018-04-15', 'Abierta', 'Pago realizado', '6423022.53', '6423022.53');
	INSERT INTO CuentaPagar (Numero, RIF_Proveed, Num_Factura, F_aper, F_lim, Estado, Descripcion, Saldo, Monto) VALUES ('37422', '5477514320', '62707', '2018-04-10 15:58:53', '2018-04-17', 'Activa', 'Pago efectuado', '2086663.41', '2086663.41');
	INSERT INTO CuentaPagar (Numero, RIF_Proveed, Num_Factura, F_aper, F_lim, Estado, Descripcion, Saldo, Monto) VALUES ('40359', '9035177997', '46981', '2018-04-14 10:44:54', '2018-04-15', 'Abierta', NULL, '7216999.22', '7216999.22');
	INSERT INTO CuentaPagar (Numero, RIF_Proveed, Num_Factura, F_aper, F_lim, Estado, Descripcion, Saldo, Monto) VALUES ('49584', '6409739906', '14534', '2018-04-06 18:42:17', '2018-04-13', 'Por procesar', NULL, '8591074.95', '8591074.95');
	INSERT INTO CuentaPagar (Numero, RIF_Proveed, Num_Factura, F_aper, F_lim, Estado, Descripcion, Saldo, Monto) VALUES ('55132', '5637769851', '57636', '2018-04-15 05:22:10', '2018-04-23', 'Activa', 'Pago realizado', '8160099.24', '8160099.24');
	INSERT INTO CuentaPagar (Numero, RIF_Proveed, Num_Factura, F_aper, F_lim, Estado, Descripcion, Saldo, Monto) VALUES ('5773', '7622061974', '28552', '2018-04-11 16:31:51', '2018-04-15', 'Abierta', 'Pago efectuado', '8383159.78', '8383159.78');
	INSERT INTO CuentaPagar (Numero, RIF_Proveed, Num_Factura, F_aper, F_lim, Estado, Descripcion, Saldo, Monto) VALUES ('62890', '9035177997', '77111', '2018-04-11 04:45:17', '2018-04-16', 'Activa', 'Pago efectuado', '9824277.80', '9824277.80');
	INSERT INTO CuentaPagar (Numero, RIF_Proveed, Num_Factura, F_aper, F_lim, Estado, Descripcion, Saldo, Monto) VALUES ('65332', '4229885128', '99652', '2018-04-06 11:41:30', '2018-04-10', 'Por procesar', NULL, '4598080.23', '4598080.23');
	INSERT INTO CuentaPagar (Numero, RIF_Proveed, Num_Factura, F_aper, F_lim, Estado, Descripcion, Saldo, Monto) VALUES ('6840', '9339956821', '67581', '2018-04-12 14:31:24', '2018-04-13', 'Por procesar', 'Pago realizado', '386086.76', '386086.76');
	INSERT INTO CuentaPagar (Numero, RIF_Proveed, Num_Factura, F_aper, F_lim, Estado, Descripcion, Saldo, Monto) VALUES ('71223', '4874382655', '57636', '2018-04-14 15:20:21', '2018-04-15', 'Por procesar', 'Pago efectuado', '2387999.75', '2387999.75');
	INSERT INTO CuentaPagar (Numero, RIF_Proveed, Num_Factura, F_aper, F_lim, Estado, Descripcion, Saldo, Monto) VALUES ('71352', '6613845882', '73349', '2018-04-06 07:35:13', '2018-04-10', 'Por procesar', 'Pago efectuado', '8006620.23', '8006620.23');
	INSERT INTO CuentaPagar (Numero, RIF_Proveed, Num_Factura, F_aper, F_lim, Estado, Descripcion, Saldo, Monto) VALUES ('72643', '8390533217', '21358', '2018-04-09 08:16:25', '2018-04-13', 'Abierta', 'Pago realizado', '731519.43', '731519.43');
	INSERT INTO CuentaPagar (Numero, RIF_Proveed, Num_Factura, F_aper, F_lim, Estado, Descripcion, Saldo, Monto) VALUES ('73068', '6409739906', '96311', '2018-04-05 03:45:42', '2018-04-14', 'Por procesar', 'Pago realizado', '509626.46', '509626.46');
	INSERT INTO CuentaPagar (Numero, RIF_Proveed, Num_Factura, F_aper, F_lim, Estado, Descripcion, Saldo, Monto) VALUES ('79519', '2736012909', '99296', '2018-04-04 18:06:49', '2018-05-23', 'Por procesar', 'Pago efectuado', '6518286.10', '6518286.10');
	INSERT INTO CuentaPagar (Numero, RIF_Proveed, Num_Factura, F_aper, F_lim, Estado, Descripcion, Saldo, Monto) VALUES ('80197', '3766016693', '60423', '2018-04-11 21:43:04', '2018-04-27', 'Abierta', NULL, '4543248.95', '4543248.95');
	INSERT INTO CuentaPagar (Numero, RIF_Proveed, Num_Factura, F_aper, F_lim, Estado, Descripcion, Saldo, Monto) VALUES ('82186', '9818162875', '19668', '2018-04-15 11:54:32', '2018-04-18', 'Abierta', NULL, '6323128.02', '6323128.02');
	INSERT INTO CuentaPagar (Numero, RIF_Proveed, Num_Factura, F_aper, F_lim, Estado, Descripcion, Saldo, Monto) VALUES ('90523', '7027146087', '4003', '2018-04-10 07:12:41', '2018-04-15', 'Activa', 'Pago realizado', '9531118.32', '9531118.32');

UNLOCK TABLES;

--
-- DUMP TABLE FormaPagoC
--

LOCK TABLES FormaPagoC WRITE;

	INSERT INTO FormaPagoC (Fecha, Banco, Nref, Descripcion, Tipo, Monto) VALUES ('2018-04-02 00:16:23', 'Venezuela', 503816747, 'Pago realizado', 'TC', '95399.60');
	INSERT INTO FormaPagoC (Fecha, Banco, Nref, Descripcion, Tipo, Monto) VALUES ('2018-04-02 02:41:31', 'Nacional de Crédito', NULL, 'Pago efectuado', 'EF', '59343.95');
	INSERT INTO FormaPagoC (Fecha, Banco, Nref, Descripcion, Tipo, Monto) VALUES ('2018-04-04 15:50:07', 'BANESCO', NULL, 'Pago efectuado', 'EF', '88639.05');
	INSERT INTO FormaPagoC (Fecha, Banco, Nref, Descripcion, Tipo, Monto) VALUES ('2018-04-04 20:43:50', 'Venezuela', 765009965, '', 'TD', '447.84');
	INSERT INTO FormaPagoC (Fecha, Banco, Nref, Descripcion, Tipo, Monto) VALUES ('2018-04-05 15:41:24', 'Mercantil', 624921812, 'Pago efectuado', 'CH', '263.04');
	INSERT INTO FormaPagoC (Fecha, Banco, Nref, Descripcion, Tipo, Monto) VALUES ('2018-04-06 05:23:26', 'Venezuela', 513876389, '', 'TD', '61903.64');
	INSERT INTO FormaPagoC (Fecha, Banco, Nref, Descripcion, Tipo, Monto) VALUES ('2018-04-07 01:51:10', 'Mercantil', NULL, 'Pago realizado', 'EF', '3516.83');
	INSERT INTO FormaPagoC (Fecha, Banco, Nref, Descripcion, Tipo, Monto) VALUES ('2018-04-08 04:13:46', 'BANESCO', 532214415, 'Pago realizado', 'CH', '7510.77');
	INSERT INTO FormaPagoC (Fecha, Banco, Nref, Descripcion, Tipo, Monto) VALUES ('2018-04-09 01:20:46', 'Mercantil', 668622875, '', 'TD', '58575.43');
	INSERT INTO FormaPagoC (Fecha, Banco, Nref, Descripcion, Tipo, Monto) VALUES ('2018-04-09 05:12:24', 'Nacional de Crédito', 358403213, 'Pago realizado', 'CH', '26196.48');
	INSERT INTO FormaPagoC (Fecha, Banco, Nref, Descripcion, Tipo, Monto) VALUES ('2018-04-09 22:10:08', 'Mercantil', NULL, 'Pago realizado', 'EF', '99565.55');
	INSERT INTO FormaPagoC (Fecha, Banco, Nref, Descripcion, Tipo, Monto) VALUES ('2018-04-09 22:17:35', 'Nacional de Crédito', NULL, 'Pago efectuado', 'EF', '3955.04');
	INSERT INTO FormaPagoC (Fecha, Banco, Nref, Descripcion, Tipo, Monto) VALUES ('2018-04-10 10:12:58', 'Venezuela', 957411869, 'Pago efectuado', 'TD', '92296.64');
	INSERT INTO FormaPagoC (Fecha, Banco, Nref, Descripcion, Tipo, Monto) VALUES ('2018-04-10 17:07:57', 'Mercantil', 495276623, 'Pago realizado', 'TRF', '98927.41');
	INSERT INTO FormaPagoC (Fecha, Banco, Nref, Descripcion, Tipo, Monto) VALUES ('2018-04-11 00:18:33', 'Bicentenario', 716250505, 'Pago efectuado', 'TC', '10719.58');
	INSERT INTO FormaPagoC (Fecha, Banco, Nref, Descripcion, Tipo, Monto) VALUES ('2018-04-11 02:44:12', 'Venezuela', 666238149, 'Pago realizado', 'CH', '47897.32');
	INSERT INTO FormaPagoC (Fecha, Banco, Nref, Descripcion, Tipo, Monto) VALUES ('2018-04-11 22:29:10', 'Nacional de Crédito', NULL, 'Pago efectuado', 'EF', '24874.87');
	INSERT INTO FormaPagoC (Fecha, Banco, Nref, Descripcion, Tipo, Monto) VALUES ('2018-04-14 07:50:53', 'Nacional de Crédito', 852939078, 'Pago realizado', 'TC', '64918.95');
	INSERT INTO FormaPagoC (Fecha, Banco, Nref, Descripcion, Tipo, Monto) VALUES ('2018-04-15 04:16:00', 'Nacional de Crédito', NULL, 'Pago efectuado', 'EF', '52425.36');
	INSERT INTO FormaPagoC (Fecha, Banco, Nref, Descripcion, Tipo, Monto) VALUES ('2018-04-15 10:36:43', 'BANESCO', 892572023, 'Pago efectuado', 'TC', '43915.02');

UNLOCK TABLES;

--
-- DUMP TABLE CuentaPagar_FormaPagoC
--

LOCK TABLES CuentaPagar_FormaPagoC WRITE;

	INSERT INTO CuentaPagar_FormaPagoC (Num_Cuenta, RIF_Proveed, Fecha_Pago) VALUES ('23571', '2607662921', '2018-04-02 02:41:31');
	INSERT INTO CuentaPagar_FormaPagoC (Num_Cuenta, RIF_Proveed, Fecha_Pago) VALUES ('23571', '3306600502', '2018-04-14 07:50:53');
	INSERT INTO CuentaPagar_FormaPagoC (Num_Cuenta, RIF_Proveed, Fecha_Pago) VALUES ('37248', '4229885128', '2018-04-10 10:12:58');
	INSERT INTO CuentaPagar_FormaPagoC (Num_Cuenta, RIF_Proveed, Fecha_Pago) VALUES ('37422', '3306600502', '2018-04-09 22:17:35');
	INSERT INTO CuentaPagar_FormaPagoC (Num_Cuenta, RIF_Proveed, Fecha_Pago) VALUES ('37422', '6409739906', '2018-04-02 02:41:31');
	INSERT INTO CuentaPagar_FormaPagoC (Num_Cuenta, RIF_Proveed, Fecha_Pago) VALUES ('49584', '4229885128', '2018-04-09 22:17:35');
	INSERT INTO CuentaPagar_FormaPagoC (Num_Cuenta, RIF_Proveed, Fecha_Pago) VALUES ('55132', '7027146087', '2018-04-15 04:16:00');
	INSERT INTO CuentaPagar_FormaPagoC (Num_Cuenta, RIF_Proveed, Fecha_Pago) VALUES ('5773', '4229885128', '2018-04-02 00:16:23');
	INSERT INTO CuentaPagar_FormaPagoC (Num_Cuenta, RIF_Proveed, Fecha_Pago) VALUES ('62890', '2607662921', '2018-04-02 02:41:31');
	INSERT INTO CuentaPagar_FormaPagoC (Num_Cuenta, RIF_Proveed, Fecha_Pago) VALUES ('71223', '2607662921', '2018-04-09 22:17:35');
	INSERT INTO CuentaPagar_FormaPagoC (Num_Cuenta, RIF_Proveed, Fecha_Pago) VALUES ('71223', '6409739906', '2018-04-09 01:20:46');
	INSERT INTO CuentaPagar_FormaPagoC (Num_Cuenta, RIF_Proveed, Fecha_Pago) VALUES ('71352', '2607662921', '2018-04-02 02:41:31');
	INSERT INTO CuentaPagar_FormaPagoC (Num_Cuenta, RIF_Proveed, Fecha_Pago) VALUES ('71352', '3766016693', '2018-04-09 22:17:35');
	INSERT INTO CuentaPagar_FormaPagoC (Num_Cuenta, RIF_Proveed, Fecha_Pago) VALUES ('71352', '4874382655', '2018-04-04 15:50:07');
	INSERT INTO CuentaPagar_FormaPagoC (Num_Cuenta, RIF_Proveed, Fecha_Pago) VALUES ('73068', '3306600502', '2018-04-10 17:07:57');
	INSERT INTO CuentaPagar_FormaPagoC (Num_Cuenta, RIF_Proveed, Fecha_Pago) VALUES ('73068', '8390533217', '2018-04-02 00:16:23');
	INSERT INTO CuentaPagar_FormaPagoC (Num_Cuenta, RIF_Proveed, Fecha_Pago) VALUES ('73068', '9339956821', '2018-04-10 10:12:58');
	INSERT INTO CuentaPagar_FormaPagoC (Num_Cuenta, RIF_Proveed, Fecha_Pago) VALUES ('80197', '3306600502', '2018-04-02 02:41:31');
	INSERT INTO CuentaPagar_FormaPagoC (Num_Cuenta, RIF_Proveed, Fecha_Pago) VALUES ('80197', '5477514320', '2018-04-05 15:41:24');
	INSERT INTO CuentaPagar_FormaPagoC (Num_Cuenta, RIF_Proveed, Fecha_Pago) VALUES ('90523', '2736012909', '2018-04-11 02:44:12');

UNLOCK TABLES;

--
-- DUMP TABLE FacturaCompra_FormaPagoC
--

LOCK TABLES FacturaCompra_FormaPagoC WRITE;

	INSERT INTO FacturaCompra_FormaPagoC (Num_Factura, RIF_Proveed, Fecha_Pago) VALUES ('14534', '3680972675', '2018-04-02 02:41:31');
	INSERT INTO FacturaCompra_FormaPagoC (Num_Factura, RIF_Proveed, Fecha_Pago) VALUES ('19074', '2312558255', '2018-04-10 17:07:57');
	INSERT INTO FacturaCompra_FormaPagoC (Num_Factura, RIF_Proveed, Fecha_Pago) VALUES ('19074', '6852379868', '2018-04-02 00:16:23');
	INSERT INTO FacturaCompra_FormaPagoC (Num_Factura, RIF_Proveed, Fecha_Pago) VALUES ('19074', '7951989013', '2018-04-09 22:17:35');
	INSERT INTO FacturaCompra_FormaPagoC (Num_Factura, RIF_Proveed, Fecha_Pago) VALUES ('19141', '7951989013', '2018-04-11 02:44:12');
	INSERT INTO FacturaCompra_FormaPagoC (Num_Factura, RIF_Proveed, Fecha_Pago) VALUES ('21358', '6503882827', '2018-04-10 10:12:58');
	INSERT INTO FacturaCompra_FormaPagoC (Num_Factura, RIF_Proveed, Fecha_Pago) VALUES ('26685', '6852379868', '2018-04-05 15:41:24');
	INSERT INTO FacturaCompra_FormaPagoC (Num_Factura, RIF_Proveed, Fecha_Pago) VALUES ('52299', '2312558255', '2018-04-15 04:16:00');
	INSERT INTO FacturaCompra_FormaPagoC (Num_Factura, RIF_Proveed, Fecha_Pago) VALUES ('57636', '7468380684', '2018-04-11 22:29:10');
	INSERT INTO FacturaCompra_FormaPagoC (Num_Factura, RIF_Proveed, Fecha_Pago) VALUES ('63958', '3680972675', '2018-04-02 02:41:31');
	INSERT INTO FacturaCompra_FormaPagoC (Num_Factura, RIF_Proveed, Fecha_Pago) VALUES ('63958', '3766016693', '2018-04-02 00:16:23');
	INSERT INTO FacturaCompra_FormaPagoC (Num_Factura, RIF_Proveed, Fecha_Pago) VALUES ('83441', '3766016693', '2018-04-08 04:13:46');
	INSERT INTO FacturaCompra_FormaPagoC (Num_Factura, RIF_Proveed, Fecha_Pago) VALUES ('87903', '3680972675', '2018-04-10 10:12:58');
	INSERT INTO FacturaCompra_FormaPagoC (Num_Factura, RIF_Proveed, Fecha_Pago) VALUES ('90129', '6546809290', '2018-04-02 02:41:31');
	INSERT INTO FacturaCompra_FormaPagoC (Num_Factura, RIF_Proveed, Fecha_Pago) VALUES ('90836', '5587593285', '2018-04-09 05:12:24');
	INSERT INTO FacturaCompra_FormaPagoC (Num_Factura, RIF_Proveed, Fecha_Pago) VALUES ('91334', '7468380684', '2018-04-02 02:41:31');
	INSERT INTO FacturaCompra_FormaPagoC (Num_Factura, RIF_Proveed, Fecha_Pago) VALUES ('91334', '7622061974', '2018-04-10 10:12:58');
	INSERT INTO FacturaCompra_FormaPagoC (Num_Factura, RIF_Proveed, Fecha_Pago) VALUES ('91873', '5587593285', '2018-04-02 00:16:23');
	INSERT INTO FacturaCompra_FormaPagoC (Num_Factura, RIF_Proveed, Fecha_Pago) VALUES ('91873', '6503882827', '2018-04-09 05:12:24');
	INSERT INTO FacturaCompra_FormaPagoC (Num_Factura, RIF_Proveed, Fecha_Pago) VALUES ('99296', '5587593285', '2018-04-04 20:43:50');

UNLOCK TABLES;

--
-- DUMP TABLE Cuenta_FormaPagoC
--

LOCK TABLES Cuenta_FormaPagoC WRITE;

	INSERT INTO Cuenta_FormaPagoC (Num_Cuenta, Fecha_Pago) VALUES ('42395432491923931000', '2018-04-02 00:16:23');
	INSERT INTO Cuenta_FormaPagoC (Num_Cuenta, Fecha_Pago) VALUES ('42395432491923931000', '2018-04-04 15:50:07');
	INSERT INTO Cuenta_FormaPagoC (Num_Cuenta, Fecha_Pago) VALUES ('42395432491923931000', '2018-04-07 01:51:10');
	INSERT INTO Cuenta_FormaPagoC (Num_Cuenta, Fecha_Pago) VALUES ('42395432491923931000', '2018-04-08 04:13:46');
	INSERT INTO Cuenta_FormaPagoC (Num_Cuenta, Fecha_Pago) VALUES ('42395432491923931000', '2018-04-09 22:17:35');
	INSERT INTO Cuenta_FormaPagoC (Num_Cuenta, Fecha_Pago) VALUES ('42395432491923931000', '2018-04-10 17:07:57');
	INSERT INTO Cuenta_FormaPagoC (Num_Cuenta, Fecha_Pago) VALUES ('42395432491923931000', '2018-04-11 00:18:33');
	INSERT INTO Cuenta_FormaPagoC (Num_Cuenta, Fecha_Pago) VALUES ('42395432491923931000', '2018-04-11 02:44:12');
	INSERT INTO Cuenta_FormaPagoC (Num_Cuenta, Fecha_Pago) VALUES ('4239543749192393100', '2018-04-02 02:41:31');
	INSERT INTO Cuenta_FormaPagoC (Num_Cuenta, Fecha_Pago) VALUES ('4239543749192393100', '2018-04-09 22:17:35');
	INSERT INTO Cuenta_FormaPagoC (Num_Cuenta, Fecha_Pago) VALUES ('4239543749192393100', '2018-04-11 02:44:12');
	INSERT INTO Cuenta_FormaPagoC (Num_Cuenta, Fecha_Pago) VALUES ('4239543749192393100', '2018-04-14 07:50:53');
	INSERT INTO Cuenta_FormaPagoC (Num_Cuenta, Fecha_Pago) VALUES ('9239543649192293100', '2018-04-04 20:43:50');
	INSERT INTO Cuenta_FormaPagoC (Num_Cuenta, Fecha_Pago) VALUES ('9239543649192293100', '2018-04-06 05:23:26');
	INSERT INTO Cuenta_FormaPagoC (Num_Cuenta, Fecha_Pago) VALUES ('9239543649192293100', '2018-04-09 05:12:24');
	INSERT INTO Cuenta_FormaPagoC (Num_Cuenta, Fecha_Pago) VALUES ('9239543649192293100', '2018-04-09 22:10:08');
	INSERT INTO Cuenta_FormaPagoC (Num_Cuenta, Fecha_Pago) VALUES ('9239543649192293100', '2018-04-09 22:17:35');
	INSERT INTO Cuenta_FormaPagoC (Num_Cuenta, Fecha_Pago) VALUES ('9239543649192293100', '2018-04-11 02:44:12');
	INSERT INTO Cuenta_FormaPagoC (Num_Cuenta, Fecha_Pago) VALUES ('9239543649192293100', '2018-04-11 22:29:10');

UNLOCK TABLES;

--
-- DUMP TABLE Orden_FacturaVenta
--

LOCK TABLES Orden_FacturaVenta WRITE;

	INSERT INTO Orden_FacturaVenta (Num_Orden, Num_Factura) VALUES ('15703', '95910');
	INSERT INTO Orden_FacturaVenta (Num_Orden, Num_Factura) VALUES ('25755', '35514');
	INSERT INTO Orden_FacturaVenta (Num_Orden, Num_Factura) VALUES ('25755', '62343');
	INSERT INTO Orden_FacturaVenta (Num_Orden, Num_Factura) VALUES ('33459', '74290');
	INSERT INTO Orden_FacturaVenta (Num_Orden, Num_Factura) VALUES ('37608', '96013');
	INSERT INTO Orden_FacturaVenta (Num_Orden, Num_Factura) VALUES ('41382', '55801');
	INSERT INTO Orden_FacturaVenta (Num_Orden, Num_Factura) VALUES ('4553', '43697');
	INSERT INTO Orden_FacturaVenta (Num_Orden, Num_Factura) VALUES ('53564', '76705');
	INSERT INTO Orden_FacturaVenta (Num_Orden, Num_Factura) VALUES ('55269', '75458');
	INSERT INTO Orden_FacturaVenta (Num_Orden, Num_Factura) VALUES ('63474', '63893');
	INSERT INTO Orden_FacturaVenta (Num_Orden, Num_Factura) VALUES ('64500', '7649');
	INSERT INTO Orden_FacturaVenta (Num_Orden, Num_Factura) VALUES ('684', '64006');
	INSERT INTO Orden_FacturaVenta (Num_Orden, Num_Factura) VALUES ('70509', '19310');
	INSERT INTO Orden_FacturaVenta (Num_Orden, Num_Factura) VALUES ('70509', '23174');
	INSERT INTO Orden_FacturaVenta (Num_Orden, Num_Factura) VALUES ('87838', '91472');
	INSERT INTO Orden_FacturaVenta (Num_Orden, Num_Factura) VALUES ('9105', '63893');
	INSERT INTO Orden_FacturaVenta (Num_Orden, Num_Factura) VALUES ('9144', '23604');
	INSERT INTO Orden_FacturaVenta (Num_Orden, Num_Factura) VALUES ('93269', '65998');
	INSERT INTO Orden_FacturaVenta (Num_Orden, Num_Factura) VALUES ('96982', '43697');

UNLOCK TABLES;

--
-- DUMP TABLE Orden_Equipo
--

LOCK TABLES Orden_Equipo WRITE;

	INSERT INTO Orden_Equipo (Num_Orden, Serial_Equipo, Falla_Equipo) VALUES ('15703', '14908596640', 'Falla interna del equipo');
	INSERT INTO Orden_Equipo (Num_Orden, Serial_Equipo, Falla_Equipo) VALUES ('2117', '65524353412', 'Error de software');
	INSERT INTO Orden_Equipo (Num_Orden, Serial_Equipo, Falla_Equipo) VALUES ('25755', '99389043413', 'Error de hardware');
	INSERT INTO Orden_Equipo (Num_Orden, Serial_Equipo, Falla_Equipo) VALUES ('30263', '67284067641', 'Error de software');
	INSERT INTO Orden_Equipo (Num_Orden, Serial_Equipo, Falla_Equipo) VALUES ('30263', '72540120738', 'Error de hardware');
	INSERT INTO Orden_Equipo (Num_Orden, Serial_Equipo, Falla_Equipo) VALUES ('33459', '70951436101', 'Error de software');
	INSERT INTO Orden_Equipo (Num_Orden, Serial_Equipo, Falla_Equipo) VALUES ('37608', '87356153037', 'Error de hardware/software');
	INSERT INTO Orden_Equipo (Num_Orden, Serial_Equipo, Falla_Equipo) VALUES ('3983', '44264630707', 'Falla interna del equipo');
	INSERT INTO Orden_Equipo (Num_Orden, Serial_Equipo, Falla_Equipo) VALUES ('41086', '59377323873', 'Falla interna del equipo');
	INSERT INTO Orden_Equipo (Num_Orden, Serial_Equipo, Falla_Equipo) VALUES ('41382', '92720296224', 'Error de hardware');
	INSERT INTO Orden_Equipo (Num_Orden, Serial_Equipo, Falla_Equipo) VALUES ('50991', '78513924377', 'Falla interna del equipo');
	INSERT INTO Orden_Equipo (Num_Orden, Serial_Equipo, Falla_Equipo) VALUES ('55269', '19545683613', 'Error de hardware/software');
	INSERT INTO Orden_Equipo (Num_Orden, Serial_Equipo, Falla_Equipo) VALUES ('55269', '59377323873', 'Error de software');
	INSERT INTO Orden_Equipo (Num_Orden, Serial_Equipo, Falla_Equipo) VALUES ('55269', '92220518081', 'Falla interna del equipo');
	INSERT INTO Orden_Equipo (Num_Orden, Serial_Equipo, Falla_Equipo) VALUES ('55807', '60009572757', 'Error de hardware/software');
	INSERT INTO Orden_Equipo (Num_Orden, Serial_Equipo, Falla_Equipo) VALUES ('56739', '30155539647', 'Error de software');
	INSERT INTO Orden_Equipo (Num_Orden, Serial_Equipo, Falla_Equipo) VALUES ('63474', '92220518081', 'Falla interna del equipo');
	INSERT INTO Orden_Equipo (Num_Orden, Serial_Equipo, Falla_Equipo) VALUES ('63485', '92720296224', 'Error de hardware');
	INSERT INTO Orden_Equipo (Num_Orden, Serial_Equipo, Falla_Equipo) VALUES ('64158', '92720296224', 'Falla interna del equipo');
	INSERT INTO Orden_Equipo (Num_Orden, Serial_Equipo, Falla_Equipo) VALUES ('684', '85876605515', 'Error de software');
	INSERT INTO Orden_Equipo (Num_Orden, Serial_Equipo, Falla_Equipo) VALUES ('70509', '55912880911', 'Error de hardware/software');
	INSERT INTO Orden_Equipo (Num_Orden, Serial_Equipo, Falla_Equipo) VALUES ('70509', '70951436101', 'Error de hardware/software');
	INSERT INTO Orden_Equipo (Num_Orden, Serial_Equipo, Falla_Equipo) VALUES ('70509', '95214270325', 'Falta de repuestos para funcionamiento del equipo');
	INSERT INTO Orden_Equipo (Num_Orden, Serial_Equipo, Falla_Equipo) VALUES ('7718', '36715684193', 'Error de hardware/software');
	INSERT INTO Orden_Equipo (Num_Orden, Serial_Equipo, Falla_Equipo) VALUES ('77257', '29298720215', 'Falla interna del equipo');
	INSERT INTO Orden_Equipo (Num_Orden, Serial_Equipo, Falla_Equipo) VALUES ('77257', '77115054475', 'Falla interna del equipo');
	INSERT INTO Orden_Equipo (Num_Orden, Serial_Equipo, Falla_Equipo) VALUES ('77257', '99420089940', 'Error de hardware/software');
	INSERT INTO Orden_Equipo (Num_Orden, Serial_Equipo, Falla_Equipo) VALUES ('7888', '59377323873', 'Falta de repuestos para funcionamiento del equipo');
	INSERT INTO Orden_Equipo (Num_Orden, Serial_Equipo, Falla_Equipo) VALUES ('7888', '98575835945', 'Falta de repuestos para funcionamiento del equipo');
	INSERT INTO Orden_Equipo (Num_Orden, Serial_Equipo, Falla_Equipo) VALUES ('80644', '88274474851', 'Error de hardware/software');
	INSERT INTO Orden_Equipo (Num_Orden, Serial_Equipo, Falla_Equipo) VALUES ('80644', '98575835945', 'Error de hardware');
	INSERT INTO Orden_Equipo (Num_Orden, Serial_Equipo, Falla_Equipo) VALUES ('81136', '98575835945', 'Error de hardware/software');
	INSERT INTO Orden_Equipo (Num_Orden, Serial_Equipo, Falla_Equipo) VALUES ('83998', '65524353412', 'Error de hardware');
	INSERT INTO Orden_Equipo (Num_Orden, Serial_Equipo, Falla_Equipo) VALUES ('83998', '81087981192', 'Error de hardware');
	INSERT INTO Orden_Equipo (Num_Orden, Serial_Equipo, Falla_Equipo) VALUES ('85009', '94248035834', 'Falta de repuestos para funcionamiento del equipo');
	INSERT INTO Orden_Equipo (Num_Orden, Serial_Equipo, Falla_Equipo) VALUES ('85427', '26811031578', 'Error de hardware/software');
	INSERT INTO Orden_Equipo (Num_Orden, Serial_Equipo, Falla_Equipo) VALUES ('85427', '94726056023', 'Falla interna del equipo');
	INSERT INTO Orden_Equipo (Num_Orden, Serial_Equipo, Falla_Equipo) VALUES ('87838', '30155539647', 'Error de hardware');
	INSERT INTO Orden_Equipo (Num_Orden, Serial_Equipo, Falla_Equipo) VALUES ('89768', '67374456156', 'Error de hardware/software');

UNLOCK TABLES;

--
-- DUMP TABLE FacturaVenta_EquipoVenta
--

LOCK TABLES FacturaVenta_EquipoVenta WRITE;

	INSERT INTO FacturaVenta_EquipoVenta (Num_Factura, Codigo_Equipo, Cantidad, PVP) VALUES ('13239', '82568', 77, '3446964.41');
	INSERT INTO FacturaVenta_EquipoVenta (Num_Factura, Codigo_Equipo, Cantidad, PVP) VALUES ('19310', '81495', 5, '8761454.31');
	INSERT INTO FacturaVenta_EquipoVenta (Num_Factura, Codigo_Equipo, Cantidad, PVP) VALUES ('19310', '83611', 87, '4056972.26');
	INSERT INTO FacturaVenta_EquipoVenta (Num_Factura, Codigo_Equipo, Cantidad, PVP) VALUES ('23174', '86136', 13, '5203974.78');
	INSERT INTO FacturaVenta_EquipoVenta (Num_Factura, Codigo_Equipo, Cantidad, PVP) VALUES ('23604', '86136', 62, '9244058.38');
	INSERT INTO FacturaVenta_EquipoVenta (Num_Factura, Codigo_Equipo, Cantidad, PVP) VALUES ('2797', '52914', 83, '2899570.47');
	INSERT INTO FacturaVenta_EquipoVenta (Num_Factura, Codigo_Equipo, Cantidad, PVP) VALUES ('2797', '5497', 23, '5599719.46');
	INSERT INTO FacturaVenta_EquipoVenta (Num_Factura, Codigo_Equipo, Cantidad, PVP) VALUES ('34262', '52914', 27, '3951011.29');
	INSERT INTO FacturaVenta_EquipoVenta (Num_Factura, Codigo_Equipo, Cantidad, PVP) VALUES ('35514', '82820', 91, '3581511.28');
	INSERT INTO FacturaVenta_EquipoVenta (Num_Factura, Codigo_Equipo, Cantidad, PVP) VALUES ('36004', '83611', 14, '3798902.22');
	INSERT INTO FacturaVenta_EquipoVenta (Num_Factura, Codigo_Equipo, Cantidad, PVP) VALUES ('36346', '59291', 7, '6511941.01');
	INSERT INTO FacturaVenta_EquipoVenta (Num_Factura, Codigo_Equipo, Cantidad, PVP) VALUES ('36346', '83060', 98, '6919544.39');
	INSERT INTO FacturaVenta_EquipoVenta (Num_Factura, Codigo_Equipo, Cantidad, PVP) VALUES ('36346', '83364', 77, '6193252.29');
	INSERT INTO FacturaVenta_EquipoVenta (Num_Factura, Codigo_Equipo, Cantidad, PVP) VALUES ('36346', '86136', 20, '4946421.46');
	INSERT INTO FacturaVenta_EquipoVenta (Num_Factura, Codigo_Equipo, Cantidad, PVP) VALUES ('39089', '82568', 47, '3548865.04');
	INSERT INTO FacturaVenta_EquipoVenta (Num_Factura, Codigo_Equipo, Cantidad, PVP) VALUES ('4386', '73107', 38, '9491975.81');
	INSERT INTO FacturaVenta_EquipoVenta (Num_Factura, Codigo_Equipo, Cantidad, PVP) VALUES ('44779', '82820', 86, '2128803.76');
	INSERT INTO FacturaVenta_EquipoVenta (Num_Factura, Codigo_Equipo, Cantidad, PVP) VALUES ('44779', '91201', 67, '8695505.53');
	INSERT INTO FacturaVenta_EquipoVenta (Num_Factura, Codigo_Equipo, Cantidad, PVP) VALUES ('47167', '5497', 32, '2871014.14');
	INSERT INTO FacturaVenta_EquipoVenta (Num_Factura, Codigo_Equipo, Cantidad, PVP) VALUES ('49040', '71498', 99, '3076334.44');
	INSERT INTO FacturaVenta_EquipoVenta (Num_Factura, Codigo_Equipo, Cantidad, PVP) VALUES ('49040', '8888', 27, '5567720.77');
	INSERT INTO FacturaVenta_EquipoVenta (Num_Factura, Codigo_Equipo, Cantidad, PVP) VALUES ('52647', '89009', 80, '6365732.19');
	INSERT INTO FacturaVenta_EquipoVenta (Num_Factura, Codigo_Equipo, Cantidad, PVP) VALUES ('55801', '75408', 1, '2854638.31');
	INSERT INTO FacturaVenta_EquipoVenta (Num_Factura, Codigo_Equipo, Cantidad, PVP) VALUES ('55801', '83611', 28, '4741621.15');
	INSERT INTO FacturaVenta_EquipoVenta (Num_Factura, Codigo_Equipo, Cantidad, PVP) VALUES ('55801', '86136', 33, '76439.50');
	INSERT INTO FacturaVenta_EquipoVenta (Num_Factura, Codigo_Equipo, Cantidad, PVP) VALUES ('56472', '52914', 90, '4173518.40');
	INSERT INTO FacturaVenta_EquipoVenta (Num_Factura, Codigo_Equipo, Cantidad, PVP) VALUES ('60726', '73107', 94, '681009.71');
	INSERT INTO FacturaVenta_EquipoVenta (Num_Factura, Codigo_Equipo, Cantidad, PVP) VALUES ('64515', '73107', 23, '7521118.47');
	INSERT INTO FacturaVenta_EquipoVenta (Num_Factura, Codigo_Equipo, Cantidad, PVP) VALUES ('64788', '59291', 28, '2414681.30');
	INSERT INTO FacturaVenta_EquipoVenta (Num_Factura, Codigo_Equipo, Cantidad, PVP) VALUES ('65998', '5497', 81, '7968973.13');
	INSERT INTO FacturaVenta_EquipoVenta (Num_Factura, Codigo_Equipo, Cantidad, PVP) VALUES ('65998', '82568', 91, '7520928.20');
	INSERT INTO FacturaVenta_EquipoVenta (Num_Factura, Codigo_Equipo, Cantidad, PVP) VALUES ('65998', '83593', 7, '1524637.72');
	INSERT INTO FacturaVenta_EquipoVenta (Num_Factura, Codigo_Equipo, Cantidad, PVP) VALUES ('68784', '83364', 62, '4156943.17');
	INSERT INTO FacturaVenta_EquipoVenta (Num_Factura, Codigo_Equipo, Cantidad, PVP) VALUES ('71133', '62596', 54, '687937.33');
	INSERT INTO FacturaVenta_EquipoVenta (Num_Factura, Codigo_Equipo, Cantidad, PVP) VALUES ('75458', '73107', 49, '5572978.88');
	INSERT INTO FacturaVenta_EquipoVenta (Num_Factura, Codigo_Equipo, Cantidad, PVP) VALUES ('7649', '82820', 67, '310817.15');
	INSERT INTO FacturaVenta_EquipoVenta (Num_Factura, Codigo_Equipo, Cantidad, PVP) VALUES ('8273', '81495', 18, '6843092.30');
	INSERT INTO FacturaVenta_EquipoVenta (Num_Factura, Codigo_Equipo, Cantidad, PVP) VALUES ('8903', '91201', 27, '2081481.49');
	INSERT INTO FacturaVenta_EquipoVenta (Num_Factura, Codigo_Equipo, Cantidad, PVP) VALUES ('91472', '8888', 85, '8793395.81');
	INSERT INTO FacturaVenta_EquipoVenta (Num_Factura, Codigo_Equipo, Cantidad, PVP) VALUES ('98687', '59291', 63, '1910392.30');

UNLOCK TABLES;

--
-- DUMP TABLE VentaWeb_EquipoVenta
--

LOCK TABLES VentaWeb_EquipoVenta WRITE;

	INSERT INTO VentaWeb_EquipoVenta (Num_Venta, Codigo_Equipo, Cantidad, Precio) VALUES ('14445', '5728', 38, '3408566.13');
	INSERT INTO VentaWeb_EquipoVenta (Num_Venta, Codigo_Equipo, Cantidad, Precio) VALUES ('20953', '83593', 10, '7764120.71');
	INSERT INTO VentaWeb_EquipoVenta (Num_Venta, Codigo_Equipo, Cantidad, Precio) VALUES ('21835', '83593', 89, '1621531.47');
	INSERT INTO VentaWeb_EquipoVenta (Num_Venta, Codigo_Equipo, Cantidad, Precio) VALUES ('23351', '82820', 6, '3748179.15');
	INSERT INTO VentaWeb_EquipoVenta (Num_Venta, Codigo_Equipo, Cantidad, Precio) VALUES ('31476', '82820', 83, '9738298.17');
	INSERT INTO VentaWeb_EquipoVenta (Num_Venta, Codigo_Equipo, Cantidad, Precio) VALUES ('32288', '52914', 2, '8352127.77');
	INSERT INTO VentaWeb_EquipoVenta (Num_Venta, Codigo_Equipo, Cantidad, Precio) VALUES ('32288', '71498', 86, '948247.76');
	INSERT INTO VentaWeb_EquipoVenta (Num_Venta, Codigo_Equipo, Cantidad, Precio) VALUES ('32288', '75408', 70, '8060663.27');
	INSERT INTO VentaWeb_EquipoVenta (Num_Venta, Codigo_Equipo, Cantidad, Precio) VALUES ('32288', '83611', 6, '9827526.45');
	INSERT INTO VentaWeb_EquipoVenta (Num_Venta, Codigo_Equipo, Cantidad, Precio) VALUES ('45650', '71498', 27, '3548026.50');
	INSERT INTO VentaWeb_EquipoVenta (Num_Venta, Codigo_Equipo, Cantidad, Precio) VALUES ('45650', '71501', 95, '6602171.49');
	INSERT INTO VentaWeb_EquipoVenta (Num_Venta, Codigo_Equipo, Cantidad, Precio) VALUES ('47959', '62596', 92, '9419919.62');
	INSERT INTO VentaWeb_EquipoVenta (Num_Venta, Codigo_Equipo, Cantidad, Precio) VALUES ('48433', '81495', 64, '4730375.92');
	INSERT INTO VentaWeb_EquipoVenta (Num_Venta, Codigo_Equipo, Cantidad, Precio) VALUES ('48946', '75408', 42, '1107357.15');
	INSERT INTO VentaWeb_EquipoVenta (Num_Venta, Codigo_Equipo, Cantidad, Precio) VALUES ('48946', '83060', 24, '8339322.87');
	INSERT INTO VentaWeb_EquipoVenta (Num_Venta, Codigo_Equipo, Cantidad, Precio) VALUES ('48946', '86136', 51, '811265.05');
	INSERT INTO VentaWeb_EquipoVenta (Num_Venta, Codigo_Equipo, Cantidad, Precio) VALUES ('51080', '75408', 14, '5746366.60');
	INSERT INTO VentaWeb_EquipoVenta (Num_Venta, Codigo_Equipo, Cantidad, Precio) VALUES ('51080', '83364', 26, '9645757.51');
	INSERT INTO VentaWeb_EquipoVenta (Num_Venta, Codigo_Equipo, Cantidad, Precio) VALUES ('59505', '62596', 86, '3145337.24');
	INSERT INTO VentaWeb_EquipoVenta (Num_Venta, Codigo_Equipo, Cantidad, Precio) VALUES ('61481', '62596', 57, '6485315.07');
	INSERT INTO VentaWeb_EquipoVenta (Num_Venta, Codigo_Equipo, Cantidad, Precio) VALUES ('65522', '73107', 4, '1124858.41');
	INSERT INTO VentaWeb_EquipoVenta (Num_Venta, Codigo_Equipo, Cantidad, Precio) VALUES ('65522', '81495', 9, '4333901.82');
	INSERT INTO VentaWeb_EquipoVenta (Num_Venta, Codigo_Equipo, Cantidad, Precio) VALUES ('65522', '83060', 62, '9832086.90');
	INSERT INTO VentaWeb_EquipoVenta (Num_Venta, Codigo_Equipo, Cantidad, Precio) VALUES ('68351', '73107', 87, '2192656.48');
	INSERT INTO VentaWeb_EquipoVenta (Num_Venta, Codigo_Equipo, Cantidad, Precio) VALUES ('7269', '71501', 84, '6692568.47');
	INSERT INTO VentaWeb_EquipoVenta (Num_Venta, Codigo_Equipo, Cantidad, Precio) VALUES ('74230', '83611', 18, '9119097.52');
	INSERT INTO VentaWeb_EquipoVenta (Num_Venta, Codigo_Equipo, Cantidad, Precio) VALUES ('74784', '73107', 33, '2169713.86');
	INSERT INTO VentaWeb_EquipoVenta (Num_Venta, Codigo_Equipo, Cantidad, Precio) VALUES ('77786', '71501', 70, '3964386.18');
	INSERT INTO VentaWeb_EquipoVenta (Num_Venta, Codigo_Equipo, Cantidad, Precio) VALUES ('77786', '82820', 13, '8352515.51');
	INSERT INTO VentaWeb_EquipoVenta (Num_Venta, Codigo_Equipo, Cantidad, Precio) VALUES ('78930', '71498', 11, '5574619.44');
	INSERT INTO VentaWeb_EquipoVenta (Num_Venta, Codigo_Equipo, Cantidad, Precio) VALUES ('80056', '62596', 58, '5889307.83');
	INSERT INTO VentaWeb_EquipoVenta (Num_Venta, Codigo_Equipo, Cantidad, Precio) VALUES ('80866', '52914', 7, '829149.88');
	INSERT INTO VentaWeb_EquipoVenta (Num_Venta, Codigo_Equipo, Cantidad, Precio) VALUES ('8187', '82820', 68, '1779837.15');
	INSERT INTO VentaWeb_EquipoVenta (Num_Venta, Codigo_Equipo, Cantidad, Precio) VALUES ('83479', '73107', 55, '722441.52');
	INSERT INTO VentaWeb_EquipoVenta (Num_Venta, Codigo_Equipo, Cantidad, Precio) VALUES ('84595', '81495', 22, '1178260.95');
	INSERT INTO VentaWeb_EquipoVenta (Num_Venta, Codigo_Equipo, Cantidad, Precio) VALUES ('86136', '71501', 99, '8568854.27');
	INSERT INTO VentaWeb_EquipoVenta (Num_Venta, Codigo_Equipo, Cantidad, Precio) VALUES ('87052', '62596', 69, '9198097.33');
	INSERT INTO VentaWeb_EquipoVenta (Num_Venta, Codigo_Equipo, Cantidad, Precio) VALUES ('96274', '83060', 13, '1987043.99');
	INSERT INTO VentaWeb_EquipoVenta (Num_Venta, Codigo_Equipo, Cantidad, Precio) VALUES ('98138', '81495', 50, '9016372.02');

UNLOCK TABLES;

--
-- DUMP TABLE FacturaCompra_Inventario
--

LOCK TABLES FacturaCompra_Inventario WRITE;

	INSERT INTO FacturaCompra_Inventario (Num_Fact, Proveed_Fact, Cod_Inv, Cantidad, Costo) VALUES ('14534', '7122454935', '83593', 84, '8198074.06');
	INSERT INTO FacturaCompra_Inventario (Num_Fact, Proveed_Fact, Cod_Inv, Cantidad, Costo) VALUES ('14534', '7622061974', '59291', 58, '90696.80');
	INSERT INTO FacturaCompra_Inventario (Num_Fact, Proveed_Fact, Cod_Inv, Cantidad, Costo) VALUES ('16168', '6546809290', '71501', 69, '2817283.80');
	INSERT INTO FacturaCompra_Inventario (Num_Fact, Proveed_Fact, Cod_Inv, Cantidad, Costo) VALUES ('19141', '3680972675', '43240', 60, '7919942.26');
	INSERT INTO FacturaCompra_Inventario (Num_Fact, Proveed_Fact, Cod_Inv, Cantidad, Costo) VALUES ('25488', '7725879862', '91839', 49, '1554538.62');
	INSERT INTO FacturaCompra_Inventario (Num_Fact, Proveed_Fact, Cod_Inv, Cantidad, Costo) VALUES ('26685', '7468380684', '75408', 83, '9721977.21');
	INSERT INTO FacturaCompra_Inventario (Num_Fact, Proveed_Fact, Cod_Inv, Cantidad, Costo) VALUES ('4003', '5587593285', '52914', 18, '8751524.90');
	INSERT INTO FacturaCompra_Inventario (Num_Fact, Proveed_Fact, Cod_Inv, Cantidad, Costo) VALUES ('4003', '7122454935', '59291', 68, '4984936.18');
	INSERT INTO FacturaCompra_Inventario (Num_Fact, Proveed_Fact, Cod_Inv, Cantidad, Costo) VALUES ('4003', '7122454935', '81495', 83, '5552052.36');
	INSERT INTO FacturaCompra_Inventario (Num_Fact, Proveed_Fact, Cod_Inv, Cantidad, Costo) VALUES ('46874', '2312558255', '82568', 87, '98657.44');
	INSERT INTO FacturaCompra_Inventario (Num_Fact, Proveed_Fact, Cod_Inv, Cantidad, Costo) VALUES ('46981', '6546809290', '2688', 86, '3430431.12');
	INSERT INTO FacturaCompra_Inventario (Num_Fact, Proveed_Fact, Cod_Inv, Cantidad, Costo) VALUES ('5080', '1541887123', '89009', 49, '347375.01');
	INSERT INTO FacturaCompra_Inventario (Num_Fact, Proveed_Fact, Cod_Inv, Cantidad, Costo) VALUES ('5080', '5477514320', '43240', 67, '921041.18');
	INSERT INTO FacturaCompra_Inventario (Num_Fact, Proveed_Fact, Cod_Inv, Cantidad, Costo) VALUES ('51376', '5587593285', '18269', 52, '594997.69');
	INSERT INTO FacturaCompra_Inventario (Num_Fact, Proveed_Fact, Cod_Inv, Cantidad, Costo) VALUES ('51376', '6339617126', '20151', 20, '8911841.40');
	INSERT INTO FacturaCompra_Inventario (Num_Fact, Proveed_Fact, Cod_Inv, Cantidad, Costo) VALUES ('57636', '5775683946', '91839', 69, '5748154.36');
	INSERT INTO FacturaCompra_Inventario (Num_Fact, Proveed_Fact, Cod_Inv, Cantidad, Costo) VALUES ('62707', '5775683946', '91201', 16, '91580.69');
	INSERT INTO FacturaCompra_Inventario (Num_Fact, Proveed_Fact, Cod_Inv, Cantidad, Costo) VALUES ('62707', '6339617126', '91201', 22, '1715862.40');
	INSERT INTO FacturaCompra_Inventario (Num_Fact, Proveed_Fact, Cod_Inv, Cantidad, Costo) VALUES ('62707', '7951989013', '43240', 50, '7997021.76');
	INSERT INTO FacturaCompra_Inventario (Num_Fact, Proveed_Fact, Cod_Inv, Cantidad, Costo) VALUES ('63958', '7622061974', '18269', 48, '7975966.03');
	INSERT INTO FacturaCompra_Inventario (Num_Fact, Proveed_Fact, Cod_Inv, Cantidad, Costo) VALUES ('63958', '7622061974', '73107', 16, '2617954.98');
	INSERT INTO FacturaCompra_Inventario (Num_Fact, Proveed_Fact, Cod_Inv, Cantidad, Costo) VALUES ('72995', '1541887123', '83060', 70, '7008975.94');
	INSERT INTO FacturaCompra_Inventario (Num_Fact, Proveed_Fact, Cod_Inv, Cantidad, Costo) VALUES ('73349', '7468380684', '2564', 32, '18046.89');
	INSERT INTO FacturaCompra_Inventario (Num_Fact, Proveed_Fact, Cod_Inv, Cantidad, Costo) VALUES ('77111', '9818162875', '4146', 49, '4715638.03');
	INSERT INTO FacturaCompra_Inventario (Num_Fact, Proveed_Fact, Cod_Inv, Cantidad, Costo) VALUES ('83441', '4874382655', '71498', 76, '812789.73');
	INSERT INTO FacturaCompra_Inventario (Num_Fact, Proveed_Fact, Cod_Inv, Cantidad, Costo) VALUES ('87903', '2084380257', '81495', 68, '5263890.29');
	INSERT INTO FacturaCompra_Inventario (Num_Fact, Proveed_Fact, Cod_Inv, Cantidad, Costo) VALUES ('87903', '3680972675', '92712', 69, '8295521.29');
	INSERT INTO FacturaCompra_Inventario (Num_Fact, Proveed_Fact, Cod_Inv, Cantidad, Costo) VALUES ('87903', '6503882827', '89009', 77, '5285399.29');
	INSERT INTO FacturaCompra_Inventario (Num_Fact, Proveed_Fact, Cod_Inv, Cantidad, Costo) VALUES ('87903', '9818162875', '83611', 44, '9744233.21');
	INSERT INTO FacturaCompra_Inventario (Num_Fact, Proveed_Fact, Cod_Inv, Cantidad, Costo) VALUES ('90129', '6852379868', '52914', 26, '7271341.40');
	INSERT INTO FacturaCompra_Inventario (Num_Fact, Proveed_Fact, Cod_Inv, Cantidad, Costo) VALUES ('90288', '7622061974', '52713', 96, '1372587.22');
	INSERT INTO FacturaCompra_Inventario (Num_Fact, Proveed_Fact, Cod_Inv, Cantidad, Costo) VALUES ('90836', '3766016693', '61563', 76, '807600.47');
	INSERT INTO FacturaCompra_Inventario (Num_Fact, Proveed_Fact, Cod_Inv, Cantidad, Costo) VALUES ('91334', '3766016693', '73107', 93, '5207265.25');
	INSERT INTO FacturaCompra_Inventario (Num_Fact, Proveed_Fact, Cod_Inv, Cantidad, Costo) VALUES ('94922', '5587593285', '83593', 31, '6488991.81');
	INSERT INTO FacturaCompra_Inventario (Num_Fact, Proveed_Fact, Cod_Inv, Cantidad, Costo) VALUES ('99652', '5477514320', '48027', 60, '827623.84');

UNLOCK TABLES;

--
-- ROLES STRUCTURE
--

-- drop all roles
DROP ROLE IF EXISTS 'rol_admin';
DROP ROLE IF EXISTS 'rol_asistente';
DROP ROLE IF EXISTS 'rol_tecnico';
DROP ROLE IF EXISTS 'rol_cliente';
DROP ROLE IF EXISTS 'rol_cliente_web';

-- create all roles
CREATE ROLE 'rol_admin';
GRANT ALL PRIVILEGES ON *.* TO 'rol_admin' WITH GRANT OPTION;

CREATE ROLE 'rol_asistente';
GRANT ALL PRIVILEGES ON BlackTechStore.* TO 'rol_asistente' WITH GRANT OPTION;

CREATE ROLE 'rol_tecnico';
GRANT INSERT, UPDATE, SELECT ON BlackTechStore.Reparacion TO 'rol_tecnico';
GRANT INSERT, UPDATE, SELECT ON BlackTechStore.Orden TO 'rol_tecnico';
GRANT UPDATE, SELECT ON BlackTechStore.Personal TO 'rol_tecnico';
GRANT LOCK TABLES ON BlackTechStore.* TO 'rol_tecnico';
GRANT EXECUTE ON PROCEDURE BlackTechStore.Generar_Nueva_Orden_Cliente TO 'rol_tecnico';
GRANT EXECUTE ON PROCEDURE BlackTechStore.Reporte_Diario_Equipos TO 'rol_tecnico';
GRANT EXECUTE ON PROCEDURE BlackTechStore.Equipos_Tecnicos_Diarios TO 'rol_tecnico';
GRANT EXECUTE ON PROCEDURE BlackTechStore.Repuestos_Utilizados TO 'rol_tecnico';
GRANT EXECUTE ON PROCEDURE BlackTechStore.Equipos_Tecnicos_Diarios TO 'rol_tecnico';
GRANT SELECT ON BlackTechStore.Reporte_Diario_Equipos TO 'rol_tecnico';
GRANT SELECT ON BlackTechStore.Equipos_Tecnicos_Diarios TO 'rol_tecnico';
GRANT SELECT ON BlackTechStore.Herramienta TO 'rol_tecnico';
GRANT SELECT ON BlackTechStore.Repuesto TO 'rol_tecnico';
GRANT SELECT ON BlackTechStore.Repuesto_Reparacion TO 'rol_tecnico';
GRANT SELECT ON BlackTechStore.Repuestos_Utilizados TO 'rol_tecnico';
GRANT SELECT ON BlackTechStore.Existencia_Repuestos TO 'rol_tecnico';
GRANT SELECT ON BlackTechStore.Equipos_Tecnicos_Diarios TO 'rol_tecnico';

CREATE ROLE 'rol_cliente';
GRANT UPDATE, SELECT ON BlackTechStore.Cliente TO 'rol_cliente';
GRANT SELECT ON BlackTechStore.Equipo TO 'rol_cliente';
GRANT SELECT ON BlackTechStore.Orden TO 'rol_cliente';
GRANT SELECT ON BlackTechStore.Orden_Equipo TO 'rol_cliente';
GRANT SELECT ON BlackTechStore.Categoria TO 'rol_cliente';
GRANT SELECT ON BlackTechStore.Marca TO 'rol_cliente';
GRANT SELECT ON BlackTechStore.Modelo TO 'rol_cliente';
GRANT SELECT ON BlackTechStore.CuentaCobrar TO 'rol_cliente';
GRANT SELECT ON BlackTechStore.FacturaVenta TO 'rol_cliente';
GRANT SELECT ON BlackTechStore.FormaPagoV TO 'rol_cliente';

CREATE ROLE 'rol_cliente_web';
GRANT SELECT ON BlackTechStore.VentaWeb TO 'rol_cliente_web';
GRANT SELECT ON BlackTechStore.Notificacion TO 'rol_cliente_web';
GRANT SELECT ON BlackTechStore.Ventas_Web_Diarias TO 'rol_cliente_web';
GRANT SELECT ON BlackTechStore.Estado_Venta_Web_Diaria TO 'rol_cliente_web';

FLUSH PRIVILEGES;

--
-- USERS STRUCTURE
--

-- drop all users
DROP USER IF EXISTS 'gerente'@'localhost';
DROP USER IF EXISTS 'asistente'@'localhost';
DROP USER IF EXISTS 'tecnico_software'@'localhost';
DROP USER IF EXISTS 'tecnico_hardware'@'localhost';
DROP USER IF EXISTS 'cliente'@'localhost';
DROP USER IF EXISTS 'cliente_web'@'localhost';

-- create all users
CREATE USER 'gerente'@'localhost' IDENTIFIED BY 'cgerente';
CREATE USER 'asistente'@'localhost' IDENTIFIED BY 'casistente';
CREATE USER 'tecnico_software'@'localhost' IDENTIFIED BY 'ctecnico_software';
CREATE USER 'tecnico_hardware'@'localhost' IDENTIFIED BY 'ctecnico_hardware';
CREATE USER 'cliente'@'localhost' IDENTIFIED BY 'ccliente';
CREATE USER 'cliente_web'@'localhost' IDENTIFIED BY 'ccliente_web';

-- grant privileges to users
GRANT 'rol_admin' TO 'gerente'@'localhost';
GRANT 'rol_asistente' TO 'asistente'@'localhost';
GRANT 'rol_tecnico' TO 'tecnico_software'@'localhost', 'tecnico_hardware'@'localhost';
GRANT 'rol_cliente' TO 'cliente'@'localhost';
GRANT 'rol_cliente_web' TO 'cliente'@'localhost';
GRANT 'rol_cliente_web' TO 'cliente_web'@'localhost';

FLUSH PRIVILEGES;