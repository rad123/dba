-- drop database
DROP DATABASE IF EXISTS BlackTechStore;

-- create database
CREATE DATABASE BlackTechStore COLLATE='utf8_bin';