-- drop all users
DROP USER IF EXISTS 'gerente'@'localhost';
DROP USER IF EXISTS 'asistente'@'localhost';
DROP USER IF EXISTS 'tecnico_software'@'localhost';
DROP USER IF EXISTS 'tecnico_hardware'@'localhost';
DROP USER IF EXISTS 'cliente'@'localhost';
DROP USER IF EXISTS 'cliente_web'@'localhost';

-- create all users
CREATE USER 'gerente'@'localhost' IDENTIFIED BY 'cgerente';
CREATE USER 'asistente'@'localhost' IDENTIFIED BY 'casistente';
CREATE USER 'tecnico_software'@'localhost' IDENTIFIED BY 'ctecnico_software';
CREATE USER 'tecnico_hardware'@'localhost' IDENTIFIED BY 'ctecnico_hardware';
CREATE USER 'cliente'@'localhost' IDENTIFIED BY 'ccliente';
CREATE USER 'cliente_web'@'localhost' IDENTIFIED BY 'ccliente_web';

-- grant privileges to users
GRANT 'rol_admin' TO 'gerente'@'localhost';
GRANT 'rol_asistente' TO 'asistente'@'localhost';
GRANT 'rol_tecnico' TO 'tecnico_software'@'localhost', 'tecnico_hardware'@'localhost';
GRANT 'rol_cliente' TO 'cliente'@'localhost';
GRANT 'rol_cliente_web' TO 'cliente'@'localhost';
GRANT 'rol_cliente_web' TO 'cliente_web'@'localhost';

FLUSH PRIVILEGES;