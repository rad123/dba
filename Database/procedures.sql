-- drop all procedures
DROP PROCEDURE IF EXISTS Generar_Nueva_Orden_Cliente;
DROP PROCEDURE IF EXISTS Reporte_Diario_Equipos;
DROP PROCEDURE IF EXISTS Ventas_Web_Diarias;
DROP PROCEDURE IF EXISTS Equipos_Tecnicos_Diarios;
DROP PROCEDURE IF EXISTS Pagos_Semanal;
DROP PROCEDURE IF EXISTS Ingresos_Obtenidos;
DROP PROCEDURE IF EXISTS Ventas_Realizadas;
DROP PROCEDURE IF EXISTS Repuestos_Utilizados;

-- create all procedures

DELIMITER //

-- transaction A
CREATE PROCEDURE Generar_Nueva_Orden_Cliente(
Tipo_Identif_Cliente enum('V', 'E', 'J', 'G'),
Identif_Cliente varchar(15),
Nombre_Cliente varchar(50),
Email_Cliente varchar(100),
Direccion_Cliente varchar(50),
Telefono_Cliente varchar(20),
Numero_Orden varchar(5),
F_inicio_Orden timestamp,
F_fin_Orden timestamp,
Estado_Orden enum('ESP', 'REV', 'REP', 'DEV'),
Total_Orden decimal(9,2),
IN IDs_Categorias_Equipos text,
IN Seriales_Equipos text,
IN Nombres_Equipos text,
IN Descripcion_Equipos text,
IN Precios_Equipos text,
IN Fallas_Equipos text
)
Proc_Generar_Nueva_Orden_Cliente:BEGIN

	DECLARE CONTINUE HANDLER FOR SQLEXCEPTION SET @code = '45000';

	START TRANSACTION;

		SET @code = '00000';

		INSERT INTO Cliente (Tipo_Identif, Identif, Nombre, Email, Direccion, Telefono) 
		VALUES (Tipo_Identif_Cliente, Identif_Cliente, Nombre_Cliente, Email_Cliente, Direccion_Cliente, Telefono_Cliente);
		
		IF @code = '45000' THEN
			ROLLBACK;
			SELECT 'Error insertando los datos del cliente' AS ERROR_MESSAGE;
			LEAVE Proc_Generar_Nueva_Orden_Cliente;
		END IF;

		SET @code = '00000';

		INSERT INTO Orden (Numero, F_inicio, F_fin, Estado, Total)
		VALUES (Numero_Orden, F_inicio_Orden, F_fin_Orden, Estado_Orden, Total_Orden);

		IF @code = '45000' THEN
		   	ROLLBACK;
			SELECT 'Error insertando los datos de la orden' AS ERROR_MESSAGE;
			LEAVE Proc_Generar_Nueva_Orden_Cliente;
		END IF;

        -- validate data consistence
		SET @CantIdsCategorias = CHAR_LENGTH(IDs_Categorias_Equipos) - CHAR_LENGTH(REPLACE(IDs_Categorias_Equipos, ',', ''));
		SET @CantSerialesEquipos = CHAR_LENGTH(Seriales_Equipos) - CHAR_LENGTH(REPLACE(Seriales_Equipos, ',', ''));
		SET @CantNombresEquipos = CHAR_LENGTH(Nombres_Equipos) - CHAR_LENGTH(REPLACE(Nombres_Equipos, ',', ''));
		SET @CantDescripcionEquipos = CHAR_LENGTH(Descripcion_Equipos) - CHAR_LENGTH(REPLACE(Descripcion_Equipos, ',', ''));
		SET @CantPreciosEquipos = CHAR_LENGTH(Precios_Equipos) - CHAR_LENGTH(REPLACE(Precios_Equipos, ',', ''));
		SET @CantFallasEquipos = CHAR_LENGTH(Fallas_Equipos) - CHAR_LENGTH(REPLACE(Fallas_Equipos, ',', ''));

		IF NOT(@CantIdsCategorias = @CantSerialesEquipos AND @CantSerialesEquipos = @CantNombresEquipos AND
			@CantNombresEquipos = @CantDescripcionEquipos AND @CantDescripcionEquipos = @CantPreciosEquipos AND
			@CantPreciosEquipos = @CantFallasEquipos) THEN
		       ROLLBACK;
		       SELECT 'Error con los datos del(los) equipo(s) que no coincide(n)' AS ERROR_MESSAGE;
		       LEAVE Proc_Generar_Nueva_Orden_Cliente;
		END IF;

		SET @Datos = @CantIdsCategorias;
		SET @Pos = 0;
		SET @signToSplit = ',';

		IF @Datos > 0 THEN
		    SET @Datos = @Datos + 1;
		END IF;

        -- while data is available
		WHILE (@Datos) DO

			-- set current pos for data
			SET @Pos = @Pos + 1;

		    -- first get the data to insert in current transaction
			SET @ID_Categoria = SplitString(IDs_Categorias_Equipos, @signToSplit, @Pos);
			SET @Serial_Equipo = SplitString(Seriales_Equipos, @signToSplit, @Pos);
			SET @Nombre_Equipo = SplitString(Nombres_Equipos, @signToSplit, @Pos);
			SET @Descripcion_Equipo = SplitString(Descripcion_Equipos, @signToSplit, @Pos);
			SET @Precio_Equipo = SplitString(Precios_Equipos, @signToSplit, @Pos);
			SET @Falla_Equipo = SplitString(Fallas_Equipos, @signToSplit, @Pos);

			SET @code = '00000';

			-- now insert the data in respective tables
			INSERT INTO Equipo (Serial_Eq, ID_Categoria, Identif_Cliente, Nombre, Descripcion, Precio) VALUES
			(@Serial_Equipo, @ID_Categoria, Identif_Cliente, @Nombre_Equipo, @Descripcion_Equipo, @Precio_Equipo);

			IF @code = '45000' THEN
		    	ROLLBACK;
				SELECT CONCAT('Error insertando los datos del equipo ', @Pos) AS ERROR_MESSAGE;
				LEAVE Proc_Generar_Nueva_Orden_Cliente;
			END IF;

			SET @code = '00000';

			INSERT INTO Orden_Equipo (Num_Orden, Serial_Equipo, Falla_Equipo) VALUES
			(Numero_Orden, @Serial_Equipo, @Falla_Equipo);

			IF @code = '45000' THEN
		    	ROLLBACK;
		    	SELECT CONCAT('Error insertando los datos del equipo ', @Pos, ', asociado a la orden') AS ERROR_MESSAGE;
		    	LEAVE Proc_Generar_Nueva_Orden_Cliente;
			END IF;

			-- remove item from data
			SET @Datos = @Datos - 1;

		END WHILE;

	COMMIT;

END;//

-- transaction B
CREATE PROCEDURE Reporte_Diario_Equipos()
BEGIN

	START TRANSACTION;

		SELECT Equipo.Nombre AS nombre_equipo, Equipo.Serial_Eq AS serial_equipo, Orden.Estado AS estado_orden 
		FROM ((Orden INNER JOIN Orden_Equipo ON Orden.Numero = Orden_Equipo.Num_Orden) 
	      INNER JOIN Equipo ON Orden_Equipo.Serial_Equipo = Equipo.Serial_Eq) 
		WHERE (DATE(Orden.F_inicio) = CURRENT_DATE)
		ORDER BY Orden.Estado, Orden.F_inicio DESC;

	COMMIT;

END;//

-- transaction C
CREATE PROCEDURE Ventas_Web_Diarias()
BEGIN

	START TRANSACTION;

		SELECT VentaWeb.Numero AS num_venta, VentaWeb.Concepto AS concepto_venta, VentaWeb.Estado
		AS estado_venta_web, VentaWeb.Num_Factura AS num_factura_venta, VentaWeb.Identif_Cliente 
		AS identif_cliente, Cliente.Tipo_Identif AS tipo_identif_cliente, Cliente.Nombre AS nombre_cliente,
		FacturaVenta.Fecha AS fecha_venta, FacturaVenta_EquipoVenta.Cantidad AS cantidad_equipo_venta,
		FacturaVenta_EquipoVenta.PVP AS pvp_equipo_venta FROM 
		(((VentaWeb INNER JOIN FacturaVenta ON VentaWeb.Num_Factura = FacturaVenta.Numero)
				   INNER JOIN Cliente ON VentaWeb.Identif_Cliente = Cliente.Identif)
				   INNER JOIN FacturaVenta_EquipoVenta ON FacturaVenta.Numero = FacturaVenta_EquipoVenta.Num_Factura)
		WHERE (DATE(FacturaVenta.Fecha) = CURRENT_DATE)
		ORDER BY FacturaVenta.Fecha, VentaWeb.Estado DESC;

	COMMIT;

END;//

-- transaction D
CREATE PROCEDURE Equipos_Tecnicos_Diarios() 
BEGIN

	START TRANSACTION;

		SELECT Personal.Nombre AS tecnico, Personal.CI AS cedula_personal, Equipo.Serial_Eq AS serial_equipo,
		Equipo.Nombre AS nombre_equipo, Reparacion.Estado AS estado_reparacion, Orden.Estado AS estado_orden 
		FROM ((((Orden INNER JOIN Orden_Equipo ON Orden.Numero = Orden_Equipo.Num_Orden) 
		               INNER JOIN Equipo ON Orden_Equipo.Serial_Equipo = Equipo.Serial_Eq) 
		               INNER JOIN Reparacion ON Orden.Numero = Reparacion.Numero_Orden)  
		               INNER JOIN Personal ON Personal.CI = Reparacion.CI_Personal) 
		WHERE (DATE(Orden.F_inicio) = CURRENT_DATE)  
		ORDER BY Personal.CI, Orden.F_inicio DESC;

	COMMIT;

END;//

-- transaction E
CREATE PROCEDURE Pagos_Semanal()
BEGIN

	START TRANSACTION;

		SELECT Cliente.Identif AS identif_cliente, Cliente.Nombre AS nombre_cliente, Cliente.Direccion
		AS direccion_cliente, Cliente.Telefono AS telefono_cliente, Cliente.Email AS email_cliente,
		CuentaCobrar.Monto AS monto_cuenta_cobrar, FormaPagoV.Descripcion AS descripcion_forma_pago_v
		FROM (((Cliente INNER JOIN CuentaCobrar ON Cliente.Identif = CuentaCobrar.Identif_Cliente) 
	                INNER JOIN CuentaCobrar_FormaPagoV ON CuentaCobrar.Numero = CuentaCobrar_FormaPagoV.Num_Cuenta) 
	                INNER JOIN FormaPagoV ON CuentaCobrar_FormaPagoV.Fecha_Pago = FormaPagoV.Fecha)
		WHERE (DATE(FormaPagoV.Fecha) >= DATE_SUB(CURRENT_DATE, INTERVAL 6 DAY) AND DATE(FormaPagoV.Fecha) <= (CURRENT_DATE))
		ORDER BY FormaPagoV.Fecha DESC;

	COMMIT;

END;//

-- transaction F
CREATE PROCEDURE Ingresos_Obtenidos()
BEGIN

	START TRANSACTION;

		SELECT Categoria.ID AS id_categoria, Categoria.Nombre AS nombre_categoria, SUM(Orden.Total) AS monto_total FROM
		(((Equipo INNER JOIN Orden_Equipo ON Equipo.Serial_Eq = Orden_Equipo.Serial_Equipo)
				 INNER JOIN Orden ON Orden_Equipo.Num_Orden = Orden.Numero)
		         INNER JOIN Categoria ON Equipo.ID_Categoria = Categoria.ID)
		WHERE (Orden.Estado = 'REP' AND DATE(Orden.F_fin) >= DATE_SUB(CURRENT_DATE, INTERVAL 6 DAY) AND DATE(Orden.F_fin) <= (CURRENT_DATE))
		GROUP BY Equipo.ID_Categoria
		ORDER BY Categoria.ID, Orden.F_fin DESC;

	COMMIT;

END;//

-- transaction G
CREATE PROCEDURE Ventas_Realizadas()
BEGIN
	
	START TRANSACTION;

		SELECT Cliente.Identif AS identif_cliente, Cliente.Nombre AS nombre_cliente, Cliente.Direccion
		AS direccion_cliente, Cliente.Telefono AS telefono_cliente, Cliente.Email AS email_cliente,
		EquipoVenta.Nombre AS articulo, FacturaVenta.Monto AS monto, FacturaVenta_EquipoVenta.Cantidad AS cantidad,
		FacturaVenta.Fecha AS fecha FROM
		(((Cliente INNER JOIN FacturaVenta ON Cliente.Identif = FacturaVenta.Identif_Cliente)
			     INNER JOIN FacturaVenta_EquipoVenta ON FacturaVenta.Numero = FacturaVenta_EquipoVenta.Num_Factura)
			     INNER JOIN EquipoVenta ON EquipoVenta.Codigo = FacturaVenta_EquipoVenta.Codigo_Equipo)
		WHERE (DATE(FacturaVenta.Fecha) >= DATE_SUB(CURRENT_DATE, INTERVAL 6 DAY) AND DATE(FacturaVenta.Fecha) <= (CURRENT_DATE))
		ORDER BY FacturaVenta.Fecha DESC;

	COMMIT;

END;//

-- transaction H
CREATE PROCEDURE Repuestos_Utilizados()
BEGIN

	START TRANSACTION;

		SELECT Repuesto.Nombre AS nombre_repuesto, Repuesto.Precio AS precio_repuesto_actual,
		Repuesto_Reparacion.Cant_Rep AS cantidad_usada, Repuesto_Reparacion.Precio_Rep AS precio_repuesto_uso,
		Orden.Numero AS numero_orden, Orden.F_inicio AS fecha_inicio_orden,
		Orden.F_fin AS fecha_fin_orden, Orden.Estado AS estado_orden, Orden.Total AS total_orden FROM
		(((Repuesto INNER JOIN Repuesto_Reparacion ON Repuesto.Codigo = Repuesto_Reparacion.Codigo_Repues)
		          INNER JOIN Reparacion ON Repuesto_Reparacion.F_asig_Rep = Reparacion.F_asig AND
		          Repuesto_Reparacion.CI_Pers_Rep = Reparacion.CI_Personal AND
		          Repuesto_Reparacion.Num_Ord_Rep = Reparacion.Numero_Orden)
		          INNER JOIN Orden ON Reparacion.Numero_Orden = Orden.Numero)
		WHERE (DATE(Reparacion.F_asig) >= DATE_SUB(CURRENT_DATE, INTERVAL 6 DAY) AND DATE(Reparacion.F_asig) <= (CURRENT_DATE))
		ORDER BY Reparacion.F_asig DESC;

	COMMIT;

END;//

DELIMITER ;