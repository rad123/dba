-- drop all roles
DROP ROLE IF EXISTS 'rol_admin';
DROP ROLE IF EXISTS 'rol_asistente';
DROP ROLE IF EXISTS 'rol_tecnico';
DROP ROLE IF EXISTS 'rol_cliente';
DROP ROLE IF EXISTS 'rol_cliente_web';

-- create all roles
CREATE ROLE 'rol_admin';
GRANT ALL PRIVILEGES ON *.* TO 'rol_admin' WITH GRANT OPTION;

CREATE ROLE 'rol_asistente';
GRANT ALL PRIVILEGES ON BlackTechStore.* TO 'rol_asistente' WITH GRANT OPTION;

CREATE ROLE 'rol_tecnico';
GRANT INSERT, UPDATE, SELECT ON BlackTechStore.Reparacion TO 'rol_tecnico';
GRANT INSERT, UPDATE, SELECT ON BlackTechStore.Orden TO 'rol_tecnico';
GRANT UPDATE, SELECT ON BlackTechStore.Personal TO 'rol_tecnico';
GRANT LOCK TABLES ON BlackTechStore.* TO 'rol_tecnico';
GRANT EXECUTE ON PROCEDURE BlackTechStore.Generar_Nueva_Orden_Cliente TO 'rol_tecnico';
GRANT EXECUTE ON PROCEDURE BlackTechStore.Reporte_Diario_Equipos TO 'rol_tecnico';
GRANT EXECUTE ON PROCEDURE BlackTechStore.Equipos_Tecnicos_Diarios TO 'rol_tecnico';
GRANT EXECUTE ON PROCEDURE BlackTechStore.Repuestos_Utilizados TO 'rol_tecnico';
GRANT EXECUTE ON PROCEDURE BlackTechStore.Equipos_Tecnicos_Diarios TO 'rol_tecnico';
GRANT SELECT ON BlackTechStore.Reporte_Diario_Equipos TO 'rol_tecnico';
GRANT SELECT ON BlackTechStore.Equipos_Tecnicos_Diarios TO 'rol_tecnico';
GRANT SELECT ON BlackTechStore.Herramienta TO 'rol_tecnico';
GRANT SELECT ON BlackTechStore.Repuesto TO 'rol_tecnico';
GRANT SELECT ON BlackTechStore.Repuesto_Reparacion TO 'rol_tecnico';
GRANT SELECT ON BlackTechStore.Repuestos_Utilizados TO 'rol_tecnico';
GRANT SELECT ON BlackTechStore.Existencia_Repuestos TO 'rol_tecnico';
GRANT SELECT ON BlackTechStore.Equipos_Tecnicos_Diarios TO 'rol_tecnico';

CREATE ROLE 'rol_cliente';
GRANT UPDATE, SELECT ON BlackTechStore.Cliente TO 'rol_cliente';
GRANT SELECT ON BlackTechStore.Equipo TO 'rol_cliente';
GRANT SELECT ON BlackTechStore.Orden TO 'rol_cliente';
GRANT SELECT ON BlackTechStore.Orden_Equipo TO 'rol_cliente';
GRANT SELECT ON BlackTechStore.Categoria TO 'rol_cliente';
GRANT SELECT ON BlackTechStore.Marca TO 'rol_cliente';
GRANT SELECT ON BlackTechStore.Modelo TO 'rol_cliente';
GRANT SELECT ON BlackTechStore.CuentaCobrar TO 'rol_cliente';
GRANT SELECT ON BlackTechStore.FacturaVenta TO 'rol_cliente';
GRANT SELECT ON BlackTechStore.FormaPagoV TO 'rol_cliente';

CREATE ROLE 'rol_cliente_web';
GRANT SELECT ON BlackTechStore.VentaWeb TO 'rol_cliente_web';
GRANT SELECT ON BlackTechStore.Notificacion TO 'rol_cliente_web';
GRANT SELECT ON BlackTechStore.Ventas_Web_Diarias TO 'rol_cliente_web';
GRANT SELECT ON BlackTechStore.Estado_Venta_Web_Diaria TO 'rol_cliente_web';

FLUSH PRIVILEGES;